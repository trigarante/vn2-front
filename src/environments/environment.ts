// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // CORE_DOCUMENTOS: 'https://documentos-core.trigarante2022.com/v1',
  CORE_DOCUMENTOS: 'http://localhost:8080/v1',
  // CORE_URL: 'https://vn-core-pruebas.trigarante2022.com/v1',
  // CORE_URL: 'https://ucontact-core.trigarante2022.com/v1',
  CORE_URL: 'http://localhost:8080/v1',

  /* NET2PHONE */
  // CORE_NET2PHONE: 'http://localhost:8000/net2phone/',
  CORE_NET2PHONE: 'https://net2phone-core.trigarante2022.com/net2phone/',

  // LOGIN_URL: 'http://localhost:8081/login',
  LOGIN_URL: 'https://login-core.trigarante2022.com/login',
  nodeWhats: 'http://localhost:8000',
  CORE_ESCUCHA: 'https://escucha-core.trigarante2022.com',
  /*** Telefonia inicio ***/
  SERVICIOS_TELEFONIA_DATOS: 'https://telefoniadatos-core.trigarante2022.com/api',
  // SERVICIOS_TELEFONIA: 'https://telefoniadatos-core.trigarante2022.com/api',
  SERVICIOS_TELEFONIA: 'https://telefoniadatos-core.trigarante2022.com/api',
  // SERVICIOS_TELEFONIA: 'http://localhost:8000/api',
  // SOCKETS_SERVICIOS_TELEFONIA: 'http://localhost:8000',
  SOCKETS_SERVICIOS_TELEFONIA: 'https://cobranzaperu-core.trigarante2022.com',
  // SOCKETS_SERVICIOS_TELEFONIA: 'http://localhost:8000',
  SERVICIOS_TELEFONIA_NET2PHONE: 'https://ahorra.ucontactcloud.com/',
  /*** Telefonia fin ***/
  SERVICIOS_NUEVA_LOGICA_NODE: 'https://vn-core-pruebas.trigarante2022.com/',
  GLOBAL_SERVICIOS_OPERACIONES: 'https://operaciones.mark-43.net/mark43-service/',
  // GLOBAL_SERVICIOS_OPERACIONES: 'null',
  // CORE_APP_V1: 'https://app.mark-43.net/v1/apps',
  // CORE_APP_V1: 'http://core-app-dev.us-east-1.elasticbeanstalk.com/v1/users',
  // CORE_APP_V1: 'https://app.core-ahorraseguros.com/v1/',
  CORE_APP_V1: 'https://app-dev.core-ahorraseguros.com/v1/',
  CORE_externo: 'https://app.core-ahorraseguros.com/v1/',
  // GLOBAL_SERVICIOS_OPERACIONES_Lectura: 'null',
  GLOBAL_SERVICIOS_OPERACIONES_Lectura: 'https://operacion-lectura.mark-43.net/mark43-service/',
  GLOBAL_SERVICIOS: 'https://mark-43.net/mark43-service/',
  // GLOBAL_SERVICIOS: 'null',
  GLOBAL_SOCKET: 'https://socket.mark-43.net/',
  // GLOBAL_SOCKET: 'null',
  // GLOBAL_SERVICIOS_DOCUMENTOS: 'null',
  GLOBAL_SERVICIOS_DOCUMENTOS: 'https://documentos.mark-43.net/mark43-service/',
  // GLOBAL_SERVICIOS_DOCUMENTOS_OPERACION: 'null',
  GLOBAL_SERVICIOS_DOCUMENTOS_OPERACION: 'https://operacionesdo.mark-43.net/mark43-service/',
  CATALOGO_AUTOSQUALITAS: 'https://dev.ws-qualitas.com/',
  // CATALOGO_AUTOSQUALITAS: 'https://ws-qualitas.com/',
  CATALOGO_AUTOSGNP: 'https://web-gnp.mx/',
  CATALOGO_AUTOSGNPDEV: 'https://dev.web-gnp.mx/',
  CATALOGO_AUTOSMAPFRE: 'https://ws-mapfre.com/',
  CORE_DASH: 'https://dash-core.trigarante2022.com/v1',
  REPUVE: 'https://api.theapiworld.com/v1/api_vehicular/search',
  CATALOGO_AUTOSABA: 'https:/ws-abaseguros.com/',
  AUTH: 'https://core-mejorseguro.com/v1/authenticate',
  APPURL: 'https://core-mejorseguro.com/v2/prepaid/link'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *s
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
