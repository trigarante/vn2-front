import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-telefonia',
  template:  `
    <router-outlet></router-outlet>
  `,
})
export class TelefoniaComponent {}
