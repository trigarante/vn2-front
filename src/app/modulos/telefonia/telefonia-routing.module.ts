import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TelefoniaComponent} from "./telefonia.component";
import {HistorialLlamadasComponent} from "./historial-llamadas/historial-llamadas.component";
import {EnLlamadasOutComponent} from  "./en-llamadas-out/en-llamadas-out.component"
import {DirectorioAseguradoraComponent} from "./directorio-aseguradora/directorio-aseguradora.component";
import {HistoricoIbComponent} from "./historico-ib/historico-ib.component";
import {PredictivoConfiguracionesComponent} from "./predictivo-configuraciones/predictivo-configuraciones.component";
import {ConfigurarComponent} from "./predictivo-configuraciones/configurar/configurar.component";

const routes: Routes = [
  {
  path: '',
  component: TelefoniaComponent,
  children: [
    {
      path: 'historial-llamadas/:idSolicitud',
      component: HistorialLlamadasComponent,
    },
    {
      path: 'historial-llamadas/registro/:idRegistro',
      component: HistorialLlamadasComponent,
    },
    {
      path: 'monitoreo',
      component: EnLlamadasOutComponent,
    },
    {
      path: 'directorio-aseguradora',
      component: DirectorioAseguradoraComponent,
    },
    {
      path: 'historico-ib',
      component: HistoricoIbComponent,
    },
    {
      path: 'predictivo-configuraciones',
      component: PredictivoConfiguracionesComponent,
    },
    {
      path: 'configurar/:id',
      component: ConfigurarComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TelefoniaRoutingModule { }
