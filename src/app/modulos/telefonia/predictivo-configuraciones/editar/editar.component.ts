import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ConfiguracionesService} from '../../../../@core/data/services/telefonia/predictivo/configuraciones.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss']
})
export class EditarComponent implements OnInit {

  options: any[];
  configuracion: FormGroup = new FormGroup({
    activo: new FormControl('t'),
    idDepartamento: new FormControl(null, Validators.required),
    descripcion: new FormControl(null, Validators.required),
  });
  /*** ***/
  myControl = new FormControl(null, Validators.required);
  filteredOptions: Observable<any[]>;
  constructor(public dialogRef: MatDialogRef<EditarComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private configuracionesService: ConfiguracionesService) {
  }

  ngOnInit(): void {
    this.getConfiguracionById();
  }

  getConfiguracionById() {
    this.configuracionesService.getConfiguracionByIdEdit(this.data).subscribe(result => {
      this.configuracion.patchValue(result);
      this.setValueControl(result.idDepartamento);
    });
  }

  setValueControl(idDepartamento) {
    this.getDepartamentos(idDepartamento);
  }

  editar() {
    this.configuracionesService.putConfiguracion(this.configuracion.value, this.data).toPromise().then(() => { this.dialogRef.close(); });
  }

  cerrar() {
    this.dialogRef.close();
  }

  getDepartamentos(idDepartamento) {
    this.configuracionesService.getDepartamentos().subscribe(result => {
      this.options = result;
      idDepartamento === 0 ? this.myControl.setValue({id: 0, descripcion: 'DEFAULT'}) : this.myControl.setValue(this.options.find(opt => opt.id === idDepartamento));
      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => (typeof value === 'string' ? value : value?.descripcion)),
        map(name => (name ? this._filter(name) : this.options.slice())),
      );
    });
  }

  displayFn(intento: any): string {
    return intento && intento.descripcion ? intento.descripcion : '';
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.descripcion.toLowerCase().includes(filterValue));
  }

  valores(val) {
    this.configuracion.controls[`idDepartamento`].setValue(val.id);
  }
}
