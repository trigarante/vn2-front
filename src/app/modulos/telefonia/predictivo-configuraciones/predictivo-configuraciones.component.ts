import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {Router} from '@angular/router';
import {ConfiguracionesService} from '../../../@core/data/services/telefonia/predictivo/configuraciones.service';
import {CrearComponent} from "./crear/crear.component";
import {EditarComponent} from "./editar/editar.component";
import {Notificaciones} from "../../../notificaciones";

@Component({
  selector: 'app-predictivo-configuraciones',
  templateUrl: './predictivo-configuraciones.component.html',
  styleUrls: ['./predictivo-configuraciones.component.scss']
})
export class PredictivoConfiguracionesComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  cols: any[] = ['titulo', 'departamento', 'fecha', 'acciones'];
  dataSource = new MatTableDataSource([]);
  cargando = true;

  constructor(private dialog: MatDialog,
              private configuracionesService: ConfiguracionesService,
              private notificacionesService: NotificacionesService,
              private router: Router,
              private notificaciones: Notificaciones) { }

  ngOnInit() {
    this.getConfiguraciones();
  }

  ngOnDestroy() { }

  getConfiguraciones() {
    this.configuracionesService.getConfiguracionesAll().subscribe((resp: any[]) => {
      if (!resp.length) {
        this.notificacionesService.advertencia('No tienes configuraciones', 'Sin configuraciones');
      }
      this.dataSource = new MatTableDataSource(resp);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.setFilterEmision();
    });
  }

  crearConfiguracion() {
    const dialogRef = this.dialog.open(CrearComponent, {
      width: '400px',
      data: 'crear',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getConfiguraciones();
      // this.animal = result;
    });
  }

  editarConfiguracion(id) {
    const dialogRef = this.dialog.open(EditarComponent, {
      width: '400px',
      data: id,
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getConfiguraciones();
      // this.animal = result;
    });
  }

  configuracion(id) {
    this.router.navigate(['/modulos/telefonia/configurar/' + id]);
  }

  eliminar(id) {
    const aprovar = this.notificaciones.pregunta('ESTAS SEGUR@ DE ELIMINAR LA CONFIGURACIÓN');
    aprovar.then(result => {
      if (result.isConfirmed) {
        this.configuracionesService.eliminarConfiguracion({activo: 'f'}, id).toPromise().then(() => { });
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setFilterEmision() {
    this.dataSource.filterPredicate = (data, filter) => {
      const dataStr = (
        data.nombre
        + ' ' + data.id
        + ' ' + data.deparamento
        + ' ' + data.titulo
      ).toLocaleLowerCase();
      return dataStr.indexOf(filter.toLocaleLowerCase()) !== -1;
    };
  }
}
