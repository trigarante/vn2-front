import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {ConfiguracionesService} from '../../../../@core/data/services/telefonia/predictivo/configuraciones.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Notificaciones} from "../../../../notificaciones";

@Component({
  selector: 'app-configurar',
  templateUrl: './configurar.component.html',
  styleUrls: ['./configurar.component.scss']
})
export class ConfigurarComponent implements OnInit {
  tipoContacto = new FormControl('');
  idConfiguracion;
  configuracion;
  intentos;
  intentos2: any[] = [];

  formConfiguraciones = this.formBuilder.group({
    intervalo: this.formBuilder.array([])
  });
  /*** ***/
  filteredOptions: Observable<any[]>;
  constructor(private configuracionesService: ConfiguracionesService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private notificaciones: Notificaciones) {
    this.route.params.subscribe((params: Params) => {
      this.idConfiguracion = params.id;
    });
  }

  ngOnInit(): void {
    this.getConfiguracionById();
    // this.getDepartamentos();
  }

  getIntentoContacto(configuracion) {
    this.configuracionesService.getIntentoContacto().subscribe(result => {
      this.intentos = result;
      this.agregarIntervaloConfiguracion(configuracion, this.intentos);
      this.filteredOptions = this.tipoContacto.valueChanges.pipe(
        startWith(''),
        map(value => (typeof value === 'string' ? value : value?.descripcion)),
        map(name => (name ? this._filter(name) : this.intentos.slice())),
      );
    });
  }

  getConfiguracionById() {
    this.configuracionesService.getConfiguracionById(this.idConfiguracion).subscribe(result => {
      this.configuracion = result;
      this.getIntentoContacto(this.configuracion.configuracion);
    });
  }

  agregarIntervalo() {
    const intervaloGrupo = this.formBuilder.group({
      id: this.intervalos.length + 1,
      idIntento: null,
      dia: 0,
      days: 0,
      hours: 0,
      minutes: 0
    });
    this.intervalos.push(intervaloGrupo);
  }

  agregarIntervaloConfiguracion(configuracion, intentos) {
    for (let i = 0; i < configuracion.length; i++) {
      const intervaloGrupo = this.formBuilder.group({
        id: this.intervalos.length + 1,
        idIntento: this.intentoFormat(configuracion[i].idIntento, intentos),
        dia: configuracion[i].dia,
        days: configuracion[i].days,
        hours: configuracion[i].hours,
        minutes: configuracion[i].minutes
      });
      this.intervalos.push(intervaloGrupo);
    }
  }

  intentoFormat(id, intentos) {
    this.intentos2.push(...intentos);
    return this.intentos2.find(intento => {
      if (Number(intento.id) === Number(id)) {
        return intento;
      }
    });
  }

  removerIntervalo(indice) {
    this.intervalos.removeAt(indice);
  }

  get intervalos() {
    return this.formConfiguraciones.get('intervalo') as FormArray;
  }

  guardarConfiguracion() {
    this.configuracionesService.guardarConfiguracion(this.idConfiguracion, this.formConfiguraciones.value).subscribe(result => {
      this.notificaciones.exito('TEST');
      this.getConfiguracionById();
    });
  }

  displayFn(intento: any): string {
    return intento && intento.descripcion ? intento.descripcion : '';
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();

    return this.intentos.filter(option => option.descripcion.toLowerCase().includes(filterValue));
  }

  // getDepartamentos() {
  //   this.configuracionesService.getDepartamentos().subscribe(result => {
  //     this.optionsDepartor = result;
  //     this.filteredOptionsDeparamentos = this.departamento.valueChanges.pipe(
  //       startWith(''),
  //       map(value => (typeof value === 'string' ? value : value?.descripcion)),
  //       map(name => (name ? this._filter(name) : this.optionsDepartor.slice())),
  //     );
  //   });
  // }

  // private _filter(name: string): any[] {
  //   const filterValue = name.toLowerCase();
  //
  //   return this.optionsDepartor.filter(option => option.descripcion.toLowerCase().includes(filterValue));
  // }

  test(val) {
    // this.configuracion.controls[`idDepartamento`].setValue(val.id);
    console.log(val);
  }
}
