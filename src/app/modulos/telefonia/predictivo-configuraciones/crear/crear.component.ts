import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ConfiguracionesService} from '../../../../@core/data/services/telefonia/predictivo/configuraciones.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styleUrls: ['./crear.component.scss']
})
export class CrearComponent implements OnInit {

  configuracion: FormGroup = new FormGroup({
    configuracion: new FormControl([]),
    activo: new FormControl('t'),
    idDepartamento: new FormControl(null, Validators.required),
    descripcion: new FormControl(null, Validators.required),
  });
  myControl = new FormControl('');
  options: any[];
  filteredOptions: Observable<any[]>;

  constructor(public dialogRef: MatDialogRef<CrearComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private configuracionesService: ConfiguracionesService) {
  }

  ngOnInit(): void {
    this.getDepartamentos();
  }

  getDepartamentos() {
    this.configuracionesService.getDepartamentos().subscribe(result => {
      this.options = result;
      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => (typeof value === 'string' ? value : value?.descripcion)),
        map(name => (name ? this._filter(name) : this.options.slice())),
      );
    });
  }

  registrar() {
    this.configuracionesService.postConfiguracion(this.configuracion.value).toPromise().then(() => { this.dialogRef.close(); });
  }

  cerrar() {
    this.dialogRef.close();
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.descripcion.toLowerCase().includes(filterValue));
  }

  valores(val) {
    this.configuracion.controls[`idDepartamento`].setValue(val.id);
  }
}
