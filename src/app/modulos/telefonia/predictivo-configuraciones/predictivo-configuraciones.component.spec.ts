import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictivoConfiguracionesComponent } from './predictivo-configuraciones.component';

describe('PredictivoConfiguracionesComponent', () => {
  let component: PredictivoConfiguracionesComponent;
  let fixture: ComponentFixture<PredictivoConfiguracionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PredictivoConfiguracionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictivoConfiguracionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
