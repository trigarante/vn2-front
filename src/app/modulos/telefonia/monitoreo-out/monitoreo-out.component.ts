import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-monitoreo-out',
  templateUrl: './monitoreo-out.component.html',
  styleUrls: ['./monitoreo-out.component.scss']
})
export class MonitoreoOutComponent implements OnInit {
  permisos = JSON.parse(window.localStorage.getItem('User'));
  constructor() { }

  ngOnInit(): void {
  }

}
