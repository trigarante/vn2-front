import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TelefoniaRoutingModule } from './telefonia-routing.module';
import { LlamadaEntranteComponent } from './modals/llamada-entrante/llamada-entrante.component';
import {RouterModule} from "@angular/router";
import { DialogoLlamadaComponent } from './modals/dialogo-llamada/dialogo-llamada.component';
import {MaterialModule} from "../material.module";
import { AgregarNumeroComponent } from './modals/agregar-numero/agregar-numero.component';
import { LlamadaSalidaComponent } from './modals/llamada-salida/llamada-salida.component';
import { TipificarComponent } from './modals/tipificar/tipificar.component';
import { HistorialLlamadasComponent } from './historial-llamadas/historial-llamadas.component';
import {TelefoniaComponent} from "./telefonia.component";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatTableModule} from "@angular/material/table";
import {EnLlamadasOutComponent} from "./en-llamadas-out/en-llamadas-out.component";
import { LlamadasOutComponent } from './modals/llamadas-out/llamadas-out.component';
import { MonitoreoOutComponent } from './monitoreo-out/monitoreo-out.component';
import { EscucharGrabacionComponent } from './modals/escuchar-grabacion/escuchar-grabacion.component';
import { PropspectoIbComponent } from './components/propspecto-ib/propspecto-ib.component';
import { DirectorioAseguradoraComponent } from './directorio-aseguradora/directorio-aseguradora.component';
import { KeypadComponent } from './modals/keypad/keypad.component';
import {SnackPausaComponent} from "./modals/snack-pausa/snack-pausa.component";
import { HistoricoIbComponent } from './historico-ib/historico-ib.component';
import {MatSortModule} from "@angular/material/sort";
import { PredictivoConfiguracionesComponent } from './predictivo-configuraciones/predictivo-configuraciones.component';
import { EditarComponent } from './predictivo-configuraciones/editar/editar.component';
import { CrearComponent } from './predictivo-configuraciones/crear/crear.component';
import { ConfigurarComponent } from './predictivo-configuraciones/configurar/configurar.component';
import {MatDialogModule} from "@angular/material/dialog";
import { ComentariosAgendaComponent } from './modals/comentarios-agenda/comentarios-agenda.component';


@NgModule({
  declarations: [
    TelefoniaComponent,
    LlamadaEntranteComponent,
    DialogoLlamadaComponent,
    AgregarNumeroComponent,
    LlamadaSalidaComponent,
    TipificarComponent,
    HistorialLlamadasComponent,
    EnLlamadasOutComponent,
    LlamadasOutComponent,
    MonitoreoOutComponent,
    EscucharGrabacionComponent,
    PropspectoIbComponent,
    DirectorioAseguradoraComponent,
    KeypadComponent,
    SnackPausaComponent,
    HistoricoIbComponent,
    PredictivoConfiguracionesComponent,
    EditarComponent,
    CrearComponent,
    ConfigurarComponent,
    ComentariosAgendaComponent,
  ],
    imports: [
        CommonModule,
        TelefoniaRoutingModule,
        MaterialModule,
        RouterModule,
        FlexLayoutModule,
        MatTableModule,
        MatSortModule,
        MatDialogModule,


        // ReactiveFormsModule,
    ],
  exports: [
    LlamadaEntranteComponent,
    LlamadaSalidaComponent,
    AgregarNumeroComponent,
    EnLlamadasOutComponent,
    PropspectoIbComponent,
    MaterialModule,
  ],
  entryComponents: [
    LlamadaEntranteComponent,
    DialogoLlamadaComponent,
    AgregarNumeroComponent,
    LlamadaSalidaComponent,
    TipificarComponent,
    EnLlamadasOutComponent,
    LlamadasOutComponent,

  ]
})
export class TelefoniaModule { }
