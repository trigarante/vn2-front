import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComentariosAgendaComponent } from './comentarios-agenda.component';

describe('ComentariosAgendaComponent', () => {
  let component: ComentariosAgendaComponent;
  let fixture: ComponentFixture<ComentariosAgendaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComentariosAgendaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComentariosAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
