import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProspectoService} from '../../../../@core/data/services/venta-nueva/prospecto.service';
import {AgendaLlamadasService} from '../../../../@core/data/services/telefonia/agendaLlamadas/agenda-llamadas.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {finalize} from 'rxjs/operators';
import {SocketsService} from "../../../../@core/data/services/telefonia/sockets/sockets.service";

@Component({
  selector: 'app-comentarios-agenda',
  templateUrl: './comentarios-agenda.component.html',
  styleUrls: ['./comentarios-agenda.component.scss']
})
export class ComentariosAgendaComponent implements OnInit {

  comentario: string;

  constructor(
    public dialogRef: MatDialogRef<ComentariosAgendaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private prospectoService: ProspectoService,
    private agendaLlamadasService: AgendaLlamadasService,
    private notificacionesService: NotificacionesService,
    private socketsService: SocketsService,
  ) { }

  ngOnInit(): void {
  }
  dismiss() {
    this.dialogRef.close();
  }
  llamadaPendiente() {
    let idProspecto;
    const idEmpleado = sessionStorage.getItem('Empleado');
    const idEstadoLlamada = 2;
    const numero = this.data.numero;
    const tipoLlamada = 2;
    const comentario = this.comentario;
    const idSolicitud = this.data.idSolicitud;
    this.prospectoService.getProspectoByPhone(this.data.numero).pipe(finalize(() => {
      const agregaPndiete = {idEstadoLlamada, idEmpleado, numero, tipoLlamada, comentario, idProspecto, idSolicitud};
      this.agendaLlamadasService.postPendiente(agregaPndiete).subscribe(data => {
        this.socketsService.agenda();
        this.notificacionesService.exito('Llamada enviada a pendientes');
      });
    })).subscribe(data => {
      idProspecto = data[0].id;
    });
    this.dismiss();
  }

}
