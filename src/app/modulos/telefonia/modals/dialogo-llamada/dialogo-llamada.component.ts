import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {WebrtcService} from '../../../../@core/data/services/telefonia/webrtc.service';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {SpeechService} from '../../../../@core/data/services/telefonia/speech.service';
import {SolicitudesvnTelefoniaService} from '../../../../@core/data/services/telefonia/solicitudesvn-telefonia.service';
import {StepsCotizador} from "../../../../@core/data/interfaces/venta-nueva/steps-cotizador";
import {StepsCotizadorService} from "../../../../@core/data/services/venta-nueva/steps-cotizador.service";
import {NotificacionesService} from "../../../../@core/data/services/others/notificaciones.service";

@Component({
  selector: 'app-dialogo-llamada',
  templateUrl: './dialogo-llamada.component.html',
  styleUrls: ['./dialogo-llamada.component.scss']
})
export class DialogoLlamadaComponent implements OnInit, OnDestroy{

  llamadaEvents: Subscription;
  speech: string;
  salva = '';


  constructor(@Inject(MAT_DIALOG_DATA) public data,
              private router: Router,
              public matDialogRef: MatDialogRef<DialogoLlamadaComponent>,
              private solicitudesVn: SolicitudesvnTelefoniaService,
              public speechService: SpeechService,
              private stepsService: StepsCotizadorService,
              private notificaciones: NotificacionesService,
              private webRtcService: WebrtcService) {
  }

  ngOnInit() {
    this.getSpeech();
    this.asignarSolicitud();
    this.llamadaEvents = this.webRtcService.listenEvents().subscribe((resp) => {
      switch (resp.evento) {
        case 'disconnected':
        case 'terminated':
          this.matDialogRef.close();
          break;
      }
    });
  }


  ngOnDestroy() {

    this.llamadaEvents.unsubscribe();
  }

  generarEmision(idSolicitud) {
    this.router.navigate([`modulos/venta-nueva/step-cotizador/${idSolicitud}`]);
    this.matDialogRef.close();
  }

  getSpeech() {
    this.speechService.getSpeechByDepartamento().subscribe((resp: any) => {
      this.speech = resp;
    });
  }

  asignarSolicitud() {
    if (this.data.solicitud.id) {
      this.solicitudesVn.asignarEmpleadoSolicitud(this.data.solicitud.id).subscribe();
      // this.solicitudesVn.desactivarContactaciones(this.dialogoData.idHistorico).subscribe();
    }
  }

  recotizar() {

    let infoCliente: StepsCotizador;
    this.stepsService.getByIdOnline(this.data.solicitud.id).subscribe(
      data => {
        infoCliente = data[0];
      },
      () => {

      },
      () => {
        if (infoCliente.idRegistro !== null) {
          this.swalError();
        }else {
          this.router.navigate(['/modulos/venta-nueva/recotizador-vn/' + 'contactado' + '/'
          + this.data.solicitud.CotizacionesAli.Cotizacione.ProductoSolicitud.Prospecto.id + '/' + 2 + '/' + this.data.solicitud.id ]);
        }
      });

  }
  generarEmisionOnlineFunnel() {
        switch (this.data.solicitud.CotizacionesAli.peticion.aseguradora) {
          case 'QUALITAS':
            this.router.navigate(['modulos/venta-nueva/step-online/' + this.data.solicitud.id + '/'
            + this.data.solicitud.CotizacionesAli.peticion.aseguradora]);
            break;
          case 'GNP' :

            break;
          default:

            break;
        }
  }
  swalError() {
    this.notificaciones.advertencia('No se puede Recotizar, Ya se creo la emision').then((result) => {
      if (result.value) {
        window.close();
      }
    });
  }

}
