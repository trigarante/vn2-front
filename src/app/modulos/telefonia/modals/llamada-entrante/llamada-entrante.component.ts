import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {WebrtcService} from "../../../../@core/data/services/telefonia/webrtc.service";
import {MAT_SNACK_BAR_DATA, MatSnackBarRef} from "@angular/material/snack-bar";
import {Observable, Subscription} from "rxjs";
import {ExtensionesService} from "../../../../@core/data/services/telefonia/extensiones.service";
import {Extensiones} from "../../../../@core/data/interfaces/telefonia/extensiones";
import {FormControl} from "@angular/forms";
import {map, startWith} from "rxjs/operators";
import {PeticioneAmiService} from "../../../../@core/data/services/telefonia/peticione-ami.service";
import {MatDialog} from "@angular/material/dialog";
import {DialogoLlamadaComponent} from "../dialogo-llamada/dialogo-llamada.component";
import {SolicitudesvnTelefoniaService} from "../../../../@core/data/services/telefonia/solicitudesvn-telefonia.service";
import {LlamadasInService} from "../../../../@core/data/services/telefonia/llamadas-in.service";
import {TipificarComponent} from "../tipificar/tipificar.component";
import {EstadoUsuarioTelefoniaService} from "../../../../@core/data/services/telefonia/estado-usuario-telefonia.service";
import {KeypadComponent} from "../keypad/keypad.component";
import {ComentariosAgendaComponent} from "../comentarios-agenda/comentarios-agenda.component";

@Component({
  selector: 'app-llamada-entrante',
  templateUrl: './llamada-entrante.component.html',
  styleUrls: ['./llamada-entrante.component.scss']
})
export class LlamadaEntranteComponent implements OnInit, OnDestroy {
  private llamadaEvents: Subscription;
  public solicitudSubs: Subscription;
  public options: Extensiones[];
  filteredOptions: Observable<Extensiones[]>;
  myControl = new FormControl();
  public transferir = false;
  public estadoMute: boolean = true;
  public holdStatus: boolean = true;
  holdAudio = false;
  public counter = 0;
  private intervalId: any;
  public dialogoData: any;
  private idSolicitud: any;
  private idLlamada: number;
  public contesto = false;
  idUsuario = sessionStorage.getItem('Usuario');
  idEstadoLlamada = 3;


  constructor(private webRtcService: WebrtcService,
              private snackBarRef: MatSnackBarRef<LlamadaEntranteComponent>,
              private extensionesService: ExtensionesService,
              private dialogService: MatDialog,
              private llamadasInService: LlamadasInService,
              private peticionesAmiService: PeticioneAmiService,
              private estadoUsrTelefS: EstadoUsuarioTelefoniaService,
              private solicitudesVn: SolicitudesvnTelefoniaService,
              @Inject(MAT_SNACK_BAR_DATA) public data: any,
              private dialog: MatDialog,
              ) {
    this.estadoUsrTelefS.cambiarEstado(3, this.idUsuario).subscribe();
  }

  ngOnInit(): void {
    console.log('LLAMADA ENTRANTE')
    this.getEvents();
    this.getSolicitud();
    this.getNumerosTransferencia();
  }

  getSolicitud() {
    this.solicitudSubs = this.peticionesAmiService.respSolicitud.subscribe((resp) => {
      this.dialogoData = resp;
      if (resp.solicitud) {
        this.asignarSolicitud(resp.solicitud.id);
        this.idSolicitud = resp.solicitud.id;
      }
      if (resp.idAsterisk) {
        this.iniciarLlamada(resp.idAsterisk);
      }
      if (this.contesto) {
        this.abrirDialogo();
      }
    });
  }

  iniciarLlamada(idAsterisk) {
    let tipoLlamada;
    if(this.dialogoData.solicitud) {
      tipoLlamada = 1;
    } else {
      tipoLlamada = 3;
    }
    this.llamadasInService.iniciarLlamadaIn(this.data.numero, idAsterisk, tipoLlamada, this.dialogoData.solicitud.id)
      .subscribe((resp: any) => {
        this.idLlamada = resp.id;
      });
  }

  asignarSolicitud(idSolicitud) {
    if (idSolicitud) {
      this.solicitudesVn.asignarEmpleadoSolicitud(idSolicitud).subscribe();
      // this.solicitudesVn.desactivarContactaciones(this.dialogoData.idHistorico).subscribe();
    }
  }

  ngOnDestroy() {
    if (this.llamadaEvents) {this.llamadaEvents.unsubscribe();}
    this.solicitudSubs.unsubscribe();
    clearInterval(this.intervalId);
  }

  contestar() {
    this.webRtcService.sipCall('call-audio', null);
    this.iniciarContador();
    this.contesto = true;
  }

  colgar() {
    this.idEstadoLlamada = 5;
    this.webRtcService.sipHangUp();
    this.snackBarRef.dismiss();
    // this.snackBarRef.dismiss();
  }

  getEvents() {
    this.llamadaEvents = this.webRtcService.listenEvents().subscribe((resp) => {
      switch (resp.evento) {
        case 'disconnected':
        case 'terminated':
          if (this.idLlamada) {
            this.finalizarLlamada();
          }
          if (!this.contesto) {
            this.estadoUsrTelefS.cambiarEstado(1, this.idUsuario).subscribe();
          }
          this.llamadaEvents.unsubscribe();
          this.webRtcService.sipHangUp();
          this.snackBarRef.dismiss();
          break;
      }
    });
  }

  getNumerosTransferencia() {
    this.extensionesService.getContactos().subscribe((resp) => {
      this.options = resp;
      this.filteredOptions = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(value => {
            return this._filter(value);
          }),
        );
    });
  }

  private _filter(value: string): Extensiones[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.nombreCampania.toLowerCase().includes(filterValue));
  }

  transfer(extension: string) {
    this.webRtcService.sipTransfer(extension);
    this.transferir = true;
    this.finalizarLlamada();
    this.llamadaEvents.unsubscribe();
    this.snackBarRef.dismiss();
  }

  mute(estadoMute) {
    this.estadoMute = (estadoMute === 1) ? false : true;
    this.webRtcService.sipToggleMute();
  }
  hold(holdState) {
    this.holdStatus = (holdState === 1) ? false : true;
    this.webRtcService.sipToggleMute();
    this.holdAudio = holdState === 1;
  }

  iniciarContador() {
    this.intervalId = setInterval(() => {
      this.counter = this.counter + 1;
    }, 1000);
  }

  abrirDialogo() {
    this.dialogService.open(DialogoLlamadaComponent, {
      data: this.dialogoData,
      width: '550px',
    });
  }
  finalizarLlamada() {
    this.llamadasInService.finLlamadaIn(this.idLlamada, this.idEstadoLlamada).subscribe();
    this.dialogService.open(TipificarComponent, {
      data: {
        idLlamada: this.idLlamada,
        predictivo: 1
      },
      disableClose: true
    });
  }
  openDialpad() {
    this.dialogService.open(KeypadComponent);
  }

  llamadaPendiente() {
    this.dialog.open(ComentariosAgendaComponent, {
      data: this.data
    });
  }

}
