import { Component, OnInit,Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-llamadas-out',
  templateUrl: './llamadas-out.component.html',
  styleUrls: ['./llamadas-out.component.scss']
})
export class LlamadasOutComponent implements OnInit {

  onlyNumbers: RegExp = /^[0-9]+$/; // Expresion regular que acepta numeros unicamente
  num = '';
  datos : any ;
  constructor(public dialogRef: MatDialogRef<LlamadasOutComponent>,
              @Inject(MAT_DIALOG_DATA) public data
  ){
    this.datos = data;
  }

  ngOnInit(): void {
    console.log('LLAMADA ENTRANTE 2')
    this.datos.nombre;
    this.datos.extension;
  }


  getValue() {
    const value = (<HTMLInputElement>document.getElementById('output')).value;
    return value;
  }
  takevalue(x) { // Funcion que va colocando un numero ingresado por el boton del dialpad
    let num = this.getValue();
    if (num.length < 11 ) {
      num += x;
      this.num = num;
    }

   //  this.pjsip.sipSendDTMF(x.toString());
  }


  borrar() { // Funcion que borra un numero en el input donde va el telefono a marcar
    let phoneNumber = this.getValue();
    phoneNumber = phoneNumber.slice(0, phoneNumber.length - 1);
    this.num = phoneNumber;
  }

  colgar() {
   // this.pjsip.sipHangUp();
    this.dialogRef.close();
  }




}
