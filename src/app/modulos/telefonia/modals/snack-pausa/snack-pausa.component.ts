import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBarRef} from '@angular/material/snack-bar';
import {EstadoUsuarioTelefoniaService} from "../../../../@core/data/services/telefonia/estado-usuario-telefonia.service";
import {WebrtcService} from "../../../../@core/data/services/telefonia/webrtc.service";
import {NotificacionesService} from "../../../../@core/data/services/others/notificaciones.service";

@Component({
  selector: 'app-snack-pausa',
  templateUrl: './snack-pausa.component.html',
  styleUrls: ['./snack-pausa.component.scss']
})
export class SnackPausaComponent implements OnInit, OnDestroy {
  segundos: any;
  mensaje: string;
  interval: any;
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data,
              private matSnackRef: MatSnackBarRef<SnackPausaComponent>,
              private webRTC: WebrtcService,
              private telefoniaUsuario: EstadoUsuarioTelefoniaService,
              private notificacionesService: NotificacionesService,
              ) {
    this.setMensaje();
  }

  ngOnInit(): void {
    if (this.data.tiempo) {
      this.startInterval();
    }
  }

  ngOnDestroy() { }

  startInterval() {
    this.interval = setInterval(() => {
      this.data.tiempo --;
      if (this.data.tiempo === 0) {
        clearInterval(this.interval);
        return this.mensaje = 'Excediste tu tiempo de pausa';
      }
      this.setMensaje();
    }, 1000);
  }

  setMensaje() {
    if (this.data.tiempo) {
      this.segundos = new Date(this.data.tiempo * 1000).toISOString().substr(11, 8);
      this.mensaje = `Cuentas con ${this.segundos}`;
    } else {
      this.mensaje = 'Te encuentras en pausa';
    }
  }

  finalizarPausa() {
    const pausaFin = this.notificacionesService.pregunta('¿Deseas finaliza la pausa?');
    pausaFin.then(res => {
      if (res.isConfirmed) {
        this.webRTC.sipCall('call-audio', '*47');
        clearInterval(this.interval);
        this.telefoniaUsuario.activarUsuario(sessionStorage.getItem('Usuario')).subscribe();
        this.telefoniaUsuario.finPausa(this.data.idPausaUsuario).subscribe();
        this.matSnackRef.dismiss();
      }
    });
  }

}
