import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ModulosComponent} from './modulos.component';
import {AuthGuard} from '../@core/data/services/login/auth.guard';
import {DashboardComponent} from "./Dashboard/dashboard/dashboard.component";

const routes: Routes = [{
  path: '',
  component: ModulosComponent,
  children: [
    {
      path: 'dashboard',
      loadChildren: () => import('./Dashboard/dashboard.module').then(m => m.DashboardModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'venta-nueva',
      // loadChildren: './venta-nueva/venta-nueva.module#VentaNuevaModule',
      loadChildren: () => import('./venta-nueva/venta-nueva.module').then(m => m.VentaNuevaModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'telefonia',
      // loadChildren: './venta-nueva/venta-nueva.module#VentaNuevaModule',
      loadChildren: () => import('./telefonia/telefonia.module').then(m => m.TelefoniaModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'module-federation',
      loadChildren: () => import('cotizadoresInternos/Module').then(m => m.CotizadoresInternosModule)
    },
    {
      path: 'policy-registration-mf',
      loadChildren: () => import('policyRegistration/Module').then(m => m.PolicyRegistrationStepsModule)
    },
    // {
    //   path: 'ocr-stepper',
    //   loadChildren: () => import('policyRegister/Module').then(m => m.OcrStepperModule)
    // },
    {
      path: '**',
      component: DashboardComponent,
      canActivate: [AuthGuard]
    },
  ]

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulosRoutingModule {
}
