import { Component, OnInit } from '@angular/core';
import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';
import {DashBoardNew} from "../../../@core/data/interfaces/reportes/dash-board-new";
import {XlsxService} from "../../../@core/data/services/reportes/xlsx.service";
import {DescargasService} from "../../../@core/data/services/reportes/descargas.service";
import {DashBoardNewService} from "../../../@core/data/services/reportes/dash-board-new.service";
const moment = _rollupMoment || _moment;
@Component({
  selector: 'app-dash-reno-pjsip',
  templateUrl: './dash-reno-pjsip.component.html',
  styleUrls: ['./dash-reno-pjsip.component.scss']
})
export class DashRenoPjsipComponent implements OnInit {
  //
  // displayedColumns: string[] = ['subarea', 'ejecutivo', 'leads', 'leadsError', 'error%', 'noContactados', 'noC%', 'enMarcacion1a4'
  //   , 'enM14%', 'enMarcacion5', 'enM5%', 'contactados', 'con%', 'contactoEfectivo', 'conE%', 'horaLogueo', 'tiempoLogueo'
  //   , 'pausas', 'tiempoPausaa', 'tiempoProductivo', 'llamadas'];
  // dataSource: any;
  // dashBoard: DashBoardNew[];
  // dashBoardFiltro: DashBoardNew[] = [{idEmpleado: 0, idSubarea: 0, subarea: 'TODAS', ejecutivo: 'a', leads: 0,
  //   llamadas: 0, contactoErroneo: 0, contactados: 0, enMarcacion5: 0, contactoEfectivo: 0, horaLogueo: 0, tiempoLogueo: 0, pausas: 0,
  //   tiempoPausaa: ' ', tiempoProductivo: 0, noContactados: 0, enMarcacion1a4: 0}];
  // rangeDates: Date[];
  // fechaCompleta1: any;
  // fechaCompleta2: any;
  // totalDays: number;
  // idSubarea: number = 0;
  // llamadasTotal: number;
  // llamadasAcd: number;
  // llamadasOut: number;
  // totalBase: number;
  // puesto: string = sessionStorage.getItem('idPuesto');
  //
  // constructor(private xlsxService: XlsxService,
  //             private dashBoardNewService: DashBoardNewService,
  //             private descargasService: DescargasService) {
  //   this.rangeDates = [
  //     new Date(new Date().setDate(new Date().getDate())),
  //     new Date(new Date().setDate(new Date().getDate())),
  //   ];
  //   this.fechaCompleta1 = moment(this.rangeDates[0]).valueOf();
  //   this.fechaCompleta2 = moment(this.rangeDates[1]).valueOf();
  //   this.totalDays = moment(this.rangeDates[1]).diff(moment(this.rangeDates[0]).valueOf(), 'days');
  //   this.dashBoardNewService.getDashBoardFiltroRenoPjsip(this.fechaCompleta1, this.fechaCompleta2).subscribe(data => {
  //     data.forEach(value => {
  //       this.dashBoardFiltro.push(value);
  //     });
  //   });
  // }
  //
  ngOnInit(): void {
    // this.getData();
  }
  // getData() {
  //   this.descargasService.getAvanceEjecutivos(this.fechaCompleta1, this.fechaCompleta2).subscribe(data => {
  //     this.totalBase = data.map(t => t.base).reduce((acc, value) => acc + value, 0);
  //   });
  //   this.descargasService.getLlamadasReno(this.fechaCompleta1, this.fechaCompleta2).subscribe(data => {
  //     this.llamadasAcd = data.map(t => t.acd).reduce((acc, value) => acc + value, 0);
  //     this.llamadasOut = data.map(t => t.out).reduce((acc, value) => acc + value, 0);
  //     this.llamadasTotal = this.llamadasAcd + this.llamadasOut;
  //   });
  // }
  // getFechas() {
  //   this.fechaCompleta1 = moment(this.rangeDates[0]).valueOf();
  //   this.fechaCompleta2 = moment(this.rangeDates[1]).valueOf();
  //   this.descargasService.getAvanceEjecutivos(this.fechaCompleta1, this.fechaCompleta2).subscribe(data => {
  //     this.totalBase = data.map(t => t.base).reduce((acc, value) => acc + value, 0);
  //   });
  //   this.descargasService.getLlamadasReno(this.fechaCompleta1, this.fechaCompleta2).subscribe(data => {
  //     this.llamadasAcd = data.map(t => t.acd).reduce((acc, value) => acc + value, 0);
  //     this.llamadasOut = data.map(t => t.out).reduce((acc, value) => acc + value, 0);
  //     this.llamadasTotal = this.llamadasAcd + this.llamadasOut;
  //   });
  // }
  //
  // descargaAvanceEje() {
  //   this.descargasService.getAvanceEjecutivos(this.fechaCompleta1, this.fechaCompleta2).subscribe(value => {
  //     const columns = ['idEmpleado',  'ejecutivo',  'base',  'renovadas',  'reexpedidas',  'migradas',  'gestion',  'canceladas'];
  //     this.xlsxService.imprimirXLS(value, columns, 'Avance Ejecutivos');
  //   });
  // }
  // descargaAvance() {
  //   this.descargasService.getAvance(this.fechaCompleta1, this.fechaCompleta2).subscribe(
  //     value => {
  //       const columns = ['poliza',  'fechaInicio',  'fechaFin',  'estado',  'estadoPago',  'primerLlamada',  'ultimaLlamada',
  //         'ultimaEtiqueta',  'llamadas',  'ultimoComentario'];
  //       this.xlsxService.imprimirXLS(value, columns, 'Avance Polizas');
  //     });
  // }
  // descargaLlamadas() {
  //   this.descargasService.getLlamadasReno(this.fechaCompleta1, this.fechaCompleta2).subscribe(
  //     value => {
  //       const columns = ['idEmpleado', 'ejecutivo', 'acd', 'tiempoMedioAcd', 'out', 'tiempoMedioOut', 'tiempoPausa',
  //         'tiempoProductivo', 'llamadasAbandonadas', 'horaLogueo', 'tiempoLogueo'];
  //       this.xlsxService.imprimirXLS(value, columns, 'Llamadas ejecutivos');
  //     });
  // }
  // descargaPNC() {
  //   this.descargasService.getPnc(this.fechaCompleta1, this.fechaCompleta2).subscribe(data => {
  //     const columns = ['id', 'fechaPago', 'montoPago', 'aseguradora', 'numeroPoliza', 'inicioVigencia', 'finVigencia',
  //       'tipoPago', 'metodoPago', 'seriePago', 'estadoPoliza', 'estadoRecibo', 'estadoPago', 'estadoVerificacion', 'fechaCreacionMesa',
  //       'fechaInicioMesa', 'nombreCliente', 'paternoCliente', 'maternoCliente', 'idEmpleado', 'nombreEjecutivo', 'areaEjecutivo',
  //       'areaPoliza', 'idSolictud', 'numeroSerieVehículo', 'cobertura', 'tipoUso', 'campañaEjecutivo', 'campañaPoliza',
  //       'idInspeccion', 'nombreSede', 'tipoContacto', 'productividad'];
  //     this.xlsxService.imprimirXLS(data, columns, 'Prima Neta Cobrada');
  //   });
  // }
  // descargaLlamadasSalida() {
  //   this.descargasService.getLlamadasRenoSalidas(this.fechaCompleta1, this.fechaCompleta2).subscribe(
  //     value => {
  //       const columns = ['id',  'estado',  'subarea',  'ejecutivo',  'numero',  'horaInicio',  'horaFinal',  'duracion',
  //         'etiqueta',  'subEtiqueta'];
  //       this.xlsxService.imprimirXLS(value, columns, 'Llamadas manuales');
  //     });
  // }
  // descargaLlamadasEntrada() {
  //   this.descargasService.getLlamadasRenoEntrantes(this.fechaCompleta1, this.fechaCompleta2).subscribe(
  //     value => {
  //       const columns = ['id',  'estado',  'subarea',  'ejecutivo',  'numero',  'horaInicio',  'horaFinal',  'duracion',
  //         'etiqueta',  'subEtiqueta'];
  //       this.xlsxService.imprimirXLS(value, columns, 'Llamadas acd');
  //     });
  // }
  // applyFilter(event: Event) {
  //   const filterValue = (event.target as HTMLInputElement).value;
  //   this.dataSource.filter = filterValue.trim().toLowerCase();
  //
  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }

}
