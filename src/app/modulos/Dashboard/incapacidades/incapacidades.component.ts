import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-incapacidades',
  templateUrl: './incapacidades.component.html',
  styleUrls: ['./incapacidades.component.scss']
})
export class IncapacidadesComponent implements OnInit {
  tabActual = 0;
  permisos = JSON.parse(window.localStorage.getItem('User')).JUST;

  constructor(
  ) { }

  ngOnInit(): void {
  }
  tabClick(event) {
    this.tabActual =  event.index;
    console.log(this.tabActual);
  }
}
