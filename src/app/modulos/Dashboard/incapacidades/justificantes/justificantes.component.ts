import {Component, Input, OnInit} from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {CandidatoService} from '../../../../@core/data/services/atraccion-talento/candidato/candidato.service';
import {JustificacionesService} from '../../../../@core/data/services/administracion-personal/justificaciones.service';
import {RechazarJustificanteComponent} from '../modals/rechazar-justificante/rechazar-justificante.component';
import {SolicitarJustificanteComponent} from '../modals/solicitar-justificante/solicitar-justificante.component';

@Component({
  selector: 'app-justificantes',
  templateUrl: './justificantes.component.html',
  styleUrls: ['./justificantes.component.scss']
})
export class JustificantesComponent implements OnInit {

  completas;
  solicitudesPendientes = [];
  solicitudesCompletas = [];
  datos: any[] = [];
  paginator = [];
  data = null;
  length = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 15, 20, 50];
  currentTab = 100;
  fecha = new Date();
  @Input() EstadoJustificacion;
  permisos = JSON.parse(window.localStorage.getItem('User'));

  constructor(
    private matDialog: MatDialog,
    private justificaciones: JustificacionesService,
    private candidatoService: CandidatoService,
    private notificaciones: NotificacionesService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getSolicitudes();
  }

  getSolicitudes() {
    // Variable que dependera de los permisos
    const estado = this.EstadoJustificacion === 0 ? this.completas = 3 : this.completas = 4;
    const permiso = 1;
    this.justificaciones.getByEmpleadoAndEstado(estado, permiso).subscribe( values => {
      this.completas === 3 ? this.solicitudesCompletas = values : this.solicitudesPendientes = values;
      if (values.length === 0) { this.notificaciones.informacion(`No tienes solicitudes ${this.completas === 3 ? 'pendientes' : 'completadas'} por mostrar`); }
      this.updateDatos();
      this.length = values.length;
    });
  }

  onPage(event: PageEvent) {
    const start = event.pageIndex * event.pageSize;
    const final = event.pageIndex === 0 ? start + event.pageSize : start + event.pageSize + 1;
    this.paginator = this.datos.slice(start, final);
    this.length = event.length;
  }

  async solicitarIncapacidad() {
    const respuesta = await this.matDialog.open(SolicitarJustificanteComponent, { width: '800px' }).afterClosed().toPromise();
    if (respuesta) {
      this.completas === 3 ? this.solicitudesPendientes = [] : this.solicitudesCompletas = [];
      this.getSolicitudes();
    }
  }

  updateDatos() {
    this.datos = this.completas === 3 ? this.solicitudesCompletas : this.solicitudesPendientes;
    this.onPage( { pageIndex: 0, pageSize: this.pageSize, length: this.datos.length });
  }

  update() {
    this.completas === 1 ? this.solicitudesPendientes = [] : this.solicitudesCompletas = [];
    this.getSolicitudes();
  }

  onOpen(idArchivo, index) {
    this.data = null;
    this.currentTab = index;
    this.justificaciones.getByArchivo(idArchivo).subscribe(archivo => this.data = archivo[0]);
  }

  async finalizar(id) {
    const respuesta = await this.notificaciones.pregunta(`¿Quieres autorizar esta solicitud?`);
    if (respuesta.isConfirmed) {
      this.notificaciones.carga();
      this.justificaciones.updateEstatus({
        id,
        idEmpleadoAutorizo: +sessionStorage.getItem('Empleado'),
        idEstadoJustificacion: 2,
      }).subscribe(() => this.notificaciones.exito().then(() => this.getSolicitudes()));
    }
  }

  abrirDialog(id) {
    console.log(id);
    this.dialog.open(RechazarJustificanteComponent, {
      data: id,
      width: '500px',
      disableClose: true,
    }).afterClosed().subscribe(reload => {
      if (reload) { this.getSolicitudes(); }
    });
  }

}
