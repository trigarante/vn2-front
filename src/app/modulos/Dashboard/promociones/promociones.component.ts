import { Component, OnInit } from '@angular/core';
import {BeneficiosService} from '../../../@core/data/services/desarrollo-organizacional/beneficios.service';

@Component({
  selector: 'app-promociones',
  templateUrl: './promociones.component.html',
  styleUrls: ['./promociones.component.scss']
})
export class PromocionesComponent implements OnInit {

  beneficio: any = [];

  constructor(
    public beneficioService: BeneficiosService
  ) { }

  ngOnInit(): void {
    this.beneficioService.getBeneficioActual().subscribe(data => this.beneficio = data[0]);
    console.log(this.beneficio);
  }

  redireccionar() {
    if (this.beneficio.link) {
      window.open(this.beneficio.link);
    }
  }

}
