import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UltimoMovimientoComponent } from './ultimo-movimiento.component';

describe('UltimoMovimientoComponent', () => {
  let component: UltimoMovimientoComponent;
  let fixture: ComponentFixture<UltimoMovimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UltimoMovimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UltimoMovimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
