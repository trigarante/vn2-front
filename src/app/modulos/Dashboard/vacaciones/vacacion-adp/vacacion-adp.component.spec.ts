import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VacacionAdpComponent } from './vacacion-adp.component';

describe('VacacionAdpComponent', () => {
  let component: VacacionAdpComponent;
  let fixture: ComponentFixture<VacacionAdpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VacacionAdpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VacacionAdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
