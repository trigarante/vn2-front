import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizarVacacionesComponent } from './autorizar-vacaciones.component';

describe('AutorizarVacacionesComponent', () => {
  let component: AutorizarVacacionesComponent;
  let fixture: ComponentFixture<AutorizarVacacionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutorizarVacacionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizarVacacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
