import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import {FormControl, Validators} from '@angular/forms';
import {SolicitudesVacacionesService} from '../../../../../@core/data/services/administracion-personal/solicitudes-vacaciones.service';

@Component({
  selector: 'app-cancelar-vacaciones',
  templateUrl: './cancelar-vacaciones.component.html',
  styleUrls: ['./cancelar-vacaciones.component.scss']
})
export class CancelarVacacionesComponent implements OnInit {

  comentarios = new FormControl('', Validators.compose([Validators.required]));

  constructor(
    public dialogRef: MatDialogRef<CancelarVacacionesComponent>,
    private notificaciones: NotificacionesService,
    private vacacionesService: SolicitudesVacacionesService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit(): void {
  }

  guardar() {
    console.log(this.data);
    this.notificaciones.carga(`Se están guardando los comentarios`);
    this.vacacionesService.update(this.data, {
      idEmpleadoADP: +sessionStorage.getItem('Empleado'),
      fechaCancelacion: new Date(),
      idEstadoVacacion: 4,
      comentariosADP: this.comentarios.value,
    }).subscribe(() => this.notificaciones.exito().then(() => this.cerrar(true)));
  }

  cerrar(reload: boolean) {
    this.notificaciones.cerrar();
    this.dialogRef.close(reload);
  }

}
