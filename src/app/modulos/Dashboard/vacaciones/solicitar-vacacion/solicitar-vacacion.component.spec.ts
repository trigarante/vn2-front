import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarVacacionComponent } from './solicitar-vacacion.component';

describe('SolicitarVacacionComponent', () => {
  let component: SolicitarVacacionComponent;
  let fixture: ComponentFixture<SolicitarVacacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitarVacacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarVacacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
