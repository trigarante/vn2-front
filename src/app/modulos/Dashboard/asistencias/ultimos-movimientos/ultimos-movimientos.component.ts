import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AsistenciasService} from '../../../../@core/data/services/administracion-personal/asistencias/asistencias.service';
import {EstadoAsistenciaService} from '../../../../@core/data/services/administracion-personal/asistencias/estado-asistencia.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {MatDialog} from '@angular/material/dialog';
import {UltimosMovimientosService} from '../../../../@core/data/services/administracion-personal/asistencias/ultimos-movimientos.service';
import {SolicitarIncapacidadComponent} from '../../incapacidades/modals/solicitar-incapacidad/solicitar-incapacidad.component';
import {UltimoMovimiento} from '../../../../@core/data/interfaces/administracion-personal/ultimoMovimiento';

@Component({
  selector: 'app-ultimos-movimientos',
  templateUrl: './ultimos-movimientos.component.html',
  styleUrls: ['./ultimos-movimientos.component.scss']
})
export class UltimosMovimientosComponent implements OnInit {
  hayCorte: boolean = true;
  estados;
  displayedColumns = ['id', 'nombre'];
  asisTypes;
  areas;
  anios: number[] = [];
  input = new FormControl('', Validators.required);
  respaldo;
  asistencias;
  yaHayDatos = false;
  empezando = true;
  actualRow: string;
  actualColumn: string;
  menuAbierto = false;
  texto = 'Selecciona un modo';
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });
  moviendo = false;
  constructor(
    private asistenciasService: AsistenciasService,
    private estadoAsistenciaService: EstadoAsistenciaService,
    private notificacionesService: NotificacionesService,
    private matDialog: MatDialog,
    private ultimosMovimientos: UltimosMovimientosService
  ) {
    this.displayedColumns.length = 33;
  }

  showHide(){
    if (this.menuAbierto) {
      this.menuAbierto = false;
    }
    else if (!this.menuAbierto) {
      this.menuAbierto = true;
    }
  }
  edit(row, el){
    this.actualRow = row.nombre;
    this.actualColumn = el.def;
    this.menuAbierto = true;
  }

  ngOnInit(): void {
    this.estadoAsistenciaService.getActivos().subscribe(data => this.estados = data);
    if (this.hayCorte){
      this.getAsistenciasByDpto( );
    }
  }
  search(value: string) {
    const filter = value.toLowerCase();
    return this.respaldo.filter(area => area.nombre.toLowerCase().startsWith(filter));
  }
  getAsistenciasByDpto(){
    if (this.empezando) this.empezando = false;
    if (this.yaHayDatos) this.yaHayDatos = false;
    this.displayedColumns = ['id', 'nombre'];
    this.asistenciasService.getUltimos('2022-04-01', '2022-04-25', 3, 14).subscribe({
      // this.asistenciasService.getAsistencias(fInicio, fFin, idDepartamento, idArea).subscribe({
      next: data => {
        this.asistencias = data;
        for (const dia of this.asistencias.diasFinal) {
          this.displayedColumns.push(dia.def);
        }
      },
      complete: () => {
        this.getEstados();
        this.yaHayDatos = true;
      },
      error: () => {
        this.notificacionesService.error('Error al cargar las asistencias');
        this.yaHayDatos = false;
        this.empezando = true;
      }
    });
  }
  getEstados(){
    this.asistenciasService.getEstadosAsistencia().subscribe(data => this.asisTypes = data );
  }
  updateAsistencia(idAsistencia, idEstadoAsistencia, id){
    this.ultimosMovimientos.unaEnProceso(idAsistencia).subscribe({
      next: res => {
        this.menuAbierto = false;
        if (res) {
          this.notificacionesService.informacion('Ya cuentas con una solicitud en proceso para esta asistencia');
        } else {
          this.matDialog.open(SolicitarIncapacidadComponent, {
            width: '800px',
            data: {asistencia: 1, id, idEstadoAsistencia},
          }).afterClosed().subscribe({
            next: data => {
              if (data) {
                this.moviendo = true;
                const {
                  idEmpleado, idEmpleadoSupervisor, idMotivo, comentarios,
                  fechaFin, fechaInicio, archivo
                } = data;
                const ultimoMovimiento: UltimoMovimiento = {
                  idEmpleado,
                  idEmpleadoSupervisor,
                  idMotivo,
                  documento: archivo,
                  comentarios,
                  fechaFin,
                  fechaInicio,
                  idEstadoAsistencia,
                  idAsistencia,
                  idCierre: 1
                };
                this.ultimosMovimientos.postMovimiento(ultimoMovimiento).subscribe({
                  error: () => {
                    this.moviendo = false;
                    this.notificacionesService.error('Error al solicitar ultimoMovimiento');
                  },
                  complete: () => {
                    this.moviendo = false;
                    this.notificacionesService.exito('Tu solicitud de ultimoMovimiento fue registrada, espera su revisión por ADP');
                    this.getAsistenciasByDpto();
                  }
                });
              }
              // this.editar(idAsitencia, idEstadoAsistencia, idEmpleadoAEditar, idEmpleadoEditor);
            },
            error: () => this.notificacionesService.error('Error al solicitar ultimoMovimiento'),
          });
        }
      },
      error: () => this.notificacionesService.error()
    });
  }
  editar(idAsitencia, idEstadoAsistencia, idEmpleadoAEditar, idEmpleadoEditor){
    this.asistenciasService.updateEstadosAsistencia(idAsitencia, idEstadoAsistencia, idEmpleadoAEditar, idEmpleadoEditor).subscribe({
      next: () => this.menuAbierto = false,
      complete: () => {
        this.notificacionesService.exito('Tu asistencia fue modificada');
        this.getAsistenciasByDpto();
      },
      error: () => this.notificacionesService.error('No se pudo editar la asistencia')
    });
  }

}
