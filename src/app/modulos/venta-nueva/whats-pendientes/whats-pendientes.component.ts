import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Subscription} from "rxjs";
import {WhatsappMsgService} from "../../../@core/data/services/whatsappMsg/whatsapp-msg.service";
import {MatDialog} from "@angular/material/dialog";
import {DomSanitizer} from "@angular/platform-browser";
import {Router} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";
import {NotificacionesService} from "../../../@core/data/services/others/notificaciones.service";
import {ChatWhatsappComponent} from "../modals/chat-whatsapp/chat-whatsapp.component";
import moment from "moment-timezone";
import Swal from "sweetalert2";
import {ViewerImgWhatsComponent} from "../modals/viewer-img-whats/viewer-img-whats.component";

@Component({
  selector: 'app-whats-pendientes',
  templateUrl: './whats-pendientes.component.html',
  styleUrls: ['./whats-pendientes.component.scss']
})
export class WhatsPendientesComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('tablaInboundPaginator', {read: MatPaginator}) tablaInboundPaginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: any;
  mensajesData: any[];
  mensajesClasificados: any[];
  arrDataMensajes: any[] = [];
  dataLoaded = false;
  cols: any[] = ['solicitud', 'fecha', 'mensaje', 'acciones'];
  sinDatos = true;

  texto = '';
  textoAux = '';
  elemento: HTMLElement;
  mensajes: any[] = [];
  mensajesMultimedia: any[] = [];
  mensajesAux: any[] = [];
  websocketsWhatsapp: any;
  localizacion: any;
  dataChat: any;
  dataMensajesCanal: any;
  socketServiceSubscription: Subscription;
  idSolicitud = sessionStorage.getItem('idSolicitud');
  idSolicitudChat: any;
  idSession: any;
  hide = true;
  counter = {};
  mediaMessage = [
    // {
    //   id: 1,
    //   tipo: 'TEXT',
    // },
    // {
    //   id: 2,
    //   tipo: 'SMS',
    // },
    {
      id: 3,
      tipo: 'Imágenes',
    },
    {
      id: 4,
      tipo: 'Audio',
    },
    {
      id: 5,
      tipo: 'Localizaciones',
    },
    // {
    //   id: 6,
    //   tipo: 'STICKER',
    // },
    // {
    //   id: 7,
    //   tipo: 'PLANTILLA',
    // },
    {
      id: 8,
      tipo: 'Documentos',
    },
  ];
  mediaMessageArr = [];
  cargarPanel = false;
  cargando = true;
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(
    private whatsMsgService: WhatsappMsgService,
    private dialog: MatDialog,
    // private socket: Socket,
    private dom: DomSanitizer,
    private router: Router,
    private notificacionesService: NotificacionesService,
    // private socketsWhatsappService: SocketsWhatsappService,
  ) { }

  ngOnInit() {
    this.mensajesPendientes();
    // this.getMensajesCanal(id);
  }

  mensajesPendientes(cargar?) {
    this.arrDataMensajes = [];
    this.dataSource = new MatTableDataSource([]);
    this.whatsMsgService.getMensajesCanalGeneral().subscribe({
      next: (value: any) => {
        this.mensajesClasificados = value.dataClasificada;

        for (let i = 0; i < this.mensajesClasificados.length; i++) {
          for (let j = 0; j < this.mensajesClasificados[i].length; j++) {
            if (j === this.mensajesClasificados[i].length - 1) {
              this.arrDataMensajes.push({...this.mensajesClasificados[i][j], cant: this.mensajesClasificados[i].length});
            }
          }
        }
        this.arrDataMensajes.length !== 0 ? this.sinDatos = false : this.sinDatos;
        this.mensajesData = value;
        this.dataSource = new MatTableDataSource(this.arrDataMensajes);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        // console.log(this.mensajesClasificados[1][2].mensaje, 'this.mensajesData')
      },
      complete: () => {
        // this.socket.emit(`panelPendientes`, 1);
        this.dataLoaded = true;
        if (this.arrDataMensajes[0]) {
          this.cargarPanel = true;
          this.getMensajesCanal(this.arrDataMensajes[0].correlationId);
        } else {
          this.cargarPanel = false;
        }
      },
    });
    // }
  }

  enviarWha(data) {
    this.whatsMsgService.getMensajesByIdCanal(data.correlationId).subscribe({
      next: (value: any) => {
        // console.log(value, 'value--')
        if (value.cuerpo === null) {
          if (value.cantidadDeEnvioDePlantillas === 5) {
            // Swal.fire({
            //   type: 'warning',
            //   title: '¡Atención, alzanzaste el límite de mensajes!',
            //   text: 'Solo puedes enviar 5 plantillas a un mismo número.',
            // });
            this.notificacionesService.advertencia('Límite de mensajes alcanzado. Sólo puedes enviar 5 plantillas a un mismo número');
          } else {
            // this.ref.close();
            // this.dialog.open(WhatsappTipoSubareaComponent, {
            //   width: '400px',
            //   data: data,
            // });
          }
        } else {
          if (value.cuerpo !== null) {
            // this.datosCanalMensajes = value;
            const credentials = value.catalogoData;
            // this.mandarPlantilla = true;
            const dataChat = {
              id: data.correlationId,
              numeroProspecto: value.canalData.destino,
            };
            this.modalChat(dataChat, credentials, value);
          }
        }
      },
      error: err => {
        this.notificacionesService.error('Ocurrió un error');
      },
    });
  }

  modalChat(data, credentials, dataCanal) {
    sessionStorage.setItem('idSolicitud', String(data.id));
    this.dialog.open(ChatWhatsappComponent, {
      width: '550px',
      // panelClass: ['estilosModal', 'col-lg-8'],
      data: {data, credentials, dataCanal},
    }).afterClosed().subscribe(() => {
      // this.socket.emit(`tablaMensajesPendientesRoom`, 4);
      /*this.arrDataMensajes = [];
      this.dataSource = new MatTableDataSource([]);*/
      this.mensajesPendientes(true);
    });
  }

  ngOnDestroy(): void {
    sessionStorage.removeItem('idSolicitud');
    // this.socket.emit(`chatRoom`, `salir-${this.idSolicitud}`);
  }

  // Para saber cuando un canal ya pasó sus 24 hrs y volver a enviar alguna plantilla para reactivar el chat
  identificadorDeTiempo(fecha) {
    const date = new Date(fecha);
    const dateConHoras = new Date(fecha).setHours(24);
    const dateActual = new Date();
    const dateMsecActual = dateActual.getTime();

    const fechasData = this.conversion(date, dateConHoras, dateMsecActual);
    return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
  }

  conversion(fechaC, fechaMasHora, dateMsec) {
    const msecPorMinuto = 1000 * 60;
    const msecPorHora = msecPorMinuto * 60;
    const msecPorDia = msecPorHora * 24;

    const milisecFechaRegistro = fechaC / msecPorDia;
    const milisecFechaRegistroMasHoras = fechaMasHora / msecPorDia;
    const milisecActual = dateMsec / msecPorDia;

    return {
      milisecFechaRegistro: milisecFechaRegistro,
      milisecFechaRegistroMasHoras: milisecFechaRegistroMasHoras,
      milisecActual: milisecActual,
    };
  }

  getMensajesCanal(id) {
    sessionStorage.setItem('idSolicitud', String(id));
    this.idSession = sessionStorage.getItem('idSolicitud');
    this.mensajes = [];
    this.mensajesMultimedia = [];
    this.idSolicitudChat = id;
    // this.arrDataMensajes = [];
    this.mediaMessageArr = [];
    this.whatsMsgService.getMensajesByIdCanal(id).subscribe({
      next: (value: any) => {
        this.dataMensajesCanal = value;
        // Se manda la fecha del segundo mensaje que es la respuesta de MO y es cuando se abre el canal
        if (value.mensajes.length === 1) {
          this.dataChat = this.dataMensajesCanal.mensajes.filter(moMessages => {
            return moMessages.idTipoEnvio === 2;
          });
          for (let i = 0; i < this.dataMensajesCanal.mensajes.length; i++) {
            this.localizacion = this.dom.bypassSecurityTrustResourceUrl(`

            https://maps.google.com/maps?q=${this.dataMensajesCanal.mensajes[i].mensaje.split(',')[0]},${this.dataMensajesCanal.mensajes[i].mensaje.split(',')[1]}&z=15&output=embed
                      `);

            this.mensajes.push(
              {
                idTipoEnvio: this.dataMensajesCanal.mensajes[i].idTipoEnvio,
                mensaje: this.dataMensajesCanal.mensajes[i].mensaje,
                fecha: moment(this.dataMensajesCanal.mensajes[i].fecha).format('LT'),
                idTipoMensajeWhatsapp: this.dataMensajesCanal.mensajes[i].idTipoMensajeWhatsapp,
                localizacion: this.localizacion,
              },
            );
            //   }
          }
        } else {
          if (this.identificadorDeTiempo(this.dataMensajesCanal.mensajes[1].fecha) || this.dataMensajesCanal.canalData.activacion === 1) {
            this.dataChat = this.dataMensajesCanal.mensajes.filter(moMessages => {
              return moMessages.idTipoEnvio === 2;
            });
            /*.....separa los mensajes cuendo son mas de uno...............................................*/
            for (let i = 0; i < this.dataMensajesCanal.mensajes.length; i++) {
              //   for (let j = 0; j <= value[i].mensaje.length - 1; j++) {
              // this.localizacion = this.dom.bypassSecurityTrustResourceUrl(`
              //           https://maps.google.com/maps?
              //           q=${value[i].mensaje.split(',')[0]},${value[i].mensaje.split(',')[1]}&hl=es;z=14&amp;output=embed`);
              this.localizacion = this.dom.bypassSecurityTrustResourceUrl(`

            https://maps.google.com/maps?q=${this.dataMensajesCanal.mensajes[i].mensaje.split(',')[0]},${this.dataMensajesCanal.mensajes[i].mensaje.split(',')[1]}&z=15&output=embed
                      `);

              this.mensajes.push(
                {
                  idTipoEnvio: this.dataMensajesCanal.mensajes[i].idTipoEnvio,
                  mensaje: this.dataMensajesCanal.mensajes[i].mensaje,
                  fecha: moment(this.dataMensajesCanal.mensajes[i].fecha).format('LT'),
                  idTipoMensajeWhatsapp: this.dataMensajesCanal.mensajes[i].idTipoMensajeWhatsapp,
                  localizacion: this.localizacion,
                },
              );
              if (this.dataMensajesCanal.mensajes[i].idTipoMensajeWhatsapp === 3 ||
                this.dataMensajesCanal.mensajes[i].idTipoMensajeWhatsapp === 4 ||
                this.dataMensajesCanal.mensajes[i].idTipoMensajeWhatsapp === 8) {
                this.mensajesMultimedia.push(this.dataMensajesCanal.mensajes[i]);
              }
              if (!(this.dataMensajesCanal.mensajes[i].idTipoMensajeWhatsapp in this.counter))this.counter[this.dataMensajesCanal.mensajes[i].idTipoMensajeWhatsapp] = 0;
              this.counter[this.dataMensajesCanal.mensajes[i].idTipoMensajeWhatsapp]++;

              //   }
            }
            for (let j = 0; j < this.mediaMessage.length; j++) {
              if (this.mediaMessage[j].id in this.counter) {
                this.mediaMessageArr.push(this.mediaMessage[j]);
              }
            }
          } else {
            if (!this.router.url.includes('mensajes-pendientes')) {
              Swal.fire({
                icon: 'warning',
                title: '¡Atención!',
                text: 'El usuario ya no puede recibir mensajes debido a que ya pasó un lapso de 24 horas desde que ' +
                  'respondió la primer plantilla.' +
                  'Si es necesario continuar con la conversaación, por favor envía otra plantilla.',
                showConfirmButton: true,
                confirmButtonText: 'Enviar nueva plantilla',
                showCancelButton: true,
                cancelButtonText: 'Cerrar',
                reverseButtons: true,
              }).then( val => {
                if (val.value) {
                  const jsonData = {
                    // ...this.data.data,
                    numeroOrigen: this.dataMensajesCanal.canalData.origen,
                  };
                  // this.dialog.open(WhatsappTipoSubareaComponent, {
                  //   width: '400px',
                  //   // data: this.data.data,
                  //   data: jsonData,
                  // });
                }
              });
            }
            // this.dialog.open(WhatsappTipoSubareaComponent, {
            //   width: '400px',
            //   data: data,
            // });
          }
        }
      },
      error: e => {
        if (e.error.cuerpo === 'undefined') {
          this.notificacionesService.advertencia('Es necesario enviar una plantilla primero');
        }
      },
      complete: () => {
        this.cargando = false;
        /***************Se va hacia el último mensaje de la conversación*****************/
        this.elemento = document.getElementById('container');
        // console.log(this.elemento, 'this.elementothis.elementothis.elemento')
        if (this.elemento) {
          setTimeout(() => {
            this.elemento.scrollTop = this.elemento.scrollHeight;
          }, 10);
        }

        // this.socket.emit(`chatRoom`, 1);
        // this.socketServiceSubscription = this.socketsWhatsappService.fromEventGeneral(3).subscribe({
        // this.socket.on(`mensaje-nuevo${this.idSolicitud}`, (value: any) => {
        // this.elemento = document.getElementById('container');
        // if (this.elemento) {
        //     /******mostrar mensaje inmediato*****************/
        //     if (this.textoAux === value.mensaje) {
        //       this.mensajes.pop();
        //     }
        //     /**************************************************/
        //     this.localizacion = this.dom.bypassSecurityTrustResourceUrl(`
        //
        //     https://maps.google.com/maps?q=${value.mensaje.split(',')[0]},${value.mensaje.split(',')[1]}&z=15&output=embed
        //               `);
        //     this.mensajes.push({
        //       idTipoEnvio: value.idTipoEnvio,
        //       mensaje: value.mensaje,
        //       fecha: moment(value.fecha).fromNow(),
        //       idTipoMensajeWhatsapp: Number(value.idTipoMensajeWhatsapp),
        //       localizacion: this.localizacion,
        //     });
        //     if (+value.idTipoMensajeWhatsapp === 3 ||
        //       +value.idTipoMensajeWhatsapp === 4 ||
        //       +value.idTipoMensajeWhatsapp === 8) {
        //       this.mensajesMultimedia.push({...value, idTipoMensajeWhatsapp: +value.idTipoMensajeWhatsapp});
        //     }
        //     setTimeout(() => {
        //       this.elemento.scrollTop = this.elemento.scrollHeight;
        //     }, 50);
        //   }
        // });
      },
    });
  }

  getMensajesSocket() {
    this.elemento = document.getElementById('container');

    this.websocketsWhatsapp.getMensaje().subscribe( (msg: any) => {
      // console.log('esto es lo que emite el socket', msg);
      /******mostrar mensaje inmediato*****************/
      if (this.textoAux === msg.mensaje) {
        this.mensajes.pop();
      }
      /**************************************************/
      this.localizacion = this.dom.bypassSecurityTrustResourceUrl(`

            https://maps.google.com/maps?q=${msg.mensaje.split(',')[0]},${msg.mensaje.split(',')[1]}&z=15&output=embed
                      `);
      this.mensajes.push( {
        idTipoEnvio: msg.idTipoEnvio,
        mensaje: msg.mensaje,
        fecha: moment(msg.fecha).fromNow(),
        idTipoMensajeWhatsapp: Number(msg.idTipoMensajeWhatsapp),
        localizacion: this.localizacion,
      } );

      setTimeout(() => {
        this.elemento.scrollTop = this.elemento.scrollHeight;
      }, 50);
    });
  }

  press(e) {
    if (e.key === 'Enter') {
      this.enviar();
    }
  }
  enviar() {

    if ( this.texto.trim().length === 0 ) {
      return;
    }

    /****************************************************************************************************************/
    const mensaje = {
      'destinations': [
        {
          'correlationId': `${this.idSolicitudChat}`,
          'destination': `521${this.dataMensajesCanal.solicitudData.numeroProspecto}`,
        },
      ],
      'message': {
        // 'messageText': 'Mensaje de prueba para MO',
        'messageText': `${this.texto}`,
      },
    };
    /******mostrar mensaje inmediato*****************/
    this.elemento = document.getElementById('container');
    this.mensajes.push( {idTipoEnvio: 1, mensaje: this.texto} );
    this.textoAux = this.texto;
    this.texto = '';
    setTimeout(() => {
      this.elemento.scrollTop = this.elemento.scrollHeight;
    }, 50);
    /**************************************************/
    const jsonCredentials = {
      userName: this.dataMensajesCanal.catalogoData.userName,
      authToken: this.dataMensajesCanal.catalogoData.authToken,
    };
    this.whatsMsgService.sendMessage(mensaje, jsonCredentials).subscribe({
      next: valu => {
        const respuestaDelEjecutivo: any = valu;
        const resp = {
          dataSend: [
            {
              'correlationId': `${this.idSolicitudChat}`,
              'origin': respuestaDelEjecutivo.destinations[0].destination,
              'datos': respuestaDelEjecutivo,
              'message': {
                // 'messageText': 'Mensaje de prueba para MO, desde un ejecutivo',
                // 'messageText': `${this.texto}`,
                'messageText': `${this.textoAux}`,
                'type': 'TEXT',
              },
            },
          ],
          'idEmpleadoEnvio': sessionStorage.getItem('Empleado'),
        };
        this.whatsMsgService.postCanalWhatsapp(resp).subscribe({
          error: err => {
            this.notificacionesService.error('Ocurrio un error');
          },
          complete: () => {
            const json = {
              idSolicitud: this.idSolicitudChat,
              mensaje: this.textoAux,
            };
            // this.socketsWhatsappService.enviarMensaje( this.texto );
            // this.socketsWhatsappService.enviarMensaje( json );
            // this.texto = '';
          },
        });
      },
    });
    this.whatsMsgService.mensajeRespondido(this.dataChat).subscribe({
      next: msgRespondido => {
        // console.log(msgRespondido);
      },
      error: err => {},
      complete: () => {},
    });
    /****************************************************************************************************************/
  }

  verImagen(imagen) {
    this.dialog.open(ViewerImgWhatsComponent, {
      width: '500px',
      height: 'auto',
      data: imagen,
    });
  }

}
