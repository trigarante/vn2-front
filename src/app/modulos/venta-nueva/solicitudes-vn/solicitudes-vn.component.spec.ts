import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesVnComponent } from './solicitudes-vn.component';

describe('SolicitudesVnComponent', () => {
  let component: SolicitudesVnComponent;
  let fixture: ComponentFixture<SolicitudesVnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitudesVnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesVnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
