import {Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import { setLead, cleanLead } from 'src/app/state/actions/cotizador.actions';
import {CotizacionesAli} from '../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';
import {AgendaLlamadasService} from '../../../@core/data/services/telefonia/agendaLlamadas/agenda-llamadas.service';
import {ReasignarSolicitudComponent} from '../modals/reasignar-solicitud/reasignar-solicitud.component';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {interval, Subscription} from 'rxjs';
import {StepsCotizador} from '../../../@core/data/interfaces/venta-nueva/steps-cotizador';
import {TooltipPosition} from '@angular/material/tooltip';
import {DatosSolicitudComponent} from '../modals/datos-solicitud/datos-solicitud.component';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {FormControl} from '@angular/forms';
import {Solicitudes} from '../../../@core/data/interfaces/venta-nueva/solicitudes';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SesionUsuarioService} from '../../../@core/data/services/sesiones/sesion-usuario.service';
import {StepsCotizadorService} from '../../../@core/data/services/venta-nueva/steps-cotizador.service';
import {SolicitudesvnService} from '../../../@core/data/services/venta-nueva/solicitudes.service';
import {CotizacionesAliService} from '../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {EmpleadosService} from '../../../@core/data/services/rrhh/empleados.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {WebrtcService} from '../../../@core/data/services/telefonia/webrtc.service';
import {LlamadaSalidaComponent} from '../../telefonia/modals/llamada-salida/llamada-salida.component';
import {AgregarNumeroComponent} from '../../telefonia/modals/agregar-numero/agregar-numero.component';
import {ChatWhatsappComponent} from '../modals/chat-whatsapp/chat-whatsapp.component';
import {WhatsMsgService} from '../../../@core/data/services/whatspp/whats-msg.service';
import {PickerAseguradoraComponent} from "../modals/picker-aseguradora/picker-aseguradora.component";
import {LogOcrService} from "../../../@core/data/services/venta-nueva/log-ocr.service";

@Component({
  selector: 'app-solicitudes-vn',
  templateUrl: './solicitudes-vn.component.html',
  styleUrls: ['./solicitudes-vn.component.scss']
})
export class SolicitudesVnComponent implements OnInit, OnDestroy {
  // -----variables tabla---
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  dataSource;
  displayedColumns: string[] = ['detalle', 'id', 'nombre', 'nombreProspecto', 'numeroProspecto','departamento',
    'fechaSolicitud', 'acciones'];
  filterSelectObj = [];
  permisosS = JSON.parse(window.localStorage.getItem('User'));
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  max: any = new Date();
  errorSpin = false;
  // -----fin variables tabla---

  @ViewChild('tablaInboundPaginator', {read: MatPaginator}) tablaInboundPaginator: MatPaginator; //
  @ViewChild('btnOptions') btnOptions: ElementRef;
  @ViewChild('addStyle') addStyle: ElementRef;
  @ViewChild('addStyleMM') addStyleMM: ElementRef;
  @ViewChild('mMenu') mMenu: ElementRef;
  @ViewChild('tablaInboundSort', {read: MatSort}) tablaInboundSort: MatSort; //
  extension = localStorage.getItem('extension');
  /**Tooltip**/
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[0]);
  mensajes = new FormControl('');
  // sockets
  private stompClient = null;
  // Esta variable define el estado de las solicitudes de outbound
  tipo: string;
  tipopermiso: string;
  tituloComponent: string;
  permisos: any;
  activo: boolean;
  crearSolicitud: boolean;
  exportarExcel: boolean;
  // VALIDACIÓN ASINCRONA
  btnSendingDates = false;
  sendingDatesCounter = 0;
  // Para la optimización de vn
  // Puesto Empleado
  puestoEmpleado: number;
  mandarPlantilla = false;
  datosCanalMensajes: any;
  solicitudesPorTelefono: any;
  identificador: number;
  notificacion: number;
  auxIdSolicitud;
  socketsWhatsappService: any;
  solicitudesConMensajes: Array<number> = [];
  solicitudConMensajes;
  index;
  inde;
  aux = [];
  i;
  dataMensajesCanal: any[];
  cantidadMensajesPorCanal: any;
  cantidadNotificaciones: any;
  cantidadNotificacion: number;
  efect: number;
  private estadoCambiadoSubs: Subscription;
  estadoCambiado: Subscription;
  cambioDisponible: Subscription;
  Events: Subscription;
  ocultar: number;
  idSesion = Number(localStorage.getItem('sesion'));
  isinterno = false;
  routeListener: Subscription;
  plantillaEnviada: boolean;
  private mensajesWhats = interval(3000);
  observableWhats: Subscription;
  idPuesto = +sessionStorage.getItem('idPuesto');
  socketServiceSubscription: Subscription;
  cargando = true;
  numeros: any[];
  puedeLlamar: boolean = false;
  pjsipEvents: Subscription;
  // cotizacion funnel
  cotizacionAli: CotizacionesAli;
  peticiones: any;
  idTipoContacto;
  salva = '';

  constructor(
    private actualRoute: ActivatedRoute,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router,
    private render: Renderer2,
    private notificaciones: NotificacionesService,
    private empleadoService: EmpleadosService,
    private pjsip: WebrtcService,
    private sesionUsuarioService: SesionUsuarioService,
    private agendaService: AgendaLlamadasService,
    private stepsService: StepsCotizadorService,
    private solicitudesVnService: SolicitudesvnService,
    private cotizacionesAliService: CotizacionesAliService,
    private whatsMsgService: WhatsMsgService,
    private notificacionesService: NotificacionesService,
    private logOcrService: LogOcrService
  ) {}

  // ====================================================
  // =========== metodos tabla===========
  // ====================================================

  ngOnDestroy() {

    if (this.estadoCambiado) {
      this.estadoCambiado.unsubscribe();
    }
    if (this.cambioDisponible) {
      this.cambioDisponible.unsubscribe();
    }
    if (this.Events) {
      this.Events.unsubscribe();
    }
    if (this.pjsipEvents) {
      this.pjsipEvents.unsubscribe();
    }
  }

  // Lineas para los filtros
  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();
    this.dataSource.filter = JSON.stringify(this.filterValues);
  }

  // Lineas para los filtros
  resetFilters() {
    this.filterValues = {};
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    });
    this.dataSource.filter = '';
  }
  // ====================================================
  // ===========fin metodos  tabla===========
  // ====================================================

  getEmpleado() {
    this.empleadoService.getEmpleadoById(sessionStorage.getItem('Empleado')).subscribe(data => {
      this.puestoEmpleado = data.idPuesto;
    });
  }

  disconnect() {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
  }

  getFechas(recarga: boolean) {
    if (recarga) { this.dataSource = null; }
    this.errorSpin = false;
    this.sendingDatesCounter = 0;
    this.btnSendingDates = true;
    const fechaInicio = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaFin = (this.rangeDates[1].toISOString().split('T')[0]);

    this.solicitudesVnService.getFechas(fechaInicio, fechaFin).subscribe({
      next: valor => {
        this.solicitudesPorTelefono = valor;
        if (valor.length === 0 && this.router.url.includes('interno')) {
          this.notificaciones.informacion('Oops, no tienes solicitudes por mostrar');
        }

        this.dataSource = new MatTableDataSource(valor);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        // Lineas para los filtros
      }, error: () => {
        this.btnSendingDates = false;
        this.errorSpin = true;
        this.notificaciones.error('Error al cargar los datos');
      }, complete: () => {
        this.sendingDatesCounter ++;
        if (this.sendingDatesCounter >= 2) {
          this.btnSendingDates = false;
        }
        this.btnSendingDates = false;
      }});
  }
  ngOnInit() {
    // this.listenPjsipEvents();
    this.getEmpleado();
    this.actualRoute.url.subscribe(() => {
      this.tipo = this.actualRoute.snapshot.paramMap.get('tipo');
      this.dataSource = new MatTableDataSource([]);
      // this.connect();
      this.estadoCambiadoSubs = this.sesionUsuarioService.getestadoSesionListener()
        .subscribe(resp => {
          this.ocultar = resp;
        });
      this.sesionUsuarioService.getEstadoSesionById(localStorage.getItem('sesion')).subscribe((resp: any) => {
        this.ocultar = resp.sesion[0].idEstadoSesion;
      });
      switch (this.tipo) {
        case 'no-contactado':
          this.tituloComponent = 'NO CONTACTADOS';
          break;
        case 'contactado':
          this.tituloComponent = 'CONTACTADOS';
          break;

      }
      this.getFechas(false);
    });
    /**********************Notificaciones de mensajes de whatsapp*****************************/
    const tipo = this.actualRoute.snapshot.paramMap.get('tipo');
    const strTipo = 'no-contactado, contactado, inbound, interno';
    if (this.idPuesto === 8) {
      const idEmpleado = sessionStorage.getItem('Empleado');
    }
  }

  listenPjsipEvents() {
    if (this.extension.length > 0 && this.extension !== 'null') {
      this.pjsipEvents = this.pjsip.listenEvents().subscribe((resp) => {
        switch (resp.evento) {
          case 'connected':
          {
            setTimeout(() => {
              this.puedeLlamar = true;
            }, 5000);
            break;
          }
          case 'disconnected':
          {
            this.puedeLlamar = false;
            break;
          }
        }
      });
    }
  }

  actualizarProspecto(idProspecto, nombreProspecto, numeroProspecto) {
    const jsonAux = [{
      idProspecto,
      nombreProspecto,
      numeroProspecto,
    }];
    this.dataSource.data = this.dataSource.data.map(solicitud => {
      const found = jsonAux.find(e => e.idProspecto === solicitud.idProspecto);
      if (found) {
        solicitud = Object.assign(solicitud, found);
      }
      return solicitud;
    });
  }

  removeSolicitud(idSolicitud) {
    const index = this.dataSource.data.findIndex(dato => dato.id === idSolicitud);
    if (index >= 0) {
      const aux = this.dataSource.data;
      aux.splice(index, 1);
      this.dataSource.data = aux;
    }
  }

  updateEmpleadoSolicitud(idSolicitud, nombre, apellidoPaterno, apellidoMaterno) {
    this.dataSource.data.map(function(dato) {
      if (dato.id === idSolicitud) {
        dato.nombre = nombre;
        dato.apellidoPaterno = apellidoPaterno;
        dato.apellidoMaterno = apellidoMaterno;
      }
    });
  }

  actualizarEtiquetaSolicitud(idSolicitud, estado, etiquetaSolicitud) {
    this.dataSource.data.map(function(dato) {
      if (dato.id === idSolicitud) {
        dato.estado = estado;
        dato.etiquetaSolicitud = etiquetaSolicitud;
      }
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `0 / ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} DE ${length}`;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  filtrarPorMensajeNoLeido(correlationId) {
    if (this.dataSource.data.length !== 0) {
      const coId = String(correlationId);
      this.dataSource.filter = coId.trim().toLowerCase();
    } else {
      this.notificaciones.informacion('¡Atencion! Tabla sin datos.');
    }
  }

  verDatos(idProspecto, idProducto, idCotizacionAli) {
    this.dialog.open(DatosSolicitudComponent, {
      width: '100%',
      panelClass: 'datosSolicitud',
      data: {idProspecto, idProducto, idCotizacionAli},
    });
  }

  historicoLlamadas(idSolicitud) {
    this.router.navigate(['/modulos/telefonia/historial-llamadas/' + idSolicitud]);
  }


  reasignarSolicitud(solicitud: Solicitudes) {
    this.dialog.open(ReasignarSolicitudComponent, {
      width: '400px',
      data: {
        solicitud,
        tipo: this.tipo,
      },
    });
  }

  /**
   * Function used to get the requestId and redirect to the policy-registration microfrontend
   * @param requestId the id of the request
   */
  registerPolicy(requestId: number) {
    // sessionStorage.setItem('requestId', requestId);
    // console.log('STORAGE SOLICITUD', sessionStorage.getItem('requestId'));
    this.solicitudesVnService.validarOCR(requestId, 1).subscribe((data: any) => {
      console.log('Data: ', data);
      this.logOcrService.getById(requestId).subscribe({
        next: value => {
          if (value && data.idProducto){
            this.router.navigate([`/modulos/policy-registration-mf/stepper-policy-emision/${requestId}`]);
          } else {
            this.router.navigate([`/modulos/policy-registration-mf/autocomplete-data-form/${requestId}`]);
          }
        },
        error: err => {
          this.router.navigate([`/modulos/policy-registration-mf/stepper-policy-emision/${requestId}`]);
        }
      });
    });
  }

  generarEmision(idSolicitud) {
    this.solicitudesVnService.validarOCR(idSolicitud, 1)
      .subscribe((data: any) => {
        this.logOcrService.getById(idSolicitud).subscribe({
          next: value => {
            if (value && data.idProducto){
              this.router.navigate([`modulos/venta-nueva/step-cotizador/${idSolicitud}`]);
            } else {
              this.dialog.open(PickerAseguradoraComponent, {
                data: idSolicitud,
                width: '500px',
                height: '500px'
              });
            }
          },
          error: err => {
            this.router.navigate([`modulos/venta-nueva/step-cotizador/${idSolicitud}`]);
          }
        });
      });
  }

  generarEmisionOnline(idSolicitud) {
    this.router.navigate(['modulos/venta-nueva/step-cotizador-online/' + idSolicitud]);
  }
  generarEmisionOnlineFunnel(idSolicitud) {

    this.solicitudesVnService.getSolicitudesByIdOnline(idSolicitud).subscribe(data => {
      this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        this.peticiones = this.cotizacionAli.peticion;
        console.log(this.peticiones.aseguradora);
        switch (this.peticiones.aseguradora) {
          case 'QUALITAS':
            this.router.navigate(['modulos/module-federation/stepper-qualitas/' + idSolicitud + '/'
            + this.peticiones.aseguradora + '/' + this.peticiones.servicio]);
            break;
          case 'GNP' :
            this.router.navigate(['modulos/module-federation/stepper-gnp/' + idSolicitud  + '/'
            + this.peticiones.aseguradora + '/' + this.peticiones.servicio ]);
            break;
          case 'MAPFRE' :
            this.router.navigate(['modulos/module-federation/stepper-mapfre/' + idSolicitud  + '/'
            + this.peticiones.aseguradora + '/' + this.peticiones.servicio]);
            break;
          case 'ABA' :
            this.router.navigate(['modulos/module-federation/stepper-aba/' + idSolicitud  + '/'
              + this.peticiones.aseguradora + '/' + this.peticiones.servicio]);
            break;
          case 'BANORTE' :
            this.router.navigate(['modulos/module-federation/stepper-banorte/' + idSolicitud  + '/'
            + this.peticiones.aseguradora + '/' + this.peticiones.servicio]);
            break;
          case 'AFIRME' :
            this.router.navigate(['modulos/module-federation/stepper-afirme/' + idSolicitud  + '/'
            + this.peticiones.aseguradora + '/' + this.peticiones.servicio]);
            break;
          default:

            break;
        }

      });
    }); // fin de solicitud

  }

  //
  // llamar(idSolicitud, numero) {
  //   this.snackBar.openFromComponent(LlamadaSalidaComponent, {
  //     data: {
  //       idSolicitud,
  //       numero,
  //     }
  //   });
  // }


  llamar(idSolicitud, numero){
    this.puedeLlamar = true;
    this.solicitudesVnService.llamadaUcontact(idSolicitud, numero).subscribe();
    this.notificaciones.exito('SE A REALIZADO LA LLAMADA EN UCONTACT');
    setTimeout(() => {
      this.puedeLlamar = false;
    }, 1000 * 30);
  }

// función para mandar mensajes una vez hayan respondido la plantilla
  mensajeNormal(data) {
    const mensaje = {
      destinations: [
        {
          correlationId: `${data.id}`,
          destination: `521${data.numeroProspecto}`,
        },
      ],
      message: {
        messageText: 'Mensaje de prueba para MO',
      },
    };
  }
  enviarSMS() {
    const json = {
      destination: 5217711908401,
      correlationId: '152364',
      messageText: 'HOLA TU!!',
    };
  }

  enviarWha(data) {
    if (!data.notificacion || data.notificacion === 1) {
      this.dataSource.data.map((dataMap, index) => {
        if (String(dataMap.id) === String(data.id)) {
          this.dataSource.data[index].notificacion = 0;
        }
      });
    }
    const indice = this.solicitudesConMensajes.indexOf(data.id);
    this.solicitudesConMensajes.splice(indice, 1);
  }

  mostrarChat(data) {
    this.notificacionesService.carga('Abriendo chat');
    this.whatsMsgService.getMensajesByIdCanal(data.id, data.numeroProspecto)
      .subscribe({
        next: (value: any) => {
          if (value.cuerpo === null) {
            this.plantillaEnviada = false;
            this.notificacionesService.informacion('No se puede mostrar el chat debido a que no se ha enviado una plantilla a este lead');
          } else {
            if (value.cuerpo !== null) {
              this.notificacionesService.exitoWhats();
              this.datosCanalMensajes = value;
              const credentials = value.catalogoData;
              this.plantillaEnviada = true;
              this.modalChat(data, credentials, value);
            }
          }
        },
        error: err => {
          this.notificacionesService.error('Ocurrió un error');
        }
      });
  }

  modalChat(data, credentials, dataCanal) {
    sessionStorage.setItem('idSolicitud', String(data.id));
    this.dialog.open(ChatWhatsappComponent, {
      width: '550px',
      height: '450px',
      data: {
        data,
        credentials,
        dataCanal
      }
    })
  }

  clickBtn() {
    if (this.dataSource.length !== 0) {
      if (this.cantidadNotificacion !== 0) {
        const style = this.addStyle.nativeElement;
        this.render.setAttribute(this.addStyle.nativeElement, 'style', 'width: 280px !important;');
        this.render.setAttribute(this.mMenu.nativeElement, 'style', 'max-height: 235px !important; overflow-x: hidden;');
        document.getElementsByClassName('mat-menu-content')[0]
          .setAttribute('style', 'width: 280px; overflow: hidden;');
      } else {
        this.notificaciones.advertencia('No tienes mensajes por responder');
      }
    } else {
      this.notificaciones.advertencia('No hay informacion en la tabla de solicitudes.');
    }
  }
  iconoMatMenu(id) {
    const data = this.solicitudesPorTelefono.filter(val => {
      return val.id === id;
    });
  }

  agregarNumero(idSolicitud) {
    this.dialog.open(AgregarNumeroComponent, {
      data: {
        idSolicitud
      }
    });
  }

  getNumeros(idSolicitud, numeroOriginal) {
    this.cargando = true;
    this.numeros = [];
    this.agendaService.getNumerosByIdSolicitud(idSolicitud).subscribe((resp) => {
      resp.forEach((telefono) => {
        this.numeros.push(telefono.numero);
      });
      this.numeros.unshift(numeroOriginal);
    }, () => {}, () => {
      this.cargando = false;
    });
  }

  recotizar(idProspecto, idTipoc, idSolicitudDetalles, nombreCliente: string) {
    sessionStorage.setItem('idSolicitud', idSolicitudDetalles);
    sessionStorage.setItem('idProspecto', idProspecto);
    sessionStorage.setItem('nombreCliente', nombreCliente);
    console.log('STORAGE SOLICITUD', sessionStorage.getItem('idSolicitud'));
    console.log('STORAGE PROSPECTO', sessionStorage.getItem('idProspecto'));
    let infoCliente: StepsCotizador;
    this.stepsService.getByIdOnline(idSolicitudDetalles).subscribe(
      data => {
        infoCliente = data[0];
      },
      () => {

      },
      () => {
        // si ya hay un registro mostrara un modal donde no te permitira continuar
        if (infoCliente && infoCliente.idRegistro) {
          this.swalError();
        }else {
          // si no hay registro entrara a una ventana donde se cotizara por compaña
          this.router.navigate([`/modulos/module-federation`]);
        }
      });

    // this.ref.close();

  }

  swalError() {
    this.notificaciones.advertencia('No se puede Recotizar, Ya se creo la emision').then((result) => {
      if (result.value) {
        window.close();
      }
    });
  }
}
