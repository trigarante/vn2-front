import {Component,  OnInit,  ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatStepper} from '@angular/material/stepper';
import {SolicitudesvnService} from '../../../@core/data/services/venta-nueva/solicitudes.service';
import {LogOcrService} from "../../../@core/data/services/venta-nueva/log-ocr.service";

@Component({
  selector: 'app-step-cotizador',
  templateUrl: './step-cotizador.component.html',
  styleUrls: ['./step-cotizador.component.scss']
})
export class StepCotizadorComponent implements OnInit {
  @ViewChild('stepper')  stepper: MatStepper;
  idSolicitud: number = parseFloat(this.route.snapshot.paramMap.get('id'));
  dataPoliza;

  dataCliente;
  dataDomicilio;
  dataRepuve;
  socio;
  solicitud;
  step = [
    {idCliente: null},
    {idProducto: null},
    {idRegistro: null},
    {idRecibo: null},
    {idPago: null},
    {idInspeccion: null},
  ];
  constructor(private route: ActivatedRoute,
              private router: Router,
              private solicitudesService: SolicitudesvnService,
              private logOcrService: LogOcrService) {
    this.logOcrService.getById(this.idSolicitud).subscribe({
      next: value => {
        if (value){
          this.dataCliente = value.datos.dataCliente;
          this.dataPoliza = value.datos.dataPoliza;
          this.dataDomicilio = value.datos.dataDomicilio;
          this.dataRepuve = value.datos.dataRepuve;
          this.socio = value.idSocio;
        } else {
          this.dataCliente = 0;
          this.dataPoliza = 0;
          this.dataDomicilio = 0;
          this.dataRepuve = 0;
          this.socio = 0;
        }
        console.log(this.dataPoliza);
        console.log(this.dataCliente);
        console.log(this.dataDomicilio);
        console.log(this.dataRepuve);
      },
      error: err => {
        this.dataCliente = 0;
        this.dataPoliza = 0;
        this.dataDomicilio = 0;
        this.dataRepuve = 0;
        this.socio = 0;
      }
    });
  }

  ngOnInit() {
    this.getSolicitud();
    console.log(this.idSolicitud);
    console.log(this.dataPoliza);
  }

  selectionChange(event) {
    const aux: any = Object.keys(this.step[event.selectedIndex]);
    this.step[event.selectedIndex][aux] = (this.solicitud[aux] != null) ? this.solicitud[aux] : null;
  }

  nextSteper(num: number) {
    this.stepper.selectedIndex += num;
  }

  emisionHijo(value){
    /* flujo normal
    {numero: 0, id: 'idCliente', idCliente: result.idCliente, numAux: 1, idAux: 'idProducto'}
    this.stepper.nex();

    edicion
    {numero: 0, id: 'idCliente', idCliente: result.idCliente}

    recibos pasa a inspeccion
    {numero: 0, id: 'idCliente', idCliente: result.idCliente, numAux: 1, idAux: 'idProducto', avanzar: 2}
     */
    this.step[value.numero][value.id] = value[value.id];
    this.solicitud[value.id] = value[value.id];
    // Activar el siguiente step
    if (value.numAux) {
      this.step[value.numAux][value.idAux] = 0;
      this.solicitud[value.idAux] = 0;
    } else if (value.esRegistro) {
      this.step[value.numero++]['idRecibo'] = value[value.id];
      this.solicitud.idRecibo = value[value.id];
    }
    this.nextSteper(value.avanzar ? value.avanzar : 1);
  }

  getSolicitud() {
    this.solicitudesService.stepsById(this.idSolicitud).subscribe({
      next: data => {
      this.solicitud = data;
    },
    complete: () => {
      if (this.solicitud.idPago) {
        this.stepper.selectedIndex = 5;
        this.step[5]['idInspeccion'] = 0;
        this.solicitud.idInspeccion = 0;
      } else if (this.solicitud.idRecibo) {
        this.step[4]['idPago'] = 0;
        this.solicitud.idPago = 0;
        this.stepper.selectedIndex = 4;
      } else if (this.solicitud.idRegistro && !this.solicitud.idRecibo) {
        this.stepper.selectedIndex = 2;
      } else if (this.solicitud.idRegistro) {
        this.stepper.selectedIndex = 2;
      } else if (this.solicitud.idProducto) {
        this.stepper.selectedIndex = 2;
        this.step[2]['idRegistro'] = 0;
        this.solicitud.idRegistro = 0;
      } else {
        this.step[0]['idCliente'] = 0;
        this.solicitud.idCliente = 0;
      }
    }});
  }
}
