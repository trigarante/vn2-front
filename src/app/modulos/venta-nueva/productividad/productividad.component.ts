import {Component, OnInit, ViewChild} from '@angular/core';
import {Usuario} from '../../../@core/data/interfaces/ti/usuario';
import {Empleados} from '../../../@core/data/interfaces/rrhh/empleados';
import {TipoContactoVn} from '../../../@core/data/interfaces/catalogos/tipo-contacto-vn';
import {MatAccordion} from '@angular/material/expansion';
import {ProductividadEjecutivo} from '../../../@core/data/interfaces/reportes/productividad-ejecutivo';
import {ProductividadUrl} from '../../../@core/data/interfaces/reportes/productividad-url';
import {TipoSubarea} from '../../../@core/data/interfaces/marketing/catalogos/tipoSubarea';
import {ProductividadDistribuciones} from '../../../@core/data/interfaces/reportes/productividad-distribuciones';
import {MatTableDataSource} from '@angular/material/table';
import Swal from 'sweetalert2';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {EmpleadosService} from '../../../@core/data/services/rrhh/empleados.service';
import {UsuarioService} from '../../../@core/data/services/ti/usuario.service';
import {ProductividadUrlService} from '../../../@core/data/services/reportes/productividad-url.service';
import {ProductividadEjecutivoService} from '../../../@core/data/services/reportes/productividad-ejecutivo.service';
import {
  ProductividadDistribucionesService
} from '../../../@core/data/services/reportes/productividad-distribuciones.service';
import {TipoSubareaService} from '../../../@core/data/services/marketing/catalogos/tipo-subarea.service';
import {TipoContactoVnService} from '../../../@core/data/services/catalogos/tipo-contacto-vn.service';
import {SedeService} from '../../../@core/data/services/catalogos/sede.service';
import * as moment from 'moment';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {DashBoardNewService} from "../../../@core/data/services/reportes/dash-board-new.service";
@Component({
  selector: 'app-productividad',
  templateUrl: './productividad.component.html',
  styleUrls: ['./productividad.component.scss']
})
export class ProductividadComponent implements OnInit {
  @ViewChild('myaccordion') myPanels: MatAccordion;
  @ViewChild('paginador', {read: MatPaginator}) paginador: MatPaginator;
  @ViewChild('paginadorUrls', {read: MatPaginator}) paginadorUrls: MatPaginator;
  @ViewChild('paginadorDist', {read: MatPaginator}) paginadorDist: MatPaginator;
  @ViewChild('sort', {read: MatSort}) sort: MatSort;
  @ViewChild('sortUrls', {read: MatSort}) sortUrls: MatSort;
  @ViewChild('sortDist', {read: MatSort}) sortDist: MatSort;
  displayedColumns: string[] = ['tipoSubarea', 'nombreEjecutivo', 'antiguedad', 'conversion', 'contactos',
    'polizasEmitidas', 'primaNetaEmitida', 'polizasCobradas', 'primaNetaCobrada', 'polizasAplicadas', 'primaNetaAplicada'];
  displayedColumnsUrl: string[] = ['tipoSubarea', 'urlCompleta', 'conversion', 'contactos',
    'polizasEmitidas', 'primaNetaEmitida', 'polizasCobradas', 'primaNetaCobrada', 'polizasAplicadas', 'primaNetaAplicada'];
  displayedColumnsDistribucion: string[] = ['nombreComercial', 'primaNetaCobrada', 'porcentajePrima',
    'polizasCobradas', 'porcentajePolizas'];
  rangeDates: Date[] = [
    new Date(new Date().setDate(new Date().getDate() - 1)),
    new Date(),
  ];
  permisos = JSON.parse(window.localStorage.getItem('User'));
  dataSource: any;
  dataSourceUrls: any;
  dataSourceDistribucion: any;
  dates: any ;
  tipoSubareas: TipoSubarea[];
  tipoSubarea: TipoSubarea;
  idDepartamento = 0;
  idSede = 0;
  idTipoContacto = 0;
  idArea: number;
  productividad: ProductividadEjecutivo[];
  productividadUrl: ProductividadUrl[];
  productividadDistribucion: ProductividadDistribuciones[];
  tipoContactos: TipoContactoVn[] = [{id: 0, descripcion: 'TODOS', activo: 1}, {id: 1, descripcion: 'OUTBOUND', activo: 1},
    {id: 3, descripcion: 'INTERNO', activo: 1}, {id: 6, descripcion: 'ONLINE', activo: 1}];
  tipoContacto: TipoContactoVn;
  // VARIABLES PRIMA //
  primaNetaAplicada: number;
  primaNetaCobrada: number;
  primaNetaEmitida: number;
  polizasEmitidas: number;
  polizasCobradas: number;
  polizasAplicadas: number;
  totalContactos: number;
  conversiones: number;
  conversion: number;
  primaNetaAplicadaUrl: number;
  primaNetaCobradaUrl: number;
  primaNetaEmitidaUrl: number;
  polizasEmitidasUrl: number;
  polizasCobradasUrl: number;
  polizasAplicadasUrl: number;
  totalContactosUrl: number;
  primaNetaAplicadaDist: number;
  primaNetaCobradaDist: number;
  primaNetaEmitidaDist: number;
  polizasEmitidasDist: number;
  polizasCobradasDist: number;
  polizasAplicadasDist: number;
  totalContactosDist: number;
  conversionesUrl: number;
  conversionUrl: number;
  fechaCompleta1: any;
  fechaCompleta2: any;
  // ESTA VARIABLE HACE LA INTERACCIÓN DEACUERDO AL ESTADO DE LA TABLA,
  // ES DECIR SI ESTA ABIERTA O CERRADA
  panelExpand = false;
  panelExpand2 = false;
  panelExpand3 = false;
  idEmpleado: string = window.sessionStorage.getItem('Empleado');
  usuario: Usuario;
  empleado: Empleados;
  idUsuario: string = window.sessionStorage.getItem('Usuario');
  btnSendingDates = false;
  btnSendingDates2 = false;
  sendingDatesCounter = 0;
  verTodo: any;
  supervisor: any;
  grupo: any;

  constructor(private ejecutivoService: ProductividadEjecutivoService,
              private productividadUrlService: ProductividadUrlService,
              private sedeService: SedeService,
              private tipoSubareaService: TipoSubareaService,
              private tipoContactoVn: TipoContactoVnService,
              private usuariosService: UsuarioService,
              private dashBoardNewService: DashBoardNewService,
              private productividadDistribucionesService: ProductividadDistribucionesService,
              private notificaciones: NotificacionesService) {
    this.fechaCompleta1 = moment(this.rangeDates[0]).valueOf();
    this.fechaCompleta2 = moment(this.rangeDates[1]).valueOf();
  }

  ngOnInit(): void {
    this.usuariosService.getUsuarioById(this.idUsuario).subscribe({
      next: data => {
        if (data === []) {
          this.getData();
        } else {
          this.tipoSubareas = [{idDepartamento: 0, descripcion: 'TODAS'}];
          for(let i = 0; i < data.length; i++) {
            this.tipoSubareas.push(data[i]);
            console.log(this.tipoSubareas);
          }
          this.idDepartamento = this.tipoSubareas[1].idDepartamento;
          this.getData();
        }
      },
    });
  }
  getData() {
    const fechaInicio = (moment(this.rangeDates[0]).format('YYYY-MM-DD'));
    const fechaFin = (moment(this.rangeDates[1]).format('YYYY-MM-DD'));
    this.primaNetaAplicada = undefined;
    this.primaNetaCobrada = undefined;
    this.primaNetaEmitida = undefined;
    this.polizasEmitidas = undefined;
    this.polizasCobradas = undefined;
    this.polizasAplicadas = undefined;
    this.totalContactos = undefined;
    this.conversiones = undefined;
    this.conversion = undefined;
    this.primaNetaAplicadaUrl = undefined;
    this.primaNetaCobradaUrl = undefined;
    this.primaNetaEmitidaUrl = undefined;
    this.polizasEmitidasUrl = undefined;
    this.polizasCobradasUrl = undefined;
    this.polizasAplicadasUrl = undefined;
    this.totalContactosUrl = undefined;
    this.conversionesUrl = undefined;
    this.conversionUrl = undefined;
    this.ejecutivoService.getProductividadEjecutivo(this.idDepartamento, this.idTipoContacto, fechaInicio,
      fechaFin).subscribe((res: any) => {
      this.productividad = res;
      this.dataSource = new MatTableDataSource(this.productividad);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginador;
      this.totalContactos = this.productividad.
      map(t => t.contactos).reduce((acc, value) => acc + value, 0);
      this.polizasEmitidas = this.productividad.
      map(t => t.polizasEmitidas).reduce((acc, value) => acc + value, 0);
      this.primaNetaEmitida = this.productividad.
      map(t => t.primaNetaEmitida).reduce((acc, value) => acc + value, 0);
      this.polizasCobradas = this.productividad.
      map(t => t.polizasCobradas).reduce((acc, value) => acc + value, 0);
      this.primaNetaCobrada = this.productividad.
      map(t => t.primaNetaCobrada).reduce((acc, value) => acc + value, 0);
      this.polizasAplicadas = this.productividad.
      map(t => t.polizasAplicadas).reduce((acc, value) => acc + value, 0);
      this.primaNetaAplicada = this.productividad.
      map(t => t.primaNetaAplicada).reduce((acc, value) => acc + value, 0);
      this.totalContactos = this.productividad.
      map(t => t.contactos).reduce((acc, value) => acc + value, 0);
      if (this.totalContactos === 0) {
        this.conversiones = 0;
      } else {
        this.conversiones = this.polizasCobradas / this.totalContactos;
      }
    });

    // this.productividadDistribucionesService.getProductividadDistribuciones(this.idDepartamento, this.idTipoContacto, this.fechaCompleta1,
    //   this.fechaCompleta2).subscribe((res: any) => {
    //   this.productividadDistribucion = res;
    //   this.dataSourceDistribucion = new MatTableDataSource(this.productividadDistribucion);
    //   this.dataSourceDistribucion.sort = this.sortDist;
    //   this.dataSourceDistribucion.paginator = this.paginadorDist;
    //   this.polizasEmitidasDist = this.productividadDistribucion.
    //   map(t => t.polizasEmitidas).reduce((acc, value) => acc + value, 0);
    //   this.primaNetaEmitidaDist = this.productividadDistribucion.
    //   map(t => t.primaNetaEmitida).reduce((acc, value) => acc + value, 0);
    //   this.polizasCobradasDist = this.productividadDistribucion.
    //   map(t => t.polizasCobradas).reduce((acc, value) => acc + value, 0);
    //   this.primaNetaCobradaDist = this.productividadDistribucion.
    //   map(t => t.primaNetaCobrada).reduce((acc, value) => acc + value, 0);
    //   this.polizasAplicadasDist = this.productividadDistribucion.
    //   map(t => t.polizasAplicadas).reduce((acc, value) => acc + value, 0);
    //   this.primaNetaAplicadaDist = this.productividadDistribucion.
    //   map(t => t.primaNetaAplicada).reduce((acc, value) => acc + value, 0);
    // });
  }
  getFechas(rangeData: Date[]) {
    const fechaInicio = (moment(this.rangeDates[0]).format('YYYY-MM-DD'));
    const fechaFin = (moment(this.rangeDates[1]).format('YYYY-MM-DD'));
    this.sendingDatesCounter = 0;
    this.btnSendingDates = true;
    this.primaNetaAplicada = undefined;
    this.primaNetaCobrada = undefined;
    this.primaNetaEmitida = undefined;
    this.polizasEmitidas = undefined;
    this.polizasCobradas = undefined;
    this.polizasAplicadas = undefined;
    this.totalContactos = undefined;
    this.conversiones = undefined;
    this.conversion = undefined;
    this.primaNetaAplicadaUrl = undefined;
    this.primaNetaCobradaUrl = undefined;
    this.primaNetaEmitidaUrl = undefined;
    this.polizasEmitidasUrl = undefined;
    this.polizasCobradasUrl = undefined;
    this.polizasAplicadasUrl = undefined;
    this.totalContactosUrl = undefined;
    this.conversionesUrl = undefined;
    this.conversionUrl = undefined;
    this.fechaCompleta1 = moment(rangeData[0]).valueOf();
    this.fechaCompleta2 = moment(rangeData[1]).valueOf() + 86399;
    this.ejecutivoService.getProductividadEjecutivo(this.idDepartamento, this.idTipoContacto, fechaInicio,
      fechaFin).subscribe((res: any) => {
        this.productividad = res;
        this.dataSource = new MatTableDataSource(this.productividad);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginador;
        this.totalContactos = this.productividad.map(t => t.contactos).reduce((acc, value) => acc + value, 0);
        this.polizasEmitidas = this.productividad.map(t => t.polizasEmitidas).reduce((acc, value) => acc + value, 0);
        this.primaNetaEmitida = this.productividad.map(t => t.primaNetaEmitida).reduce((acc, value) => acc + value, 0);
        this.polizasCobradas = this.productividad.map(t => t.polizasCobradas).reduce((acc, value) => acc + value, 0);
        this.primaNetaCobrada = this.productividad.map(t => t.primaNetaCobrada).reduce((acc, value) => acc + value, 0);
        this.polizasAplicadas = this.productividad.map(t => t.polizasAplicadas).reduce((acc, value) => acc + value, 0);
        this.primaNetaAplicada = this.productividad.map(t => t.primaNetaAplicada).reduce((acc, value) => acc + value, 0);
        if (this.totalContactos === 0) {
          this.conversiones = 0;
        } else {
          this.conversiones = this.polizasCobradas / this.totalContactos;
        }
      }, () => {
        this.btnSendingDates = false;
      }, () => {
        this.sendingDatesCounter ++;
        if (this.sendingDatesCounter >= 2) {
          this.btnSendingDates = false;
        }
      },
    );

    // this.productividadDistribucionesService.getProductividadDistribuciones(this.idDepartamento, this.idTipoContacto, this.fechaCompleta1,
    //   this.fechaCompleta2).subscribe((res: any) => {
    //     this.productividadDistribucion = res;
    //     this.dataSourceDistribucion = new MatTableDataSource(this.productividadDistribucion);
    //     this.dataSourceDistribucion.sort = this.sortDist;
    //     this.dataSourceDistribucion.paginator = this.paginadorDist;
    //     this.polizasEmitidasDist = this.productividadDistribucion.
    //     map(t => t.polizasEmitidas).reduce((acc, value) => acc + value, 0);
    //     this.primaNetaEmitidaDist = this.productividadDistribucion.
    //     map(t => t.primaNetaEmitida).reduce((acc, value) => acc + value, 0);
    //     this.polizasCobradasDist = this.productividadDistribucion.
    //     map(t => t.polizasCobradas).reduce((acc, value) => acc + value, 0);
    //     this.primaNetaCobradaDist = this.productividadDistribucion.
    //     map(t => t.primaNetaCobrada).reduce((acc, value) => acc + value, 0);
    //     this.polizasAplicadasDist = this.productividadDistribucion.
    //     map(t => t.polizasAplicadas).reduce((acc, value) => acc + value, 0);
    //     this.primaNetaAplicadaDist = this.productividadDistribucion.
    //     map(t => t.primaNetaAplicada).reduce((acc, value) => acc + value, 0);
    //   }, () => {
    //     this.btnSendingDates2 = false;
    //     this.notificaciones.error('Algo salió mal', 'Error');
    //   }, () => {
    //     this.sendingDatesCounter ++;
    //     if (this.sendingDatesCounter >= 2) {
    //       this.btnSendingDates2 = false;
    //     }
    //   },
    // );
  }
  findAll() {
    const fechaInicio = (moment(this.rangeDates[0]).format('YYYY-MM-DD'));
    const fechaFin = (moment(this.rangeDates[1]).format('YYYY-MM-DD'));
    this.primaNetaAplicada = undefined;
    this.primaNetaCobrada = undefined;
    this.primaNetaEmitida = undefined;
    this.polizasEmitidas = undefined;
    this.polizasCobradas = undefined;
    this.polizasAplicadas = undefined;
    this.totalContactos = undefined;
    this.conversiones = undefined;
    this.conversion = undefined;
    this.primaNetaAplicadaUrl = undefined;
    this.primaNetaCobradaUrl = undefined;
    this.primaNetaEmitidaUrl = undefined;
    this.polizasEmitidasUrl = undefined;
    this.polizasCobradasUrl = undefined;
    this.polizasAplicadasUrl = undefined;
    this.totalContactosUrl = undefined;
    this.conversionesUrl = undefined;
    this.conversionUrl = undefined;
    this.ejecutivoService.getProductividadEjecutivo(this.idDepartamento, this.idTipoContacto, fechaInicio,
      fechaFin)
      .subscribe((res: any) => {
        this.productividad = res;
        this.dataSource = new MatTableDataSource(this.productividad);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginador;
        this.totalContactos = this.productividad.
        map(t => t.contactos).reduce((acc, value) => acc + value, 0);
        this.polizasEmitidas = this.productividad.
        map(t => t.polizasEmitidas).reduce((acc, value) => acc + value, 0);
        this.primaNetaEmitida = this.productividad.
        map(t => t.primaNetaEmitida).reduce((acc, value) => acc + value, 0);
        this.polizasCobradas = this.productividad.
        map(t => t.polizasCobradas).reduce((acc, value) => acc + value, 0);
        if (this.totalContactos === 0) {
          this.conversiones = 0;
        } else {
          this.conversiones = this.polizasCobradas / this.totalContactos;
        }
        this.primaNetaCobrada = this.productividad.
        map(t => t.primaNetaCobrada).reduce((acc, value) => acc + value, 0);
        this.polizasAplicadas = this.productividad.
        map(t => t.polizasAplicadas).reduce((acc, value) => acc + value, 0);
        this.primaNetaAplicada = this.productividad.
        map(t => t.primaNetaAplicada).reduce((acc, value) => acc + value, 0);
      });

    // this.productividadDistribucionesService.getProductividadDistribuciones(this.idDepartamento, this.idTipoContacto, this.fechaCompleta1,
    //   this.fechaCompleta2).subscribe((res: any) => {
    //   this.productividadDistribucion = res;
    //   this.dataSourceDistribucion = new MatTableDataSource(this.productividadDistribucion);
    //   this.dataSourceDistribucion.sort = this.sortDist;
    //   this.dataSourceDistribucion.paginator = this.paginadorDist;
    //   this.polizasEmitidasDist = this.productividadDistribucion.
    //   map(t => t.polizasEmitidas).reduce((acc, value) => acc + value, 0);
    //   this.primaNetaEmitidaDist = this.productividadDistribucion.
    //   map(t => t.primaNetaEmitida).reduce((acc, value) => acc + value, 0);
    //   this.polizasCobradasDist = this.productividadDistribucion.
    //   map(t => t.polizasCobradas).reduce((acc, value) => acc + value, 0);
    //   this.primaNetaCobradaDist = this.productividadDistribucion.
    //   map(t => t.primaNetaCobrada).reduce((acc, value) => acc + value, 0);
    //   this.polizasAplicadasDist = this.productividadDistribucion.
    //   map(t => t.polizasAplicadas).reduce((acc, value) => acc + value, 0);
    //   this.primaNetaAplicadaDist = this.productividadDistribucion.
    //   map(t => t.primaNetaAplicada).reduce((acc, value) => acc + value, 0);
    // });
  }
  openHelpModal() {
    Swal.fire({
      title: 'INTERACCIÓN CON LAS TABLAS',
      text: 'Existen 2 funciones las cuales te ayudaran a interactuar correctamente con las tablas',
      allowEscapeKey: false,
      allowOutsideClick: false,
      html: '<section style="text-align: left; font-family: Helvetica; margin-left: 50px;">' +
        '<p>Para abrir o cerrar las tablas puedes dar click en el botón que dice desplegar tablas o cerrar tablas, ' +
        'ó dando click en el título de cada tabla' +
        '</p>' +
        '<br />' +
        '<p>Para visualizar completa la tabla encontrarás bajo la tabla una barra para desplazarte horizontalmente' +
        ' y asi podrás ver la tabla completa' +
        '</p>' +
        '</section>',
      icon: 'info',
    });
  }
  applyFilterDist(filterValue: string) {
    this.dataSourceDistribucion.filter = filterValue.trim().toLowerCase();
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
