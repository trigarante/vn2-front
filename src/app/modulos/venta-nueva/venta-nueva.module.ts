import {NgModule } from '@angular/core';
import {CommonModule, CurrencyPipe} from '@angular/common';
import {VentaNuevaRoutingModule } from './venta-nueva-routing.module';
import {VentaNuevaComponent } from './venta-nueva.component';
import {DatosProductoComponent } from './componentes-steps/datos-producto/datos-producto.component';
import {RegistroPolizaComponent } from './componentes-steps/registro-poliza/registro-poliza.component';
import {StepCotizadorComponent } from './step-cotizador/step-cotizador.component';
import {InspeccionesComponent } from './componentes-steps/inspecciones/inspecciones.component';
import {PagosComponent } from './componentes-steps/pagos/pagos.component';
import {RecibosComponent } from './componentes-steps/recibos/recibos.component';
import {CreateEmisionComponent} from './componentes-steps/create-emision/create-emision.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SolicitudesVnInternaComponent } from './solicitudes-vn-interna/solicitudes-vn-interna.component';
import {MaterialModule} from '../material.module';
import { DatosSolicitudComponent } from './modals/datos-solicitud/datos-solicitud.component';
import { ReasignarSolicitudComponent } from './modals/reasignar-solicitud/reasignar-solicitud.component';
import { ProspectoCreateComponent } from './modals/prospecto-create/prospecto-create.component';
import { CotizadorComponent } from './cotizador/cotizador.component';
import {TelefoniaModule} from '../telefonia/telefonia.module';
import {SolicitudesComponent} from './solicitudes/solicitudes.component';
import { AdministracionPolizasComponent } from './administracion-polizas/administracion-polizas.component';
import { TablaCascaronComponent } from './tabla-cascaron/tabla-cascaron.component';
import { AutorizacionesProcesoComponent } from './autorizacion-errores/autorizaciones-proceso/autorizaciones-proceso.component';
import { MostrarDocumentosAutorizacionComponent } from './autorizacion-errores/mostrar-documentos-autorizacion/mostrar-documentos-autorizacion.component';

import { ViewerComponent } from './viewer/viewer.component';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import { CorreccionErroresDatosComponent } from './correccion-errores/correccion-errores-datos/correccion-errores-datos.component';
import { CorreccionErroresDocumentosComponent } from './correccion-errores/correccion-errores-documentos/correccion-errores-documentos.component';
import { DocumentosCorreccionComponent } from './correccion-errores/documentos-correccion/documentos-correccion.component';
import { DatosClienteCorreccionComponent } from './correccion-errores/componentes/datos-cliente-correccion/datos-cliente-correccion.component';
import { DatosPagoCorreccionComponent } from './correccion-errores/componentes/datos-pago-correccion/datos-pago-correccion.component';
import { DatosRegistroCorreccionComponent } from './correccion-errores/componentes/datos-registro-correccion/datos-registro-correccion.component';
import { ProductoClienteCorreccionComponent } from './correccion-errores/componentes/producto-cliente-correccion/producto-cliente-correccion.component';
import { ViewerCorreccionComponent } from './correccion-errores/componentes/viewer-correccion/viewer-correccion.component';
import { DetallesPolizaComponent } from './detalles-poliza/detalles-poliza.component';
import { EndososComponent } from './endosos/endosos.component';
import { PendientesVerificacionComponent } from './pendientes-verificacion/pendientes-verificacion.component';
import {SolicitudEndosoCreateComponent} from './modals/solicitud-endoso-create/solicitud-endoso-create.component';
import {SubirArchivoClienteComponent} from './modals/subir-archivo-cliente/subir-archivo-cliente.component';
import { AdministradorRecibosComponent } from './componentes-steps/administrador-recibos/administrador-recibos.component';
import { SolicitudesVnComponent } from './solicitudes-vn/solicitudes-vn.component';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatBadgeModule} from "@angular/material/badge";
import { ChatWhatsappComponent } from './modals/chat-whatsapp/chat-whatsapp.component';
import { WhatsPendientesComponent } from './whats-pendientes/whats-pendientes.component';
import { ViewerImgWhatsComponent } from './modals/viewer-img-whats/viewer-img-whats.component';
import { LogAseguradorasComponent } from './vn-online/log-aseguradoras/log-aseguradoras.component';
import { ModalCotizacionComponent } from './vn-online/modal/modal-cotizacion/modal-cotizacion.component';
import {MostrarCotizacionComponent} from './vn-online/mostrar-cotizacion/mostrar-cotizacion.component';
import {SharedModule} from "../../shared/shared.module";
import { NewStepOnlineComponent } from './vn-online/new-step-online/new-step-online.component';
import { CreateEmisionOnlineNewComponent } from './vn-online/componentes-step-online/create-emision-online-new/create-emision-online-new.component';
import {MatStepperModule} from "@angular/material/stepper";
import { DatosProductosOnlineNewComponent } from './vn-online/componentes-step-online/datos-productos-online-new/datos-productos-online-new.component';
import { PagosOnlineNewComponent } from './vn-online/componentes-step-online/pagos-online-new/pagos-online-new.component';
import {MatDialogModule} from "@angular/material/dialog";

import { InspeccionesOnlineNewComponent } from './vn-online/componentes-step-online/inspecciones-online-new/inspecciones-online-new.component';
import { ModalViewPdfPolizaFunnel } from '../../shared/componentes/modal-view-pdf-poliza-funnel/modal-view-pdf-poliza-funnel.component';
import {getSpanishPaginatorIntl} from "../../traduccion-paginator";
import {MatPaginatorIntl} from "@angular/material/paginator";
import {STEPPER_GLOBAL_OPTIONS} from "@angular/cdk/stepper";
import { RecotizadorTabsComponent } from './vn-online/recotizador-tabs/recotizador-tabs.component';
import {RegistroPolizaOnlineNewComponent} from './vn-online/componentes-step-online/registro-poliza-online-new/registro-poliza-online-new.component';
import { EmitirOnlineNewComponent } from './vn-online/componentes-step-online/emitir-online-new/emitir-online-new.component';
import { ActivacionEmpleadoComponent } from './desactivacion-ejecutivos/activacion-empleado/activacion-empleado.component';
import { ProductividadComponent } from './productividad/productividad.component';
import { SocitudesActivacionComponent } from './desactivacion-ejecutivos/socitudes-activacion/socitudes-activacion.component';
import { MiEstadoCuentaComponent } from './mi-estado-cuenta/mi-estado-cuenta.component';
import { AgregarAgenteComponent } from './vn-online/modal/agregar-agente/agregar-agente.component';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import { ModificarClienteComponent } from './modals/modificar-cliente/modificar-cliente.component';
import { EsquemaComisionComponent } from './esquema-comision/esquema-comision.component';
import { RechazoComponent } from './esquema-comision/modals/rechazo/rechazo.component';
import { VisualizarPolizaComponent } from './vn-online/modal/visualizar-poliza/visualizar-poliza.component';
import {HomeComponent} from "./modals/curp/home.component";
import { NgxMaskModule } from 'ngx-mask';
import { NewStepOnlineMapfreComponent } from './vn-online/vn-online-mapfre/new-step-online-mapfre/new-step-online-mapfre.component';
import { CreateEmisionOnlineMapfreComponent } from './vn-online/vn-online-mapfre/componentes-step-online-mapfre/create-emision-online-mapfre/create-emision-online-mapfre.component';
import { DatosProductosOnlineMapfreComponent } from './vn-online/vn-online-mapfre/componentes-step-online-mapfre/datos-productos-online-mapfre/datos-productos-online-mapfre.component';
import { EmitirOnlineMapfreComponent } from './vn-online/vn-online-mapfre/componentes-step-online-mapfre/emitir-online-mapfre/emitir-online-mapfre.component';
import { PagosOnlineMapfreComponent } from './vn-online/vn-online-mapfre/componentes-step-online-mapfre/pagos-online-mapfre/pagos-online-mapfre.component';
import { InspeccionesOnlineMapfreComponent } from './vn-online/vn-online-mapfre/componentes-step-online-mapfre/inspecciones-online-mapfre/inspecciones-online-mapfre.component';
import { LinkPagoComponent } from './link-pago/link-pago.component';
import { PagarLinkPagoComponent } from './modals/pagar-link-pago/pagar-link-pago.component';
import { PickerAseguradoraComponent } from './modals/picker-aseguradora/picker-aseguradora.component';
import {SolicitudesPagoComponent} from "./solicitudes-pago/solicitudes-pago.component";
import {InfoTarjetaComponent} from "./modals/info-tarjeta/info-tarjeta.component";
import {UploadDocsImgComponent} from './modals/uploadDocsImg/uploadDocsImg.component';


@NgModule({
  declarations: [
    VentaNuevaComponent,
    CreateEmisionComponent,
    DatosProductoComponent,
    RegistroPolizaComponent,
    StepCotizadorComponent,
    InspeccionesComponent,
    PagosComponent,
    RecibosComponent,
    SolicitudesVnInternaComponent,
    DatosSolicitudComponent,
    ReasignarSolicitudComponent,
    ProspectoCreateComponent,
    CotizadorComponent,
    SolicitudesComponent,
    AdministracionPolizasComponent,
    TablaCascaronComponent,
    AutorizacionesProcesoComponent,
    MostrarDocumentosAutorizacionComponent,
    ViewerComponent,
    CorreccionErroresDatosComponent,
    CorreccionErroresDocumentosComponent,
    DocumentosCorreccionComponent,
    DatosClienteCorreccionComponent,
    DatosPagoCorreccionComponent,
    DatosRegistroCorreccionComponent,
    ProductoClienteCorreccionComponent,
    ViewerCorreccionComponent,
    DetallesPolizaComponent,
    EndososComponent,
    PendientesVerificacionComponent,
    SolicitudEndosoCreateComponent,
    SubirArchivoClienteComponent,
    AdministradorRecibosComponent,
    SolicitudesVnComponent,
    ChatWhatsappComponent,
    WhatsPendientesComponent,
    ViewerImgWhatsComponent,
    LogAseguradorasComponent,
    ModalCotizacionComponent,
    MostrarCotizacionComponent,
    NewStepOnlineComponent,
    CreateEmisionOnlineNewComponent,
    DatosProductosOnlineNewComponent,
    PagosOnlineNewComponent,
    RegistroPolizaOnlineNewComponent,
    InspeccionesOnlineNewComponent,
    RecotizadorTabsComponent,
    EmitirOnlineNewComponent,
    ActivacionEmpleadoComponent,
    ProductividadComponent,
    SocitudesActivacionComponent,
    MiEstadoCuentaComponent,
    AgregarAgenteComponent,
    ModificarClienteComponent,
    EsquemaComisionComponent,
    RechazoComponent,
    VisualizarPolizaComponent,
    HomeComponent,
    NewStepOnlineMapfreComponent,
    CreateEmisionOnlineMapfreComponent,
    DatosProductosOnlineMapfreComponent,
    EmitirOnlineMapfreComponent,
    PagosOnlineMapfreComponent,
    InspeccionesOnlineMapfreComponent,
    LinkPagoComponent,
    PagarLinkPagoComponent,
    PickerAseguradoraComponent,
    SolicitudesPagoComponent,
    InfoTarjetaComponent,
    UploadDocsImgComponent
  ],
  imports: [
    CommonModule,
    VentaNuevaRoutingModule,
    TelefoniaModule,
    FlexLayoutModule,
    MaterialModule,
    NgxExtendedPdfViewerModule,
    MatProgressBarModule,
    MatBadgeModule,
    SharedModule,
    // CurrencyPipe,
    MatStepperModule,
    MatDialogModule,
    NgxMaskModule

    // NgxExtendedPdfViewerModule,

  ],
  providers: [
    {provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl()},
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],

  exports: [
    ChatWhatsappComponent
  ]
})
export class VentaNuevaModule { }
