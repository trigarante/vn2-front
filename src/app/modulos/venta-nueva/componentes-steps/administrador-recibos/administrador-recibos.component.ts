import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Recibos} from '../../../../@core/data/interfaces/venta-nueva/recibos';
import {RecibosService} from '../../../../@core/data/services/venta-nueva/recibos.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {PagarLinkPagoComponent} from '../../modals/pagar-link-pago/pagar-link-pago.component';
import {SolicitudesPagoService} from '../../../../@core/data/services/venta-nueva/link-pago/solicitudes-pago';
import {NotificacionesService} from "../../../../@core/data/services/others/notificaciones.service";
@Component({
  selector: 'app-administrador-recibos',
  templateUrl: './administrador-recibos.component.html',
  styleUrls: ['./administrador-recibos.component.scss']
})
export class AdministradorRecibosComponent implements OnInit {
  recibo;
  idRegistro = +this.rutaActiva.snapshot.params.idRegistro;
  conjuntoRecibos: Recibos[];
  count: number;
  permisos = JSON.parse(window.localStorage.getItem('User'));


  constructor(
    private recibosService: RecibosService,
    public dialog: MatDialog,
    private rutaActiva: ActivatedRoute,
    private router: Router,
    private solicitudPago: SolicitudesPagoService,
    private _location: Location,
    private notificacionesService: NotificacionesService

  ) {
 }

  ngOnInit() {
    this.count = 0;
    this.getRegistro();
  }

  siguiente() {
    if (this.count < this.conjuntoRecibos.length - 1) {
      this.count ++;
    }
    this.recibo = this.conjuntoRecibos[this.count];
  }

  regresar() {
    if (this.count > 0) {
      this.count--;
    }
    this.recibo = this.conjuntoRecibos[this.count];
  }

  getRegistro() {
    // To identify when is entering by policy administrator or from payment link
    const idRecibo = !this.rutaActiva.snapshot.params.idSolicitud ? this.rutaActiva.snapshot.params.idRecibo : '0';
    this.recibosService.getRecibosByIdRegistro(this.idRegistro, idRecibo).subscribe(data => {
      this.conjuntoRecibos = data;
      this.recibo = this.conjuntoRecibos[0];
    });
  }

  pagarRecibo() {
    const ruta = '/modulos/venta-nueva/pagos-pagos';
    !this.rutaActiva.snapshot.params.idSolicitud ? this.router.navigate([ruta, this.recibo.id, this.recibo.idRegistro]) :
      this.router.navigate([ruta, this.recibo.id, this.recibo.idRegistro, this.rutaActiva.snapshot.params.idSolicitud]);
  }
  cerrar() {
    this._location.back();
  }

  pagarLinkPago() {
    this.notificacionesService.carga('Creando solicitud de pago');
      this.solicitudPago.postAuth().subscribe({
        next: value => {
          this.solicitudPago.getLinkPago(this.recibo.id, value.accessToken).subscribe({
            next: value1 => {
              this.notificacionesService.exito('Solicitud de pago enviado con exito');
              console.log(value1);
            },
            error: err => {
              this.notificacionesService.error(err.error.message);
              console.log(err.error.message);
            }
          });
        }, error: err => {
          this.notificacionesService.error(err.error.message);
        }
      });
  }
}
