import {Component, EventEmitter, Input, OnInit, Output, } from '@angular/core';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import { TipoPagoVn} from '../../../../@core/data/interfaces/venta-nueva/catalogos/tipo-pago-vn';
import {ProductoSocios} from '../../../../@core/data/interfaces/comerciales/producto-socios';
import * as  moment from 'moment';
import Swal from 'sweetalert2';
import {SociosComercial} from '../../../../@core/data/interfaces/comerciales/socios-comercial';
import {Ramo} from '../../../../@core/data/interfaces/comerciales/ramo';
import {SubRamo} from '../../../../@core/data/interfaces/comerciales/sub-ramo';
import {PeriodicidadService} from '../../../../@core/data/services/venta-nueva/catalogos/periodicidad.service';
import {Periodicidad} from '../../../../@core/data/interfaces/venta-nueva/catalogos/periodicidad';
import {ActivatedRoute} from '@angular/router';
import {ProductoSociosService} from '../../../../@core/data/services/comerciales/producto-socios.service';
import {TipoPagoVnService} from '../../../../@core/data/services/venta-nueva/catalogos/tipo-pago-vn.service';
import {RegistroService} from '../../../../@core/data/services/venta-nueva/registro.service';
import {RecibosService} from '../../../../@core/data/services/venta-nueva/recibos.service';
import {SociosService} from '../../../../@core/data/services/comerciales/socios.service';
import {RamoService} from '../../../../@core/data/services/comerciales/ramo.service';
import {SubRamoService} from '../../../../@core/data/services/comerciales/sub-ramo.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {filter, map} from "rxjs/operators";
import {pipe} from "rxjs";


@Component({
  selector: 'app-registro-poliza',
  templateUrl: './registro-poliza.component.html',
  styleUrls: ['./registro-poliza.component.scss']
})
export class RegistroPolizaComponent implements OnInit {
  instrucciones = 'INTRODUCE LOS SIGUIENTES DATOS CORRECTAMENTE:';
  @Input () idRegistroPoliza: number;
  @Input () idReciboPoliza: number;
  @Input() dataPoliza;
  @Input() socioOcr;
  dataPol;
  tipoPagos: TipoPagoVn[];
  periodicidad: Periodicidad[];
  socios: SociosComercial[];
  registroForm: FormGroup = new FormGroup({
    idEmpleado: new FormControl(+sessionStorage.getItem('Empleado')),
    idProducto: new FormControl(''),
    idTipoPago: new FormControl('', Validators.required),
    idSocio: new FormControl('', Validators.required),
    idProductoSocio: new FormControl('', Validators.required),
    idFlujoPoliza: new FormControl(1),
    idPeriodicidad: new FormControl('', Validators.required),
    poliza: new FormControl('', [Validators.required]),
    fechaInicio: new FormControl('', Validators.required),
    primaNeta: new FormControl('', [Validators.required, Validators.pattern('[0-9]+([.][0-9]{1,2})?')]),
    oficina: new FormControl(''),
    idDepartamento: new FormControl(+sessionStorage.getItem('idDepartamento')),
    idRamo: new FormControl('', Validators.required),
    idSubRamo: new FormControl('', Validators.required),
    primerPago: new FormControl('', [Validators.required, Validators.pattern('[0-9]+([.][0-9]{1,2})?')]),
  });
  ramos: Ramo[];
  subRamos: SubRamo[];
  productosSocio: ProductoSocios[];
  socio;
  numeroPagos: number;
  seRegistroArchivo = false;
  cantidad: number;
  @Output() salida = new EventEmitter();
  filesToUpload: FileList;
  archivoValido = false;
  idTipoPago;

  constructor(private tipoPagoService: TipoPagoVnService,
              private periodicidadService: PeriodicidadService,
              private sociosService: SociosService,
              private ramoService: RamoService,
              private subRamoService: SubRamoService,
              private registroService: RegistroService,
              private productoSocioService: ProductoSociosService,
              private recibosService: RecibosService,
              private route: ActivatedRoute,
              private notificaciones: NotificacionesService) {
  }

  ngOnInit() {
    this.getTipoPago();
    this.getSocios();
    if (this.idRegistroPoliza !== 0) {
      this.getRegistroPoliza();
    } else if (this.dataPoliza !== 0) {
      this.dataPol = this.dataPoliza;
      console.log(this.dataPol);
      this.setOCRForm();
    }
    this.getPeriodicidad();
  }

  /** Función para obtener el país de origen del precandidato y lo almacena en el arreglo "pais". */

  /** Obtiene los tipos de pago **/
  getTipoPago() {
    this.tipoPagoService.get().subscribe(result => {
      this.tipoPagos = result;

      if (this.dataPoliza !== 0){
        const formaPago = this.dataPol.poliza.formaPago;
        const idTipoPago = this.tipoPagos.filter(id => id.tipoPago === formaPago)[0].id;
        this.idTipoPago = idTipoPago;
        this.registroForm.controls.idTipoPago.setValue(this.idTipoPago);
        if (this.idTipoPago !== 1) {
          this.registroForm.addControl('pagoMensual', new FormControl(this.dataPol.poliza.pagoSubsecuente,
            [Validators.required, Validators.pattern('[0-9]+([.][0-9]{1,2})?')], this.setErrorMensual.bind(this)));
          let pagoMensual = this.dataPol.poliza.pagoSubsecuente.replace(',', '');
          pagoMensual = pagoMensual.replace('$', '');
          this.registroForm.controls.pagoMensual.setValue(pagoMensual);
          if (this.socioOcr === 18){
            this.registroForm.controls.pagoMensual.disable();
          }
        }
      }
    });
  }

  /** Obtiene la periodicidad en los pagos **/
  getPeriodicidad() {
    this.periodicidadService.get().subscribe({
      next: res => this.periodicidad = res,
      complete: () => this.getNumeroPagos(),
    });
  }

  getPeriodicidadFilter() {
    this.periodicidad = this.periodicidad.filter(perio => perio.id !== 1);
    this.registroForm.controls.idPeriodicidad.setValue('');
    this.getNumeroPagos();
  }

  /** Obtiene las aseguradoras como "Socios" **/
  getSocios() {
    this.sociosService.getByIdPaisStep(1).subscribe( {
      next: data => this.socios = data,
      complete: () => {
       if (this.idRegistroPoliza !== 0) {
         this.registroForm.controls.idSocio.enable();
         this.getRamo(this.registroForm.controls.idSocio.value, this.registroForm.value.idRamo);
         this.registroForm.controls.idSocio.disable();
       }
      },
      });
  }

  getRamo(idSocio: number, idRamo?: number) {
    console.log('noooo');
    console.log(idSocio);
    this.socio = this.socios.filter( socios => socios.id === this.registroForm.controls.idSocio.value)[0];

    // if (!this.socio.expresionRegular) {
    this.registroForm.controls.poliza.setValidators(Validators.required);
    // } else {
    //   if (this.socio.id !== 2 && this.socio.id !== 8) {
    //     this.registroForm.controls.poliza.setValidators(Validators.compose(
    //       [Validators.required, Validators.pattern(`${this.socio.expresionRegular}`)]));
    //   } else {
    //     const arrExp = this.socio.expresionRegular.split(',');
    //     const expresionesRegulares =
    //       this.socio.id === 2 ? `${arrExp[0]}|${arrExp[1].trim()}|${arrExp[2].trim()}` : `${arrExp[0]}|${arrExp[1].trim()}`;
    //     this.registroForm.controls.poliza.setValidators(Validators.compose(
    //       [Validators.required, Validators.pattern(expresionesRegulares)]));
    //   }
    // }

    this.registroForm.controls.poliza.updateValueAndValidity();
    if (!idRamo) {
      this.registroForm.controls.idRamo.setValue('');
      this.registroForm.controls.idSubRamo.setValue('');
      this.registroForm.controls.idProductoSocio.setValue('');
      this.ramos = [];
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.ramoService.getByIdSocio(idSocio).subscribe( {next: data => {
        this.ramos = data;
        console.log(this.ramos);
        if (this.ramos.length === 0) {
          this.mensajeErrorCatalogos('ramos');
        }
      }, complete: () => {
        if (idRamo) {
          this.registroForm.controls.idRamo.enable();
          this.registroForm.controls.idRamo.setValue(idRamo);
          this.registroForm.controls.idRamo.disable();
        }
      }});
  }

  getSubRamo(idRamo: number, idSubRamo?: number) {
    if (!idSubRamo) {
      this.registroForm.controls.idSubRamo.setValue('');
      this.registroForm.controls.idProductoSocio.setValue('');
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.subRamoService.getByIdRamo(idRamo).subscribe( {next: data => {
        this.subRamos = data;
        console.log(this.subRamos);
        if (this.subRamos.length === 0) {
          this.mensajeErrorCatalogos('subramos');
        }
      }, complete: () => {
        if (idSubRamo) {
          this.registroForm.controls.idSubRamo.enable();
          this.registroForm.controls.idSubRamo.setValue(idSubRamo);
          this.registroForm.controls.idSubRamo.disable();
        }
      }});
  }

  getProductoSocio(subRamo: number, idProductoSocio?: number) {
    if (!idProductoSocio) {
      this.registroForm.controls.idProductoSocio.setValue('');
      this.productosSocio = [];
    }

    this.productoSocioService.getByIdSubRamo(subRamo).subscribe({next: result => {
        this.productosSocio = result;
        if (this.productosSocio.length === 0) {
          this.mensajeErrorCatalogos('productos socio');
        }
      }, complete: () => {
        if (idProductoSocio) {
          this.registroForm.controls.idProductoSocio.enable();
          this.registroForm.controls.idProductoSocio.setValue(idProductoSocio);
          this.registroForm.controls.idProductoSocio.disable();
        }
      }});
  }

  mensajeErrorCatalogos(catalogo: string) {
    this.notificaciones.error('¡Por favor agregue ' + catalogo + ' en el catálogo correspondiente para poder continuar!');

  }

  /** Obtiene los datos de la póliza **/
  getRegistroPoliza() {
    this.registroService.getForRegistroById(this.idRegistroPoliza).subscribe( data => {
      this.registroForm.patchValue(data);
      console.log(this.registroForm.controls.idTipoPago.value);
      // this.registroForm.disable();
      this.registroForm.controls.idSocio.disable();
      this.registroForm.controls.poliza.disable();
      this.registroForm.controls.fechaInicio.disable();
      this.registroForm.controls.primaNeta.disable();
      this.registroForm.controls.idPeriodicidad.disable();
      this.registroForm.controls.oficina.disable();
      if (this.idReciboPoliza){
        this.registroForm.controls.primerPago.disable();
      } else {
        this.notificaciones.error('Tus recibos estan incompletos, favor de rellenar el formulario y dar cliq en boton Registrar');
      }
      this.getPeriodicidad();
      this.getSocios();
      this.getSubRamo(data.idRamo, data.idSubRamo);
      this.getProductoSocio(data.idSubRamo, data.idProductoSocio);
      if (data.archivo) {
        this.instrucciones = 'YA NO PUEDES EDITAR LAS OPCIONES INGRESADAS';
        this.seRegistroArchivo = true;
      }
    });
  }

  getNumeroPagos() {
    if (this.registroForm.controls.idTipoPago.valid) {
      this.numeroPagos = this.tipoPagos.find(arr => arr.id === this.registroForm.controls.idTipoPago.value).cantidadPagos;
      if (this.numeroPagos > 1) {
        this.registroForm.addControl('pagoMensual', new FormControl('',
          [Validators.required, Validators.pattern('[0-9]+([.][0-9]{1,2})?')], this.setErrorMensual.bind(this)));
      } else {
        if (this.registroForm.contains('pagoMensual')) {
          this.registroForm.removeControl('pagoMensual');
        }
      }
      if (this.idRegistroPoliza !== 0) {
        this.registroForm.controls.idTipoPago.disable();
      }
      this.getMontoPagos();
    }
  }

  getMontoPagos() {
    this.recibosService.getRecibosByIdRegistro(this.idRegistroPoliza).subscribe( data => {
      if (data && data.length > 0) {
        if (data.length > 1) {
          this.registroForm.controls.pagoMensual.setValidators([Validators.required, Validators.pattern('[0-9]+([.][0-9]{1,2})?')]);
          this.registroForm.controls.pagoMensual.setValue(data[0].cantidad);
          this.registroForm.controls.pagoMensual.updateValueAndValidity();
        }
        console.log(data[data.length - 1].cantidad);
        console.log(data[data.length - 1]);
        this.registroForm.controls.primerPago.setValue(data[data.length - 1].cantidad);
      }
    });
  }

  crearRegistroEnUno() {
    this.registroForm.enable();
    const poliza = this.registroForm.controls.poliza.value;
    this.notificaciones.carga('Comenzando proceso de registro, por favor espere');
    this.registroService.getPolizaExistente(poliza, this.registroForm.controls.idSocio.value,
      moment(this.registroForm.controls.fechaInicio.value).format('YYYY-MM-DD')).subscribe({
      next: value => {
        if (value.length > 0) {
          this.notificaciones.advertencia(`La póliza ${poliza} ya ha sido registrada.`);
        } else {
          const fecha = moment(this.registroForm.controls.fechaInicio.value).format('YYYY-MM-DD');

          const periodo = this.periodicidad.find(arr => arr.id === this.registroForm.controls.idPeriodicidad.value);
          this.cantidad = this.registroForm.controls.idTipoPago.value === 1 ? 1 : (periodo.id === 1 ? this.numeroPagos * .5 :
          this.numeroPagos * periodo.cantidadPagos);
          console.log(this.registroForm.controls.idTipoPago.value);
          console.log(periodo);
          console.log(this.numeroPagos);
          console.log(this.cantidad);


          let textoPrimerAviso =
            '<style>#swal2-content {' +
            '  text-align: left;' +
            '  margin-left: 40px;' +
            '}</style>' +
            '<strong>Tipo de pago: </strong>' +
            this.tipoPagos.find(val => val.id === this.registroForm.controls.idTipoPago.value).tipoPago +
            '</br>' +
            '<strong>Número de mensualidades: </strong> <a style="color: red; font-weight: bold">' +
            this.cantidad +
            '</a></br>' +
            '<strong>Socio: </strong>' +
            this.socios.find( r => r.id === this.registroForm.controls.idSocio.value).nombreComercial +
            '</br>' +
            '<strong>Ramo: </strong>' +
            this.ramos.find( r => r.id === this.registroForm.controls.idRamo.value).tipoRamo +
            '</br>' +
            '<strong>Subramo: </strong>' +
            this.subRamos.find( r => r.id === this.registroForm.controls.idSubRamo.value).tipoSubRamo +
            '</br>' +
            '<strong>Producto socio: </strong>' +
            this.productosSocio.find( r => r.id === this.registroForm.controls.idProductoSocio.value).tipoProducto +
            '</br>' +
            '<strong>Inicio de vigencia: </strong>' +
            fecha +
            '</br>' +
            '<strong>Oficina: </strong>' +
            this.registroForm.controls.oficina.value +
            '</br>' +
            '<strong>Poliza: </strong>' +
            this.registroForm.controls.poliza.value +
            '</br>' +
            '<strong>Prima neta: </strong> $' +
            this.registroForm.controls.primaNeta.value +
            '</br>' +
            '<strong>Prima neta del primer pago: </strong> $' +
            this.registroForm.controls.primerPago.value +
            '</br>';
          if (this.numeroPagos > 1) {
            textoPrimerAviso +=
              '<strong>Prima neta de las mensualidades: </strong> $' +
              this.registroForm.controls.pagoMensual.value +
              '</br>';
          }


          let cantidadAPagar: number = this.registroForm.controls.primerPago.value;
          const recibos: any[] = [];
          let fechaVigencia = moment(this.registroForm.controls.fechaInicio.value);


          this.notificaciones.carga('Generando registro');
          for (let i = 0; i < this.cantidad ; i++) {
            if (i > 0) {
              switch (this.registroForm.controls.idTipoPago.value) {
                case 2:
                  fechaVigencia = moment(fechaVigencia).add(1, 'months');
                  break;
                case 3:
                  fechaVigencia = moment(fechaVigencia).add(3, 'months');
                  break;
                case 4:
                  fechaVigencia = moment(fechaVigencia).add(4, 'months');
                  break;
                case 6:
                  fechaVigencia = moment(fechaVigencia).add(2, 'months');
                  break;
                case 5:
                  fechaVigencia = moment(fechaVigencia).add(6, 'months');
                  break;
              }
              cantidadAPagar = this.registroForm.controls.pagoMensual.value;
            }
            const fechaN = this.registroForm.controls.idTipoPago.value === 1 ? moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00') :
              this.registroForm.controls.idPeriodicidad.value === 1 && this.registroForm.controls.idTipoPago.value === 5 ?
                moment(moment(fechaVigencia).add(6, 'months').format('YYYY-MM-DD') + 'T06:00:00') :
                moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00');
            recibos.push({
              idRegistro: null,
              idEmpleado: sessionStorage.getItem('Empleado'),
              numero: i + 1,
              cantidad: cantidadAPagar,
              fechaVigencia: fechaN,
            });
          }
          textoPrimerAviso += '</br></br><strong>¿Estás seguro que desas continuar? </strong>';

          Swal.fire({
            icon: 'warning',
            title: 'Estos son los datos que ingresaste:',
            showCancelButton: true,
            cancelButtonText: 'Revisar nuevamente',
            confirmButtonText: '¡Sí, estoy seguro!',
            cancelButtonColor: 'red',
            confirmButtonColor: 'green',
            reverseButtons: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            html: textoPrimerAviso,
          }).then((firstWarning) => {
            // this.btnLoad = false;
            if (firstWarning.value) {
              Swal.fire({
                icon: 'warning',
                title: 'Confirmación',
                html: '<p>¡Recuerda que ya no podrás cambiar los datos ingresados en este paso y en caso de error podría ' +
                  'ameritar una <strong>sanción económica</strong>!</p>' +
                  '<p><b>¿Estás seguro que deseas continuar?</b></p>',
                cancelButtonColor: 'red',
                confirmButtonColor: 'green',
                showCancelButton: true,
                cancelButtonText: 'Revisar nuevamente',
                confirmButtonText: '¡Sí, estoy seguro!',
                reverseButtons: true,
              }).then(secondWarning => {
                if (secondWarning.value) {
                  this.notificaciones.carga('Generando registro');
                  console.log({...this.registroForm.getRawValue(), recibos});
                  this.registroService.postInOne({...this.registroForm.getRawValue(), recibos},
                    this.filesToUpload, this.route.snapshot.paramMap.get('id')).subscribe(
                    result => {
                      this.seRegistroArchivo = true;
                      this.notificaciones.exito('Se guardo la póliza');
                      this.idRegistroPoliza = result.idRegistro;
                      this.salida.emit({numero: 2, id: 'idRegistro', idRegistro: result.idRegistro, esRegistro: true});
                    },
                    () => {
                      this.notificaciones.error('Hubo un error al registrar los datos');
                    },
                  );
                }
              });
            }
          });
        }
      },
    });
  }

  crearRecibos() {
    this.registroForm.enable();
    const periodo = this.periodicidad.find(arr => arr.id === this.registroForm.controls.idPeriodicidad.value);
    this.cantidad = this.registroForm.controls.idTipoPago.value === 1 ? 1 : (periodo.id === 1 ? this.numeroPagos * .5 :
      this.numeroPagos * periodo.cantidadPagos);
    console.log(this.registroForm.controls.idTipoPago.value);
    console.log(periodo);
    console.log(this.numeroPagos);
    console.log(this.cantidad);
    let cantidadAPagar: number = this.registroForm.controls.primerPago.value;
    const recibos: any[] = [];
    let fechaVigencia = moment(this.registroForm.controls.fechaInicio.value);
    this.notificaciones.carga('Generando recibos');
    for (let i = 0; i < this.cantidad ; i++) {
      if (i > 0) {
        switch (this.registroForm.controls.idTipoPago.value) {
          case 2:
            fechaVigencia = moment(fechaVigencia).add(1, 'months');
            break;
            case 3:
              fechaVigencia = moment(fechaVigencia).add(3, 'months');
              break;
              case 4:
                fechaVigencia = moment(fechaVigencia).add(4, 'months');
                break;
                case 6:
                  fechaVigencia = moment(fechaVigencia).add(2, 'months');
                  break;
                  case 5:
                    fechaVigencia = moment(fechaVigencia).add(6, 'months');
                    break;
              }
        cantidadAPagar = this.registroForm.controls.pagoMensual.value;
      }
      const fechaN = this.registroForm.controls.idTipoPago.value === 1 ? moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00') :
        this.registroForm.controls.idPeriodicidad.value === 1 && this.registroForm.controls.idTipoPago.value === 5 ?
          moment(moment(fechaVigencia).add(6, 'months').format('YYYY-MM-DD') + 'T06:00:00') :
          moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00');
      recibos.push({
        idRegistro: this.idRegistroPoliza,
        idEmpleado: sessionStorage.getItem('Empleado'),
        numero: i + 1,
        cantidad: cantidadAPagar,
        fechaVigencia: fechaN,
      });
    }
    this.recibosService.post(recibos).subscribe({
      next: value => {
        this.notificaciones.exito('Se guardo con exito los recibos');
        this.idReciboPoliza = value.id;
        this.registroForm.disable();
        this.salida.emit({numero: 2, id: 'idRegistro', idRegistro: this.idRegistroPoliza, esRegistro: true});
      },
      error: err => {
        this.notificaciones.error('Hubo un error al registrar los datos');
      }
    });
  }

  almacenarArchivo() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    this.filesToUpload = (event.target as HTMLInputElement).files;
    if (this.filesToUpload.length !== 0) {
      const extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);
      const tamanioArchivo = this.filesToUpload.item(0).size * .000001;
      if (this.filesToUpload.length === 1) {
        // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
        if (extensionValida && tamanioArchivo < 7) {
          this.notificaciones.exito('¡El archivo está listo para ser guardado!');
          this.archivoValido = true;
        } else {
          let texto: string;
          if (!extensionValida) {
            texto = 'Solo puedes subir archivos pdf';
          } else {
            texto = 'Los archivos que subas deben pesar menos de 7 MB';
          }
          this.notificaciones.error(texto);
          this.archivoValido = false;
        }
      }
    }
  }

  setErrorMensual(): Promise<ValidationErrors | null> {
    return new Promise(() => {
      if (this.registroForm.controls.primerPago.valid) {
        this.registroForm.controls.pagoMensual.setErrors(+this.registroForm.controls.primerPago.value >=
        +this.registroForm.controls.pagoMensual.value ? null : {incorrect: true});
      }
    });
  }

  /** Establece el valor de estos campos conforme al servicio externo de OCR **/
  setOCRForm(){
    this.dataPol.poliza.inicioVigencia = this.dataPol.poliza.inicioVigencia.toUpperCase();
    console.log(this.dataPol.poliza.inicioVigencia);
    const str = (this.dataPol.poliza.inicioVigencia);
    const [dia, mes, año] = str.split('/');
    const newMonth = [dia, mes, año][1];
    let fechaPoliza;
    switch (newMonth) {
      case 'DIC':
        fechaPoliza = this.dataPol.poliza.inicioVigencia.replace('DIC', 'DEC');
        break;
      case 'ENE':
        fechaPoliza = this.dataPol.poliza.inicioVigencia.replace('ENE', 'JAN');
        break;
      case 'ABR':
        fechaPoliza = this.dataPol.poliza.inicioVigencia.replace('ABR', 'APR');
        break;
      case 'AGO':
        fechaPoliza = this.dataPol.poliza.inicioVigencia.replace('AGO', 'AUG');
        break;
      default:
        fechaPoliza = this.dataPol.poliza.inicioVigencia;
        break;
    }
    console.log(fechaPoliza);
    let primaNeta = this.dataPol.poliza.primaNeta.replace(',', '');
    primaNeta = primaNeta.replace('$', '');
    let primerPago = this.dataPol.poliza.primerPago.replace(',', '');
    primerPago = primerPago.replace('$', '');
    this.registroForm.controls.poliza.setValue(this.dataPol.poliza.poliza);
    this.registroForm.controls.poliza.disable();
    this.registroForm.controls.primaNeta.setValue(primaNeta);
    this.registroForm.controls.primaNeta.disable();
    if (this.dataPol.poliza.inicioVigencia !== '' ){
      this.registroForm.controls.fechaInicio.patchValue(moment(fechaPoliza).format('YYYY-MM-DD'));
      this.registroForm.controls.fechaInicio.disable();
    }
    if (this.dataPol.poliza.primerPago === '' || this.dataPol.poliza.formaPago === 'CONTADO'){
      this.registroForm.controls.primerPago.setValue(primaNeta);
      this.registroForm.controls.primerPago.disable();
    } else {
      this.registroForm.controls.primerPago.setValue(primerPago);
      this.registroForm.controls.primerPago.disable();
    }
    if (this.socioOcr !== 0) {
      this.registroForm.controls.idSocio.setValue(this.socioOcr);
      this.registroForm.controls.idSocio.disable();
      this.ramoService.getByIdSocio(this.socioOcr).subscribe( {next: data => {
          this.ramos = data;
          console.log(this.ramos);
          if (this.ramos.length === 0) {
            this.mensajeErrorCatalogos('ramos');
          }
        }});
    }
    if (this.socioOcr !== 18){
      this.registroForm.enable();
    }
    console.log(moment(this.dataPol.poliza.inicioVigencia).format('YYYY-MM-DD'));
  }

}
