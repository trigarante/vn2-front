import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesPagoComponent } from './solicitudes-pago.component';

describe('SolicitudesPagoComponent', () => {
  let component: SolicitudesPagoComponent;
  let fixture: ComponentFixture<SolicitudesPagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitudesPagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
