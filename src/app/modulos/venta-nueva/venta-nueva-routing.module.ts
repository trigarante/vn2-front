import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VentaNuevaComponent } from './venta-nueva.component';
import {CommonModule} from '@angular/common';
import {StepCotizadorComponent} from './step-cotizador/step-cotizador.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import {CotizadorComponent} from './cotizador/cotizador.component';
import {SolicitudesVnInternaComponent} from './solicitudes-vn-interna/solicitudes-vn-interna.component';
import {AdministracionPolizasComponent} from './administracion-polizas/administracion-polizas.component';
import {TablaCascaronComponent} from './tabla-cascaron/tabla-cascaron.component';
import {AutorizacionesProcesoComponent} from './autorizacion-errores/autorizaciones-proceso/autorizaciones-proceso.component';
import {MostrarDocumentosAutorizacionComponent} from './autorizacion-errores/mostrar-documentos-autorizacion/mostrar-documentos-autorizacion.component';
import {CorreccionErroresDocumentosComponent} from './correccion-errores/correccion-errores-documentos/correccion-errores-documentos.component';
import {CorreccionErroresDatosComponent} from './correccion-errores/correccion-errores-datos/correccion-errores-datos.component';
import {DocumentosCorreccionComponent} from './correccion-errores/documentos-correccion/documentos-correccion.component';
import {DetallesPolizaComponent} from './detalles-poliza/detalles-poliza.component';
import {EndososComponent} from './endosos/endosos.component';
import {PendientesVerificacionComponent} from './pendientes-verificacion/pendientes-verificacion.component';
import {ViewerComponent} from './viewer/viewer.component';
import {RecibosComponent} from './componentes-steps/recibos/recibos.component';
import {AdministradorRecibosComponent} from './componentes-steps/administrador-recibos/administrador-recibos.component';
import {PagosComponent} from './componentes-steps/pagos/pagos.component';
import {MostrarCotizacionComponent} from './vn-online/mostrar-cotizacion/mostrar-cotizacion.component';
import {NewStepOnlineComponent} from './vn-online/new-step-online/new-step-online.component';
import {RecotizadorTabsComponent} from './vn-online/recotizador-tabs/recotizador-tabs.component';
import {ProductividadComponent} from './productividad/productividad.component';
import {InspeccionesComponent} from './componentes-steps/inspecciones/inspecciones.component';
import {SolicitudesVnComponent} from './solicitudes-vn/solicitudes-vn.component';
import {WhatsPendientesComponent} from './whats-pendientes/whats-pendientes.component';
import {ActivacionEmpleadoComponent} from './desactivacion-ejecutivos/activacion-empleado/activacion-empleado.component';
import {
  SocitudesActivacionComponent
} from './desactivacion-ejecutivos/socitudes-activacion/socitudes-activacion.component';
import {MiEstadoCuentaComponent} from './mi-estado-cuenta/mi-estado-cuenta.component';
import {EsquemaComisionComponent} from './esquema-comision/esquema-comision.component';
import {NewStepOnlineMapfreComponent} from './vn-online/vn-online-mapfre/new-step-online-mapfre/new-step-online-mapfre.component';
import {LinkPagoComponent} from './link-pago/link-pago.component';
import {SolicitudesPagoComponent} from "./solicitudes-pago/solicitudes-pago.component";


const routes: Routes = [{
  path: '',
  component: VentaNuevaComponent,
  children: [
    {
      path: 'step-cotizador/:id',
      component: StepCotizadorComponent,
    },
    // Solicitudes out-contactados/no contactados
    // {
    //   path: 'solicitudes-vn/:tipo',
    //   component: SolicitudesVnComponent,
    // },
    {
      path: 'solicitudes-vn',
      component: SolicitudesVnComponent,
    },
    {
      path: 'solicitudes-interna',
      component: SolicitudesVnInternaComponent,
    },
    {
      path: 'cotizador-vn/:id',
      component: CotizadorComponent
    },
    {
      path: 'cotizador-vn/:id/:idSolicitud',
      component: CotizadorComponent
    },
    {path: 'solicitudes', component: SolicitudesComponent},
    {
      path: 'administracion-polizas', component: AdministracionPolizasComponent},
    {
      path: 'tabla-cascaron',
      component: TablaCascaronComponent
    },
    {
      path: 'administracion-polizas',
      component: AdministracionPolizasComponent
    },
    {
      path: 'autorizacion-errores/:estadoAutorizacion',
      component: AutorizacionesProcesoComponent
    },
    {
      path: 'mi-estado-cuenta',
      component: MiEstadoCuentaComponent
    },
    {
      path: 'esquemas-comision',
      component: EsquemaComisionComponent
    },
    {
      path: 'validar-documentos/:id_autorizacion_registro',
      component: MostrarDocumentosAutorizacionComponent
    },
    {
      path: 'correccion-errores/documentos/:tipo_correccion',
      component: CorreccionErroresDocumentosComponent
    },
    {
      path: 'endosos',
      component: EndososComponent
    },
    {
      path: 'correccion-errores/datos/:tipo_correccion',
      component: CorreccionErroresDatosComponent
    },

    {
      path: 'validar-documentos/:id_tipo_documento/:id_autorizacion_registro',
      component: MostrarDocumentosAutorizacionComponent
    },
    {
      path: 'documentos-correccion/:tipoError/:etapaCorreccion/:idTipoDocumento/:idverificacionAutorizacionRegistro',
      component: DocumentosCorreccionComponent,
    },
    {path: 'solicitudes', component: SolicitudesComponent},
    {
      path: 'detalles-poliza/:idPoliza',
      component: DetallesPolizaComponent,
    },
    {
      path: 'viewer/:id_tipo_documento/:id_autorizacion_registro',
      component: ViewerComponent,
    },
    {
      path: 'recibos-recibos/:idRegistro/:idRecibo',
      component: RecibosComponent,
    },
    {
      path: 'recibos-administrador-recibos/:idRegistro/:idRecibo/:idSolicitud',
      component: AdministradorRecibosComponent,
    },
    {
      path: 'recibos-administrador-recibos/:idRegistro/:idRecibo',
      component: AdministradorRecibosComponent,
    },
    {
      path: 'pagos-pagos/:idRecibo/:idRegistro/:id',
      component: PagosComponent,
    },
    {
      path: 'pagos-pagos/:idRecibo/:idRegistro',
      component: PagosComponent,
    },
    {
      path: 'inspecciones/:idRegistro/:idCliente',
      component: InspeccionesComponent,
    },
    {
      path: 'pendientes-verificacion',
      component: PendientesVerificacionComponent
    },
    {path: 'solicitudes', component: SolicitudesComponent
    },
      {path: 'whats-pendientes', component: WhatsPendientesComponent},
      {
        path: 'mostrarCotizacion/:cotizacion/' +
          ':tipo/:idProspecto/:urlServidor/:urlCotizacion/:nombreAseguradora/:submarca/:idTipoModificar/:idSolicitudDetalles/:urlCatalogo/:nombreCliente',
        component: MostrarCotizacionComponent,
      },
      {
        path: 'step-online/:id_solicitud/:nombreAseguradora',
        component: NewStepOnlineComponent,
      },
    {
      path: 'step-onlineMapfre/:id_solicitud/:nombreAseguradora',
      component: NewStepOnlineMapfreComponent,
    },
      // para recotizar
      {
        path: 'recotizador-vn/:tipo_contacto/:idProspectoExis/:idTipoModificar/:idSolicitudDetalles',
        component: RecotizadorTabsComponent,
      },
    {
      path: 'reportes/conversion-venta/productividad',
      component: ProductividadComponent
    },
    {path: 'solicitudes', component: SolicitudesComponent},
    {path: 'whats-pendientes', component: WhatsPendientesComponent},
    {path: 'activacion-empleados', component: ActivacionEmpleadoComponent},
    {path: 'solicitudes-desactivacion', component: SocitudesActivacionComponent},
    {path: 'link-pago', component: SolicitudesPagoComponent},
  ],
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VentaNuevaRoutingModule { }
