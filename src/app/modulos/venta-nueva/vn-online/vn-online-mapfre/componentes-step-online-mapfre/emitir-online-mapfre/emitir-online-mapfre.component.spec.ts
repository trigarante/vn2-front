import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmitirOnlineMapfreComponent } from './emitir-online-mapfre.component';

describe('EmitirOnlineMapfreComponent', () => {
  let component: EmitirOnlineMapfreComponent;
  let fixture: ComponentFixture<EmitirOnlineMapfreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmitirOnlineMapfreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmitirOnlineMapfreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
