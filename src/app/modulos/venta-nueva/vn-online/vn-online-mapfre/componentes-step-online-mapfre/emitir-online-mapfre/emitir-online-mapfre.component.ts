import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {CotizacionesAli} from '../../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';
import {
  SociosComercial,
  SociosComercialOnline
} from '../../../../../../@core/data/interfaces/comerciales/socios-comercial';
import {Ramo} from '../../../../../../@core/data/interfaces/comerciales/ramo';
import {SubRamo} from '../../../../../../@core/data/interfaces/comerciales/sub-ramo';
import {ProductoNameCorto, ProductoSocios} from '../../../../../../@core/data/interfaces/comerciales/producto-socios';
import {Periodicidad} from '../../../../../../@core/data/interfaces/venta-nueva/catalogos/periodicidad';
import {TipoPagoVn} from '../../../../../../@core/data/interfaces/venta-nueva/catalogos/tipo-pago-vn';
import {ProductoCliente} from '../../../../../../@core/data/interfaces/venta-nueva/producto-cliente';
import {TicketsInspeccion} from '../../../../../../@core/data/interfaces/cotizador/ticketsInspeccion';
import {SolicitudesvnService} from '../../../../../../@core/data/services/venta-nueva/solicitudes.service';
import {ActivatedRoute} from '@angular/router';
import {CotizacionesAliService} from '../../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {CatalogoService} from '../../../../../../@core/data/services/cotizador/catalogo.service';
import {LogCotizadorService} from '../../../../../../@core/data/services/cotizador/log-cotizador.service';
import {MatDialog} from '@angular/material/dialog';
import {SubRamoService} from '../../../../../../@core/data/services/comerciales/sub-ramo.service';
import {SociosService} from '../../../../../../@core/data/services/comerciales/socios.service';
import {RamoService} from '../../../../../../@core/data/services/comerciales/ramo.service';
import {ProductoSociosService} from '../../../../../../@core/data/services/comerciales/producto-socios.service';
import {RegistroService} from '../../../../../../@core/data/services/venta-nueva/registro.service';
import {NotificacionesService} from '../../../../../../@core/data/services/others/notificaciones.service';
import {PeriodicidadService} from '../../../../../../@core/data/services/venta-nueva/catalogos/periodicidad.service';
import {ProductoClienteService} from '../../../../../../@core/data/services/venta-nueva/producto-cliente.service';
import {TicketsInspeccionService} from '../../../../../../@core/data/services/cotizador/tickets-inspeccion.service';
import {ClienteVnService} from '../../../../../../@core/data/services/venta-nueva/cliente-vn.service';
import {SepomexService} from '../../../../../../@core/data/services/catalogos/sepomex.service';
import {urlAseguradoras} from '../../../../../../@core/data/interfaces/cotizador/urlAseguradoras';
import moment from 'moment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-emitir-online-mapfre',
  templateUrl: './emitir-online-mapfre.component.html',
  styleUrls: ['./emitir-online-mapfre.component.scss']
})
export class EmitirOnlineMapfreComponent implements OnInit {
  @Input() idProductoCliente: number;
  @Output() idProductoClientesaliente = new EventEmitter<{ idproductoc: number, idRegistro: number }>();
  @Input() idRegistroPoliza: number;

  registroForm: FormGroup = new FormGroup({
    idTipoPago: new FormControl('', Validators.required),
    idSocio: new FormControl('', Validators.required),
    idRamo: new FormControl('', Validators.required),
    idSubRamo: new FormControl('', Validators.required),
    idProductoSocio: new FormControl('', Validators.required),
    poliza: new FormControl('', [Validators.required]),
    oficina: new FormControl(''),
    fechaInicio: new FormControl(new Date(), [Validators.required]),
    fechaFin: new FormControl(''),
    primaNeta: new FormControl('', [Validators.required, Validators.pattern('[0-9.]+')]),
    primerPago: new FormControl(''),
    periodo: new FormControl('', Validators.required),
  });

  idSolicitud: number;
  cotizacionAli: CotizacionesAli;
  responseEmision: any;
  responseEmisionPoliza: any;
  nombreAseguradora: string;
  service: any;
  urlService: string;
  urldocumentos: string;
  urlEmision: string;
  urlEmisionCoberturas: string;
  dataArrayEmision: any;
  banderaEmitirMal = false;
  responsedocumentosPDF: any;
  socio: SociosComercial;
  socioOnline: SociosComercialOnline;
  socios: SociosComercial[];
  sociosOnline: SociosComercialOnline[];
  ramos: Ramo[];
  subRamos: SubRamo[];
  productosSocio: ProductoSocios[];
  productoSocioName: ProductoNameCorto;
  x: string;
  filesToUpload: FileList;
  archivoValido = false;
  gruposCargados = false;
  cantidad: number;
  periodicidad: Periodicidad[];
  numeroPagos: number;
  tipoPagos: TipoPagoVn[];
  ocultarboton = false;
  ocultarFormulario = false;
  urlPreventa: any;
  filesToUploadCliente: FileList;
  archivoValidoCliente = false;
  productoCliente: ProductoCliente;
  extensionesDeArchivoAceptadas = ['application/pdf'];

  jsonEmitirQualitas: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
    poliza: null,
  };
  ticketsInspeccions: TicketsInspeccion[];
  peticionE: any;
  responseProducto: any;
  vehiculoJson: object;
  ciudad: string;
  poblacion: string;
  fechaNacCambio: any;
  codPostal: string;
  clientejson: object;
  registroEnUno: any;

  constructor(private solicitudesService: SolicitudesvnService,
              private route: ActivatedRoute,
              private cotizacionesAliService: CotizacionesAliService,
              private router: ActivatedRoute,
              private catalogoService: CatalogoService,
              private logCotizadorService: LogCotizadorService,
              private matDialog: MatDialog,
              private subRamoService: SubRamoService,
              private sociosService: SociosService,
              private ramoService: RamoService,
              private productoSocioService: ProductoSociosService,
              private registroService: RegistroService,
              private notificaciones: NotificacionesService,
              private periodicidadService: PeriodicidadService,
              private notificacionesService: NotificacionesService,
              private productoClienteService: ProductoClienteService,
              private ticketsInspeccionService: TicketsInspeccionService,
              private dialog: MatDialog,
              protected clienteService: ClienteVnService,
              protected cpService: SepomexService) {
    this.nombreAseguradora = this.router.snapshot.paramMap.get('nombreAseguradora');
    this.service = urlAseguradoras.find(aseg => aseg.aseguradora === this.nombreAseguradora);
    this.urlService = this.service.service;
    this.urlEmision = this.service.urlEmision;
    this.urlEmisionCoberturas = this.service.urlEmisionCoberturas;
    this.urldocumentos = this.service.urldocumentos;
    console.log(this.urlEmisionCoberturas);
  }

  ngOnInit(): void {
    this.idSolicitud = Number(this.route.snapshot.paramMap.get('id_solicitud'));
    this.emitir(this.idSolicitud);

  }

  getRamoNuevo(idSocio: number, idRamo?: number) {
    this.sociosService.getByIdPaisStep(1).subscribe(data => {
      this.sociosOnline = data;
      // console.log(data);
      // console.log(this.sociosOnline)
      this.socioOnline = this.sociosOnline.filter(sociosOnline => sociosOnline.id === idSocio)[0];
      // console.log(this.socioOnline);
      if (!this.socioOnline.expresionRegular) {
        this.registroForm.controls.poliza.setValidators(Validators.required);
      } else {
        if (this.socioOnline.id !== 2 && this.socioOnline.id !== 8) {
          this.registroForm.controls.poliza.setValidators(Validators.compose(
            [Validators.required, Validators.pattern(`${this.socioOnline.expresionRegular}`)]));
        } else {
          const arrExp = this.socioOnline.expresionRegular.split(',');
          const expresionesRegulares =
            this.socioOnline.id === 2 ? `${arrExp[0]}|${arrExp[1].trim()}|${arrExp[2].trim()}` : `${arrExp[0]}|${arrExp[1].trim()}`;
          this.registroForm.controls.poliza.setValidators(Validators.compose(
            [Validators.required, Validators.pattern(expresionesRegulares)]));
        }
      }

      this.registroForm.controls.poliza.updateValueAndValidity();
      if (idRamo === undefined) {
        this.registroForm.controls.idRamo.setValue('');
        this.registroForm.controls.idSubRamo.setValue('');
        this.registroForm.controls.idProductoSocio.setValue('');
        this.ramos = [];
        this.subRamos = [];
        this.productosSocio = [];
      }

      this.ramoService.getByIdSocio(idSocio).subscribe({
        next: dataR => {
          this.ramos = dataR;
          if (this.ramos.length === 0) {
            this.mensajeErrorCatalogos('ramos');
          }
        }, complete: () => {
          if (idRamo !== undefined) {
            this.registroForm.controls.idRamo.setValue(idRamo);
          }
        }
      });
    });

  }

  getRamo(idSocio: number, idRamo?: number) {
    this.socio = this.socios.filter(socios => socios.id === this.registroForm.controls.idSocio.value)[0];

    if (!this.socio.expresionRegular) {
      this.registroForm.controls.poliza.setValidators(Validators.required);
    } else {
      if (this.socio.id !== 2 && this.socio.id !== 8) {
        this.registroForm.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(`${this.socio.expresionRegular}`)]));
      } else {
        const arrExp = this.socio.expresionRegular.split(',');
        const expresionesRegulares =
          this.socio.id === 2 ? `${arrExp[0]}|${arrExp[1].trim()}|${arrExp[2].trim()}` : `${arrExp[0]}|${arrExp[1].trim()}`;
        this.registroForm.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(expresionesRegulares)]));
      }
    }

    this.registroForm.controls.poliza.updateValueAndValidity();
    if (idRamo === undefined) {
      this.registroForm.controls.idRamo.setValue('');
      this.registroForm.controls.idSubRamo.setValue('');
      this.registroForm.controls.idProductoSocio.setValue('');
      this.ramos = [];
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.ramoService.getByIdSocio(idSocio).subscribe({
      next: data => {
        this.ramos = data;
        if (this.ramos.length === 0) {
          this.mensajeErrorCatalogos('ramos');
        }
      }, complete: () => {
        if (idRamo !== undefined) {
          this.registroForm.controls.idRamo.setValue(idRamo);
        }
      }
    });
  }

  getProductoSocio(subRamo: number, idProductoSocio?: number) {
    if (idProductoSocio === undefined) {
      this.registroForm.controls.idProductoSocio.setValue('');
      this.productosSocio = [];
    }

    this.productoSocioService.getProductoSocioByIdSubRamo(subRamo).subscribe({
      next: result => {
        this.productosSocio = result;
        if (this.productosSocio.length === 0) {
          this.mensajeErrorCatalogos('productos socio');
        }
      }, complete: () => {
        if (idProductoSocio !== undefined) {
          this.registroForm.controls.idProductoSocio.setValue(idProductoSocio);
        }
      }
    });
  }

  getSubRamo(idRamo: number, idSubRamo?: number) {
    if (idSubRamo === undefined) {
      this.registroForm.controls.idSubRamo.setValue('');
      this.registroForm.controls.idProductoSocio.setValue('');
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.subRamoService.getSubramosByIdRamo(idRamo).subscribe({
      next: data => {
        this.subRamos = data;
        if (this.subRamos.length === 0) {
          this.mensajeErrorCatalogos('subramos');
        }
      }, complete: () => {
        if (idSubRamo !== undefined) {
          this.registroForm.controls.idSubRamo.setValue(idSubRamo);
        }
      }
    });
  }

  getProductoSocioNombre(subRamo: number, nombre: string, idProductoSocio?: number) {

    if (idProductoSocio === undefined) {
      this.registroForm.controls.idProductoSocio.setValue('');
      this.productosSocio = [];
    }
    this.productoSocioService.getProductoSocioByIdSubRamoByNombre(subRamo, nombre).subscribe({
      next: result => {
        this.productoSocioName = result;
        console.log(this.productoSocioName);
        if (this.productoSocioName === null) {
          this.mensajeErrorCatalogos('productos socio');
        }
      }, complete: () => {
        this.registroForm.controls.idProductoSocio.setValue(this.productoSocioName.id);
        // console.log(this.registroForm.controls.idProductoSocio.value)
      }
    });
  }

  getNumeroPagos(): number {
    if (this.tipoPagos !== undefined && this.registroForm.controls.idTipoPago.valid) {
      this.numeroPagos = this.tipoPagos.find(arr => arr.id === this.registroForm.controls.idTipoPago.value).cantidadPagos;
      if (this.numeroPagos > 1) {
        this.registroForm.addControl('pagoMensual', new FormControl('',
          [Validators.required, Validators.pattern('[0-9.]+')], this.setErrorMensual.bind(this)));
      } else {
        if (this.registroForm.contains('pagoMensual')) {
          this.registroForm.removeControl('pagoMensual');
        }
      }
      return this.numeroPagos;
    } else {
      return 0;
    }
  }

  getPeriodicidad() {
    this.periodicidadService.get().subscribe(res => {
      this.periodicidad = res;
    });
  }

  getPeriodicidadFilter() {
    this.periodicidad = this.periodicidad.filter(perio => perio.id !== 1);
    this.registroForm.controls.periodo.setValue('');
  }

  setErrorMensual(): Promise<ValidationErrors | null> {
    return new Promise(() => {
      if (Number(this.registroForm.controls.primerPago.value) >= Number(this.registroForm.controls.pagoMensual.value)) {
        this.registroForm.controls.pagoMensual.setErrors(null);
      } else {
        this.registroForm.controls.pagoMensual.setErrors({'incorrect': true});
      }
    });
  }

  emitir(idSol) {


    let dataEmision: any;
    this.solicitudesService.getSolicitudesByIdOnline(idSol).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;

        this.responseEmision = this.cotizacionAli.respuesta;

        switch (this.nombreAseguradora) {
          case 'MAPFRE':

            if (this.responseEmision['codigoError'] === 'la poliza ' + this.responseEmision['emision']['poliza'] + ' '
              + 'ha sido emitida no cobrada.' || this.responseEmision['emision']['poliza'] !== '') {
              this.ocultarboton = true;
              console.log('la poliza ya emitida no cobrada');
              // por si y emitio pero no descargo la poliza, y sale puede volve a entrar y quedarse en emision y descargar poliza
              // checar futuro con emitir previa
              if (this.idRegistroPoliza === undefined) {
                this.obtenerPoliza(idSol);
              }
            } else {
              Swal.fire({
                title: 'Se está emitiendo, espere un momento.',
                allowOutsideClick: false,
                allowEscapeKey: false,
                onBeforeOpen: () => {
                  Swal.showLoading();
                },
              });
              // si ta tiene datos guardados de idProductoCliente los guarda en el json para poder emitir

              this.peticionE = this.cotizacionAli.peticion;


              console.log(this.idProductoCliente);
              this.productoClienteService.getProductoClienteById(this.idProductoCliente).subscribe(dataProducto => {
                  this.productoCliente = dataProducto;
                  console.log(this.productoCliente);
                  this.responseProducto = this.productoCliente.datos;
                  this.vehiculoJson = {
                    "uso": "PARTICULAR",
                    "clave": this.responseProducto.clave,
                    "marca": this.responseProducto.marca,
                    "codUso": "",
                    "modelo": this.responseProducto.modelo,
                    "noMotor": this.responseProducto.numeroMotor,
                    "noSerie": this.responseProducto.numeroSerie,
                    "codMarca": "",
                    "noPlacas": this.responseProducto.numeroPlacas,
                    "servicio": this.responseProducto.servicio,
                    "subMarca": this.responseProducto.subMarca,
                    "descripcion": this.responseProducto.descripcion,
                    "codDescripcion": ""
                  };

                  this.clienteService.getClienteById(this.productoCliente.idCliente).subscribe(dataCliente => {
                    this.cpService.getColoniaByCp(dataCliente.cp).subscribe(datacp => {

                      if (dataCliente.numInt === null) {
                        dataCliente.numInt = '';
                      }
                      // console.log(datacp)
                      // se quitan acentos en poblacion y ciudad
                      // si no existe cuidad manda vacio
                      if (datacp[0].ciudad === null) {
                        this.poblacion = '';
                      } else if (datacp[0].ciudad !== null) {
                        this.poblacion = datacp[0].ciudad.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
                      }
                      this.ciudad = datacp[0].delMun.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
                      // console.log(this.poblacion);
                      // console.log(this.ciudad);

                      this.fechaNacCambio = dataCliente.fechaNacimiento;
                      this.fechaNacCambio = moment(dataCliente.fechaNacimiento).format('YYYY-MM-DD');

                      // se convierte codigo portal a string y si empieza con o0 se le agrega
                      this.codPostal = dataCliente.cp.toString();
                      // console.log(this.codPostal)
                      this.codPostal = this.codPostal.padStart(5, '0');
                      // console.log(this.codPostal)
                      this.clientejson = {
                        "rfc": dataCliente.rfc,
                        "curp": dataCliente.curp,
                        "edad": this.peticionE.edad,
                        "email": dataCliente.correo,
                        "genero": dataCliente.genero,
                        "nombre": dataCliente.nombre,
                        "telefono": dataCliente.telefonoMovil,
                        "direccion": {
                          "pais": dataCliente.nombrePaises,
                          "calle": dataCliente.calle,
                          "noExt": dataCliente.numExt,
                          "noInt": dataCliente.numInt,
                          "ciudad": this.ciudad,
                          "colonia": dataCliente.colonia,
                          "codPostal": this.codPostal,
                          "poblacion": this.poblacion
                        },
                        "ocupacion": "",
                        "apellidoMat": dataCliente.materno,
                        "apellidoPat": dataCliente.paterno,
                        "tipoPersona": "F",
                        "fechaNacimiento": this.fechaNacCambio
                      };

                      if (dataCliente.genero === 'M') {
                        delete this.clientejson['genero'];
                        this.clientejson['genero'] = "MASCULINO";
                      }
                      if (dataCliente.genero === 'F') {
                        delete this.clientejson['genero'];
                        this.clientejson['genero'] = "FEMENINO";
                      }
                      delete this.responseEmision['cliente'];
                      delete this.responseEmision['idSubRamo'];

                      this.responseEmision['cliente'] = this.clientejson;
                      delete this.responseEmision['vehiculo'];
                      this.responseEmision['vehiculo'] = this.vehiculoJson;
                      this.responseEmision['pago']['resultado'] = "false";
                      this.responseEmision['pago']['referencia'] = "";
                      this.cotizacionAli['respuesta'] = this.responseEmision;
                      console.log(this.cotizacionAli);
                      this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
                        this.catalogoService.getEmisionCoberturas(this.urlService, this.urlEmision, this.responseEmision).subscribe(
                          (data) => {
                            dataEmision = data;

                          },
                          (error) => {
                            console.error('Error => ', error);
                            this.dataArrayEmision = error;
                            console.log(this.dataArrayEmision);

                            this.jsonEmitirQualitas.request = this.responseEmision;

                            this.jsonEmitirQualitas.idEmpleado = sessionStorage.getItem('Empleado');
                            this.jsonEmitirQualitas.idSolicitudVn = this.idSolicitud;
                            this.jsonEmitirQualitas.response = this.dataArrayEmision;
                            this.jsonEmitirQualitas.error = JSON.parse(this.dataArrayEmision['codigoError']);
                            this.jsonEmitirQualitas.poliza = this.dataArrayEmision['emision']['poliza'];
                            this.logCotizadorService.mapfreEmitir(this.jsonEmitirQualitas).subscribe(log => {
                            });
                            delete this.responseEmision['emision'];

                            this.responseEmision['emision'] = this.dataArrayEmision['emision'];
                            this.responseEmision['codigoError'] = this.dataArrayEmision['codigoError'];
                            this.responseEmision['urlRedireccion'] = this.dataArrayEmision['urlRedireccion'];
                            this.responseEmision['periodicidadDePago'] = this.dataArrayEmision['periodicidadDePago'];
                            console.log(this.responseEmision['emision']['poliza'])
                            this.cotizacionAli['respuesta'] = this.responseEmision;
                            this.cotizacionesAliService.put(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
                              },
                              errorb => {
                              },
                              () => {
                              });
                          },
                          () => {
                            this.dataArrayEmision = dataEmision;
                            console.log(this.dataArrayEmision);

                            this.jsonEmitirQualitas.request = this.responseEmision;

                            this.jsonEmitirQualitas.idEmpleado = sessionStorage.getItem('Empleado');
                            this.jsonEmitirQualitas.idSolicitudVn = this.idSolicitud;
                            this.jsonEmitirQualitas.response = this.dataArrayEmision;
                            this.jsonEmitirQualitas.error = this.dataArrayEmision['codigoError'];
                            this.jsonEmitirQualitas.poliza = this.dataArrayEmision['emision']['poliza'];
                            this.logCotizadorService.mapfreEmitir(this.jsonEmitirQualitas).subscribe(log => {
                            });

                            delete this.responseEmision['emision'];

                            this.responseEmision['emision'] = this.dataArrayEmision['emision'];
                            this.responseEmision['codigoError'] = this.dataArrayEmision['codigoError'];
                            this.responseEmision['urlRedireccion'] = this.dataArrayEmision['urlRedireccion'];
                            this.responseEmision['periodicidadDePago'] = this.dataArrayEmision['periodicidadDePago'];
                            console.log(this.responseEmision['emision']['poliza']);
                            this.cotizacionAli['respuesta'] = this.responseEmision;
                            if (this.responseEmision['codigoError'] === 'la poliza ' + this.responseEmision['emision']['poliza'] + ' '
                              + 'ha sido emitida no cobrada.') {
                              this.banderaEmitirMal = false;
                              // despues de obtener bien la poliza en qualitas se prodece a visualizar la poliza y descargarla
                              this.obtenerPoliza(this.idSolicitud);
                              // si salio la leyenda la poliza emitida no cobrada va a parecer el boton subir documentos para continuar
                              this.ocultarboton = true;
                              // this.swalCargando3();
                              console.log('entro emitio bien');
                              Swal.close();
                            } else {
                              Swal.close();
                              this.swalCargando4();
                              // this.emisionMal.emit();
                              this.banderaEmitirMal = true;
                              // this.emisionMal.emit();
                              console.log('no emitida');
                            }
                            this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
                              },
                              error => {
                              },
                              () => {
                              });
                          }); // fin de servicio

                      });
                    });

                  });
                },
                errorc => {

                },
                () => {

                });
             // this.swalCargando2();
              // this.responseEmision['aseguradora'];



              console.log(this.responseEmision);

            }
            break;
          case 'GNP0':
          // no se utiliza
          // break;
          default:

            break;
        }
      });
    });

  }

  swalCargando2() {
    Swal.fire({
      title: 'Se está emitiendo, espere un momento.',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 50000);
  }

  swalCargando4() {
    Swal.fire({
      title: 'Se obtuvo un error al emitir',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 6000);


  }

// obtener la poliza para poder visualizarla y descargarla
  obtenerPoliza(idSolicitud) {
    this.solicitudesService.getSolicitudesByIdOnline(idSolicitud).subscribe(data => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
          this.cotizacionAli = dataAli;
          console.log(this.cotizacionAli);
          this.responseEmisionPoliza = this.cotizacionAli.respuesta;

          this.registroForm.controls.poliza.setValue(this.responseEmisionPoliza['emision']['poliza']);
          this.registroForm.controls.poliza.disable();
          this.registroForm.controls.idTipoPago.setValue(1);
          this.registroForm.controls.idTipoPago.disable();
          this.registroForm.controls.primaNeta.setValue(this.responseEmision['emision']['primaNeta']);
          this.registroForm.controls.primaNeta.disable();

          this.subRamoService.getSubramoById(this.cotizacionAli.idSubRamo).subscribe(datasubramo => {
            // console.log(datasubramo);
            this.registroForm.controls.idSocio.setValue(datasubramo.idSocio);
            console.log(this.registroForm.controls.idSocio.value)
            this.getRamoNuevo(datasubramo.idSocio, datasubramo.idRamo);
            this.getSubRamo(datasubramo.idRamo, datasubramo.id);
            // this.getProductoSocio(datasubramo.id, 25);

            //  // obtener el nombre de cobertura producto socio
            // this.nombreCobertura = Object.keys(this.responseEmision['coberturas'])
            //    // nombre de cobertura
            // this.x = this.nombreCobertura[0];
            this.x = this.responseEmision['paquete'];
            this.x = this.x.toUpperCase();
            this.getProductoSocioNombre(datasubramo.id, this.x, datasubramo.idSocio);
            this.registroForm.controls.idSocio.disable();
            this.registroForm.controls.idRamo.disable();
            this.registroForm.controls.idSubRamo.disable();
            this.registroForm.controls.idProductoSocio.disable();

          });
          this.registroForm.controls.periodo.setValue(2);
          this.registroForm.controls.periodo.disable();
          const fecha = moment(this.responseEmision['emision']['inicioVigencia']);
          console.log(fecha);
          this.registroForm.controls.fechaInicio.setValue(fecha);
          this.registroForm.controls.fechaInicio.disable();

        },
        (error) => {
          console.error('Error => ', error);
        },
        () => {


          switch (this.nombreAseguradora) {
            case 'MAPFRE':

              let datadoc: any;

              this.responsedocumentosPDF = this.responseEmisionPoliza['emision']['documento'];
              break;
            case 'GNP0':

              break;
            default:

              break;
          }

        });
    });
  }
  // getDocuments() {
  //   // simulacion
  //   // const arrayDocuments = this.response['Emision'].Documento.split('|');
  //   // real
  //
  //   const arrayDocuments = this.responsedocumentosPDF.split('|');
  //
  //   console.log('Array Documentos => ', arrayDocuments);
  //   return arrayDocuments;
  //
  // }

  mensajeErrorCatalogos(catalogo: string) {
    Swal.fire({
      title: 'Sin elementos disponibles',
      text: '¡Por favor agregue ' + catalogo + ' en el catálogo correspondiente para poder continuar!',
    });
  }

  // Método para buscar la url que contiene el pdf
  findPolicieUrl(aseguradora: string, policieNumber: string) {
    // Arreglo el cual traera consigo los documentos del servicio y el que se usará para buscar la URL
    // nuevo servicio
    const documentsArray = this.responsedocumentosPDF;
    // const documentsArray = this.responseEmision['emision']['documento'].split('|');
    switch (this.nombreAseguradora) {
      case 'QUALITAS0':
        break;
      case 'MAPFRE' :

        // // Variable donde se almacenará la url que tenga el prefijo espcificado
        // console.log('URL Policie Contain => ', urlPolicie1);
        // return urlPolicie1;
        let urlPolicie1: string;
        urlPolicie1 = documentsArray;
        return urlPolicie1;
        break;
      default:

        break;
    }

  }

  // se guarda registro y recibo en la base de datos
  crearRegistroEnUno() {
    console.log(this.idProductoCliente);
    const poliza = this.registroForm.controls.poliza.value;
    console.log(this.registroForm.controls.fechaInicio.value);
    this.registroService.getPolizaExistente(poliza, this.registroForm.controls.idSocio.value,
      this.registroForm.controls.fechaInicio.value.toISOString().split('T')[0]).subscribe({
      next: value => {
        const advertencia = `
          <p>La póliza <strong>${poliza}</strong> ya ha sido registrada. </p>
        `;
        if (value.length > 0) {
          Swal.fire({
            title: '¡Advertencia!',
            html: advertencia,
          });
        } else {
          const fecha = moment(this.registroForm.controls.fechaInicio.value).format('YYYY-MM-DD');
          let idRegistroPoliza: number;
          this.gruposCargados = false;
          if (this.registroForm.controls.idTipoPago.value === 1) {
            this.cantidad = 1;
          } else {
            if (this.periodicidad.find(arr => arr.id === this.registroForm.controls['periodo'].value).id === 1) {
              this.cantidad = this.numeroPagos * .5;
            } else {
              this.cantidad = this.numeroPagos * (
                this.periodicidad.find(arr => arr.id === this.registroForm.controls['periodo'].value).cantidadPagos
              );
            }
          }
          const datosRegistro: any = {
            idProducto: this.idProductoCliente,
            idEmpleado: sessionStorage.getItem('Empleado'),
            archivo: '',
            idEstadoPoliza: 1,
            idTipoPago: this.registroForm.controls.idTipoPago.value,
            idProductoSocio: this.registroForm.controls.idProductoSocio.value,
            idFlujoPoliza: 1,
            idSocio: this.registroForm.controls.idSocio.value,
            poliza: this.registroForm.controls.poliza.value,
            oficina: this.registroForm.controls.oficina.value,
            fechaInicio: this.registroForm.controls.fechaInicio.value,
            primaNeta: this.registroForm.controls.primaNeta.value,
            idDepartamento: +sessionStorage.getItem('idDepartamento'),
            idPeriodicidad: this.registroForm.controls.periodo.value,
            online: 0,
            emisionCotizador: 1,
          };
          let textoPrimerAviso = '';
          if (this.getNumeroPagos() > 1) {
            textoPrimerAviso +=
              '<strong>Prima neta de las mensualidades: </strong> $' +
              this.registroForm.controls.pagoMensual.value +
              '</br>';

          }
          let cantidadAPagar: number = this.registroForm.controls.primerPago.value;
          let cantidadAPagarprima: number = this.registroForm.controls.primaNeta.value;
          const recibos: any[] = [];
          let fechaVigencia = moment(this.registroForm.controls.fechaInicio.value);
          if (this.registroForm.controls.idTipoPago.value !== 1) {
            if (this.periodicidad.find(arr => arr.id === this.registroForm.controls.periodo.value).id === 1) {
              this.numeroPagos = this.numeroPagos * .5;
              this.cantidad = this.numeroPagos;
            } else {
              this.numeroPagos = this.numeroPagos * (
                this.periodicidad.find(arr => arr.id === this.registroForm.controls.periodo.value).cantidadPagos);
              this.cantidad = this.numeroPagos;
            }
          } else {
            this.numeroPagos = 1;
          }
          for (let i = 0; i < this.numeroPagos; i++) {
            if (i > 0) {
              switch (this.registroForm.controls.idTipoPago.value) {
                case 2:
                  fechaVigencia = moment(fechaVigencia).add(1, 'months');
                  break;
                case 3:
                  fechaVigencia = moment(fechaVigencia).add(3, 'months');
                  break;
                case 4:
                  fechaVigencia = moment(fechaVigencia).add(4, 'months');
                  break;
                case 6:
                  fechaVigencia = moment(fechaVigencia).add(2, 'months');
                  break;
                case 5:
                  fechaVigencia = moment(fechaVigencia).add(6, 'months');
                  break;
              }
              cantidadAPagar = this.registroForm.controls.pagoMensual.value;
              cantidadAPagarprima = this.registroForm.controls.primaNeta.value;
              // fechaVigencia = moment(fechaVigencia).add(1, 'months');
            }
            if (this.registroForm.controls.idTipoPago.value === 1) {
              recibos.push({
                idEstadoRecibos: 2,
                idEmpleado: sessionStorage.getItem('Empleado'),
                numero: i + 1,
                cantidad: cantidadAPagarprima,
                fechaVigencia: moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00'),
              });
            } else {
              if (this.registroForm.controls.periodo.value === 1 && this.registroForm.controls.idTipoPago.value === 5) {
                recibos.push({
                  idEstadoRecibos: 2,
                  idEmpleado: sessionStorage.getItem('Empleado'),
                  numero: i + 1,
                  cantidad: cantidadAPagar,
                  fechaVigencia: moment(moment(fechaVigencia).add(6, 'months').format('YYYY-MM-DD') + 'T06:00:00'),
                });
              } else {
                recibos.push({
                  idEstadoRecibos: 2,
                  idEmpleado: sessionStorage.getItem('Empleado'),
                  numero: i + 1,
                  cantidad: cantidadAPagar,
                  fechaVigencia: moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00'),
                });
              }
            }
          }
          this.gruposCargados = true;
          this.registroEnUno = {
            registro: datosRegistro,
            recibo: recibos,
            archivos: this.findPolicieUrl(this.responseEmision['aseguradora'].toLowerCase(), this.registroForm.controls.poliza.value)
          };
          this.registroService.postInOneOnlinePro(this.registroEnUno, this.route.snapshot.paramMap.get('id_solicitud')).subscribe(
            result => {
              this.notificaciones.exito('Se guardo la póliza');
              this.idRegistroPoliza = result.idRegistro;
              // this.idProductoClientesaliente.emit(this.idProductoCliente);
              this.idProductoClientesaliente.emit({
                idproductoc: this.idProductoCliente,
                idRegistro: this.idRegistroPoliza
              });
              console.log('entro bien a pago :)');
            },
            () => {
              this.notificaciones.error('Hubo un error al registrar los datos');
            },
          );
        }
      },
      error: () => {
        Swal.fire({
          title: '¡Algo salió mal!',
        });
      },
    });
  }


  almacenarArchivoCliente() {
    this.filesToUploadCliente = (<HTMLInputElement>event.target).files;
    const extensionValida = this.extensionesDeArchivoAceptadas.includes(this.filesToUploadCliente.item(0).type);
    const tamanioArchivo = this.filesToUploadCliente.item(0).size * .000001;

    if (this.filesToUploadCliente.length === 1) {
      // El servicio en Spring solo acepta como máximo 7 MB para subir archivos
      if (extensionValida && tamanioArchivo < 7) {
        Swal.fire({
          title: 'Archivos listos',
          text: '¡Tu archivo está listo para ser guardado!',
        });
        this.archivoValidoCliente = true;
        if (this.archivoValidoCliente) {
          this.subirADriveCliente('cliente');
          this.ocultarboton = false;
        }
      } else {
        let titulo: string;
        let texto: string;

        if (!extensionValida) {
          titulo = 'Extensión no soportada';
          texto = 'Solo puedes subir archivos pdf';
        } else {
          titulo = 'Archivo demasiado grande';
          texto = 'Los archivos que subas deben pesar menos de 7 MB';
        }

        Swal.fire({
          title: titulo,
          text: texto,
        });
      }
    }
  }

  subirADriveCliente(tipoArchivo: string) {
    let idFolder: string;
    Swal.fire({
      title: 'Se están subiendo los archivos',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    console.log(this.idProductoCliente);
    console.log(this.filesToUploadCliente);

    this.productoClienteService.getProductoClienteById(this.idProductoCliente).subscribe({
      next: dataproducto => {
        this.productoCliente = dataproducto;
        this.productoClienteService.putInOneOnline(this.idProductoCliente, this.productoCliente, this.filesToUploadCliente).subscribe({
          next: dataC => {
            this.notificacionesService.exito('¡Tu archivo han sido subidos exitosamente!').then(() => {
              try{
                this.crearRegistroEnUno();
              } catch (e) {
                console.log(e);
                this.notificacionesService.error();
                this.ocultarboton = true;
              }
            });
          },
          error: () => {
            this.notificacionesService.error();
          },
        });
      },
      error: () => {
        this.notificacionesService.error();
      },
    });
  }

}
