import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmisionOnlineMapfreComponent } from './create-emision-online-mapfre.component';

describe('CreateEmisionOnlineMapfreComponent', () => {
  let component: CreateEmisionOnlineMapfreComponent;
  let fixture: ComponentFixture<CreateEmisionOnlineMapfreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateEmisionOnlineMapfreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmisionOnlineMapfreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
