import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ClienteVn} from '../../../../../../@core/data/interfaces/venta-nueva/cliente-vn';
import {MatStepper} from '@angular/material/stepper';
import {MatSlideToggle} from '@angular/material/slide-toggle';
import {Recibos} from '../../../../../../@core/data/interfaces/venta-nueva/recibos';
import {FormaPagoOnline} from '../../../../../../@core/data/interfaces/venta-nueva/catalogos/forma-pago';
import {
  BancoExterno,
  BancoExternoMapfre,
  BancoVn,
  MsiView, MsiViewMapfre
} from '../../../../../../@core/data/interfaces/venta-nueva/catalogos/banco-vn';
import {Carrier} from '../../../../../../@core/data/interfaces/venta-nueva/catalogos/carrier';
import {TarjetasVn} from '../../../../../../@core/data/interfaces/venta-nueva/catalogos/tarjetas-vn';
import {Pagos} from '../../../../../../@core/data/interfaces/venta-nueva/pagos';
import {ProductoCliente} from '../../../../../../@core/data/interfaces/venta-nueva/producto-cliente';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PagosService} from '../../../../../../@core/data/services/venta-nueva/pagos.service';
import {FormaPagoService} from '../../../../../../@core/data/services/venta-nueva/catalogos/forma-pago.service';
import {CarrierService} from '../../../../../../@core/data/services/venta-nueva/catalogos/carrier.service';
import {BancoVnService} from '../../../../../../@core/data/services/venta-nueva/catalogos/banco-vn.service';
import {RecibosService} from '../../../../../../@core/data/services/venta-nueva/recibos.service';
import {RegistroService} from '../../../../../../@core/data/services/venta-nueva/registro.service';
import {CotizacionesAliService} from '../../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {CatalogoService} from '../../../../../../@core/data/services/cotizador/catalogo.service';
import {SolicitudesvnService} from '../../../../../../@core/data/services/venta-nueva/solicitudes.service';
import {ActivatedRoute} from '@angular/router';
import {ClienteVnService} from '../../../../../../@core/data/services/venta-nueva/cliente-vn.service';
import {SepomexService} from '../../../../../../@core/data/services/catalogos/sepomex.service';
import {MsiExtenoGnp} from '../../../../../../@core/data/interfaces/cotizador/urlAseguradoras';
import {urlAseguradoras} from '../../../../../../@core/data/interfaces/cotizador/urlAseguradoras';
import Swal from "sweetalert2";
import {map} from "rxjs/operators";
import {MatDialog} from '@angular/material/dialog';
import {BancoOnline} from '../../../../../../@core/data/interfaces/cotizador/cotizar';
import {MessagePagoSuccessComponent} from '../../../../../../shared/componentes/modal-pagos-online-new/componentes/message-pago-success/message-pago-success.component';
import {ProductoClienteService} from '../../../../../../@core/data/services/venta-nueva/producto-cliente.service';
import {LogCotizadorService} from '../../../../../../@core/data/services/cotizador/log-cotizador.service';
import {NotificacionesService} from '../../../../../../@core/data/services/others/notificaciones.service';
import {RegistroOnline} from '../../../../../../@core/data/interfaces/venta-nueva/registro';
import moment from "moment";
import {VisualizarPolizaComponent} from '../../../modal/visualizar-poliza/visualizar-poliza.component';
import {SubRamoService} from '../../../../../../@core/data/services/comerciales/sub-ramo.service';
import {SociosService} from '../../../../../../@core/data/services/comerciales/socios.service';
import {RamoService} from '../../../../../../@core/data/services/comerciales/ramo.service';
import {ProductoSociosService} from "../../../../../../@core/data/services/comerciales/producto-socios.service";
import {PeriodicidadService} from "../../../../../../@core/data/services/venta-nueva/catalogos/periodicidad.service";
import {TicketsInspeccionService} from "../../../../../../@core/data/services/cotizador/tickets-inspeccion.service";

@Component({
  selector: 'app-pagos-online-mapfre',
  templateUrl: './pagos-online-mapfre.component.html',
  styleUrls: ['./pagos-online-mapfre.component.scss']
})
export class PagosOnlineMapfreComponent implements OnInit {

  @Input() idProductoCliente: number;
  @Input() idCliente: number;
  @Input() datosCliente: ClienteVn;
  @Input() idRegistro: number;
  idSolicitud: number;

  @ViewChild('stepper') stepper: MatStepper;
  @ViewChild('mostrarCodigoCheck') checkin: MatSlideToggle;
  @ViewChild('capture') capture: ElementRef;
  @ViewChild('fileInput') fileInput: ElementRef;
  @Output() idCobroSaliente = new EventEmitter<{ idproductoc: number, filepagado: FileList}>();
  @Output() Inspeccione = new EventEmitter<number>();
  @Output() emisionMal = new EventEmitter<number>();

  maskInput: string;
  recibo: Recibos;
  formaPagoData: FormaPagoOnline[];
  bancosArray: BancoVn[];
  carrierArray: Carrier[];
  tarjetasArray: TarjetasVn[];
  archivoValido = false;
  activarMesesSinIntereses = false;
  inputType = 'password';
  filesToUpload: FileList;
  extensionesDeArchivoAceptadas = ['application/pdf'];
  mesesSinIntereses = ['0 meses', '3 meses', '6 meses', '9 meses', '12 meses'];
  letras: RegExp = /[\\A-Za-zñÑ ]+/;
  idCarpetaDrive: string;
  ocultarEyePermanent: boolean;
  momentoCountdown = false;
  btnLoad = false;
  toggleActivo = true;
  // pago online
  token: string;
  // cotizacionAli: CotizacionesAli;
  cotizacionAli;
  pagoOnline: Pagos;
  response: any;
  responseObtenerEmision: any;
  dataArrayPago: any;
  dataArrarObtenerPoliza: any;
  dataArrayValidacionTarjeta: any;
  responsepago: any;
  nombrebanco: any;
  responsePago: any;
  mostrarbotonPago: boolean = false;
  nombreAseguradora: string;
  service: any;
  urlService: string;
  urlPago: string;
  urlEmision: string;
  fechaV: string;
  urlBanco: string;
  urlValidarTarjeta: string;
  urlMsi: string;
  urlObtenerEmision: string;
  // emision
  productoCliente: ProductoCliente;
  responseProducto: any;
  vehiculoJson: object;
  ciudad: string;
  poblacion: string;
  fechaNacCambio: any;
  codPostal: string;
  clientejson: object;
  jsonEmitir: any;
  responseEmision: any;
  responseEmisionPrima: any;
  dataArrayEmision: any;
  loader = false;
  bancosArrayExterno: BancoExternoMapfre[];
  ocultarFormulario = false;
  msi: MsiViewMapfre[];
  msiGnp: MsiExtenoGnp[] = [{CLAVE: 1, NOMBRE: 'CONTADO', VALOR: 'CONTADO'}];
  valorMsi: any;
  // ocultarMsi: boolean = true;
  bancosOnline: BancoOnline[];
  Arraybanco: any;
  numTarj: string;
  banderaNumTarjeta = false;
  btnLoadP = false;
  valuePdfPago: any;
  pago: any;
  filesToUploadPago: FileList;
  jsonEmitirGnp: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
  };

  jsonPagoGnp: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
    poliza: null,
    banderaPoliza: 0,
  };

  jsonEmitirQualitas: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
    poliza: null,
  };

  jsonPagoQualitas: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
    poliza: null,
    banderaPagado: 0,
  };
  ocultarMsi = false;
  banderaEmitirMal = false;
  banderaPagoMal = false;
  registroOnline: RegistroOnline;
  // re
  peticionE: any;
  idBancoExterno: number;
  validarTarjetaGeneral: any;


  // variable para mostrar el dialog cuando el pago sea true
  statusPayment = false;

  @ViewChild('reportContent') reportContent: ElementRef;

  // PATRON PARA ENMASCARAR EL INPUT Y VER SOLO LOS ULTIMOS 4 DIGITOS
  pattern = { 'Y': { pattern: new RegExp('\\d'), symbol: '•' }, '0': { pattern: new RegExp('\\d')}};

  // MASK DEL INPUT TARJETA
  maskCard = 'YYYYYYYYYYYY0000';
  formDatosPago: FormGroup = new FormGroup({
    cantidad: new FormControl({ value: '', disabled: true }),
    fechaPago: new FormControl({ value: new Date(), disabled: true }),
    idFormaPago: new FormControl(''),
    idBanco: new FormControl('', Validators.required),
    idCarrier: new FormControl({ disabled: true, value: '' }, Validators.required),
    cargoRecurrente: new FormControl(false),
    toggleCodigo: new FormControl(false),
    mesesSinIntereses: new FormControl(''),
    comentarios: new FormControl(''),
    numeroTarjeta: new FormControl('', [Validators.required, Validators.pattern('[0-9]+'), Validators.minLength(14)]),
    titular: new FormControl('', [Validators.required, Validators.required]),
    codigo: new FormControl('', Validators.required),
    codigoToken: new FormControl(''),
    noClave: new FormControl(''),
    mesVencimiento: new FormControl('', [Validators.required, Validators.pattern('[0-9]+'), Validators.maxLength(2)]),
    anioVencimiento: new FormControl('', [Validators.required, Validators.pattern('[0-9]+'), Validators.maxLength(2)])
  });

  constructor(
    private pagosService: PagosService, private dc: ChangeDetectorRef,
    private formaPagoService: FormaPagoService,
    private carrierService: CarrierService,
    private bancoService: BancoVnService,
    private recibosService: RecibosService,
    private registroService: RegistroService,
    private cotizacionesAliService: CotizacionesAliService,
    private catalogoService: CatalogoService,
    private solicitudesService: SolicitudesvnService,
    private route: ActivatedRoute,
    private dialog: MatDialog, private router: ActivatedRoute,
    private productoClienteService: ProductoClienteService,
    private clienteService: ClienteVnService,
    private logCotizadorService: LogCotizadorService,
    private notificacionesService: NotificacionesService,
    protected cpService: SepomexService,
    private matDialog: MatDialog
  ) {
    this.nombreAseguradora = this.router.snapshot.paramMap.get('nombreAseguradora');
    this.service = urlAseguradoras.find(aseg => aseg.aseguradora === this.nombreAseguradora);
    this.urlService = this.service.service;
    this.urlPago = this.service.urlPago;
    this.urlEmision = this.service.urlEmision;
    this.urlBanco = this.service.urlBanco;
    this.urlValidarTarjeta = this.service.urlValidarTarjeta;
    this.validarTarjetaGeneral = this.service.validarTarjetaGeneral;
    this.urlMsi = this.service.urlMsi;
    this.urlObtenerEmision = this.service.urlObtenerEmision;
  }

  ngOnInit(): void {
    this.idSolicitud = Number(this.route.snapshot.paramMap.get('id_solicitud'));
    this.getFormaPago();
    this.getBancos();
    this.getCarriers();
    // obtiene la cantidad
    this.getRecibos(this.idSolicitud);
    // this.getRegistro();
    this.ocultarEye();
  }

  get mesV () {
    return this.formDatosPago.controls['mesVencimiento'];
  }

  get anioV () {
    return this.formDatosPago.controls['anioVencimiento'];
  }

  fechaConsole(){
    console.log(`${this.mesV.value}/${this.anioV.value}`);
  }

  getBancos() {
    // BANCOS PARA MAPFRE
    this.bancoService.getexternoMapfre().subscribe(data => {
      this.bancosArrayExterno = data;
    });


  }
  getFormaPago() {
    this.formaPagoService.getOnline().pipe(map((res) => {
      return res.filter(data => data.id === 1 || data.id === 2);
    })).subscribe(result => {
      this.formaPagoData = result;
    });
  }

  getRecibos(idSol) {
    this.solicitudesService.getSolicitudesByIdOnline(idSol).subscribe(dataSol => {
      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        this.responseEmisionPrima = this.cotizacionAli.respuesta;
        console.log(this.responseEmisionPrima["cotizacion"]["primaNeta"]);
        this.formDatosPago.controls.cantidad.setValue(this.responseEmisionPrima["cotizacion"]["primaNeta"]);
      });
    });
  }

  getCarriers() {
    this.carrierService.getMapfre().subscribe(
      data => { this.carrierArray = data; });
  }

  verificarInputs() {
    this.cambioCT();
    if (this.formDatosPago.controls.idCarrier.value === '') {
      this.formDatosPago.controls.idCarrier.enable();
    }
  }

  // MSI
  gbanco(nombreBanco) {
    this.cambioCT();
    if (this.formDatosPago.controls.idCarrier.value === '') {
      this.formDatosPago.controls.idCarrier.enable();
    }
    if (this.formDatosPago.controls['idBanco'].value === 'AMERICAN EXPRESS'){
      this.maskCard = 'YYYYYYYYYYY0000';
    }
    switch (this.nombreAseguradora) {
      case 'MAPFRE':
        // msi PARA MAPFRE
        this.ocultarMsi = true;
        console.log(this.formDatosPago.controls['idBanco'].value);
        this.bancoService.getexternoOnlyMapfre().subscribe(data => {
          console.log(data);
          if (this.formDatosPago.controls.idFormaPago.value === 2) {
            delete this.msi;
            this.msi = [
              { msi: 'CONTADO', clave: ''}
            ];
          } else {

            this.msi = data;
            this.msi.push({ msi: 'CONTADO', clave: ''})
            console.log(this.msi);
          }
        });

        break;
      case 'GNP0' :
        break;
      default:

        break;
    }
  }

  verificaMesesSinIntereses() {
    if (this.formDatosPago.controls.mesesSinIntereses.value === '0 meses') {
      this.activarMesesSinIntereses = false;
      this.formDatosPago.controls.mesesSinIntereses.setValue('');
    }
  }

  ocultarEye() {
    if (this.inputType === 'text') {
      return true;
    } else {
      this.ocultarEyePermanent = true;
      return false;
    }
  }

  ocultarInput() {
    const tarjeta = this.formDatosPago.controls.numeroTarjeta.valid;
    const codigo = this.formDatosPago.controls.codigo.valid;
    this.toggleActivo = !(!tarjeta || !codigo);

    if (!this.toggleActivo) {
      Swal.fire({
        text: 'Datos incompletos',
      });
    } else {
      let countDown = 60;
      Swal.fire({
        text: `¡Tienes ${countDown} segundos para verificar tus datos!`,
        showConfirmButton: true,
        showCancelButton: true,
      }).then(resp => {
        if (resp.value) {
          this.momentoCountdown = true;
          // SE CAMBIA EL MASK PARA VISUALIZAR EL INPUT SIN SIMBOLOS
          this.maskCard = '0000000000000000';
          this.inputType = 'text';
          const time = setInterval(() => {
            countDown--;
            const loader2 =
              `<div>
                <div class="row"">
                  <div class="spinner-grow text-info" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class="row" style="padding-top: 5px">
                    <p>&nbsp;&nbsp;&nbsp;Quedan </p> <p>&nbsp;<strong>${countDown}</strong></p><p>&nbsp; segundos &nbsp;</p>
                  </div>
                </div>
                </div>`;
            const loader3 =
              `<div>
                <div class="row">
                  <div class="spinner-grow text-info" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class="row" style="padding-top: 5px">
                    <p>&nbsp;&nbsp;&nbsp;Queda </p> <p>&nbsp;<strong>${countDown}</strong></p><p>&nbsp; segundo &nbsp;</p>
                  </div>
                </div>
                </div>`;
            switch (countDown) {
              case 1:
                const Toast3 = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                });
                Toast3.fire({
                  width: 165,
                  html: loader3,
                });
                break;
              case 0:
                clearInterval(time);
                this.momentoCountdown = false;
                // SE ENMASCARA EL INPUT NUEVAMENTE
                this.maskCard = 'YYYYYYYYYYYY0000';
                this.inputType = 'password';
                this.ocultarEyePermanent = false;
                const Toast0 = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: 2500,
                });
                Toast0.fire({
                  width: 165,
                  text: 'Tiempo terminado',
                });
                break;
              default:
                const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                });
                Toast.fire({
                  width: 180,
                  html: loader2,
                });
                break;
            }
          }, 1000);
        }
      });
    }
  }

  // Editando función de reset para conservar primeros tres datos de form
  seleccionFormaPago() {
    const cantidad = this.formDatosPago.controls.cantidad.value,
      fechaPago = this.formDatosPago.controls.fechaPago.value,
      idFormaPago = this.formDatosPago.controls.idFormaPago.value;

    this.formDatosPago.reset();
    this.formDatosPago.controls.idCarrier.setValue('');
    this.formDatosPago.controls.cantidad.setValue(cantidad);
    this.formDatosPago.controls.fechaPago.setValue(fechaPago);
    this.formDatosPago.controls.idFormaPago.setValue(idFormaPago);
  }

  desactivarBoton(): boolean {
    if (this.formDatosPago !== undefined) {
      if (this.formDatosPago.valid && this.archivoValido) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  cambioCT() {
    if (this.formDatosPago.contains('codigo')) {
      this.formDatosPago.addControl('codigoToken', new FormControl('', Validators.compose(
        [Validators.minLength(3), Validators.maxLength(6)])));
      //   this.formDatosPago.removeControl('codigo');
    } else {
      if (this.formDatosPago.contains('codigoToken')) {
        //     this.formDatosPago.addControl('codigo', new FormControl('', Validators.compose(
        //       [Validators.minLength(3), Validators.maxLength(5), Validators.required])));
        this.formDatosPago.removeControl('codigoToken');
      }
    }
  }

  // obtiene los datos del servicio pago y los guarda en cotizacion ali
  enviarpagoOnline($event: MouseEvent) {
    ($event.target as HTMLButtonElement).disabled = true;

    let dataPago: any;
    let dataValidacionTarjeta: any;
    let formaPago: string;

    this.solicitudesService.getSolicitudesByIdOnline(this.idSolicitud).subscribe(dataSol => {
      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        this.response = this.cotizacionAli.respuesta;
        // this.bancoService.getBancoById(this.formDatosPago.controls['idBanco'].value).subscribe(databanco => {
        delete this.response['pago'];
        // poner datos bien

        switch (this.nombreAseguradora) {
          case 'MAPFRE':
            // envia los pagos al localstorage para usarlos en el otro step registro
            const pago: Pagos = {
              id: 0,
              idRecibo: 0,
              idFormaPago: this.formDatosPago.controls.idFormaPago.value,
              idEstadoPago: 1,
              idEmpleado: parseInt(sessionStorage.getItem('Empleado'), 10),
              fechaPago: this.formDatosPago.controls.fechaPago.value,
              cantidad: this.formDatosPago.controls.cantidad.value,
              archivo: '',
              datos: '',
            };

            if (this.formDatosPago.controls.mesesSinIntereses.value === '') {
              this.valorMsi = 'CL';
              console.log(this.valorMsi);
            } else {
              this.valorMsi = this.formDatosPago.controls.mesesSinIntereses.value;
            }

            this.fechaV = `${this.mesV.value}/${this.anioV.value}`;
            const DatosPago = {
              idBanco: this.formDatosPago.controls.idBanco.value,
              idCarrier: this.formDatosPago.controls.idCarrier.value,
              cargoRecurrente: false,
              toggleCodigo: false,
              mesesSinIntereses: this.valorMsi,
              comentarios: this.formDatosPago.controls.comentarios.value,
              numeroTarjeta: this.formDatosPago.controls.numeroTarjeta.value,
              titular: this.formDatosPago.controls.titular.value,
              codigo: this.formDatosPago.controls.codigo.value,
              codigoToken: '',
              noClave: this.formDatosPago.controls.noClave.value,
              fechaVencimiento: this.fechaV,
            };

            localStorage.setItem('pago', JSON.stringify(pago));
            localStorage.setItem('datosPago', JSON.stringify(DatosPago));
              // si es credito
            if (this.formDatosPago.controls.idFormaPago.value === 1) {
              formaPago = 'TA';
            } else if (this.formDatosPago.controls.idFormaPago.value === 2) {
              // si es debito
              formaPago = 'BA';
            }

            this.response['pago'] = {
              msi: this.formDatosPago.controls.mesesSinIntereses.value,
              banco: this.formDatosPago.controls.idBanco.value,
              mesExp: this.mesV.value,
              anioExp: `20${this.anioV.value}`,
              carrier: this.formDatosPago.controls.idCarrier.value,
              noClabe: '',
              medioPago: formaPago,
              noTarjeta: this.formDatosPago.controls.numeroTarjeta.value,
              nombreTarjeta: this.formDatosPago.controls.titular.value,
              codigoSeguridad: this.formDatosPago.controls.codigo.value,
              resultado: false,
              referencia: '',
            }

            this.cotizacionAli['respuesta'] = this.response;
            console.log('Response cotizacion Ali =>', this.cotizacionAli.respuesta);

            this.responsePago = this.cotizacionAli.respuesta;
            console.log(this.responsePago);

            // servicio real
            this.catalogoService.postPago(this.urlService, this.urlPago, this.responsePago)
              .subscribe(
                (data) => {
                  dataPago = data;

                },
                (error) => {
                  console.error('Error => ', error);
                },
                () => {
                  this.dataArrayPago = dataPago;
                  console.log(this.dataArrayPago);

                  // log de pago

                  this.jsonPagoQualitas.request = this.responsePago;

                  this.jsonPagoQualitas.idEmpleado = sessionStorage.getItem('Empleado');
                  this.jsonPagoQualitas.idSolicitudVn =  this.idSolicitud;
                  this.jsonPagoQualitas.response = this.dataArrayPago;
                  this.jsonPagoQualitas.error = this.dataArrayPago['codigoError'];
                  this.jsonPagoQualitas.poliza = this.dataArrayPago['emision']['poliza'];
                  if (this.dataArrayPago['pago']['resultado'] === true) {
                    this.jsonPagoQualitas.banderaPagado = 1;
                  }
                  this.logCotizadorService.mapfrePago(this.jsonPagoQualitas).subscribe(log => {
                  });
                  delete this.response;
                  this.response = this.dataArrayPago;
                  this.cotizacionAli['respuesta'] = this.response;

                  this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe( result => {
                  });

                  if (this.dataArrayPago['pago']['resultado'] === true) {
                    // muestre una ventana de pago exitoso
                    this.openDialogMessageSuccess();
                    this.banderaPagoMal = false;
                  } else if (this.dataArrayPago['codigoError'] !== '' && this.dataArrayPago['emision']['resultado'] === false){
                    ($event.target as HTMLButtonElement).disabled = false;

                    // no ha pagado-
                    this.banderaPagoMal = true;
                    this.noPayment();
                    console.log('El pago ha sido negado');
                  }


                });

            // // ejemplo responde
            //
            // delete this.response['emision']['resultado'];
            // this.response['emision']['resultado'] = true;
            // // ejemplo de pago que devuelve
            //
            // this.jsonPagoQualitas.request = this.response;
            //
            // this.jsonPagoQualitas.idEmpleado = sessionStorage.getItem('Empleado');
            // this.jsonPagoQualitas.idSolicitudVn =  this.idSolicitud;
            // this.jsonPagoQualitas.response = this.response;
            // this.jsonPagoQualitas.error = this.response['codigoError'];
            // this.jsonPagoQualitas.poliza = this.response['emision']['poliza'];
            // if (this.response['emision']['resultado'] === true) {
            //         this.jsonPagoQualitas.banderaPagado = 1;
            //       }
            // this.logCotizadorService.mapfrePago(this.jsonPagoQualitas).subscribe(log => {
            //       });
            //
            //
            // if (this.response['emision']['resultado'] === true) {
            //   // muestre una ventana de pago exitoso
            //   this.openDialogMessageSuccess();
            // } else {
            //   // no ha pagado-
            //   this.noPayment();
            //   console.log('El pago ha sido negado');
            // }
            //
            // this.cotizacionAli['respuesta'] = JSON.stringify(this.response);
            //
            // this.cotizacionesAliService.put(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
            // });
            // // fin simulacion
            // });//  banco
            break;
          case 'GNP0' :

            break;
          default:

            break;
        }
      });
    });
  }

  private noPayment() {
    Swal.fire({
      title: 'El pago no ha sido procesado',
      timer: 6000,
      text: '!Por favor verificar datos de tarjeta!',
    });
  }


  private openDialogMessageSuccess() {


    switch (this.nombreAseguradora) {
      case 'MAPFRE':
        const dialogRef2 = this.dialog.open(MessagePagoSuccessComponent, {
          panelClass: 'paySuccesModal',
          data: {
             // numpoliza: this.response['emision']['poliza'],
            // real
             numpoliza:  this.dataArrayPago['emision']['poliza'],
            // numpoliza: 738778374,
            blobImage: ''
          },
          disableClose: true
        });
        // Se usa el evento después de cerrar para subir el archivo a la base de datos y a drive
        dialogRef2.afterClosed().subscribe((result) => {
          // En esta variable se almacena el blob que se retorna desde el modal y está en result
          const resultBlob = result;
          console.log('URL BLOB => ', resultBlob);
          // Se llama a la función y el resultado se asigna a la variable de tipo FileList
          // Se pasan como parámetros el blob y el nombre del archivo
          this.filesToUpload = this.dataUrlToFileList(resultBlob, 'Archivo de Prueba');

          console.log('entro al registro');
          console.log(this.filesToUpload);
          this.subirPagoDrive(this.idRegistro, this.filesToUpload);
        });
        break;

      default:

        break;
    }


  }

  // Método que convierte a un nuevo blob y lo asigna a un FileList
  private dataUrlToFileList(resultBlob: Blob, fileName: string)/* : FileList */ {
    // console.log('Data URL en dataurltofilelist', resultBlob);
    // Arreglo en el cual se asignará el nuevo blob
    const FileArray: File[] = [];
    // se crea un nuevo objeto del nuevo blob
    const blobObject = new Blob([resultBlob], { type: 'application/pdf' });
    // Se crear un nuevo objeto de tipo file pasando como parametro el blob
    const file = new File([blobObject], fileName, {
      type: 'application/pdf'
    });
    // El objecto file se agrega al arreglo
    FileArray.push(file);
    // console.log("Arreglo de archivos FileList => ", FileArray);
    // Se retorna el arreglo de tipo file simulado como un FileList
    return FileArray as unknown as FileList;
  }



  swalCargando2() {
    Swal.fire({
      title: 'Se está emitiendo, espere un momento.',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 5000);

  }
  // si se emitio bien
  swalCargando3() {
    Swal.fire({
      title: '¡Emison obtenida con exito!',
      text: 'Continua con Pago',
      confirmButtonText: ' Comenzar pago',
      width: 500,
    }).then(value => {
      if (value.value) {
        window.close();


      }
    });
    setTimeout(() => {
      Swal.close();
    }, 5000);
  }

  swalCargando4() {
    Swal.fire({
      title: 'Se obtuvo un error al emitir',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 6000);


  }


  swalCargandotarjetanovalida(){
    Swal.fire({
      title: 'Error al validar la tarjeta',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 3000);
  }
  swalCargandotarjetanovalida2(){
    Swal.fire({
      title: 'Error al validar la tarjeta',
      allowOutsideClick: false,
      allowEscapeKey: false,
    });
    setTimeout(() => {
      Swal.close();
    }, 3000);
  }
  // hacer pago
  subirPagoDrive(idRegistro, idfiles) {

    // nuevo migracion
    this.btnLoadP = true;

    this.valuePdfPago = idfiles;
    console.log('direccion de pdf pago')
    console.log(this.valuePdfPago);

    this.filesToUploadPago = this.dataUrlToFileList(this.valuePdfPago, 'Archivo de Prueba');

    console.log(this.filesToUploadPago)
    console.log(idfiles)

    // nuevo

    this.pago = JSON.parse(localStorage.getItem('pago'));


    this.recibosService.getRecibosByIdRegistro(idRegistro).subscribe(dataR => {
      if (dataR) {
        this.recibo = dataR[0];

        const pago: Pagos = {
          id: 0,
          idRecibo: this.recibo.id,
          idFormaPago: this.pago.idFormaPago,
          idEstadoPago: 1,
          idEmpleado: this.pago.idEmpleado,
          fechaPago: this.pago.fechaPago,
          cantidad: this.pago.cantidad,
          archivo: '',
          datos: JSON.parse(localStorage.getItem('datosPago')),
        };

        this.registroService.getRegistroByIdOnline(this.idRegistro).subscribe(data => {

          console.log(data);
          this.registroOnline = data;
          this.registroOnline['online'] = 1;
          console.log(this.registroOnline)

          this.registroService.putOnline(this.idRegistro, this.registroOnline).subscribe({
            complete: () => {
            },
          });
        });
        this.pagosService.subirDrive(idfiles, this.route.snapshot.paramMap.get('id_solicitud'), pago).subscribe({
          next: dataP => {
            this.notificacionesService.exito('¡Tus archivos han sido subidos exitosamente!').then(() => {
              this.Inspeccione.emit(idRegistro);
            });
          },
          error: () => {
            this.notificacionesService.error();
          },
        });
      }
    });

  }
  visualizar(){

    this.matDialog.open(VisualizarPolizaComponent, {
      width: '900px',
      height: '670px',
      disableClose: false,
      data: {
        idRegistro: this.idRegistro,
      }
    }).afterClosed().subscribe(() => {

      return;
    });
  }
  validarTarjeta(event) {
    let databanco: any;
    switch (this.nombreAseguradora) {
      case 'MAPFRE':

        break;
      case 'GNP' :
        this.formDatosPago.controls.idBanco.enable();
        if (this.formDatosPago.controls.numeroTarjeta.valid) {
          this.catalogoService.getBancosExterno(this.urlService, this.urlBanco).subscribe(dataBan => {
            // this.bancosOnline = dataBan["CATALOGO"];
            databanco = dataBan;
            this.Arraybanco = databanco;
            this.bancosOnline = this.Arraybanco["CATALOGO"];
            console.log(this.bancosOnline);
          });
        }

        break;
      default:

        break;
    }
  }
}
