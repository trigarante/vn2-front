import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosOnlineMapfreComponent } from './pagos-online-mapfre.component';

describe('PagosOnlineMapfreComponent', () => {
  let component: PagosOnlineMapfreComponent;
  let fixture: ComponentFixture<PagosOnlineMapfreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PagosOnlineMapfreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosOnlineMapfreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
