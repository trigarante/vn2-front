import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MarcasOnline} from '../../../../../../@core/data/interfaces/cotizador/marcas-online';
import {ModelosOnline} from '../../../../../../@core/data/interfaces/cotizador/modelos-online';
import {DescripcionesOnline} from '../../../../../../@core/data/interfaces/cotizador/descripciones-online';
import {SubmarcasOnline} from '../../../../../../@core/data/interfaces/cotizador/submarcas-online';
import {DetalleInterno} from '../../../../../../@core/data/interfaces/cotizador/detalle-interno';
import {TipoSubramo} from '../../../../../../@core/data/interfaces/comerciales/catalogo/tipo-subramo';
import {CotizacionesAli} from '../../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';
import {ProductoCliente} from '../../../../../../@core/data/interfaces/venta-nueva/producto-cliente';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {TicketsInspeccion} from '../../../../../../@core/data/interfaces/cotizador/ticketsInspeccion';
import {ActivatedRoute, Router} from '@angular/router';
import {TipoSubramoService} from '../../../../../../@core/data/services/comerciales/catalogo/tipo-subramo.service';
import {CatalogoService} from '../../../../../../@core/data/services/cotizador/catalogo.service';
import {ProductoClienteService} from '../../../../../../@core/data/services/venta-nueva/producto-cliente.service';
import {RegistroService} from '../../../../../../@core/data/services/venta-nueva/registro.service';
import {SolicitudesvnService} from '../../../../../../@core/data/services/venta-nueva/solicitudes.service';
import {CotizacionesAliService} from '../../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {MatDialog} from '@angular/material/dialog';
import {RepuveServiceService} from '../../../../../../@core/data/services/repuve/repuve-service.service';
import {urlAseguradoras} from '../../../../../../@core/data/interfaces/cotizador/urlAseguradoras';
import {formatDate} from '@angular/common';
import Swal from 'sweetalert2';
import {AgregarAgenteComponent} from '../../../modal/agregar-agente/agregar-agente.component';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-datos-productos-online-mapfre',
  templateUrl: './datos-productos-online-mapfre.component.html',
  styleUrls: ['./datos-productos-online-mapfre.component.scss']
})
export class DatosProductosOnlineMapfreComponent implements OnInit {

  @Input() idCliente: number;
  @Input() idSubRamo: number;
  @Input() idRegistroPoliza: number;
  @Input() idProductoCliente: number;
  @Input() datosCotizacion: string;
  @Input() datosRespuesta: string;
  @Output() idProductoClientesaliente = new EventEmitter<number>();
  @Output() idEmitir = new EventEmitter<number>();
  minInicio = new Date();
  maxInicio = new Date();
  defaultDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 1);
  marca: string;
  modelo: string;
  submarca: string;
  descripcion: string;
  marcas: MarcasOnline[];
  modelos: ModelosOnline[];
  descripciones: DescripcionesOnline[];
  submarcas: SubmarcasOnline[];
  detalles: DetalleInterno[];
  tipoSubRamos: TipoSubramo[];
  idSolicitud: number;
  datos: any;
  responseEmision: any;
  cotizacionAli: CotizacionesAli;
  productoCliente: ProductoCliente;
  vehiculoJson: object;
  responseProducto: any;
  formCheck: FormGroup = new FormGroup({
    tienePlacas: new FormControl(true)
  });
  formDatos: FormGroup = new FormGroup({
    numeroMotor: new FormControl('', [Validators.pattern('^[A-Z0-9 ]+$'), Validators.maxLength(30)]),
    numeroPlacas: new FormControl('', [Validators.required, Validators.minLength(6)]),
    numeroSerie: new FormControl('', Validators.compose([ Validators.required,
        Validators.pattern('^([A-HJ-NPR-Z0-9]{8,17})$')]),
      this.validarNoSerieNoExista.bind(this)),
    inicioVigencia: new FormControl(false),
    recotizar: new FormControl(false),
    marca: new FormControl(''),
    modelo: new FormControl(''),
    descripcion: new FormControl(''),
    submarca: new FormControl(''),
  });
  tipoSubRamo: TipoSubramo;
  token: string;
  nombreAseguradora: string;
  service: any;
  urlService: string;
  urlCatalogo: string;
  // bandera para identificar si entro a inspeccion vehicular true- cotizacion normal, false- entro inspeccion vehicular
  ocultarinspeccionvechicular = true;
  responseEmisionPoliza: any;
  ticketsInspeccions: TicketsInspeccion[];
  ocultainspeccion = false;
  activarBotonInspeccionar = false;
  activarBotonVerInspeccion = false;
  // activa los botones de inspeccion si ya hay un productocliente
  activarBotonesInspecciones = false;
  banderaInspeccionado = false;
  banderaCancelarBotonGuardarVehiculo = true;
  idProductoInspeccion: number;
  serialValue: number;
  urlMarca: string;
  urlModelos: string;
  urlSubmarcas: string;
  urlDescripciones: string;

  constructor(private router: ActivatedRoute,
              private route: Router,
              private routerr: ActivatedRoute,
              private tipoSubRamoService: TipoSubramoService,
              private catalogoService: CatalogoService,
              private productoClienteService: ProductoClienteService,
              private registroService: RegistroService,
              private solicitudesService: SolicitudesvnService,
              private cotizacionesAliService: CotizacionesAliService,
              private dialog: MatDialog,
              private repuve: RepuveServiceService) {
    this.idSolicitud =  +this.routerr.snapshot.paramMap.get('id_solicitud');
    this.nombreAseguradora = this.router.snapshot.paramMap.get('nombreAseguradora');
    this.tipoSubRamoService.getVN().subscribe(data => {
      this.tipoSubRamos = data;
    });
    this.service = urlAseguradoras.find(aseg => aseg.aseguradora === this.nombreAseguradora)
    console.log(this.service);
    this.urlService = this.service.service;
    this.urlCatalogo = this.service.catalogo;
    this.urlMarca = this.service.urlMarca;
    this.urlModelos = this.service.urlModelos ;
    this.urlSubmarcas = this.service.urlModelos ;
    this.urlDescripciones = this.service.urlDescripciones ;
    this.getMarcas();
    this.minInicio.setDate(this.minInicio.getDate() + 1);
    this.maxInicio.setDate(this.maxInicio.getDate() + 14);
    this.formDatos.controls['numeroSerie'].disable();
    // this.formDatos.controls.inicioVigencia.setValue(this.defaultDate);
    this.formDatos.controls.numeroSerie.valueChanges.subscribe(value =>{
      if (value){
        this.formDatos.controls.numeroMotor.setValue(value.slice(-6));
      }
    });
  }

  ngOnInit(): void {
    if (this.datosCotizacion === null) {
      this.datos = null;
    } else {
      this.datos = this.datosCotizacion;
      // console.log(this.datos)
    }
    if (this.idProductoCliente !== 0) {
      this.formDatos.controls.numeroSerie.clearAsyncValidators();
      this.getProductoCliente();
    }
    // if (this.nombreAseguradora === 'QUALITAS'){
    //   this.formDatos.controls.inicioVigencia.setValidators(Validators.required);
    // }
  }

  getMarcas() {
    // console.log(this.urlService)
    this.catalogoService.getMarcas(this.urlService, this.urlCatalogo, this.urlMarca).subscribe(m => {
      this.marcas = m;
    });
  }

  getProductoCliente() {
    this.productoClienteService.getProductoClienteById(this.idProductoCliente).subscribe( { next: data => {
        const datos = data.datos;
        console.log('datos---', data);
        this.formDatos.controls.numeroPlacas.setValue(datos.numeroPlacas);
        this.formDatos.controls.numeroMotor.setValue(datos.numeroMotor);
        this.formDatos.controls.numeroSerie.setValue(datos.numeroSerie);
        this.solicitudesService.getSolicitudesById(this.idSolicitud).subscribe(dataSol => {
          this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
            this.cotizacionAli = dataAli;
            this.responseEmision = this.cotizacionAli.respuesta;
            console.log(this.cotizacionAli.respuesta);
            this.formDatos.controls.inicioVigencia.setValue(this.responseEmision['emision']['inicioVigencia']);
          });
        });
        if (data.fechaInicio) {
          this.formDatos.controls.inicioVigencia.setValue(formatDate(data.fechaInicio, 'yyyy-MM-dd', 'en'));
          this.minInicio = new Date(data.fechaInicio);
        }
      }, complete: () => {
        this.formDatos.controls.numeroSerie.setAsyncValidators(this.validarNoSerieNoExista.bind(this));
      }});
  }
  repuveNIV(){
    if (this.formDatos.controls.numeroPlacas.valid && this.formCheck.controls.tienePlacas.value){
      this.repuve.getRepuve(this.formDatos.controls.numeroPlacas.value).subscribe({
        next: value => {
          if (value.description === 'Correcto'){
            this.formDatos.controls.numeroSerie.setValue('');
            this.formDatos.controls.numeroSerie.setValue(value.infoVehicular.niv);
          } else {
            this.formDatos.controls.numeroSerie.setValue('');
            this.formCheck.controls.tienePlacas.setValue(true);
            this.disbledForms();
            this.formCheck.controls.tienePlacas.setValue(false);
            this.swalErrorPlaca();
          }
        } ,
        error: err => {
          this.formDatos.controls.numeroSerie.setValue('');
          this.formCheck.controls.tienePlacas.setValue(true);
          this.disbledForms();
          this.swalErrorPlaca();
          this.formCheck.controls.tienePlacas.setValue(false);
        }
      });
    }
  }
  validarNoSerieNoExista(): Promise<ValidationErrors | null> {
    return new Promise((resolve) => {
      this.registroService.getNoSerieExistente(this.formDatos.controls.numeroSerie.value).subscribe(
        data => {
          if (!data) {
            resolve(null);
          } else {
            Swal.fire({
              title: 'Número de serie en el sistema',
              html: 'Actualmente ya existe una poliza activa con este número de serie y se encuetra en: <strong>'
                + data.noSerie + '</strong>',
            }).then(() => {
              // this.route.navigate(['/modulos/dashboard']);
              resolve({placaExiste: true});
            });
          }
        });
    });
  }
  /** Swal Cargando **/
  swalCargando() {
    Swal.fire({
      title: 'Se está subiendo el producto ingresado',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 5000);
  }

  /** Swal Error **/
  swalError() {
    Swal.fire({
      title: 'Error',
      text: 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información',
    });
  }

  swalErrorPlaca() {
    Swal.fire({
      title: 'Error',
      text: 'Este número de placa no existe en REPUVE',
    });
  }

  /** Swal apra finalizar **/
  swalFinal(producto) {

    let textoPrimerAviso =
      '<style>#swal2-content {' +
      '  text-align: left;' +
      '  margin-left: 40px;' +
      '}</style>' +
      '<strong>Numero de Placas: </strong>' +
      this.formDatos.controls['numeroPlacas'].value +
      '</br>' +

      '<strong>Numero de motor: </strong>' +
      this.formDatos.controls['numeroMotor'].value +
      '</br>' +
      '<strong>Numero de Serie: </strong>' +
      this.formDatos.controls['numeroSerie'].value +
      '</br>' +
      '</br>';
    textoPrimerAviso += '</br></br><strong> <div style="text-align: center;"> <p><b>  ¿Estás seguro que desas continuar?' +
      '</p></b> </div> </strong>';
    if (producto) {
      Swal.fire({
        title: 'Información ingresada',
        text: '¡Tu producto ha sido creado exitosamente!',
      }).then(() => {  Swal.fire({
        title: 'Estos son los datos que ingresaste:',
        showCancelButton: true,
        cancelButtonText: 'Revisar nuevamente',
        confirmButtonText: '¡Sí, estoy seguro!',
        cancelButtonColor: 'red',
        confirmButtonColor: 'green',
        reverseButtons: true,
        allowOutsideClick: false,
        allowEscapeKey: false,
        html: textoPrimerAviso,
      }).then((firstWarning) => {
        // this.btnLoad = false;
        if (firstWarning.value) {
          Swal.fire({
            title: 'Confirmación',
            html: '<p>¡En el siguiente paso se Emitira y solo se podra registrar la poliza una sola vez, si ingresa otra vez podria ' +
              'ameritar una <strong>sanción económica</strong>!</p>' +
              '<p><b>¿Estás seguro que deseas continuar?</b></p>',
            cancelButtonColor: 'red',
            confirmButtonColor: 'green',
            showCancelButton: true,
            cancelButtonText: 'Revisar nuevamente',
            confirmButtonText: '¡Sí, estoy seguro!',
            reverseButtons: true,
          }).then(secondWarning => {
            if (secondWarning.value) {
              // Swal.fire({
              //   title: 'Generando emision',
              //   allowOutsideClick: false,
              //   onBeforeOpen: () => {
              //     Swal.showLoading();
              //   },
              // });
              // this._idProductoCliente.emit(producto.id);
              // this.gruposCargados = true;
              // this.registroEnUno = {
              //   registro: datosRegistro,
              //   recibos: recibos,
              // };

            } else {

            }
            // this.btnLoad = false;
          });
        }
      }); });
    } else {
      Swal.fire({
        title: 'Información actualizada',
        text: '¡Tu producto se actualizó exitosamente!',
      });
    }
  }
  // si modifica el producto
  /** Swal apra finalizar **/
  swalFinal2(idproducto) {

    let textoPrimerAviso =
      '<style>#swal2-content {' +
      '  text-align: left;' +
      '  margin-left: 40px;' +
      '}</style>' +
      '<strong>Numero de Placas: </strong>' +
      this.formDatos.controls['numeroPlacas'].value +
      '</br>' +

      '<strong>Numero de motor: </strong>' +
      this.formDatos.controls['numeroMotor'].value +
      '</br>' +
      '<strong>Numero de Serie: </strong>' +
      this.formDatos.controls['numeroSerie'].value +
      '</br>' +
      '</br>';
    textoPrimerAviso += '</br></br><strong>¿Estás seguro que desas continuar? </strong>';
    if (idproducto) {
      Swal.fire({
        title: 'Información ingresada',
        text: '¡Tu producto ha sido modificado exitosamente!',
      }).then(() => {  Swal.fire({
        title: 'Estos son los datos que ingresaste:',
        showCancelButton: true,
        cancelButtonText: 'Revisar nuevamente',
        confirmButtonText: '¡Sí, estoy seguro!',
        cancelButtonColor: 'red',
        confirmButtonColor: 'green',
        reverseButtons: true,
        allowOutsideClick: false,
        allowEscapeKey: false,
        html: textoPrimerAviso,
      }).then((firstWarning) => {
        // this.btnLoad = false;
        if (firstWarning.value) {
          Swal.fire({
            title: 'Confirmación',
            html: '<p>¡En el siguiente paso se Emitira y solo se podra registrar la poliza una sola vez, si ingresa otra vez podria ' +
              'ameritar una <strong>sanción económica</strong>!</p>' +
              '<p><b>¿Estás seguro que deseas continuar?</b></p>',
            cancelButtonColor: 'red',
            confirmButtonColor: 'green',
            showCancelButton: true,
            cancelButtonText: 'Revisar nuevamente',
            confirmButtonText: '¡Sí, estoy seguro!',
            reverseButtons: true,
          }).then(secondWarning => {
            if (secondWarning.value) {
              Swal.fire({
                title: 'Generando emision',
                // allowOutsideClick: false,
                // onBeforeOpen: () => {
                //   Swal.showLoading();
                // },
              });
              //    this._idProductoCliente.emit(idproducto);
              // this.gruposCargados = true;
              // this.registroEnUno = {
              //   registro: datosRegistro,
              //   recibos: recibos,
              // };

            } else {

            }
            // this.btnLoad = false;
          });
        }
      }); });
    } else {
      Swal.fire({
        title: 'Información actualizada',
        text: '¡Tu producto se actualizó exitosamente!',
      });
    }
  }

  crearProducto(esPost: boolean) {
    let producto: ProductoCliente;
    let productoCliente;
    this.swalCargando();
    this.datos['numeroPlacas'] = this.formDatos.controls.numeroPlacas.value;
    this.datos['numeroMotor'] = this.formDatos.controls.numeroMotor.value;
    this.datos['numeroSerie'] = this.formDatos.controls.numeroSerie.value;
    if (this.formDatos.controls.recotizar.value) {
      this.datos['marca'] = this.marcas.filter(marca => marca.text === this.formDatos.controls.marca.value)[0].text;
      this.datos['modelo'] = this.modelos.filter(modelo => modelo.text === this.formDatos.controls.modelo.value)[0].text;
      this.datos['subMarca'] = this.submarcas.filter(submarca => submarca.text ===
        this.formDatos.controls.submarca.value)[0].text;
      this.datos['descripcion'] = this.descripciones.filter(descripcion => descripcion.text ===
        this.formDatos.controls.descripcion.value)[0].text;

    }
    // console.log(this.datos)
    productoCliente = {
      'idSolicitud': this.idSolicitud,
      'idCliente': this.idCliente,
      'datos': this.datos,
      'idSubRamo': this.idSubRamo,
    };

    if (esPost) {
      this.productoClienteService.post(productoCliente).subscribe({
          next: data => {

            producto = data;
          },
          error: () => {
            this.swalError();
          },
          complete: () => {

            if (producto.id !== 0) {
              this.activarBotonesInspecciones = true;
            }
            this.modificarJsonCotizacion(producto.id);
          },
        },
      );
    } else {
      this.productoClienteService.put(this.idProductoCliente, productoCliente).subscribe({
        error: () => {
          this.swalError();
        },
        complete: () => {
          this.modificarJsonCotizacion(this.idProductoCliente);

        },
      });
    }

  }
  modificarJsonCotizacion(idProducto) {
    // console.log(idProducto)
    this.solicitudesService.getSolicitudesById(this.idSolicitud).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;

        this.responseEmision = this.cotizacionAli.respuesta;


        this.productoClienteService.getProductoClienteById(idProducto).subscribe(dataProducto => {
          this.productoCliente = dataProducto;
          this.responseProducto = this.productoCliente.datos;
          this.vehiculoJson = {
            "uso": "PARTICULAR",
            "clave": this.responseProducto.clave,
            "marca": this.responseProducto.marca,
            "codUso": "",
            "modelo": this.responseProducto.modelo,
            "noMotor": this.responseProducto.numeroMotor,
            "noSerie": this.responseProducto.numeroSerie,
            "codMarca": "",
            "noPlacas": this.responseProducto.numeroPlacas,
            "servicio": this.responseProducto.servicio,
            "subMarca": this.responseProducto.subMarca,
            "descripcion": this.responseProducto.descripcion,
            "codDescripcion": ""
          };
          // console.log(this.vehiculoJson);
          delete this.responseEmision['vehiculo'];
          this.responseEmision['vehiculo'] = this.vehiculoJson;
          // if ( this.nombreAseguradora ==='QUALITAS'){
          //   this.responseEmision['emision']['inicioVigencia'] = this.formDatos.controls.inicioVigencia.value;
          // }
          this.cotizacionAli['respuesta'] = this.responseEmision;
          this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe({
            next: data => {
            },
            error: () => {
            },
            complete: () => {
              switch (this.nombreAseguradora) {
                case 'MAPFRE':
                  // va a al step pago, registro y inspeccion
                  this.idProductoClientesaliente.emit(idProducto);
                  break;
                case 'GNP' :
                  // va a al step pago, registro y inspeccion
                  this.idProductoClientesaliente.emit(idProducto);

                  break;
                default:

                  break;
              }

            },
          });
        });

      }); // fin cotizacion

    }); // fin solicitud
  }
  getModelos() {
    this.catalogoService.getModelos(this.urlService, this.urlCatalogo, this.marca, this.urlModelos).subscribe(m => {
      this.modelos = m;
    });
  }

  getSubmarca() {
    this.catalogoService.getSubMarca(this.urlService, this.urlCatalogo, this.marca, this.modelo, this.urlSubmarcas)
      .subscribe(m => {
        this.submarcas = m;
      });

  }

  getDescripcion() {
    this.catalogoService.getDescripcion(this.urlService, this.urlCatalogo,
      this.marca, this.modelo,
      this.submarca, this.urlDescripciones).subscribe(m => {
      this.descripciones = m;
    });
  }
  disbledForms(){
    if (!this.formCheck.controls['tienePlacas'].value){
      this.formDatos.controls['numeroSerie'].disable();
      this.formDatos.controls['numeroPlacas'].enable();
      this.formDatos.controls['numeroPlacas'].setValue('');
      this.formDatos.controls['numeroPlacas'].setValidators([Validators.required, Validators.minLength(6)]);
      console.log(this.formCheck.controls['tienePlacas'].value);
      console.log('1');
    } else {
      this.formDatos.controls['numeroMotor'].enable();
      this.formDatos.controls['numeroSerie'].enable();
      this.formDatos.controls['numeroPlacas'].setValidators(null);
      this.formDatos.controls['numeroPlacas'].setValue('S/N');
      this.formDatos.controls['numeroSerie'].setValue('');
      console.log(this.formCheck.controls['tienePlacas'].value);

    }
  }

}
