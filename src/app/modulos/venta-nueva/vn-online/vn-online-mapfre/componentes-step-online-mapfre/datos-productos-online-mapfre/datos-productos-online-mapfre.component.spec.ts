import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosProductosOnlineMapfreComponent } from './datos-productos-online-mapfre.component';

describe('DatosProductosOnlineMapfreComponent', () => {
  let component: DatosProductosOnlineMapfreComponent;
  let fixture: ComponentFixture<DatosProductosOnlineMapfreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatosProductosOnlineMapfreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosProductosOnlineMapfreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
