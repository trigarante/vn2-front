import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspeccionesOnlineMapfreComponent } from './inspecciones-online-mapfre.component';

describe('InspeccionesOnlineMapfreComponent', () => {
  let component: InspeccionesOnlineMapfreComponent;
  let fixture: ComponentFixture<InspeccionesOnlineMapfreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspeccionesOnlineMapfreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspeccionesOnlineMapfreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
