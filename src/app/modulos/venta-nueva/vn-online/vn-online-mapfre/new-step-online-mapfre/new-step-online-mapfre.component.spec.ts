import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewStepOnlineMapfreComponent } from './new-step-online-mapfre.component';

describe('NewStepOnlineMapfreComponent', () => {
  let component: NewStepOnlineMapfreComponent;
  let fixture: ComponentFixture<NewStepOnlineMapfreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewStepOnlineMapfreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewStepOnlineMapfreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
