import {Component, OnInit, ViewChild} from '@angular/core';
import {MatStepper} from '@angular/material/stepper';
import {SolicitudesView} from '../../../../../@core/data/interfaces/venta-nueva/solicitudes';
import {ClienteVn} from '../../../../../@core/data/interfaces/venta-nueva/cliente-vn';
import {CotizacionesAli} from '../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';
import {TicketsInspeccion} from '../../../../../@core/data/interfaces/cotizador/ticketsInspeccion';
import {ProductoCliente} from '../../../../../@core/data/interfaces/venta-nueva/producto-cliente';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import {ActivatedRoute, Router} from '@angular/router';
import {StepsCotizadorService} from '../../../../../@core/data/services/venta-nueva/steps-cotizador.service';
import {SolicitudesvnService} from '../../../../../@core/data/services/venta-nueva/solicitudes.service';
import {RegistroService} from '../../../../../@core/data/services/venta-nueva/registro.service';
import {CotizacionesAliService} from '../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {TicketsInspeccionService} from '../../../../../@core/data/services/cotizador/tickets-inspeccion.service';
import {ClienteVnService} from '../../../../../@core/data/services/venta-nueva/cliente-vn.service';
import {SepomexService} from '../../../../../@core/data/services/catalogos/sepomex.service';
import {ProductoClienteService} from '../../../../../@core/data/services/venta-nueva/producto-cliente.service';
import {StepsCotizador} from '../../../../../@core/data/interfaces/venta-nueva/steps-cotizador';
import Swal from 'sweetalert2';
import {map} from 'rxjs/operators';
import moment from 'moment';

@Component({
  selector: 'app-new-step-online-mapfre',
  templateUrl: './new-step-online-mapfre.component.html',
  styleUrls: ['./new-step-online-mapfre.component.scss']
})
export class NewStepOnlineMapfreComponent implements OnInit {

  @ViewChild('stepper') stepper: MatStepper;
  idSolicitud: number;
  solicitud: SolicitudesView;
  recibos: boolean;
  // Variables para el step de cliente
  idCliente: number = undefined;
  // Variables para el step de producto cliente
  idProductoCliente: number = undefined;
  // Variables para el step de registro poliza
  idRegistro: number = undefined;
  stepsIdentificados = false;
  datosCliente: ClienteVn;
  showStep = false;

  // por lo mientras solo para qualitas
  cotizacionAli: CotizacionesAli;
  cotizacionAli2: CotizacionesAli;
  responseEmision: any;
  responseEmision2: any;
  coverageArray: Array<any>;
  cobertura: any;
  nombreCobertura: any;
  primaTotall: any;
  precioTotal: any;
  ahorro: any;
  filesToUploadPagado: FileList;
  nombreAseguradorab: string;

  arrayDownladedPDF: Array<{
    solicitudID: number,
    downloaded: boolean
  }> = [];
  stepIcons = [
    'face_retouching_natural',
    'airport_shuttle',
    'payment',
    'post_add',
    'content_paste_search'
  ];
  displayedColumns: string[] = ['nombre', 'sumaAsegurada'];
  dataSource: Array<any>;
  ocultarEmision: boolean;
  inspeccion = false;
  // bandera para identificar si entro a inspeccion vehicular true- cotizacion normal, false- entro inspeccion vehicular
  ocultarinspeccionvechicular = false;
  responseEmisionPoliza: any;
  ticketsInspeccions: TicketsInspeccion[];
  ticketsInspeccion: TicketsInspeccion;
  banderaAutoInspeccionado = false;
  peticionE: any;
  vehiculoJson: object;
  ciudad: string;
  poblacion: string;
  fechaNacCambio: any;
  codPostal: string;
  clientejson: object;
  responseProducto: any;
  productoCliente: ProductoCliente;

  constructor(
    private notificacionesService: NotificacionesService,
    private route: ActivatedRoute,
    private router: Router,
    private stepsService: StepsCotizadorService,
    private solicitudesService: SolicitudesvnService,
    private registroData: RegistroService,
    private cotizacionesAliService: CotizacionesAliService,
    private ticketsInspeccionService: TicketsInspeccionService,
    protected clienteService: ClienteVnService,
    protected cpService: SepomexService,
    private productoClienteService: ProductoClienteService
  ) {
    this.notificacionesService.cerrar();
  }

  ngOnInit(): void {
    this.idSolicitud = parseFloat(this.route.snapshot.paramMap.get('id_solicitud'));
    // console.log(this.idSolicitud);
    this.nombreAseguradorab = this.route.snapshot.paramMap.get('nombreAseguradora');
    this.downloadedpdfmethod();
    this.getSolicitud();
    this.getCotizacion(this.idSolicitud);
    this.getNuevaEmision();
  }
  private downloadedpdfmethod() {
    const itemDownloadPDF = {solicitudID: this.idSolicitud, downloaded: false};
    if (!localStorage.getItem('arrayDownloadedPdf')) {
      this.arrayDownladedPDF.push(itemDownloadPDF);
      localStorage.setItem('arrayDownloadedPdf', JSON.stringify(this.arrayDownladedPDF));
    } else {
      this.arrayDownladedPDF = JSON.parse(localStorage.getItem('arrayDownloadedPdf'));
      const resultFind = this.arrayDownladedPDF.find(e => e.solicitudID === this.idSolicitud);
      if (resultFind === undefined) {
        this.arrayDownladedPDF.push(itemDownloadPDF);
        localStorage.removeItem('arrayDownloadedPdf');
        localStorage.setItem('arrayDownloadedPdf', JSON.stringify(this.arrayDownladedPDF));
      }
    }
    console.log('Array Downloaded PDF => ', JSON.parse(localStorage.getItem('arrayDownloadedPdf')));
  }

  getSolicitud() {
    this.solicitudesService.getSolicitudesByIdOnline(this.idSolicitud).subscribe(data => {
        this.solicitud = data;
      }, error => {
      },
      () => {
        this.getStepActual();
      });
  }

  getStepActual() {
    let infoCliente: StepsCotizador;
    // el servicio trae los datos de la solicutud y si tiene idCliente, IdProducto, IdRegistro, IdPago, idRecibo
    this.stepsService.getByIdOnline(this.idSolicitud).subscribe(
      data => {
        infoCliente = data[0];
      },
      () => {
        Swal.fire({
          text: 'Ocurrió un error al cargar la solicitud, se te regresará al menú de solicitudes',
        }).then(() => {
          window.close();
        });
      }, () => {
        // si ya hay cliente lo guarda en el json

        this.guardarjson(infoCliente);

        // si ya trae IdProductoCliente abrira step cliente, vehiculo
        if (infoCliente.idProductoCliente !== null) {
          console.log(infoCliente.idCliente);
          this.idCliente = infoCliente.idCliente;
          this.idProductoCliente = infoCliente.idProductoCliente;
          // this.idRegistro = 0;
          this.datosCliente = {...this.datosCliente};
          this.stepper.selectedIndex = 1;
          // console.log(this.stepper.selectedIndex);
          // this.stepper.selectedIndex = 2;

          switch (this.nombreAseguradorab) {
            case 'QUALITAS':
              // si es cotizacion normal se pare en emision
              if (this.ocultarinspeccionvechicular === true) {
                this.stepper.selectedIndex = 2;
                this.inspeccion = true;
              }
              // si es inspeccion se pare en step vehiculo y continue a emision si es auto esta inspeccionado
              if (this.ocultarinspeccionvechicular === false) {

                if (this.banderaAutoInspeccionado === true) {
                  this.stepper.selectedIndex = 2;
                }
              }
              break;
            case 'MAPFRE' :
              this.stepper.selectedIndex = 2;
              break;
            default:

              break;
          }
        }
        // si tiene IdRegistro (ya se registro la poliza) que se quede en el step emision
        // si tiene pago que vaya al step pago
        if (infoCliente.idRegistro !== null) {
          this.idRegistro = infoCliente.idRegistro;
          this.datosCliente = {...this.datosCliente};
          switch (this.nombreAseguradorab) {
            case 'QUALITAS':
              this.ocultarEmision = true;
              this.inspeccion = true;
              break;
            case 'MAPFRE' :
              break;
            default:

              break;
          }

          if (infoCliente.pagos > 0) {
            this.stepper.selectedIndex = 4;
          } else {
            this.stepper.selectedIndex = 3;
          }
        }
        this.stepsIdentificados = true;
      });
  }

  nextSteper(num?: number) {
    this.stepper.next();
  }

  activarProductoCliente(value: any) {
    this.idProductoCliente = 0;
    this.idCliente = value.idCliente;
    this.stepper.next();
  }

  activarCobro(value: number) {
    this.ocultarEmision = true;
    this.idProductoCliente = value;
    // console.log(this.idProductoCliente);
    // console.log(value)
    this.datosCliente = {...this.datosCliente};
    switch (this.nombreAseguradorab) {
      case 'QUALITAS':
        break;
      default:

        break;
    }
    this.nextSteper();
  }

  activarCobrodesdeEmitir(value: any) {
    this.ocultarEmision = true;
    this.idProductoCliente = value;
    this.idRegistro = value.idRegistro;
    // console.log(this.idProductoCliente);
    // console.log(value)
    this.datosCliente = {...this.datosCliente};
    this.nextSteper();
  }

  activarEmitir(value: number) {
    this.idProductoCliente = value;
    this.stepper.next();
    this.inspeccion = true;
  }

  activarRegistroPoliza(value: any) {
    this.idRegistro = 0;
    this.idProductoCliente = value.idproductoc;
    this.datosCliente = {...this.datosCliente};
    this.filesToUploadPagado = value.filepagado;
    // console.log(value)
    // console.log(value.filepagado)
    this.nextSteper();
  }

  activarInspecciones(idRegistro: number) {
    this.idRegistro = idRegistro;
    this.stepper.next();
  }

  anterioStep() {
    this.stepper.previous();
  }

  getCotizacion(idSolicitud) {
    // muestre los datos en resumen
    // let dataCotizacion: any;
    this.solicitudesService.getSolicitudesByIdOnline(idSolicitud).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        console.log('----ali', dataAli);
        // console.log(this.cotizacionAli);
        this.responseEmision = this.cotizacionAli.respuesta;
        console.log(this.responseEmision['coberturas'])


        const coverages = this.responseEmision['coberturas'];
        // console.log(coverages)

        this.coverageArray = coverages;
        this.nombreCobertura = this.responseEmision['paquete'];
        this.primaTotall = this.responseEmision['cotizacion']['primaTotal'];
        console.log(this.responseEmision['cotizacion']['primaTotal'])
        // QUALITAS
        if (this.ocultarinspeccionvechicular === true) {
          this.precioTotal = Math.round((+this.primaTotall / 77) * 100);
        } else if (this.ocultarinspeccionvechicular === false) {
          this.precioTotal = Math.round((+this.primaTotall / 70) * 100);
        }
        // this.precioTotal = Math.round((+this.primaTotall / 77) * 100);
        this.ahorro = Math.round((+this.precioTotal) - (+this.primaTotall));
        console.log(this.coverageArray)

        // this.responseEmision['cliente'] = this.clientejson;
        this.cotizacionAli['respuesta'] = this.responseEmision;
      }); // fin cotizacion

    }); // fin solicitud
  }

  getNuevaEmision() {

    this.ocultarEmision = false;
    switch (this.nombreAseguradorab) {
      case 'MAPFRE':
        this.stepIcons = [
          'face_retouching_natural',
          'airport_shuttle',
          'subtitles',
          'payment',
          'content_paste_search'
        ];
        break;
      case 'GNP' :
        this.ocultarEmision = true;
        break;
      default:

        break;
    }
  }
  guardarjson(infoCliente) {
    this.solicitudesService.getSolicitudesByIdOnline(this.idSolicitud).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli2 = dataAli;

        this.peticionE = this.cotizacionAli.peticion;

        this.responseEmision2 = this.cotizacionAli.respuesta;
        if (infoCliente.idCliente !== null) {
          this.clienteService.getClienteById(infoCliente.idCliente).subscribe(dataCliente => {
            this.cpService.getColoniaByCp(dataCliente.cp).subscribe(datacp => {

              if (dataCliente.numInt === null) {
                dataCliente.numInt = '';
              }
              // console.log(datacp)
              // se quitan acentos en poblacion y ciudad
              // si no existe cuidad manda vacio
              if (datacp[0].ciudad === null) {
                this.poblacion = '';
              } else if (datacp[0].ciudad !== null) {
                this.poblacion = datacp[0].ciudad.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
              }
              this.ciudad = datacp[0].delMun.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
              // console.log(this.poblacion);
              // console.log(this.ciudad);

              this.fechaNacCambio = dataCliente.fechaNacimiento;
              this.fechaNacCambio = moment(dataCliente.fechaNacimiento).format('DD/MM/YYYY');

              // se convierte codigo portal a string y si empieza con o0 se le agrega
              this.codPostal = dataCliente.cp.toString();
              // console.log(this.codPostal)
              this.codPostal = this.codPostal.padStart(5, '0');
              // console.log(this.codPostal)
              this.clientejson = {
                "rfc": dataCliente.rfc,
                "curp": dataCliente.curp,
                "edad": this.peticionE.edad,
                "email": dataCliente.correo,
                "genero": dataCliente.genero,
                "nombre": dataCliente.nombre,
                "telefono": dataCliente.telefonoMovil,
                "direccion": {
                  "pais": dataCliente.nombrePaises,
                  "calle": dataCliente.calle,
                  "noExt": dataCliente.numExt,
                  "noInt": dataCliente.numInt,
                  "ciudad": this.ciudad,
                  "colonia": dataCliente.colonia,
                  "codPostal": this.codPostal,
                  "poblacion": this.poblacion
                },
                "ocupacion": "",
                "apellidoMat": dataCliente.materno,
                "apellidoPat": dataCliente.paterno,
                "tipoPersona": "",
                "fechaNacimiento": this.fechaNacCambio
              };

              if (dataCliente.genero === 'M') {
                delete this.clientejson['genero'];
                this.clientejson['genero'] = "MASCULINO";
              }
              if (dataCliente.genero === 'F') {
                delete this.clientejson['genero'];
                this.clientejson['genero'] = "FEMENINO";
              }
              delete this.responseEmision['cliente'];
              delete this.responseEmision['idSubRamo'];

              this.responseEmision['cliente'] = this.clientejson;
              this.cotizacionAli['respuesta'] = this.responseEmision;
              console.log(this.cotizacionAli);
              this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {

              });
            });

          });
        }
        if (infoCliente.idProductoCliente !== null) {
          this.productoClienteService.getProductoClienteById(infoCliente.idProductoCliente).subscribe(dataProducto => {
            this.productoCliente = dataProducto;
            console.log(this.productoCliente);
            this.responseProducto = this.productoCliente.datos;
            this.vehiculoJson = {
              "uso": "PARTICULAR",
              "clave": this.responseProducto.clave,
              "marca": this.responseProducto.marca,
              "codUso": "",
              "modelo": this.responseProducto.modelo,
              "noMotor": this.responseProducto.numeroMotor,
              "noSerie": this.responseProducto.numeroSerie,
              "codMarca": "",
              "noPlacas": this.responseProducto.numeroPlacas,
              "servicio": this.responseProducto.servicio,
              "subMarca": this.responseProducto.subMarca,
              "descripcion": this.responseProducto.descripcion,
              "codDescripcion": ""
            };
            delete this.responseEmision['vehiculo'];
            this.responseEmision['vehiculo'] = this.vehiculoJson;
            this.cotizacionAli['respuesta'] = this.responseEmision;

            this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {

            });
          });
        }

      });
    });

  }

}
