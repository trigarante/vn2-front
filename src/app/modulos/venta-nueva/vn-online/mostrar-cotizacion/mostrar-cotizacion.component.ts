import {Component, OnInit, ViewChild} from '@angular/core';
import {CatalogoService} from "../../../../@core/data/services/cotizador/catalogo.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl} from "@angular/forms";
import Swal from "sweetalert2";
import {SubRamoService} from "../../../../@core/data/services/comerciales/sub-ramo.service";
import {ProductoSolicitudvnService} from "../../../../@core/data/services/venta-nueva/producto-solicitudvn.service";
import {CotizacionesService} from "../../../../@core/data/services/venta-nueva/cotizaciones.service";
import {CotizacionesAli} from "../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali";
import {CotizacionesAliService} from "../../../../@core/data/services/venta-nueva/cotizaciones-ali.service";
import {SolicitudesvnService} from "../../../../@core/data/services/venta-nueva/solicitudes.service";
import {CotizarRequestOnline} from "../../../../@core/data/interfaces/cotizador/cotizar";
import {StepsCotizador} from "../../../../@core/data/interfaces/venta-nueva/steps-cotizador";
import {StepsCotizadorService} from "../../../../@core/data/services/venta-nueva/steps-cotizador.service";
import {ProductoClienteService} from "../../../../@core/data/services/venta-nueva/producto-cliente.service";
import {ProductoCliente, ProductoClienteOnline} from "../../../../@core/data/interfaces/venta-nueva/producto-cliente";
import {urlAseguradoras} from "../../../../@core/data/interfaces/cotizador/urlAseguradoras";
import {TablaEmisionCotizacionComponent} from "../../../../shared/componentes/tabla-emision-cotizacion/tabla-emision-cotizacion.component";
import {TicketsInspeccionService} from "../../../../@core/data/services/cotizador/tickets-inspeccion.service";
import {ModalCotizacionComponent} from "../modal/modal-cotizacion/modal-cotizacion.component";
import {MatDialog} from "@angular/material/dialog";
import {jsPDF} from "jspdf";
import html2canvas from "html2canvas";
import {PdfComponent} from "../../../../shared/componentes/pdf/pdf.component";
import {NotificacionesService} from "../../../../@core/data/services/others/notificaciones.service";

@Component({
  selector: 'app-mostrar-cotizacion',
  templateUrl: './mostrar-cotizacion.component.html',
  styleUrls: ['./mostrar-cotizacion.component.scss']
})
export class MostrarCotizacionComponent implements OnInit {
  @ViewChild(PdfComponent) pdfhijo: PdfComponent;
  activeButton: number = 0;
  cotizacion: any;
  urlServidor: string;
  urlCotizacion: string;
  imageInsureParam: string;
  urlImages = 'assets/images/logos-seguros/';
  imageSRC: string;
  nombreAseguradora: string;
  nombreAseguradorab: string;
  nombreCliente: string;
  cotizacionResponse: any;
  cotizacionRequestOnline = null;
  idSolicitud: any;
  idProspecto: number;
  subMarca: string;
  isLoading=false;
  // Variable para mostrar los datos de las coberturas
  coberturasTabla: Array<any>;
  vehicleDataText: string;
  dataArrayCotizacion: any;
  dataArrayCotizacion2: any;
  dataArrayCotizacion3: any;
  dataReCotizacion: any;
  tabSelectedText: string;
  tabs = ['Amplia', 'Limitada', 'RC'];
  selected = new FormControl(0);
  error = false;
  mostraBoton = true;

  methodSelected: number = 0;
  methodsPayment = [
    { value: 0, text: 'Anual', checked: true },
    { value: 1, text: 'Semestral', checked: false },
    { value: 2, text: 'Trimestral', checked: false },
  ];

  primaTotal: number;
  primerPago: number;
  pagoSubsecuente: number;

  productoSolicitudForm: any = {
    idProspecto: null,
    idTipoSubRamo: null,
    datos: null,
  };
  productoSolicitudFormM: any = {
    id: null,
    idProspecto: null,
    idTipoSubRamo: null,
    datos: null,
  };
  cotizacionesForm: any = {
    idProducto: null,
    idPagina: null,
    idMedioDifusion: null,
    idTipoContacto: 1,
    idEstadoCotizacion: 1,
  };
  cotizacionesAliForm: any = {
    idCotizacion: null,
    idSubRamo: null,
    peticion: null,
    respuesta: null,
    // prima: null,
    // fechaActualizacion: null,
  };
  solicitudForm: any = {
    idCotizacionAli: null,
    idEmpleado: window.sessionStorage.getItem('Empleado'),
    idEstadoSolicitud: 1,
    comentarios: ' ',
    idEtiquetaSolicitud: 1,
    idFlujoSolicitud: 1,
    idSubarea: window.sessionStorage.getItem('idSubarea'),
  };

  datosSolicitudes: any = {
    cotizacionRequestOnline: '',
    cotizacionResponse: '',
    idProspecto: '',
    idTipoContacto: '',
    idEmpleado: '',
    idSubRamo: '',
  };
  idProductoSolicitud: any;
  idTipoModificar: number;
  idSolicitudD: number;
  datos: any;
  jsonDatos: ProductoClienteOnline;
  responseProducto: any;
  cotizacionAli: CotizacionesAli;
  peticionAli: any = {};
  tipo: string;
  dataRectizacion: any;
  roboparcial: number;
  urlModificacionCoberturas: string;
  service: any;
  banderaModificacionCoberturas = false;
  coberturasModificadas: any;
  coberturas: any;
  coberturasEnviar: any;
  ocultarPagosSubsecuentes = true;
  banderaCotizacionInspeccion = false;
  ocultarBoton = true;
  urlCatalogo: string;
  element = false;
  coberturaTipo: number;
  datosPDF: any;
  coberturaAmplia: Array<any>;
  coberturaLimitada: Array<any>;
  coberturaRC: Array<any>;
  @ViewChild(TablaEmisionCotizacionComponent) private tablaComponent: TablaEmisionCotizacionComponent;
  // ocultarPagosSubsecuentes = false;


  constructor(
    private catalogoService: CatalogoService,
    private route: ActivatedRoute,
    private subRamoService: SubRamoService,
    private notificaciones: NotificacionesService,
    private productoSolicitudService: ProductoSolicitudvnService,
    private cotizacionesService: CotizacionesService,
    private cotizacionesAliService: CotizacionesAliService,
    private solictudesVnService: SolicitudesvnService,
    private stepsService: StepsCotizadorService,
    private router: Router,
    private productoClienteService: ProductoClienteService,
    private ticketsInspeccionService: TicketsInspeccionService,
    private dialog: MatDialog
  ) {
    this.cotizacion = JSON.parse(this.route.snapshot.paramMap.get('cotizacion'));
    console.log(this.cotizacion);
    this.tipo = this.route.snapshot.paramMap.get('tipo');
    this.idProspecto = +this.route.snapshot.paramMap.get('idProspecto');
    this.urlServidor = this.route.snapshot.paramMap.get('urlServidor');
    this.urlCotizacion = this.route.snapshot.paramMap.get('urlCotizacion');
    this.urlCatalogo = this.route.snapshot.paramMap.get('urlCatalogo');
    this.nombreAseguradorab = this.route.snapshot.paramMap.get('nombreAseguradora');
    this.nombreAseguradora = this.route.snapshot.paramMap.get('nombreAseguradora');
    this.nombreCliente = this.route.snapshot.paramMap.get('nombreCliente');
    console.log(this.nombreCliente, 'Cliente ok ');
    console.log(this.nombreAseguradora);
    this.service = urlAseguradoras.find(aseg => aseg.aseguradora === this.nombreAseguradorab);
    console.log(this.service);
    this.urlModificacionCoberturas = this.service.urlModificacionCoberturas;
    this.idTipoModificar = +this.route.snapshot.paramMap.get('idTipoModificar');
    this.idSolicitudD = +this.route.snapshot.paramMap.get('idSolicitudDetalles');
    this.subMarca = this.route.snapshot.paramMap.get('submarca');

    // console.log(this.cotizacion);
    // console.log(this.urlServidor);
    // console.log(this.urlCotizacion);
  }

  ngOnInit(): void {
    this.getImasgeInsure();
    this.inicio();
  }
  getImasgeInsure() {
    this.imageInsureParam = this.cotizacion.aseguradora;
    // console.log("Imagen:" + this.imageInsureParam);
    this.imageSRC = this.urlImages + this.imageInsureParam + '.svg';
  }
  download(){
    this.printPDF(this.coberturaTipo, this.tabs[this.selected.value].toLowerCase(), this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());
  }
  printPDF(value, cobertura, metodoPago): void {

    this.isLoading = true;
    setTimeout(() => {
      html2canvas(document.querySelector("#cotizarPDF")!).then(
        canvas => {
          console.log('width',canvas.width);
          console.log('height',canvas.height);
          const pdf = new jsPDF('p', 'pt', [canvas.width, canvas.height]);
          const imgData = canvas.toDataURL("image/jpeg", 1.0);
          pdf.addImage(imgData, 0, 0, canvas.width, canvas.height);
          pdf.save('cotizacion-' + this.dataArrayCotizacion["cotizacion"][value][cobertura.toLowerCase()][0]
            [metodoPago]["idCotizacion"] + `.pdf`);
        }
      );
    }, 1000);

    setTimeout(() => {
      this.isLoading=false;
    }, 5000);

  }
  inicio() {

    this.coberturasModificadas = [];
    console.log(this.cotizacion);
    switch (this.nombreAseguradorab) {
      case 'QUALITAS':
        if (this.banderaCotizacionInspeccion){
          delete this.cotizacion["descuento"];
          this.cotizacion["descuento"] = 30;
        }
        if (!this.banderaCotizacionInspeccion){
          delete this.cotizacion["descuento"];
          this.cotizacion["descuento"] = 23;
        }
        break;
      case 'GNP' :
        // por el momento solo cotiza con anual gnp
        this.methodsPayment = [
          { value: 0, text: 'Anual', checked: true },
        ];
        console.log(this.methodsPayment);
        this.ocultarPagosSubsecuentes = true;
        break;
      default:

        break;
    }
    // se mostrara la informacion de la cotizacion seleccionada del auto
    let dataCotizacion: any;
    // this.nombreAseguradora = this.cotizacion.aseguradora.toLowerCase();
    // console.log(this.cotizacion);
    console.log(this.cotizacion);
    // se cotizara el auto
    this.catalogoService
      .getCotizacion(this.urlServidor, this.urlCotizacion, this.cotizacion)
      .subscribe(
        (data) => {
          console.log('QUALITAS DATA => ', data);
          this.dataArrayCotizacion = {...data};
          this.datosPDF = {...data};
          this.coberturaAmplia = [...this.dataArrayCotizacion["coberturas"][0]["amplia"]];
          this.coberturaRC = [... this.dataArrayCotizacion["coberturas"][2]["rc"]];
          this.coberturaLimitada = [... this.dataArrayCotizacion["coberturas"][1]["limitada"]];
          console.log(this.dataArrayCotizacion);
          console.log(this.datosPDF);
          // if (this.nombreAseguradorab === 'QUALITAS'){
          //   // this.datosPDF["coberturas"][0]["amplia"].splice(6, 3);
          //   // this.coberturaAmplia.splice(6, 3);
          //   this.datosPDF["coberturas"][0]["amplia"][4]["nombre"] = "Muerte al conductor X AA";
          //   this.coberturaAmplia.push({
          //     "nombre": "Asistencia vial AV Plus",
          //     "sumaAsegurada": "Amparada",
          //     "deducible": "Amparada"
          //   });
          //   this.roboparcial = (+this.dataArrayCotizacion["coberturas"][0]["amplia"][1]["sumaAsegurada"]) * .15;
          //   this.coberturaAmplia.push({
          //     "nombre": "Robo Parcial",
          //     "sumaAsegurada": this.roboparcial,
          //     "deducible": "25%"
          //   });
          //   // this.datosPDF["coberturas"][1]["limitada"].splice(5, 3);
          //   // this.coberturaLimitada.splice(5, 3);
          //   this.datosPDF["coberturas"][1]["limitada"][3]["nombre"] = "Muerte al conductor X AA";
          //   this.coberturaLimitada.push({
          //     "nombre": "Asistencia vial AV Plus",
          //     "sumaAsegurada": "Amparada",
          //     "deducible": "Amparada"
          //   });
          //   this.roboparcial = (+this.dataArrayCotizacion["coberturas"][1]["limitada"][1]["sumaAsegurada"]) * .15;
          //   this.coberturaLimitada.push({
          //     "nombre": "Robo Parcial",
          //     "sumaAsegurada": this.roboparcial,
          //     "deducible": "25%"
          //   });
          //   // this.datosPDF["coberturas"][2]["rc"].splice(4, 2);
          //   // this.coberturaRC.splice(4, 2);
          //   this.datosPDF["coberturas"][2]["rc"][2]["nombre"] = "Muerte al conductor X AA";
          //   this.coberturaRC.push({
          //     "nombre": "Asistencia vial AV Plus",
          //     "sumaAsegurada": "Amparada",
          //     "deducible": "Amparada"
          //   });
          //   console.log(this.dataArrayCotizacion);
          //   console.log(this.datosPDF);
          // }
          this.vehicleDataText = `${data["vehiculo"]["marca"]} ${data["vehiculo"]["modelo"]} ${data["vehiculo"]["descripcion"]}`;

          if (this.tipo === 'interno') {

            if ( data["codigoError"] !== "") {
              // si no existe el auto mandara la pantalla donde se muestra datos del vehiculo a cotizar
              // error , no existe el auto
              // no existe auto
              this.swalCargandoNoExisteAuto(data["codigoError"]);
              this.router.navigate([`modulos/venta-nueva/cotizador-vn/${this.idProspecto}`]);
              this.solictudesVnService.getSolicitudesById(this.idSolicitudD).subscribe({
                next: value => {
                  this.nombreCliente = value.nombreCliente;
                  this.dialog.open(ModalCotizacionComponent, {
                    width: 'auto',
                    panelClass: 'modalCotizacion',
                    data: {
                      nombreAseguradora: this.nombreAseguradora,
                      urlServidor: this.urlServidor,
                      tipo: this.tipo,
                      idProspecto: this.idProspecto,
                      urlCatalogo: this.urlCatalogo,
                      urlCotizacion: this.urlCotizacion,
                      idTipoModificar: '3',
                      idSolicitudDetalles: this.idSolicitudD,
                      jsonCotizacion: this.cotizacion,
                      subMarca: this.subMarca,
                      nombreCliente: this.nombreCliente
                    },
                  });
                }
              });
            }
            console.log(data["codigoError"])


          }

          if (this.tipo === 'contactado') {
            if (data["codigoError"] !== "") {
              // no existe auto
              this.swalCargandoNoExisteAuto(data["codigoError"]);
              this.router.navigate(['/modulos/venta-nueva/recotizador-vn/' + this.tipo + '/' + this.idProspecto + '/' + 2
              + '/' + this.idSolicitudD ]);
              this.solictudesVnService.getSolicitudesById(this.idSolicitudD).subscribe({
                next: value => {
                  this.nombreCliente = value.nombreCliente;
                  this.dialog.open(ModalCotizacionComponent, {
                    width: 'auto',
                    panelClass: 'modalCotizacion',
                    data: {
                      nombreAseguradora: this.nombreAseguradora,
                      urlServidor: this.urlServidor,
                      tipo: this.tipo,
                      idProspecto: this.idProspecto,
                      urlCatalogo: this.urlCatalogo,
                      urlCotizacion: this.urlCotizacion,
                      idTipoModificar: '3',
                      idSolicitudDetalles: this.idSolicitudD,
                      jsonCotizacion: this.cotizacion,
                      subMarca: this.subMarca,
                      nombreCliente: this.nombreCliente
                    },
                  });
                }
              });

            }
          }
        },
        (error) => {
          console.error('Error => ', error);

          if (this.tipo === 'interno') {
            // error en el servidor de web externo
            this.swalCargandoErrorservidor();
            this.router.navigate([`modulos/venta-nueva/cotizador-vn/${this.idProspecto}`]);
            this.solictudesVnService.getSolicitudesById(this.idSolicitudD).subscribe({
              next: value => {
                this.nombreCliente = value.nombreCliente;
                this.dialog.open(ModalCotizacionComponent, {
                  width: 'auto',
                  panelClass: 'modalCotizacion',
                  data: {
                    nombreAseguradora: this.nombreAseguradora,
                    urlServidor: this.urlServidor,
                    tipo: this.tipo,
                    idProspecto: this.idProspecto,
                    urlCatalogo: this.urlCatalogo,
                    urlCotizacion: this.urlCotizacion,
                    idTipoModificar: '3',
                    idSolicitudDetalles: this.idSolicitudD,
                    jsonCotizacion: this.cotizacion,
                    subMarca: this.subMarca,
                    nombreCliente: this.nombreCliente
                  },
                });
              }
            });
          }

          if (this.tipo === 'contactado') {
            // error en el servidor de web externo
            this.swalCargandoErrorservidor();
            this.router.navigate(['/modulos/venta-nueva/recotizador-vn/' + this.tipo + '/' + this.idProspecto + '/' + 2
            + '/' + this.idSolicitudD ]);
            this.solictudesVnService.getSolicitudesById(this.idSolicitudD).subscribe({
              next: value => {
                this.nombreCliente = value.nombreCliente;
                this.dialog.open(ModalCotizacionComponent, {
                  width: 'auto',
                  panelClass: 'modalCotizacion',
                  data: {
                    nombreAseguradora: this.nombreAseguradora,
                    urlServidor: this.urlServidor,
                    tipo: this.tipo,
                    idProspecto: this.idProspecto,
                    urlCatalogo: this.urlCatalogo,
                    urlCotizacion: this.urlCotizacion,
                    idTipoModificar: '3',
                    idSolicitudDetalles: this.idSolicitudD,
                    jsonCotizacion: this.cotizacion,
                    subMarca: this.subMarca,
                    nombreCliente: this.nombreCliente
                  },
                });
              }
            });
          }


        },
        () => {
          this.setValue(0);
        }
      );
    // mandar valores a al
    console.log(this.imageSRC);
    // this.pdfhijo.ngOnInit();
    // setTimeout(() => {
    //   this.printPDF(0, 'amplia', 'anual');
    // }, 15000);
  }

  setValue(event) {
   // dependiendo de la cobertura pintara los datos
    this.coberturaTipo = event;
    console.log(event, 'evento');
    // this.ngOnInit();
    this.selected.setValue(event);
    // ma


    this.tabSelectedText = `Cobertura ${this.tabs[this.selected.value]}`;

    this.coberturasTabla = [];
    console.log('Data Array Cotizacion => ', this.dataArrayCotizacion);
    // this.primaTotal = Math.round(this.dataArrayCotizacion["cotizacion"][0]["amplia"][0]["anual"]["primaTotal"]);
    const coverages = this.dataArrayCotizacion["coberturas"];
    switch (this.nombreAseguradorab) {
      case 'QUALITAS':
    // } else {

      switch (event) {
        case 0:
          this.coberturasTabla = this.coberturaAmplia;
          break;
        case 1:
          this.coberturasTabla = this.coberturaLimitada;
          break;
        case 2:
          this.coberturasTabla = this.coberturaRC;
          break;
      }

      console.log(this.coberturasTabla);
    // }
      break;
      case 'GNP' :
        switch (event) {
          case 0:
            this.coberturasTabla = coverages[0]["amplia"];
            break;
          case 1:
            this.coberturasTabla = coverages[1]["limitada"];
            break;
          case 2:
            this.coberturasTabla = coverages[2]["rc"];
            break;
        }
        break;

      case 'MAPFRE' :
        switch (event) {
          case 0:
            this.coberturasTabla = coverages[0]["amplia"];
            break;
          case 1:
            this.coberturasTabla = coverages[1]["limitada"];
            break;
          case 2:
            this.coberturasTabla = coverages[2]["rc"];
            break;
        }
        break;

      default:

        break;
    }
    this.primaTotal = this._primaTotal(this.selected.value,
      this.tabs[this.selected.value].toLowerCase(), 0, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());
    this.primerPago = this._primerPago(this.selected.value,
      this.tabs[this.selected.value].toLowerCase(), 0, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());

    this.pagoSubsecuente = this._pagoSubsecuente(this.selected.value,
      this.tabs[this.selected.value].toLowerCase(), 0, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());

    // mandar valores a al component pdf
    console.log(this.tabs[this.selected.value].toLowerCase());
    console.log(this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());
    this.pdfhijo.conseguirdatos(this.datosPDF, event, this.tabs[this.selected.value].toLowerCase(),
      this.imageSRC, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());
    // this.printPDF(event, this.tabs[this.selected.value].toLowerCase(), this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());
  }

  changeSelectedMethod(idMethod: number) {
    // cambiara datos de la prima o primer pago dependiento de la periocidad , anual, trimestras etc.
    this.methodSelected = idMethod;
    this.tablaComponent.inicio();

    // console.log(this.methodsPayment[this.methodSelected].text)
    // si es anual oculta el cuadro de pagosubsecuentes, si es semestral o trimestral lo muestra
    if (this.methodsPayment[this.methodSelected].text === 'Anual'){
      this.ocultarPagosSubsecuentes = true;
    } else {
      this.ocultarPagosSubsecuentes = false;
    }


    // this.router.navigateByUrl('')
    // si se cambia la cobertura se mostrara la prima, el primer pago y el pago subsecuente para trimestral o semestral
    this.primaTotal = this._primaTotal(this.selected.value,
      this.tabs[this.selected.value].toLowerCase(), 0, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());

    this.primerPago = this._primerPago(this.selected.value,
      this.tabs[this.selected.value].toLowerCase(), 0, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());

    this.pagoSubsecuente = this._pagoSubsecuente(this.selected.value,
      this.tabs[this.selected.value].toLowerCase(), 0, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());

    // mandar valores a al component pdf manda anual o semestral
    this.pdfhijo.conseguirPago(this.datosPDF, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase(), this.tabs[this.selected.value].toLowerCase(), this.selected.value, this.imageSRC );
    // this.printPDF(0, this.tabs[this.selected.value].toLowerCase(),this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());
  }

  private _primaTotal(indexCobertura: number, cobertura: string, indexMetodoPago: number, metodoPago: string): number {
    let primaTotalCalculo: number = Math.round(
      this.dataArrayCotizacion["cotizacion"][indexCobertura][cobertura][indexMetodoPago][metodoPago]["primaTotal"]
    );
    return primaTotalCalculo;
  }
  private _primerPago(indexCobertura: number, cobertura: string, indexMetodoPago: number, metodoPago: string): number {
    let primerPago: number = Math.round(
      this.dataArrayCotizacion["cotizacion"][indexCobertura][cobertura][indexMetodoPago][metodoPago]["primerPago"]
    );
    return primerPago;
  }
  private _pagoSubsecuente(indexCobertura: number, cobertura: string, indexMetodoPago: number, metodoPago: string): number {
    let pagoSubsecuente: number = Math.round(
      this.dataArrayCotizacion["cotizacion"][indexCobertura][cobertura][indexMetodoPago][metodoPago]["pagosSubsecuentes"]
    );
    return pagoSubsecuente;


  }
crearEmision(){
 try {
   this.cotizar();
 } catch (error){
   console.error(error);
   this.ocultarBoton = true;
 }
}
  cotizar() {
    this.ocultarBoton = false;
    let index;
    let valor;
    this.coberturas = [];
    console.log(this.dataRectizacion);
    // se cotiza y se guarda la informacion en la tabla cotizacionAli en respuesta para despues utilizarla para emision
    this.swalCargando();

    let dataCotizacion: any;
    this.cotizacionRequestOnline = new CotizarRequestOnline(this.cotizacion.aseguradora, this.cotizacion.clave,
      this.cotizacion.cp, this.cotizacion.descripcion, this.cotizacion.descuento, this.cotizacion.edad,
      this.cotizacion.fechaNacimiento,
      this.cotizacion.genero, this.cotizacion.marca,
      this.cotizacion.modelo, 'cotizacion', 'AMPLIA', 'PARTICULAR', this.subMarca, this.cotizacion.colonia, this.cotizacion.idColonia);
    this.nombreAseguradora = this.cotizacion.aseguradora.toLowerCase();
    // se obtiene una cotizacion y se guarda en nuestra base de dato
    this.catalogoService
      .getCotizacion(this.urlServidor, this.urlCotizacion, this.cotizacion)
      .subscribe(
        (data) => {
          console.log('QUALITAS DATA => ', data);
          dataCotizacion = data;
          // this.vehicleDataText = `${data["vehiculo"]["marca"]} ${data["vehiculo"]["modelo"]} ${data["vehiculo"]["descripcion"]}`;
        },
        (error) => {
          console.error('Error => ', error);
          this.swalError();
        },
        () => {
          this.dataArrayCotizacion2 = dataCotizacion;
          // this.setValue(0);

          console.log(this.dataArrayCotizacion2);

          this.dataArrayCotizacion3 = dataCotizacion;
          console.log(this.dataArrayCotizacion3);

          // delete this.dataArrayCotizacion3["Coberturas"];


          // PROBAR DESPUES
          this.dataArrayCotizacion3["paquete"] = this.tabs[this.selected.value].toUpperCase();
          this.dataArrayCotizacion3["periodicidadDePago"] = this.methodsPayment[this.methodSelected].text.toUpperCase();
          // //  COTIZACION REAL
          this.dataArrayCotizacion3["cotizacion"] =
            this.dataArrayCotizacion2["cotizacion"][this.selected.value][this.tabs[this.selected.value].toLowerCase()][0]
              [this.methodsPayment[this.methodSelected].text.toLowerCase()];
          // si las coberturas no estan modificadas guarda directamente lo que manda el servicio cotizacion futuro cambios de deducibles
          if (this.dataRectizacion === undefined){
            // this.dataArrayCotizacion3["cotizacion"] =
            //   this.dataArrayCotizacion2["cotizacion"][this.selected.value][this.tabs[this.selected.value].toLowerCase()][0]
            //     [this.methodsPayment[this.methodSelected].text.toLowerCase()];
            this.dataArrayCotizacion3["coberturas"] =
              this.dataArrayCotizacion2["coberturas"][this.selected.value][this.tabs[this.selected.value].toLowerCase()];
            // si se modifica una cobertura llama un servicio especial y se recoge los valores de coberturas y se guardara
            // se cambiara los atributos de ese servico en ingles por español, tambien con cotizacion
            // cuando son coberturas modificadas
          } else {
            for (valor of this.dataRectizacion["coberturas"]) {
              // console.log(valor);
              index = this.dataRectizacion["coberturas"].find(sumaa => sumaa.nombre === valor.nombre);
              // console.log(index);
              // index["sumAssured"] = sumaAsegurada;
              this.coberturas = {
                "nombre": index["nombre"],
                "sumaAsegurada": index["sumaAsegurada"],
                "deducible": index["deducible"]
              };
              // console.log(this.coberturas);
              this.coberturasModificadas.push(this.coberturas);
            }

            this.dataArrayCotizacion3["coberturas"] = this.coberturasModificadas;
            this.dataArrayCotizacion3["cotizacion"] =
              this.dataArrayCotizacion2["cotizacion"] =   {
                   "cotID": "",
                   "verID": "",
                   "cotIncID": "",
                   "derechos": this.dataRectizacion["cotizacion"]["derechos"],
                   "impuesto": this.dataRectizacion["cotizacion"]["impuesto"],
                   "recargos": "",
                   "verIncID": "",
                   "primaNeta": this.dataRectizacion["cotizacion"]["primaNeta"],
                   "resultado": this.dataRectizacion["cotizacion"]["resultado"],
                   "primaTotal": this.dataRectizacion["cotizacion"]["primaTotal"],
                   "primerPago": this.dataRectizacion["cotizacion"]["primerPago"],
                   "idCotizacion": this.dataRectizacion["cotizacion"]["idCotizacion"],
                   "pagosSubsecuentes": this.dataRectizacion["cotizacion"]["pagosSubsecuentes"]
                 };
          }
            // al areglo se le agrega el idSubRamo para identificar
          this.subRamoService.getTipoAndAlias(1, this.cotizacion.aseguradora).subscribe(rsp => {
            if (rsp !== null) {
              // console.log(rsp.id);
              // this.dataArrayCotizacion2['idSubRamo'] = rsp.id;
              this.dataArrayCotizacion3['idSubRamo'] = rsp.id;
            }

            this.cotizacionResponse = this.dataArrayCotizacion3;
            console.log(this.cotizacionResponse);

            // Se hara el lead de solicitud vn
            // si la solicitud es contactado se recotizara si no se recotiza se hara un nuevo lead
            if (this.idTipoModificar === 2){
              this.reCotizar();
            } else {
              this.crearEmisionn();
            }
          }, error => {
            this.cotizacionResponse.push(this.dataArrayCotizacion3);
            console.log(this.cotizacionResponse);
          });


        }
      );
  }

  crearEmisionn() {
    let idSolicitud: number;

    // nuevo
    // se hara un lead
    // se hara una nueva solicitud vn y se direccionara al step correspondiente para llenar datos cliente,
    // vehiculo y poder emitir y pagar
    this.datosSolicitudes.cotizacionRequestOnline = this.cotizacionRequestOnline;
    this.datosSolicitudes.cotizacionResponse = this.cotizacionResponse;
    this.datosSolicitudes.idEmpleado = window.sessionStorage.getItem('Empleado');
    this.datosSolicitudes.idTipoContacto = 3;
    this.datosSolicitudes.idProspecto  = this.idProspecto;
    this.datosSolicitudes.idSubRamo = this.cotizacionResponse.idSubRamo;
    // console.log(this.cotizacionResponse)
    // console.log(this.cotizacionResponse[0]);
    // console.log(this.cotizacionResponse.idSubRamo);
    // servicio que creara un productoSolicitud del producto hara una cotizacion - creara una cotizacionALI
    // donde guardara la peticion y la respuesta de la cotizacion, de la cotAli ara una solictud Vn
    this.productoSolicitudService.postProductoOnline(this.datosSolicitudes).subscribe({
      next: (result) => {
        console.log('----------', this.datosSolicitudes);
        this.idSolicitud = result.idSolicitud;
        console.log(this.idSolicitud);
        // solicitudes sockets
      },
      complete: () => {

        switch (this.nombreAseguradorab) {
          case 'QUALITAS':

            // si el ejecutivo escoge cotizar por inspeccion previa se hace un dato de ticket vacio para seleccionarlo como bandera
            // la idSolicitud en la tabla ticketinspeccion como bandera 1 para saber si es por inspeccion
            if ( this.banderaCotizacionInspeccion === true) {
              // se hace un archivo sin ticket ni status
              const tickes = {
                id: 0,
                idEmpleado: Number(sessionStorage.getItem('Empleado')),
                solicitudVn: this.idSolicitud,
                ticket: 0,
                status: 0,
                inspeccionPrevia: 1
              };
              this.ticketsInspeccionService.post(tickes).subscribe(
                dataTicket => {
                  return dataTicket;
                }, () => {},
                () => {
                  this.router.navigate(['modulos/venta-nueva/step-online/' + this.idSolicitud + '/'
                  + this.nombreAseguradorab ]);
                });
              //  si escge cotizacion normal a todas los tickt le pone 0 al campo inspeccion previa
            } else if (this.banderaCotizacionInspeccion === false) {
              this.ticketsInspeccionService.putInpeccionPreviadesactivado(this.idSolicitudD).subscribe(
                dataTicket => {
                  return dataTicket;
                }, () => {
                  console.log('no hay ticketsolicitud');
                  this.router.navigate(['modulos/venta-nueva/step-online/' + this.idSolicitud + '/'
                  + this.nombreAseguradorab ]);
                }, () => {
                  this.router.navigate(['modulos/venta-nueva/step-online/' + this.idSolicitud + '/'
                  + this.nombreAseguradorab ]);
                });

            }
            /*this.router.navigate(['modulos/venta-nueva/step-online/' + this.idSolicitud + '/'
            + this.nombreAseguradorab ]);*/
            break;
          case 'GNP' :
            this.router.navigate(['modulos/venta-nueva/step-online/' + this.idSolicitud + '/'
            + this.nombreAseguradorab ]);
            break;
          case 'MAPFRE' :
            this.router.navigate(['modulos/venta-nueva/step-onlineMapfre/' + this.idSolicitud + '/'
            + this.nombreAseguradorab ]);
            break;
          default:

            break;
        }
      },
      error: e => {
        this.swalError();
      }
    });
  }
  swalCargando() {
    Swal.fire({
      title: 'Se está actualizando la información, espere un momento.',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  }
  swalError() {
    Swal.fire({
      title: 'Hubo un error al actualizar la información',
      text: 'Favor de contactar a TI',
    }).then((result) => {
      if (result.value) {
        window.close();
        this.ocultarBoton = true;
      }
    });
  }
  swalCargandoNoExisteAuto(text?: string) {
    Swal.fire({
      title: 'Error en datos cotizados',
      text: 'Error: ' + text,
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 3000);
  }
  swalCargandoErrorservidor() {
    Swal.fire({
      title: 'POR EL MOMENTO NO SE CUENTA CON LA OFERTA SOLICITADA',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 3000);
  }
  // vuelve a recotizar
  // De la tabla contactados se tomara un lead y volvera a poner datos del vehiculo y recotizara datos y creara
  // un lead
  reCotizar() {
    let infoCliente: StepsCotizador;
    let productoCliente;
    console.log( 'se recotizo');
    // console.log(this.idSolicitudD);

    this.stepsService.getByIdOnline(this.idSolicitudD).subscribe(
      data => {
        infoCliente = data[0];

        // si existe producto cliente llenara los datos con  el numero de placas, serie y motor y datos del auto
        if (infoCliente.idProductoCliente !== null) {



          this.productoClienteService.getProductoClienteById(infoCliente.idProductoCliente).subscribe(dataPC => {

            this.jsonDatos = dataPC;
            this.responseProducto = this.jsonDatos.datos;
            console.log(this.jsonDatos);
            console.log(this.jsonDatos['numeroPlacas']);

            this.datos = this.cotizacion;

            this.datos['numeroPlacas'] = this.responseProducto.numeroPlacas;
            this.datos['numeroMotor'] = this.responseProducto.numeroMotor;
            this.datos['numeroSerie'] = this.responseProducto.numeroSerie;

            // this.datos['marca'] = this.cotizacion.marca;
            // this.datos['modelo'] = this.cotizacion.modelo;
            this.datos['subMarca'] = this.subMarca;
            // this.datos['descripcion'] = this.cotizacion.descripcion;
            productoCliente = {
              id: dataPC.id,
              idSolictud: dataPC.idSolictud,
              idCliente: dataPC.idCliente,
              datos: this.datos,
              idSubRamo: dataPC.idSubRamo,
            };
            this.productoClienteService.put(dataPC.id, productoCliente).subscribe({
              error: () => {
                this.swalError();
              },
              complete: () => {

              },
            });
          })

        }

        // modifica producto solicitud con los datos de cotizado nuevo
        this.solictudesVnService.getSolicitudesByIdOnline(this.idSolicitudD).subscribe(data => {

          this.productoSolicitudService.getProductoSolicitudOnline(data.idProducto).subscribe(dataP => {

            this.productoSolicitudFormM.id = dataP[0].id;
            this.productoSolicitudFormM.idProspecto = dataP[0].idProspecto;
            this.productoSolicitudFormM.datos = this.cotizacionRequestOnline;
            this.productoSolicitudFormM.idTipoSubRamo = dataP[0].idTipoSubRamo;

            this.productoSolicitudService.putOnline(dataP[0].id, this.productoSolicitudFormM).subscribe(dataPro => {
            }, (error) => {
              console.error('Error =>', error);
              this.swalError();
            });
          });
          // modifica la peticion de cotizacion ali, le deja datos que tenia y area los nuevos de cotizacion auto
          this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
            this.cotizacionAli = dataAli;
            delete this.peticionAli['cp'];
            delete this.peticionAli['edad'];
            delete this.peticionAli['clave'];
            delete this.peticionAli['marca'];
            delete this.peticionAli['genero'];
            delete this.peticionAli['modelo'];
            delete this.peticionAli['paquete'];
            delete this.peticionAli['servicio'];
            delete this.peticionAli['subMarca'];
            delete this.peticionAli['descuento'];
            delete this.peticionAli['movimiento'];
            delete this.peticionAli['aseguradora'];
            delete this.peticionAli['descripcion'];
            delete this.peticionAli['fechaNacimiento'];
            delete this.cotizacionAli['idSubRamo'];

            this.peticionAli['cp'] = this.cotizacionRequestOnline.cp;
            this.peticionAli['edad'] = this.cotizacionRequestOnline.edad;
            this.peticionAli['clave'] = this.cotizacionRequestOnline.clave;
            this.peticionAli['marca'] = this.cotizacionRequestOnline.marca;
            this.peticionAli['genero'] = this.cotizacionRequestOnline.genero;
            this.peticionAli['modelo'] = this.cotizacionRequestOnline.modelo;
            this.peticionAli['paquete'] = this.cotizacionRequestOnline.paquete;
            this.peticionAli['servicio'] = this.cotizacionRequestOnline.servicio;
            this.peticionAli['subMarca'] = this.cotizacionRequestOnline.subMarca;
            this.peticionAli['descuento'] = this.cotizacionRequestOnline.descuento;
            this.peticionAli['movimiento'] = this.cotizacionRequestOnline.movimiento;
            this.peticionAli['aseguradora'] = this.cotizacionRequestOnline.aseguradora;
            this.peticionAli['descripcion'] = this.cotizacionRequestOnline.descripcion;
            this.peticionAli['fechaNacimiento'] = this.cotizacionRequestOnline.fechaNacimiento;
            this.peticionAli['colonia'] = this.cotizacionRequestOnline.colonia;
            this.peticionAli['idColonia'] = this.cotizacionRequestOnline.idColonia;
            this.cotizacionAli['peticion'] = this.peticionAli;

            switch (this.nombreAseguradorab) {
          case 'QUALITAS':
            this.cotizacionAli['idSubRamo'] = 35;
            break;
          case 'GNP' :
            this.cotizacionAli['idSubRamo'] = 40;
            break;
            case 'MAPFRE' :
              this.cotizacionAli['idSubRamo'] = 43;
              break;
          default:

            break;
        }
        /// aqui
            this.cotizacionAli['respuesta'] = this.cotizacionResponse;
            console.log(this.cotizacionAli);

            this.peticionAli['cp'] = this.cotizacionRequestOnline.cp;
            this.peticionAli['edad'] = this.cotizacionRequestOnline.edad;
            this.peticionAli['clave'] = this.cotizacionRequestOnline.clave;
            this.peticionAli['marca'] = this.cotizacionRequestOnline.marca;
            this.peticionAli['genero'] = this.cotizacionRequestOnline.genero;
            this.peticionAli['modelo'] = this.cotizacionRequestOnline.modelo;
            this.peticionAli['paquete'] = this.cotizacionRequestOnline.paquete;
            this.peticionAli['servicio'] = this.cotizacionRequestOnline.servicio;
            this.peticionAli['subMarca'] = this.cotizacionRequestOnline.subMarca;
            this.peticionAli['descuento'] = this.cotizacionRequestOnline.descuento;
            this.peticionAli['movimiento'] = this.cotizacionRequestOnline.movimiento;
            this.peticionAli['aseguradora'] = this.cotizacionRequestOnline.aseguradora;
            this.peticionAli['descripcion'] = this.cotizacionRequestOnline.descripcion;
            this.peticionAli['fechaNacimiento'] = this.cotizacionRequestOnline.fechaNacimiento;
            this.peticionAli['colonia'] = this.cotizacionRequestOnline.colonia;
            this.peticionAli['idColonia'] = this.cotizacionRequestOnline.idColonia;
            this.cotizacionAli['peticion'] = this.peticionAli;
            /// aqui
            this.cotizacionAli['respuesta'] = this.cotizacionResponse;
            console.log(this.cotizacionAli)

            this.cotizacionesAliService.putOnline(data.idCotizacionAli, this.cotizacionAli).subscribe( result => {
              },
              error => { this.swalError();
              },
              () => {
                if (this.tipo === 'contactado') {

                  // si recotiza por inspeccion previa
                  if ( this.banderaCotizacionInspeccion === true) {
                    // se hace un archivo sin ticket ni status
                    const tickes = {
                      id: 0,
                      idEmpleado: Number(sessionStorage.getItem('Empleado')),
                      solicitudVn: this.idSolicitudD,
                      ticket: 0,
                      status: 0,
                      inspeccionPrevia: 1
                    };
                    this.ticketsInspeccionService.post(tickes).subscribe(
                      dataTicket => {
                        return dataTicket;
                      }, (error) => {
                        console.error('Error => ', error);
                        this.swalError();
                      },
                      () => {
                        switch (this.nombreAseguradorab) {
                          case 'QUALITAS':
                            this.router.navigate(['modulos/venta-nueva/step-online/' + this.idSolicitudD + '/'
                            + this.nombreAseguradorab ]);
                            break;
                          case 'GNP' :
                            this.router.navigate(['modulos/venta-nueva/step-online/' + this.idSolicitudD + '/'
                            + this.nombreAseguradorab ]);
                            break;
                          default:

                            break;
                        }
                      });
                  } else if (this.banderaCotizacionInspeccion === false) {
                    this.ticketsInspeccionService.putInpeccionPreviadesactivado(this.idSolicitudD).subscribe(
                      dataTicket => {
                        return dataTicket;
                      }, () => {
                        console.log('no hay ticketsolicitud');
                        this.swalError();
                      }, () => {
                        switch (this.nombreAseguradorab) {
                          case 'QUALITAS':
                            this.router.navigate(['modulos/venta-nueva/step-online/' + this.idSolicitudD + '/'
                            + this.nombreAseguradorab ]);
                            break;
                          case 'GNP' :
                            this.router.navigate(['modulos/venta-nueva/step-online/' + this.idSolicitudD + '/'
                            + this.nombreAseguradorab ]);
                            break;
                          case 'MAPFRE' :
                            this.router.navigate(['modulos/venta-nueva/step-onlineMapfre/' + this.idSolicitudD + '/'
                            + this.nombreAseguradorab ]);
                            break;
                          default:

                            break;
                        }
                      });

                  }

                }

                if (this.tipo === 'interno') {

                  // si recotiza por inspeccion previa
                  if ( this.banderaCotizacionInspeccion === true) {
                    // se hace un archivo sin ticket ni status
                    const tickes = {
                      id: 0,
                      idEmpleado: Number(sessionStorage.getItem('Empleado')),
                      solicitudVn: this.idSolicitudD,
                      ticket: 0,
                      status: 0,
                      inspeccionPrevia: 1
                    };
                    this.ticketsInspeccionService.post(tickes).subscribe(
                      dataTicket => {
                        return dataTicket;
                      }, (error) => {
                        console.error('Error => ', error);
                        this.swalError();
                      },
                      () => {

                      });
                  } else if (this.banderaCotizacionInspeccion === false) {
                    this.ticketsInspeccionService.putInpeccionPreviadesactivado(this.idSolicitudD).subscribe(
                      dataTicket => {
                        return dataTicket;
                      }, () => {
                        console.log('no hay ticketsolicitud');
                        this.swalError();
                      });

                  }
                  switch (this.nombreAseguradorab) {
                    case 'QUALITAS':
                      this.router.navigate(['modulos/venta-nueva/step-online/' + this.idSolicitudD + '/'
                      + this.nombreAseguradorab ]);
                      break;
                    case 'GNP' :
                      this.router.navigate(['modulos/venta-nueva/step-online/' + this.idSolicitudD + '/'
                      + this.nombreAseguradorab ]);
                      break;
                    case 'MAPFRE' :
                      this.router.navigate(['modulos/venta-nueva/step-onlineMapfre/' + this.idSolicitudD + '/'
                      + this.nombreAseguradorab ]);
                      break;
                    default:

                      break;
                  }
                }

              });
          });
        }); // fin de solicitud


      },
      () => {
        this.swalError();
      });
  }
  // futuro llamar un servicio para poder modificar las coberturas, los deducibles no borar
  botonRecotizar(value: any){
    this.dataReCotizacion = value;
    this.mostraBoton = false;
  }
  recotizarCobertura(value: any){
    Swal.fire({
      title: 'Recotizando, por favor espere',
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    let indexRobo;
    // llamar servicio externo para modificar sus coberturas reales
    console.log(value);
    // toma las coberturas
    this.coberturasEnviar = value;
    // encuentra la posicion de robo parcial , si lo encuentra a la propiedad sumaAsegurada le cambia a 1
    // para que el equipo externo tome esa bandera y haga el descuento correspondiente
    indexRobo = value.dataVehiclem.findIndex(sumaa => sumaa.name === 'Robo Parcial');
    if (indexRobo === -1){
    }else {
      value.dataVehiclem[indexRobo]["deductible"] = "25";
    }//
    console.log(value.dataVehiclem);
    this.dataRectizacion = {
      "insuranceCarrier": this.cotizacion.aseguradora,
      "key": this.cotizacion.clave,
      "cp": this.cotizacion.cp,
      "description": this.cotizacion.descripcion,
      "discount":  this.cotizacion.descuento,
      "age": this.cotizacion.edad,
      "dateOfBirth": this.cotizacion.fechaNacimiento,
      "gender": this.cotizacion.genero,
      "brand": this.cotizacion.marca,
      "model": this.cotizacion.modelo,
      "packet":  this.tabs[this.selected.value].toUpperCase(),
      "periodicity": this.methodsPayment[this.methodSelected].text.toUpperCase(),
      "coverages":
       value.dataVehiclem
    };
    console.log(this.dataRectizacion);
    //
    // se mostrara la informacion de la cotizacion seleccionada del auto
    // let dataCotizacion: any;
    //
    this.catalogoService
      .getCotizacionCoberturas(this.urlServidor, this.urlModificacionCoberturas, this.dataRectizacion)
      .subscribe(
        (data) => {
          console.log('QUALITAS DATA => ', data);
          this.dataRectizacion = data;
          this.banderaModificacionCoberturas = true;
          // se mostrara la primaTotal correspondiente a las coberturas modificadas
          this.primaTotal = this.dataRectizacion["cotizacion"]["primaTotal"];
         // this.vehicleDataText = `${data["vehiculo"]["marca"]} ${data["vehiculo"]["modelo"]} ${data["vehiculo"]["descripcion"]}`;
          this.primerPago = this.dataRectizacion["cotizacion"]["primerPago"];
          this.pagoSubsecuente = this.dataRectizacion["cotizacion"]["pagosSubsecuentes"];
          this.datosPDF["cotizacion"][this.coberturaTipo][this.tabs[this.selected.value].toLowerCase()][0]
            [this.methodsPayment[this.methodSelected].text.toLocaleLowerCase()] = this.dataRectizacion["cotizacion"];
          this.datosPDF["coberturas"][this.coberturaTipo][this.tabs[this.selected.value].toLowerCase()] = this.dataRectizacion["coberturas"];
        },
        (error) => {
          console.error('Error => ', error);
          Swal.fire({
            title: '¡Error al cambiar coberturas',
          });
        },
        () => {
          this.pdfhijo.conseguirdatos(this.datosPDF, this.coberturaTipo, this.tabs[this.selected.value].toLowerCase(),
            this.imageSRC, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());
          Swal.fire({
            // type: 'info',
            title: '¡Coberturas actualizadas exitosamente!',
          });
          this.mostraBoton = true;
          // this.dataArrayCotizacion = dataCotizacion;
          // this.setValue(0);
        }
      );
  }
  intab(event){
    console.log(event);
    if (event.index === 0 ){
      this.tablaComponent.inicio();
    }
    // if (event.index === 1 ){
    //   this.tablaComponent.inicio();
    // }
  }
  // cotizacionInspeccion(){
  //   // si el ejecutivo manda a llamar este boton llamara el servicio de  cotizacion con descuento de inspeccion previa
  //   this.banderaCotizacionInspeccion = true;
  //   this.dataArrayCotizacion = null;
  //   // this.ngOnInit();
  //   // this.inicio();
  // }
  // vuelve a cotizar con 23 % qualitas
  cotizacionNormal(){
    this.banderaCotizacionInspeccion = false;
    this.dataArrayCotizacion = null;
    this.mostraBoton = true;
    this.ngOnInit();
  }
// vuelve a cotizar con inspeccion previa 30 % qualitas
  cotizacionInspeccion(){
    this.banderaCotizacionInspeccion = true;
    // si data se pone nulo vuelve a poner la loader de cotizar
    this.dataArrayCotizacion = null;
    this.mostraBoton = true;
    this.ngOnInit();
  }
}
