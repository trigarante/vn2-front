import {Component, Input, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {ClienteVn} from '../../../../../@core/data/interfaces/venta-nueva/cliente-vn';
import {MatCheckbox} from '@angular/material/checkbox';
import {Recibos} from '../../../../../@core/data/interfaces/venta-nueva/recibos';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {TipoPagoVn} from '../../../../../@core/data/interfaces/venta-nueva/catalogos/tipo-pago-vn';
import {Periodicidad} from '../../../../../@core/data/interfaces/venta-nueva/catalogos/periodicidad';
import {
  SociosComercial,
  SociosComercialOnline
} from '../../../../../@core/data/interfaces/comerciales/socios-comercial';
import {Ramo} from '../../../../../@core/data/interfaces/comerciales/ramo';
import {SubRamo} from '../../../../../@core/data/interfaces/comerciales/sub-ramo';
import {ProductoNameCorto, ProductoSocios} from '../../../../../@core/data/interfaces/comerciales/producto-socios';
import moment from 'moment';
import {CotizacionesAli} from '../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';
import {TipoSubramoService} from '../../../../../@core/data/services/comerciales/catalogo/tipo-subramo.service';
import {ProductoSociosService} from '../../../../../@core/data/services/comerciales/producto-socios.service';
import {TipoPagoVnService} from '../../../../../@core/data/services/venta-nueva/catalogos/tipo-pago-vn.service';
import {RegistroService} from '../../../../../@core/data/services/venta-nueva/registro.service';
import {SolicitudesvnService} from '../../../../../@core/data/services/venta-nueva/solicitudes.service';
import {ProductoClienteService} from '../../../../../@core/data/services/venta-nueva/producto-cliente.service';
import {RecibosService} from '../../../../../@core/data/services/venta-nueva/recibos.service';
import {SociosService} from '../../../../../@core/data/services/comerciales/socios.service';
import {RamoService} from '../../../../../@core/data/services/comerciales/ramo.service';
import {SubRamoService} from '../../../../../@core/data/services/comerciales/sub-ramo.service';
import {DriveService} from '../../../../../@core/data/services/ti/drive.service';
import {PeriodicidadService} from '../../../../../@core/data/services/venta-nueva/catalogos/periodicidad.service';
import {WhatsappMsgService} from '../../../../../@core/data/services/whatsappMsg/whatsapp-msg.service';
import {ClienteVnService} from '../../../../../@core/data/services/venta-nueva/cliente-vn.service';
import {ActivatedRoute} from '@angular/router';
import {CotizacionesAliService} from '../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {CatalogoService} from '../../../../../@core/data/services/cotizador/catalogo.service';
import {MatDialog} from '@angular/material/dialog';
import {SepomexService} from '../../../../../@core/data/services/catalogos/sepomex.service';
import {HttpClient} from '@angular/common/http';
import {PagosService} from '../../../../../@core/data/services/venta-nueva/pagos.service';
import {urlAseguradoras} from '../../../../../@core/data/interfaces/cotizador/urlAseguradoras';
import Swal from 'sweetalert2';
import {map} from 'rxjs/operators';
import {Pagos} from '../../../../../@core/data/interfaces/venta-nueva/pagos';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import {ProductoCliente} from '../../../../../@core/data/interfaces/venta-nueva/producto-cliente';
import {
  MessagePagoSuccessComponent
} from '../../../../../shared/componentes/modal-pagos-online-new/componentes/message-pago-success/message-pago-success.component';



@Component({
  selector: 'app-registro-poliza-online-new',
  templateUrl: './registro-poliza-online-new.component.html',
  styleUrls: ['./registro-poliza-online-new.component.scss']
})
export class RegistroPolizaOnlineNewComponent implements OnInit {
  @Input() idProductoCliente: number;
  @Input() idRegistroPoliza: number;
  @Input() idCliente: number;
  @Input() datosCliente: ClienteVn;
  @Input() filesToUploadPagado: FileList;
  @Output() idRegistroPolizaSaliente = new EventEmitter<number>();
  @Output() nextStepper = new EventEmitter<number>();
  @Output() Inspeccione = new EventEmitter<number>();
  @ViewChild('checkin') checkin: MatCheckbox;

  registroForm: FormGroup = new FormGroup({
    idTipoPago: new FormControl('', Validators.required),
    idSocio: new FormControl('', Validators.required),
    idRamo: new FormControl('', Validators.required),
    idSubRamo: new FormControl('', Validators.required),
    idProductoSocio: new FormControl('', Validators.required),
    poliza: new FormControl('', [Validators.required]),
    oficina: new FormControl(''),
    fechaInicio: new FormControl(new Date()),
    fechaFin: new FormControl(''),
    primaNeta: new FormControl('', [Validators.required, Validators.pattern('[0-9.]+')]),
    primerPago: new FormControl(''),
    periodo: new FormControl('', Validators.required),
  });
  numeroGuion: RegExp = /[Aa-zZ0-9-]/;
  desactivarBotones = false;
  tipoPagos: TipoPagoVn[];
  periodicidad: Periodicidad[];
  socios: SociosComercial[];
  sociosOnline: SociosComercialOnline[];
  ramos: Ramo[];
  subRamos: SubRamo[];
  productosSocio: ProductoSocios[];
  productoSocioName: ProductoNameCorto;
  productoCliente: ProductoCliente;
  urlPreventa: any;
  date = moment();
  filesToUpload: FileList;
  filesToUploadPagootro: FileList;
  archivoValido = false;
  numeroPagos: number;
  instrucciones = 'INTRODUCE LOS SIGUIENTES DATOS CORRECTAMENTE:';
  /** FUNCIONA CON EL LOADER DE LOS BOTONES QUE LLEVAN UNA PETICIÓN **/
  gruposCargados = false;
  private textError = 5;
  btnLoad = false;
  btnLoadP = false;
  cantidad: number;
  registroEnUno: any;
  socio: SociosComercial;
  socioOnline: SociosComercialOnline;
  idSolicitud: number;
  clienteParaWhats: ClienteVn;
  // emision
  cotizacionAli: CotizacionesAli;
  responseEmision: any;
  x: string;
  ocultarFormulario = false;
  // pago
  pago: any;
  datosPago: any;
  valuePdfPago: any;
  recibo: Recibos;
  idCarpetaDrive: string;
  filesToUploadCliente: FileList;
  filesToUploadPago: FileList;
  archivoValidoCliente = false;
  archivoValidoPago = false;
  nombreAseguradora: string;
  extensionesDeArchivoAceptadas = ['application/pdf'];
  service: any;
  urlService: string;
  urldocumentos: string;
  responsedocumentosPDF: any;
  ocultarboton = true;

  constructor(private subramoServices: TipoSubramoService,
              private productoSocioService: ProductoSociosService,
              private tipoPagoService: TipoPagoVnService,
              private registroService: RegistroService,
              private solicitudesService: SolicitudesvnService,
              private productoClienteService: ProductoClienteService,
              private recibosService: RecibosService,
              private sociosService: SociosService,
              private ramoService: RamoService,
              private subRamoService: SubRamoService,
              private driveService: DriveService,
              private periodicidadService: PeriodicidadService,
              private whatsMsgService: WhatsappMsgService,
              private clienteService: ClienteVnService,
              private route: ActivatedRoute,
              private cotizacionesAliService: CotizacionesAliService,
              private catalogoService: CatalogoService,
              private matDialog: MatDialog,
              private sepomexServicio: SepomexService,
              private httpClient: HttpClient,
              private router: ActivatedRoute,
              private pagosService: PagosService,
              private notificaciones: NotificacionesService,
              private notificacionesService: NotificacionesService
              ) {

    this.nombreAseguradora = this.router.snapshot.paramMap.get('nombreAseguradora');
    this.service = urlAseguradoras.find(aseg => aseg.aseguradora === this.nombreAseguradora);
    this.urlService = this.service.service;
    this.urldocumentos = this.service.urldocumentos;
  }

  ngOnInit(): void {
    // este componente entra en gnp, se paga y despues entra a registrar poliza
    this.idSolicitud = Number(this.route.snapshot.paramMap.get('id_solicitud'));

    // this.productoClienteService.returnAseguradora().subscribe(
    //   result => {
    //     this.urlPreventa = result;
    //   });
    this.getTipoPago();
    this.getPeriodicidad();
    // this.getFlujoPoliza();
    this.getSocios();
    if (this.idRegistroPoliza !== 0) {
      this.getRegistroPoliza();
    }
    if (!this.datosCliente) {
      this.clienteService.getClienteById(this.idCliente).subscribe({
        next: value => {
          this.clienteParaWhats = value;
        },
      });
    }
    // ontener la poliza de los servicio ya obtenidos de pago
    this.obtenerPoliza(this.idSolicitud);
    //  // modificacion obtener el doc de poliza y descargarlo
    //  this.obtenerDocPoliza(this.idSolicitud);
  }

  getTipoPago() {
    this.tipoPagoService.get().pipe(map(data => {
      return data.filter(result => result.id === 1);
    })).subscribe(result => {
      this.tipoPagos = result;
    });
  }

  getPeriodicidad() {
    this.periodicidadService.get().subscribe(res => {
      this.periodicidad = res;
    });
  }

  getPeriodicidadFilter() {
    this.periodicidad = this.periodicidad.filter(perio => perio.id !== 1);
    this.registroForm.controls.periodo.setValue('');
  }

  getSocios() {
    this.sociosService.getByIdPaisStep(1).pipe(map(data => {
      return data.filter(res => res.activo === 1);
    })).subscribe(data => this.socios = data);
    // this.sociosService.get().pipe(map(data => {
    //   return data.filter(result => result.activo === 1);
    // })).subscribe( data => {this.socios = data; } );
  }

  getRamo(idSocio: number, idRamo?: number) {
    this.socio = this.socios.filter( socios => socios.id === this.registroForm.controls.idSocio.value)[0];

    if (!this.socio.expresionRegular) {
      this.registroForm.controls.poliza.setValidators(Validators.required);
    } else {
      if (this.socio.id !== 2 && this.socio.id !== 8) {
        this.registroForm.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(`${this.socio.expresionRegular}`)]));
      } else {
        const arrExp = this.socio.expresionRegular.split(',');
        const expresionesRegulares =
          this.socio.id === 2 ? `${arrExp[0]}|${arrExp[1].trim()}|${arrExp[2].trim()}` : `${arrExp[0]}|${arrExp[1].trim()}`;
        this.registroForm.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(expresionesRegulares)]));
      }
    }

    this.registroForm.controls.poliza.updateValueAndValidity();
    if (idRamo === undefined) {
      this.registroForm.controls.idRamo.setValue('');
      this.registroForm.controls.idSubRamo.setValue('');
      this.registroForm.controls.idProductoSocio.setValue('');
      this.ramos = [];
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.ramoService.getByIdSocio(idSocio).subscribe( {next: data => {
        this.ramos = data;
        if (this.ramos.length === 0) {
          this.mensajeErrorCatalogos('ramos');
        }
      }, complete: () => {
        if (idRamo !== undefined) {
          this.registroForm.controls.idRamo.setValue(idRamo);
        }
      }});
  }
  getRamoNuevo(idSocio: number, idRamo?: number) {
    this.sociosService.getByIdPaisStep(1).subscribe(data => {
      this.sociosOnline = data;
      console.log(data);
      console.log(this.sociosOnline);
      this.socioOnline = this.sociosOnline.filter(sociosOnline => sociosOnline.id === idSocio)[0];
      console.log(this.socioOnline);
      if (!this.socioOnline.expresionRegular) {
        this.registroForm.controls.poliza.setValidators(Validators.required);
      } else {
        if (this.socioOnline.id !== 2 && this.socioOnline.id !== 8) {
          this.registroForm.controls.poliza.setValidators(Validators.compose(
            [Validators.required, Validators.pattern(`${this.socioOnline.expresionRegular}`)]));
        } else {
          const arrExp = this.socioOnline.expresionRegular.split(',');
          const expresionesRegulares =
            this.socioOnline.id === 2 ? `${arrExp[0]}|${arrExp[1].trim()}|${arrExp[2].trim()}` : `${arrExp[0]}|${arrExp[1].trim()}`;
          this.registroForm.controls.poliza.setValidators(Validators.compose(
            [Validators.required, Validators.pattern(expresionesRegulares)]));
        }
      }

      this.registroForm.controls.poliza.updateValueAndValidity();
      if (idRamo === undefined) {
        this.registroForm.controls.idRamo.setValue('');
        this.registroForm.controls.idSubRamo.setValue('');
        this.registroForm.controls.idProductoSocio.setValue('');
        this.ramos = [];
        this.subRamos = [];
        this.productosSocio = [];
      }

      this.ramoService.getByIdSocio(idSocio).subscribe({
        next: dataR => {
          this.ramos = dataR;
          if (this.ramos.length === 0) {
            this.mensajeErrorCatalogos('ramos');
          }
        }, complete: () => {
          if (idRamo !== undefined) {
            this.registroForm.controls.idRamo.setValue(idRamo);
          }
        }
      });
    });
    // console.log(this.registroForm.controls.idSocio.value);
    // console.log(this.socios);
    // this.socio = this.socios.filter(socios => socios.id === this.registroForm.controls.idSocio.value)[0];
    // console.log(this.socio);
    // console.log('nuevi');
  }

  getSubRamo(idRamo: number, idSubRamo?: number) {
    if (idSubRamo === undefined) {
      this.registroForm.controls.idSubRamo.setValue('');
      this.registroForm.controls.idProductoSocio.setValue('');
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.subRamoService.getSubramosByIdRamo(idRamo).subscribe({
      next: data => {
        this.subRamos = data;
        if (this.subRamos.length === 0) {
          this.mensajeErrorCatalogos('subramos');
        }
      }, complete: () => {
        if (idSubRamo !== undefined) {
          this.registroForm.controls.idSubRamo.setValue(idSubRamo);
        }
      }
    });
  }

  getProductoSocio(subRamo: number, idProductoSocio?: number) {
    if (idProductoSocio === undefined) {
      this.registroForm.controls.idProductoSocio.setValue('');
      this.productosSocio = [];
    }

    this.productoSocioService.getProductoSocioByIdSubRamo(subRamo).subscribe({
      next: result => {
        this.productosSocio = result;
        if (this.productosSocio.length === 0) {
          this.mensajeErrorCatalogos('productos socio');
        }
      }, complete: () => {
        if (idProductoSocio !== undefined) {
          this.registroForm.controls.idProductoSocio.setValue(idProductoSocio);
        }
      }
    });
  }

  getProductoSocioNombre(subRamo: number, nombre: string, idProductoSocio?: number) {

    if (idProductoSocio === undefined) {
      this.registroForm.controls.idProductoSocio.setValue('');
      this.productosSocio = [];
    }
    this.productoSocioService.getProductoSocioByIdSubRamoByNombre(subRamo, nombre).subscribe({
      next: result => {
        this.productoSocioName = result;
        console.log(this.productoSocioName);
        if (this.productoSocioName === null) {
          this.mensajeErrorCatalogos('productos socio');
        }
      }, complete: () => {
        this.registroForm.controls.idProductoSocio.setValue(this.productoSocioName.id);
        console.log(this.registroForm.controls.idProductoSocio.value);
      }
    });
  }

  mensajeErrorCatalogos(catalogo: string) {
    Swal.fire({
      title: 'Sin elementos disponibles',
      text: '¡Por favor agregue ' + catalogo + ' en el catálogo correspondiente para poder continuar!',
    });
  }

  getRegistroPoliza() {
    this.registroService.getRegistroById(this.idRegistroPoliza).subscribe(data => {
      // console.log(moment(data.fechaInicio).format('L'), 'getRegistroPoliza');
      this.registroForm.controls.idTipoPago.setValue(data.idTipoPago);
      this.registroForm.controls.idSocio.setValue(data.idSocio);
      this.registroForm.controls.poliza.setValue(data.poliza);
      this.registroForm.controls.fechaInicio.setValue(data.fechaInicio + 'T00:00:00');
      this.registroForm.controls.primaNeta.setValue(data.primaNeta);
      this.registroForm.controls.oficina.setValue(data.oficina);
      this.registroForm.controls.periodo.setValue(data.idPeriodicidad);
      this.registroForm.controls.idTipoPago.disable;
      this.registroForm.controls.idSocio.disable;
      this.registroForm.controls.poliza.disable;
      this.registroForm.controls.fechaInicio.disable;
      this.registroForm.controls.primaNeta.disable;
      this.getRamoNuevo(data.idSocio, data.idRamo);
      this.getSubRamo(data.idRamo, data.idSubramo);
      this.getProductoSocio(data.idSubramo, data.idProductoSocio);
      if (data.archivo.length > 3) {
        this.instrucciones = 'YA NO PUEDES EDITAR LAS OPCIONES INGRESADAS';
        this.desactivarBotones = true;
        this.getMontoPagos();
      }
    });
  }

  getNumeroPagos(): number {
    if (this.tipoPagos !== undefined && this.registroForm.controls.idTipoPago.valid) {
      this.numeroPagos = this.tipoPagos.find(arr => arr.id === this.registroForm.controls.idTipoPago.value).cantidadPagos;
      if (this.numeroPagos > 1) {
        this.registroForm.addControl('pagoMensual', new FormControl('',
          [Validators.required, Validators.pattern('[0-9.]+')], this.setErrorMensual.bind(this)));
      } else {
        if (this.registroForm.contains('pagoMensual')) {
          this.registroForm.removeControl('pagoMensual');
        }
      }
      return this.numeroPagos;
    } else {
      return 0;
    }
  }

  getMontoPagos() {
    this.recibosService.getRecibosByIdRegistro(this.idRegistroPoliza).subscribe(data => {
      if (data && data.length > 0) {
        if (data.length > 1) {
          this.registroForm.addControl('pagoMensual', new FormControl('', [Validators.required, Validators.pattern('[0-9]+')]));
          this.registroForm.controls.pagoMensual.setValue(data[0].cantidad);
        }
        this.registroForm.controls.primerPago.setValue(data[data.length - 1].cantidad);
      }
    });
  }

  // crearRegistroenUNo

  crearRegistroEnUno() {
    console.log(this.idProductoCliente);
    console.log(this.filesToUploadPagado);
    const poliza = this.registroForm.controls.poliza.value;
    this.registroForm.controls.fechaInicio.setValue(new Date());

    this.registroService.getPolizaExistente(poliza, this.registroForm.controls.idSocio.value,
      this.registroForm.controls.fechaInicio.value.toISOString().split('T')[0]).subscribe({
      next: value => {
        const advertencia = `
          <p>La póliza <strong>${poliza}</strong> ya ha sido registrada. </p>
        `;
        if (value.length > 0) {
          Swal.fire({
            title: '¡Advertencia!',
            html: advertencia,
          });
        } else {
          const fecha = moment(this.registroForm.controls.fechaInicio.value).format('YYYY-MM-DD');
          let idRegistroPoliza: number;
          this.gruposCargados = false;
          if (this.registroForm.controls.idTipoPago.value === 1) {
            this.cantidad = 1;
          } else {
            if (this.periodicidad.find(arr => arr.id === this.registroForm.controls.periodo.value).id === 1) {
              this.cantidad = this.numeroPagos * .5;
            } else {
              this.cantidad = this.numeroPagos * (
                this.periodicidad.find(arr => arr.id === this.registroForm.controls.periodo.value).cantidadPagos
              );
            }
          }
          const datosRegistro: any = {
            idProducto: this.idProductoCliente,
            idEmpleado: sessionStorage.getItem('Empleado'),
            archivo: '',
            idEstadoPoliza: 1,
            idTipoPago: this.registroForm.controls.idTipoPago.value,
            idProductoSocio: this.registroForm.controls.idProductoSocio.value,
            idFlujoPoliza: 1,
            idSocio: this.registroForm.controls.idSocio.value,
            poliza: this.registroForm.controls.poliza.value,
            oficina: this.registroForm.controls.oficina.value,
            fechaInicio: this.registroForm.controls.fechaInicio.value,
            primaNeta: this.registroForm.controls.primaNeta.value,
            idDepartamento: +sessionStorage.getItem('idDepartamento'),
            idPeriodicidad: this.registroForm.controls.periodo.value,
            online: 1,
            emisionCotizador: 1,
          };
          let textoPrimerAviso = '';


          if (this.getNumeroPagos() > 1) {
            textoPrimerAviso +=
              '<strong>Prima neta de las mensualidades: </strong> $' +
              this.registroForm.controls.pagoMensual.value +
              '</br>';

          }
          let cantidadAPagar: number = this.registroForm.controls.primerPago.value;
          let cantidadAPagarprima: number = this.registroForm.controls.primaNeta.value;
          const recibos: any[] = [];
          let fechaVigencia = moment(this.registroForm.controls.fechaInicio.value);

          if (this.registroForm.controls.idTipoPago.value !== 1) {
            if (this.periodicidad.find(arr => arr.id === this.registroForm.controls.periodo.value).id === 1) {
              this.numeroPagos = this.numeroPagos * .5;
              this.cantidad = this.numeroPagos;
            } else {
              this.numeroPagos = this.numeroPagos * (
                this.periodicidad.find(arr => arr.id === this.registroForm.controls.periodo.value).cantidadPagos);
              this.cantidad = this.numeroPagos;
            }
          } else {
            this.numeroPagos = 1;
          }
          for (let i = 0; i < this.numeroPagos; i++) {
            if (i > 0) {
              switch (this.registroForm.controls.idTipoPago.value) {
                case 2:
                  fechaVigencia = moment(fechaVigencia).add(1, 'months');
                  break;
                case 3:
                  fechaVigencia = moment(fechaVigencia).add(3, 'months');
                  break;
                case 4:
                  fechaVigencia = moment(fechaVigencia).add(4, 'months');
                  break;
                case 6:
                  fechaVigencia = moment(fechaVigencia).add(2, 'months');
                  break;
                case 5:
                  fechaVigencia = moment(fechaVigencia).add(6, 'months');
                  break;
              }
              cantidadAPagar = this.registroForm.controls.pagoMensual.value;
              cantidadAPagarprima = this.registroForm.controls.primaNeta.value;
              // fechaVigencia = moment(fechaVigencia).add(1, 'months');
            }
            if (this.registroForm.controls.idTipoPago.value === 1) {
              recibos.push({
                idEstadoRecibos: 2,
                idEmpleado: sessionStorage.getItem('Empleado'),
                numero: i + 1,
                cantidad: cantidadAPagarprima,
                fechaVigencia: moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00'),
              });
            } else {
              if (this.registroForm.controls.periodo.value === 1 && this.registroForm.controls.idTipoPago.value === 5) {
                recibos.push({
                  idEstadoRecibos: 2,
                  idEmpleado: sessionStorage.getItem('Empleado'),
                  numero: i + 1,
                  cantidad: cantidadAPagar,
                  fechaVigencia: moment(moment(fechaVigencia).add(6, 'months').format('YYYY-MM-DD') + 'T06:00:00'),
                });
              } else {
                recibos.push({
                  idEstadoRecibos: 2,
                  idEmpleado: sessionStorage.getItem('Empleado'),
                  numero: i + 1,
                  cantidad: cantidadAPagar,
                  fechaVigencia: moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00'),
                });
              }
            }
          }
          textoPrimerAviso += '</br></br><strong>¿Estás seguro que desas continuar? </strong>';



          this.gruposCargados = true;
          this.registroEnUno = {
            registro: datosRegistro,
            recibo: recibos,
            archivos: this.responseEmision.emision.documento
           };
          this.registroService.postInOneOnlinePro(this.registroEnUno,
             this.route.snapshot.paramMap.get('id_solicitud')).subscribe(
            result => {
              this.notificaciones.exito('Se guardo la póliza');
              this.idRegistroPoliza = result.idRegistro;
              console.log('entro bien a pago :)');
              this.subirPagoDrive(this.idRegistroPoliza);
            },
            () => {
              this.notificaciones.error('Hubo un error al registrar los datos');
            },
            () => {
            }
          );

        }
      },
      error: () => {
        Swal.fire({
          title: '¡Hubo un error con los datos de poliza!',

        });
      },
    });
  }




  setErrorMensual(): Promise<ValidationErrors | null> {
    return new Promise(() => {
      if (Number(this.registroForm.controls.primerPago.value) >= Number(this.registroForm.controls.pagoMensual.value)) {
        this.registroForm.controls.pagoMensual.setErrors(null);
      } else {
        this.registroForm.controls.pagoMensual.setErrors({ incorrect: true });
      }
    });
  }

  obtenerPoliza(idSolicitud) {
    this.solicitudesService.getSolicitudesByIdOnline(idSolicitud).subscribe(data => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
          this.cotizacionAli = dataAli;
          console.log(this.cotizacionAli);
          this.responseEmision = this.cotizacionAli.respuesta;

          this.registroForm.controls.poliza.setValue(this.responseEmision.emision.poliza);
          this.registroForm.controls.poliza.disable();
          this.registroForm.controls.idTipoPago.setValue(1);
          this.registroForm.controls.idTipoPago.disable();
          this.registroForm.controls.primaNeta.setValue(this.responseEmision.emision.primaNeta);
          this.registroForm.controls.primaNeta.disable();

          this.subRamoService.getSubramoById(this.cotizacionAli.idSubRamo).subscribe(datasubramo => {
            // console.log(datasubramo);
            this.registroForm.controls.idSocio.setValue(datasubramo.idSocio);
            console.log(this.registroForm.controls.idSocio.value);
            this.getRamoNuevo(datasubramo.idSocio, datasubramo.idRamo);
            this.getSubRamo(datasubramo.idRamo, datasubramo.id);
            this.x = this.responseEmision.paquete;
            this.x = this.x.toUpperCase();
            this.getProductoSocioNombre(datasubramo.id, this.x, datasubramo.idSocio);
            this.registroForm.controls.idSocio.disable();
            this.registroForm.controls.idRamo.disable();
            this.registroForm.controls.idSubRamo.disable();
            this.registroForm.controls.idProductoSocio.disable();

          });
          this.registroForm.controls.periodo.setValue(2);
          this.registroForm.controls.periodo.disable();

          this.registroForm.controls.fechaInicio.disable();
        },
        (error) => {
          console.error('Error => ', error);
        },
        () => {



          switch (this.nombreAseguradora) {
            case 'GNP':
              const dialogRef1 = this.matDialog.open(MessagePagoSuccessComponent, {
                panelClass: 'paySuccesModal',
                data: {
                   // numpoliza: this.response['emision']['poliza'],
                  // real
                    numpoliza:  this.registroForm.controls.poliza.value,
                  blobImage: ''
                },
                disableClose: true
              });
              // Se usa el evento después de cerrar para subir el archivo a la base de datos y a drive
              dialogRef1.afterClosed().subscribe((result) => {
                // En esta variable se almacena el blob que se retorna desde el modal y está en result
                const resultBlob = result;
                console.log('URL BLOB => ', resultBlob);
                // Se llama a la función y el resultado se asigna a la variable de tipo FileList
                // Se pasan como parámetros el blob y el nombre del archivo
                this.filesToUploadPago = this.dataUrlToFileList(resultBlob, 'Archivo de Prueba');
              });

              break;
            default:

              break;
          }

        });
    });
  }
  // Metodo para agregar el blob al array simular ser un FileList
  private blobToFileList(blobResult: Blob, fileName: string) {
    // Se crea un arreglo de tipo file el cual se asignara el archivo blob
    console.log(blobResult);
    const fileArray: File[] = [];
    // Se crea un nuevo blob y se especifica el mimeType
    const blobObject = new Blob([blobResult], {
      type: 'application/pdf'
    });
    // Se crea un File a partir del blob generado, se envia el nombre del archivo y el mimeType
    const file = new File([blobObject], fileName, {
      type: 'application/pdf'
    });
    // El file generado, se agrega al array
    fileArray.push(file);
    // Se retorna el array simulando que es un filelist
    return fileArray as unknown as FileList;
  }

  subirPagoDrive(idRegistro) {

    // nuevo migracion
    this.btnLoadP = true;
    // Swal.fire({
    console.log(this.filesToUploadPago);
    console.log(this.filesToUploadPagado);
      // nuevo

    this.pago = JSON.parse(localStorage.getItem('pago'));


    this.recibosService.getRecibosByIdRegistro(idRegistro).subscribe(dataR => {
        if (dataR) {
          this.recibo = dataR[0];

          const pago: Pagos = {
            id: 0,
            idRecibo: this.recibo.id,
            idFormaPago: this.pago.idFormaPago,
            idEstadoPago: 1,
            idEmpleado: this.pago.idEmpleado,
            fechaPago: this.pago.fechaPago,
            cantidad: this.pago.cantidad,
            archivo: '',
            datos: JSON.parse(localStorage.getItem('datosPago')),
          };

          this.pagosService.subirDrive(this.filesToUploadPago, this.route.snapshot.paramMap.get('id_solicitud'), pago).subscribe({
            next: dataP => {
              this.notificacionesService.exito('¡Tus archivos han sido subidos exitosamente!').then(() => {
                this.Inspeccione.emit(idRegistro);
              });
            },
            error: () => {
              this.notificacionesService.error();
            },
          });
        }
      });
  }

  // Método que convierte a un nuevo blob y lo asigna a un FileList
  private dataUrlToFileList(resultBlob: Blob, fileName: string)/* : FileList */ {
    // console.log('Data URL en dataurltofilelist', resultBlob);
    // Arreglo en el cual se asignará el nuevo blob
    const FileArray: File[] = [];
    console.log(resultBlob);
    // se crea un nuevo objeto del nuevo blob
    const blobObject = new Blob([resultBlob], { type: 'application/pdf' });
    // Se crear un nuevo objeto de tipo file pasando como parametro el blob
    const file = new File([blobObject], fileName, {
      type: 'application/pdf'
    });
    // El objecto file se agrega al arreglo
    FileArray.push(file);
    console.log('Arreglo de archivos FileList => ', FileArray);
    // Se retorna el arreglo de tipo file simulado como un FileList
    return FileArray as unknown as FileList;
  }


  /** Guardar archivo **/
  almacenarArchivoCliente() {
    this.filesToUploadCliente = ( event.target as HTMLInputElement).files;
    const extensionValida = this.extensionesDeArchivoAceptadas.includes(this.filesToUploadCliente.item(0).type);
    const tamanioArchivo = this.filesToUploadCliente.item(0).size * .000001;

    if (this.filesToUploadCliente.length === 1) {
      // El servicio en Spring solo acepta como máximo 7 MB para subir archivos
      if (extensionValida && tamanioArchivo < 7) {
        Swal.fire({
          title: 'Archivos listos',
          text: '¡Tu archivo está listo para ser guardado!',
        });
        this.archivoValidoCliente = true;
        if (this.archivoValidoCliente) {
          this.subirADriveCliente('cliente');
          this.ocultarboton = false;
        }
      } else {
        let titulo: string;
        let texto: string;

        if (!extensionValida) {
          titulo = 'Extensión no soportada';
          texto = 'Solo puedes subir archivos pdf';
        } else {
          titulo = 'Archivo demasiado grande';
          texto = 'Los archivos que subas deben pesar menos de 7 MB';
        }

        Swal.fire({
          title: titulo,
          text: texto,
        });
      }
    }
  }
  subirADriveCliente(tipoArchivo: string) {
    let idFolder: string;
    Swal.fire({
      title: 'Se están subiendo los archivos',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });

    this.productoClienteService.getProductoClienteById(this.idProductoCliente).subscribe({
      next: dataproducto => {
        this.productoCliente = dataproducto;
        this.productoClienteService.putInOneOnline(this.idProductoCliente, this.productoCliente, this.filesToUploadCliente).subscribe({
          next: dataC => {
            this.notificacionesService.exito('¡Tu archivo han sido subidos exitosamente!').then(() => {
            });
          },
          error: () => {
            this.notificacionesService.error();
          },
          complete: () => {
            this.crearRegistroEnUno();
          }
        });
      },
      error: () => {
        this.notificacionesService.error();
      },
    });

  }

}

