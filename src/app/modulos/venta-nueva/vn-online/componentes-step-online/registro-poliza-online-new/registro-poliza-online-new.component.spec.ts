import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroPolizaOnlineNewComponent } from './registro-poliza-online-new.component';

describe('RegistroPolizaOnlineNewComponent', () => {
  let component: RegistroPolizaOnlineNewComponent;
  let fixture: ComponentFixture<RegistroPolizaOnlineNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroPolizaOnlineNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroPolizaOnlineNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
