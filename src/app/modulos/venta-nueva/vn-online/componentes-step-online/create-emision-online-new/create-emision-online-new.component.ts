import {Component, Input, OnInit, Output, EventEmitter, ViewChild, ChangeDetectorRef} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ClienteVn} from '../../../../../@core/data/interfaces/venta-nueva/cliente-vn';
import {Sepomex} from '../../../../../@core/data/interfaces/catalogos/sepomex';
import {Paises} from '../../../../../@core/data/interfaces/catalogos/paises';
import {CotizacionesAli} from '../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';
import {ClienteVnService} from '../../../../../@core/data/services/venta-nueva/cliente-vn.service';
import {SepomexService} from '../../../../../@core/data/services/catalogos/sepomex.service';
import {PaisesService} from '../../../../../@core/data/services/catalogos/paises.service';
import {SolicitudesvnService} from '../../../../../@core/data/services/venta-nueva/solicitudes.service';
import {DriveService} from '../../../../../@core/data/services/ti/drive.service';
import {AdminVnViewService} from '../../../../../@core/data/services/venta-nueva/adminVnView.service';
import {ActivatedRoute} from '@angular/router';
import {CotizacionesAliService} from '../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {ProspectoService} from '../../../../../@core/data/services/venta-nueva/prospecto.service';
import Swal from 'sweetalert2';
import moment from 'moment';
import {formatDate} from '@angular/common';
import {HomeComponent} from '../../../modals/curp/home.component';
import {MatDialog} from '@angular/material/dialog';
import RfcFacil from 'rfc-facil';
import generaCurp from '../../../../../../assets/js/curp.js';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'app-create-emision-online-new',
  templateUrl: './create-emision-online-new.component.html',
  styleUrls: ['./create-emision-online-new.component.scss']
})
export class CreateEmisionOnlineNewComponent implements OnInit {
  @Input() idClientes: number;
  @Input() nombreAseguradora: string;
  @Input() idSolicitudPros: number;
  @Input() idRegistroPoliza: number;
  @Output() datosSalida = new EventEmitter<any>();
  @ViewChild('tabGroup') razonSocial;

  registroEmision: any;
  /** Expresión regular para un número. */
  numero: RegExp = /[0-9]+/;
  emisionFisicaForm: FormGroup;
  emisionMoralForm: FormGroup;
  cliente: ClienteVn;
  /** Arreglo que se inicializa con los valores del campo sepomex desde el Microservicio. */
  coloniasSepomex: Sepomex[];
  // Variable que activa opcion de actualizar o crear si existe curp o rfc*/
  idTipo: boolean;
  idCotizacionAli: number;
  // Informacion del cliente **/
  clienteForm: FormGroup = new FormGroup({
    curp: new FormControl('', Validators.compose([Validators.required,
      Validators.pattern('^[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][H,M][A-Z][A-Z][A-Z][A-Z][A-Z][A-Z-0-9][A-Z-0-9]'),
      Validators.maxLength(18), Validators.minLength(18)])),
    rfc: new FormControl('', Validators.compose([Validators.required,
      Validators.pattern('[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
      Validators.maxLength(13), Validators.minLength(10)])),
    nombre: new FormControl('', Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(150)])),
    paterno: new FormControl('', Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(50)])),
    materno: new FormControl('', Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(50)])),
    correo: new FormControl('', Validators.compose([Validators.required, Validators.email])),
    cp: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(5),
      Validators.minLength(4), Validators.pattern('[0-9]{4,5}')])),
    idColonia: new FormControl(null, Validators.compose([Validators.required])),
    calle: new FormControl('', Validators.required),
    numInt: new FormControl(null),
    numExt: new FormControl('', Validators.required),
    genero: new FormControl('', Validators.required),
    estadoNacimiento: new FormControl('', Validators.required),
    telefonoFijo: new FormControl(null, Validators.pattern('[0-9]{10}')),
    telefonoMovil: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]{10}')])),
    fechaNacimiento: new FormControl('', Validators.required),
    razonSocial: new FormControl(1),
    idPais: new FormControl(1, Validators.required),
    archivoSubido: new FormControl(0),
    archivo: new FormControl(null),
  });
  /** Arreglo que se inicializa con los valores del campo cp desde el Microservicio. */
  colonias: any;
  /** Variable de tipo "item seleccionado" para almacenar el género (masculino y femenino). */
  genders = [{label: 'MASCULINO', value: 'M'}, {label: 'FEMENINO', value: 'F'}];
  /** Arreglo que se inicializa con los valores del campo paises desde el Microservicio. */
  paisOrigen: Paises[];
  // ** Variable que almacena el idCliente al validar si existe el curp o rfc*/
  idCliente: number;
  // ** FUNCIONA CON EL LOADER DE LOS BOTONES QUE LLEVAN UNA PETICIÓN **/
  extensionesDeArchivoAceptadas = ['application/pdf'];
  filesToUpload: FileList;
  archivoValido: false;
  estados: object[] = [
    {value: 'AS', text: 'AGUASCALIENTES'},
    {value: 'BC', text: 'BAJA CALIFORNIA'},
    {value: 'BS', text: 'BAJA CALIFORNIA SUR'},
    {value: 'CC', text: 'CAMPECHE'},
    {value: 'CL', text: 'COAHUILA DE ZARAGOZA'},
    {value: 'CM', text: 'COLIMA'},
    {value: 'CS', text: 'CHIAPAS'},
    {value: 'CH', text: 'CHIHUAHUA'},
    {value: 'DF', text: 'DISTRITO FEDERAL'},
    {value: 'DG', text: 'DURANGO'},
    {value: 'GT', text: 'GUANAJUATO'},
    {value: 'GR', text: 'GUERRERO'},
    {value: 'HG', text: 'HIDALGO'},
    {value: 'JC', text: 'JALISCO'},
    {value: 'MC', text: 'MEXICO'},
    {value: 'MN', text: 'MICHOACAN DE OCAMPO'},
    {value: 'MS', text: 'MORELOS'},
    {value: 'NT', text: 'NAYARIT'},
    {value: 'NL', text: 'NUEVO LEON'},
    {value: 'OC', text: 'OAXACA'},
    {value: 'PL', text: 'PUEBLA'},
    {value: 'QT', text: 'QUERETARO DE ARTEAGA'},
    {value: 'QR', text: 'QUINTANA ROO'},
    {value: 'SP', text: 'SAN LUIS POTOSI'},
    {value: 'SL', text: 'SINALOA'},
    {value: 'SR', text: 'SONORA'},
    {value: 'TC', text: 'TABASCO'},
    {value: 'TS', text: 'TAMAULIPAS'},
    {value: 'TL', text: 'TLAXCALA'},
    {value: 'VZ', text: 'VERACRUZ'},
    {value: 'YN', text: 'YUCATAN'},
    {value: 'ZS', text: 'ZACATECAS'},
    {value: 'NE', text: 'NACIDO EN EL EXTRANJERO'}];
  private textError: 5;
  fechaNacimientoMaxima = new Date();
  textoBoton = 'Crear';
  idSolicitud: number;
  cotizacionAli: CotizacionesAli;
  response: any;
  clientejson: any;
  peticion: any;
  ciudad: string;
  poblacion: string;
  fechaNacCambio: any;
  codPostal: string;
  responseEmision: any;
  peticionE: any;
  changeForm2 = false;
  num: number;
  nombre: string;
  cpnuevo: number;
  colnuevo: number;
  fechacambio: any;
  fechaObject: Date;
  genero: string;

  constructor(protected clienteService: ClienteVnService,
              protected cpService: SepomexService,
              protected paisesService: PaisesService,
              private solicitudesService: SolicitudesvnService,
              private driveService: DriveService,
              private administracionPolizaService: AdminVnViewService,
              private cdRef: ChangeDetectorRef,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private notificaciones: NotificacionesService,
              private cotizacionesAliService: CotizacionesAliService,
              private prospectoService: ProspectoService) {
    this.idSolicitud =   +this.route.snapshot.paramMap.get('id_solicitud');
  }

  ngOnInit(): void {
    console.log(this.nombreAseguradora);
    this.fechaNacimientoMaxima.setMonth(this.fechaNacimientoMaxima.getMonth() - 216);
    this.getPaises();
    // console.log(this.idClientes)
    if (this.idClientes !== undefined) {
      this.textoBoton = 'Modificar';
      this.getInfoCliente(this.idClientes); // aqui ya es registro poliza
    }
    console.log(this.idClientes);
    if (this.idClientes === undefined ) {
      // conseguir datos del codigo postal y sexo del cotizador
      this.conseguirDatos();
    }
  }
  // ** Funcion para que no marque error al actualizar el formulario **/
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
  /** Función para obtener el país de origen del precandidato y lo almacena en el arreglo "pais". */
  getPaises() {
    this.paisesService.get().subscribe(result => {
      this.paisOrigen = result;
    });
  }
  // ** Actualizar requeridos en el formulario **/
  actualizarFormulario(tipoCliente: number) {
    this.clienteForm.controls.razonSocial.setValue(tipoCliente);

    if (tipoCliente === 2) {
      this.clienteForm.controls.curp.clearValidators();
      this.clienteForm.controls.paterno.clearValidators();
      this.clienteForm.controls.materno.clearValidators();
      this.clienteForm.controls.fechaNacimiento.clearValidators();
      this.clienteForm.controls.genero.clearValidators();
      this.clienteForm.controls.rfc.setValidators([
        Validators.pattern('[A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
        Validators.maxLength(12), Validators.minLength(9)]);
    } else {
      this.clienteForm.controls.curp.setValidators(Validators.compose([Validators.required,
        Validators.pattern('^[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][H,M][A-Z][A-Z][A-Z][A-Z][A-Z][A-Z-0-9][A-Z-0-9]'),
        Validators.maxLength(18), Validators.minLength(18)]));
      this.clienteForm.controls.rfc.setValidators([
        Validators.required,
        Validators.pattern('[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
        Validators.maxLength(13), Validators.minLength(10)]);
      this.clienteForm.controls.paterno.setValidators(Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(50)]));
      this.clienteForm.controls.materno.setValidators(Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(50)]));
      this.clienteForm.controls.fechaNacimiento.setValidators(Validators.required);
      this.clienteForm.controls.genero.setValidators(Validators.required);
    }
    this.clienteForm.controls.paterno.updateValueAndValidity();
    this.clienteForm.controls.materno.updateValueAndValidity();
    this.clienteForm.controls.fechaNacimiento.updateValueAndValidity();
    this.clienteForm.controls.genero.updateValueAndValidity();
    this.clienteForm.controls.rfc.updateValueAndValidity();
    this.clienteForm.controls.curp.updateValueAndValidity();
  }
  conseguirDatos() {
    console.log(this.idSolicitud);
    this.solicitudesService.getSolicitudesByIdOnline(this.idSolicitud).subscribe(data => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        console.log(this.cotizacionAli);

        this.response = this.cotizacionAli.respuesta;
        this.peticion = this.cotizacionAli.peticion;
        // console.log(this.peticion);
        // console.log(this.peticion.cp);
        this.cpnuevo = this.peticion.cp;
        this.colnuevo = this.peticion.idColonia;
        console.log(this.colnuevo);
        this.clienteForm.controls.fechaNacimiento.setValue(this.peticion.fechaNacimiento);
        this.clienteForm.controls.cp.setValue(this.cpnuevo);
        this.clienteForm.controls.idColonia.setValue(this.colnuevo);
        console.log(this.clienteForm.controls.cp.value);
        // this.clienteForm.controls.cp.setValue('55816');
        // this.clienteForm.controls.cp.disable();

        this.getColonias2();

        if (this.peticion.genero === 'MASCULINO') {
          this.clienteForm.controls.genero.setValue('M');
          // this.clienteForm.controls.genero.disable();
        }
        if (this.peticion.genero === 'FEMENINO') {
          this.clienteForm.controls.genero.setValue('F');
          // this.clienteForm.controls.genero.disable();
        }

        this.prospectoService.getById(data.idProspecto).subscribe(
          dataPros => {
            console.log(dataPros);

            this.nombre = dataPros.nombre.trim();
            // no borrar
            // const fullName = dataPros.nombre.split(' ');
            // this.num = fullName.length - 1;
            // for (let i = 0 ; i <= (this.num - 2); i++) {
            //   this.nombre = this.nombre + ' ' + fullName[i] ;
            // }
            // // console.log(this.nombre);
            // const PatApellido = fullName[this.num - 1];
            // const MatApellido = fullName[this.num];

            // console.log(PatApellido);
            // this.nombre = this.nombre.trim();
            this.clienteForm.controls.nombre.setValue(this.nombre);
            // this.clienteForm.controls.paterno.setValue(PatApellido);
            // this.clienteForm.controls.materno.setValue(MatApellido);
            this.clienteForm.controls.correo.setValue(dataPros.correo);
            this.clienteForm.controls.telefonoMovil.setValue(dataPros.numero);
            // this.clienteForm.controls.fechaNacimiento.setValue(this.peticion.fechaNacimiento);

          });
        if (this.clienteForm.controls.fechaNacimiento.valid && this.clienteForm.controls.genero.valid
          && this.nombreAseguradora === 'GNP' && this.clienteForm.controls.cp.valid && this.clienteForm.controls.idColonia.valid) {
          this.clienteForm.controls.fechaNacimiento.disable();
          this.clienteForm.controls.genero.disable();
          this.clienteForm.controls.cp.disable();
          this.clienteForm.controls.idColonia.disable();
        }
      });
    });
  }
  // Get informacion del cliente de la DB **/
  getInfoCliente(idCliente: number) {
    this.clienteService.getClienteById(idCliente).subscribe(result => {
      console.log('----------------------', result);
      this.idCliente = result.id;
      console.log('aqui estoy' + result.id);
      this.textoBoton = 'Modificar';
      this.clienteForm.controls.curp.disable();
      this.clienteForm.controls.fechaNacimiento.setValue(formatDate(result.fechaNacimiento, 'yyyy-MM-dd', 'en'));
      this.clienteForm.patchValue(result);
      if (+result.razonSocial === 2) {
        this.clienteForm.controls.curp.clearValidators();
        this.clienteForm.controls.rfc.setValidators([
          Validators.pattern('[A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
          Validators.maxLength(12), Validators.minLength(9)]);
        this.clienteForm.controls.rfc.setValue(result.rfc);
        this.clienteForm.controls.paterno.clearValidators();
        this.clienteForm.controls.materno.clearValidators();
        this.clienteForm.controls.fechaNacimiento.clearValidators();
        this.clienteForm.controls.fechaNacimiento.setValue(null);
        this.clienteForm.controls.genero.clearValidators();
        this.clienteForm.controls.paterno.updateValueAndValidity();
        this.clienteForm.controls.materno.updateValueAndValidity();
        this.clienteForm.controls.fechaNacimiento.updateValueAndValidity();
        this.clienteForm.controls.genero.updateValueAndValidity();
        this.clienteForm.controls.curp.updateValueAndValidity();
        this.clienteForm.controls.rfc.updateValueAndValidity();
      }
      this.getColonias();
      if (this.clienteForm.controls.fechaNacimiento.valid && this.clienteForm.controls.genero.valid
        && this.nombreAseguradora === 'GNP' && this.clienteForm.controls.cp.valid && this.clienteForm.controls.idColonia.valid) {
        this.clienteForm.controls.fechaNacimiento.disable();
        this.clienteForm.controls.genero.disable();
        this.clienteForm.controls.cp.disable();
        this.clienteForm.controls.idColonia.disable();
      }
    });
  }
  // ** Validar curp cuando es fisico **/
  validarCurp() {
    if (this.clienteForm.controls.curp.valid) {
      this.clienteService.curpExist(this.clienteForm.controls.curp.value).subscribe(data => {
        if (data) {
          this.idCliente = data.id;
          this.textoBoton = 'Modificar';
          if (this.clienteForm.controls.razonSocial.value !== data.razonSocial) {
            this.actualizarFormulario(data.razonSocial);
          }
          this.clienteForm.controls.curp.disable();
          this.clienteForm.controls.fechaNacimiento.setValue(moment(data.fechaNacimiento).utcOffset('GMT-06:00').format('yyyy-MM-dd'));
          this.clienteForm.patchValue(data);
          this.getColonias();
        }
      });
    }
  }
  // ** Validar RFC cuando es moral **/
  validarRFC() {
    if (this.clienteForm.controls.razonSocial.value === 2 && this.clienteForm.controls.rfc.valid) {
      this.clienteService.rfcExist(this.clienteForm.controls.rfc.value).subscribe(result => {
        if (result) {
          this.idCliente = result.id;
          this.clienteForm.patchValue(result);
          this.getColonias();
        }
      });
    }
  }

  // ** Post cliente **/
  crearCliente() {
    Swal.fire({
      title: 'Creando cliente, por favor espere',
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    console.log(this.clienteForm.value);
    this.clienteService.postOnline(this.clienteForm.value).subscribe({
      next: result => {
        this.idCliente = result.id;
        this.textoBoton = 'Actualizar';
      },
      complete: () => {


        this.modificarJsonCotizacion();



        // se guardan los datos del cliente en el json que se enviara a la emision
        if (this.archivoValido) {
          this.subirADrive('cliente');
        } else {
          Swal.fire({
            // type: 'info',
            title: '¡Cliente creado con éxito!, recuerda adjuntar su archivo desde el administrador de pólizas',
          });
        }
      },
    });
    this.solicitudesService.entryIdCliente(this.idCliente);
  }

  // ** Actualizar cliente **/
  actualizarEmision() {
    Swal.fire({
      title: 'Actualizando cliente',
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });

    this.clienteService.put(this.idCliente, this.clienteForm.getRawValue()).subscribe({
      complete: () => {
        const json = {
          idCliente: this.idCliente,
          nombreCliente: this.clienteForm.controls.nombre.value,
        };

        this.modificarJsonCotizacion();

        if (this.clienteForm.controls.razonSocial.value === 1) {
          json['apellidoPaternoCliente'] = this.clienteForm.controls.paterno.value;
          json['apellidoMaternoCliente'] = this.clienteForm.controls.materno.value;
        }
        if (!this.archivoValido) {
          Swal.fire({
            title: 'Información actualizada',
            text: '¡La información del cliente se ha actualizado con éxito!',
            allowOutsideClick: false,
            allowEscapeKey: false,
          });
         }
      },
    });

    if (this.archivoValido) {
      this.subirADrive('cliente');
    }
  }
  // ** Get colonias a partir del cp **/
  async getColonias() {
    if (this.clienteForm.controls.cp.valid) {
      // la promesa resuelve el sewrvicio de externo solo si estas en cotizador GNP y de lo contrario devuelve true
      const validarGNP = new Promise((resolve, reject) => {
        if (this.nombreAseguradora === 'GNP') {
          // servicio proporcionado por externo para ver si el cp es cotizable
          this.cpService.getCpValido(this.clienteForm.controls.cp.value).subscribe({
            next: valid => {
              resolve(valid.valid);
            },
            error: () => reject(new Error('Error en el servicio'))
          });
        } else {
          resolve(true);
        }
      });
      // dependiendo de la promesa ejecutara el sepomex o mandara una notificacion
      validarGNP.then( prom => {
        if (prom){
          this.cpService.getColoniaByCpServer(this.clienteForm.controls.cp.value).subscribe(data => {
            this.colonias = data;
          });
        } else {
          this.notificaciones.informacion('Tu código postal no es cotizable a esta aseguradora').then( () => {
            this.clienteForm.controls.cp.reset();
          });
        }
      }).catch( error => this.notificaciones.error(error));
    }
  }
  // ** Pasar al siguiente step **/
  datosProducto() {
    this.actualizarEmision();
    this.datosSalida.emit({activarProductoCliente: true, idCliente: this.idCliente});
  }

  subirADrive(tipoArchivo: string) {
    let idFolder: string;
    Swal.fire({
      title: 'Se están subiendo los archivos',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });

    this.driveService.postOP(this.filesToUpload, tipoArchivo, this.idCliente, 0).subscribe({
      next: (data) => {
        idFolder = data;
      },
      error: error => {
        let texto: string;
        if (error.status !== 417 || error.status !== 400) {
          texto = 'Presiona OK para volver a intentarlo <b>5</b> segundos';
        } else {
          texto = error.error;
        }
        let timerInterval;
        let tiempo = 4;
        if (this.textError >= 1) {
          Swal.fire({
            title: 'Error',
            html: texto,
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            onBeforeOpen: () => {
              Swal.disableButtons();
              timerInterval = setInterval(() => {
                const content = Swal.getContent();
                const b = content.querySelector('b');
                b.innerText = `${tiempo}`;
                tiempo -= 1;
              }, 1000);
              setTimeout(() => {
                clearInterval(timerInterval);
                if (Swal.getConfirmButton() !== null) {
                  Swal.enableButtons();
                }
              }, 5000);
            },
            preConfirm: () => {
              this.subirADrive('cliente');
              this.textError -= 1;
            },
            onClose: () => {
              clearInterval(timerInterval);
            },
          });
        } else {
          Swal.fire({
            title: 'Error',
            html: 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información',
            preConfirm: () => {
              this.textError = 5;
            },
          });
        }
      },
      complete: () => {
        this.clienteService.putArchivo(this.idCliente, idFolder).subscribe({
          complete: () => {
            Swal.fire({
              title: 'Archivos subidos',
              text: '¡Tus archivos han sido subidos exitosamente!',
            }).then(() => this.datosProducto());

            if (this.archivoValido) {
              this.clienteService.putArchivoSubido(this.idCliente, 1).subscribe(() => {
                if (this.idRegistroPoliza) {
                  const json = {
                    idCliente: this.idCliente,
                    archivoSubido: 1,
                  };
                  this.administracionPolizaService.updateArchivoSubidoSockets(json).subscribe();
                }
              });
            }
          },
        });
      },
    });
  }
  // ** Get colonias a partir del cp **/
  getColonias2() {

    console.log(this.clienteForm.controls.cp.valid);
    // if (this.clienteForm.controls.cp.valid) {
    //
    //   this.cpService.getColoniaByCp(+this.peticion.cp).subscribe(data => {
    //     this.colonias = data;
    //     console.log(data)
    //   });
    // }
    if (this.clienteForm.controls.cp.valid) {
      this.cpService.getColoniaByCpServer(this.peticion.cp).subscribe(data => {
        this.colonias = data;
      });
    }
  }
  modificarJsonCotizacion() {
    this.solicitudesService.getSolicitudesById(this.idSolicitud).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;

        this.responseEmision = this.cotizacionAli.respuesta;
        this.peticionE = this.cotizacionAli.peticion;
        this.clienteService.getClienteById(this.idCliente).subscribe(dataCliente => {
          this.cpService.getColoniaByCpServer(dataCliente.cp).subscribe(datacp => {

            if (dataCliente.numInt === null) {
              dataCliente.numInt = '';
            }
            // console.log(datacp)
            // se quitan acentos en poblacion y ciudad
            // si no existe cuidad manda vacio

            this.poblacion = datacp[0].d_ciudad;

            // console.log(this.poblacion);
            // console.log(this.ciudad);

            this.fechaNacCambio = dataCliente.fechaNacimiento;
            this.fechaNacCambio = moment(dataCliente.fechaNacimiento).format('DD/MM/YYYY');

            // se convierte codigo portal a string y si empieza con o0 se le agrega
            this.codPostal = dataCliente.cp.toString();
            // console.log(this.codPostal)
            this.codPostal = this.codPostal.padStart(5, '0');
            // console.log(this.codPostal)

            console.log(dataCliente.colonia);
            this.clientejson = {
              rfc: dataCliente.rfc,
              curp: dataCliente.curp,
              edad: this.peticionE.edad,
              email: dataCliente.correo,
              genero: dataCliente.genero,
              nombre: dataCliente.nombre,
              telefono: dataCliente.telefonoMovil,
              direccion: {
                pais: dataCliente.nombrePaises,
                calle: dataCliente.calle,
                noExt: dataCliente.numExt,
                noInt: dataCliente.numInt,
                ciudad: this.ciudad,
                colonia: dataCliente.colonia,
                codPostal: this.codPostal,
                poblacion: this.poblacion
              },
              ocupacion: '',
              apellidoMat: dataCliente.materno,
              apellidoPat: dataCliente.paterno,
              tipoPersona: '',
              fechaNacimiento: this.fechaNacCambio
            };

            if (dataCliente.genero === 'M') {
              delete this.clientejson.genero;
              this.clientejson.genero = 'MASCULINO';
            }
            if (dataCliente.genero === 'F') {
              delete this.clientejson.genero;
              this.clientejson.genero = 'FEMENINO';
            }
            delete this.responseEmision.cliente;
            delete this.responseEmision.idSubRamo;
            this.responseEmision.cliente = this.clientejson;
            this.cotizacionAli.respuesta = this.responseEmision;

            this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
            });
            console.log(this.clientejson);
          }); // codigo postal
        }); // fin cliente
      }); // fin cotizacion

    }); // fin solicitud
  }

  changeFormNext(){
    this.changeForm2 = true;
  }

  changeFormBack(){
    this.changeForm2 = false;
  }

  calcularRFC() {
    this.dialog.open(HomeComponent);
  }

  generarDatos() {
    if (this.clienteForm.controls.nombre.valid && this.clienteForm.controls.paterno.valid && this.clienteForm.controls.materno.valid &&
      (this.clienteForm.controls.fechaNacimiento.valid && this.clienteForm.controls.genero.valid || this.nombreAseguradora === 'GNP')){
      const date = moment(this.clienteForm.controls.fechaNacimiento.value, 'YYYY/MM/DD');
      console.log(date);
      console.log(date.format('DD'));
      console.log(date.format('MM'));
      console.log(date.format('YYYY'));
      const rfc: string = RfcFacil.forNaturalPerson({
        name: this.clienteForm.controls.nombre.value,
        firstLastName: this.clienteForm.controls.paterno.value,
        secondLastName: this.clienteForm.controls.materno.value,
        day: Number(date.format('DD')),
        month: Number(date.format('MM')),
        year: Number(date.format('YYYY'))
      });
      if (this.clienteForm.controls.genero.value === 'M'){
        this.genero = 'H';
      }
      if (this.clienteForm.controls.genero.value === 'F') {
        this.genero = 'M';
      }
      /**
       * Funcion genera curp
       * Aqui se obtienen los valores
       * para generar la curp
       */
      const curp = generaCurp({
        nombre: this.clienteForm.controls.nombre.value,
        apellido_paterno: this.clienteForm.controls.paterno.value,
        apellido_materno: this.clienteForm.controls.materno.value,
        sexo: this.genero,
        estado: this.clienteForm.controls.estadoNacimiento.value,
        fecha_nacimiento: [date.format('DD'), date.format('MM'), date.format('YYYY')],
      });
      this.clienteForm.controls.curp.setValue(curp);
      this.clienteForm.controls.rfc.setValue(rfc);
      this.validarCurp();
    }
  }
}

