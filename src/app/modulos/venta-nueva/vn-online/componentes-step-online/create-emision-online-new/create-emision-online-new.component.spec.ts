import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmisionOnlineNewComponent } from './create-emision-online-new.component';

describe('CreateEmisionOnlineNewComponent', () => {
  let component: CreateEmisionOnlineNewComponent;
  let fixture: ComponentFixture<CreateEmisionOnlineNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateEmisionOnlineNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmisionOnlineNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
