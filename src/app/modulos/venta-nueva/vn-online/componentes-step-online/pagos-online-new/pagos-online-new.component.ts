import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ClienteVn} from "../../../../../@core/data/interfaces/venta-nueva/cliente-vn";
import {MatStepper} from "@angular/material/stepper";
import {MatSlideToggle} from "@angular/material/slide-toggle";
import {Recibos} from "../../../../../@core/data/interfaces/venta-nueva/recibos";
import {FormaPagoOnline} from "../../../../../@core/data/interfaces/venta-nueva/catalogos/forma-pago";
import {BancoExterno, BancoVn, MsiView} from "../../../../../@core/data/interfaces/venta-nueva/catalogos/banco-vn";
import {Carrier} from "../../../../../@core/data/interfaces/venta-nueva/catalogos/carrier";
import {TarjetasVn} from "../../../../../@core/data/interfaces/venta-nueva/catalogos/tarjetas-vn";
import {Pagos} from "../../../../../@core/data/interfaces/venta-nueva/pagos";
import {ProductoCliente} from "../../../../../@core/data/interfaces/venta-nueva/producto-cliente";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PagosService} from "../../../../../@core/data/services/venta-nueva/pagos.service";
import {FormaPagoService} from "../../../../../@core/data/services/venta-nueva/catalogos/forma-pago.service";
import {CarrierService} from "../../../../../@core/data/services/venta-nueva/catalogos/carrier.service";
import {BancoVnService} from "../../../../../@core/data/services/venta-nueva/catalogos/banco-vn.service";
import {RecibosService} from "../../../../../@core/data/services/venta-nueva/recibos.service";
import {RegistroService} from "../../../../../@core/data/services/venta-nueva/registro.service";
import {CotizacionesAliService} from "../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service";
import {CatalogoService} from "../../../../../@core/data/services/cotizador/catalogo.service";
import {SolicitudesvnService} from "../../../../../@core/data/services/venta-nueva/solicitudes.service";
import {ActivatedRoute} from "@angular/router";
import {ClienteVnService} from "../../../../../@core/data/services/venta-nueva/cliente-vn.service";
import {SepomexService} from "../../../../../@core/data/services/catalogos/sepomex.service";
import {MsiExtenoGnp, urlAseguradoras} from "../../../../../@core/data/interfaces/cotizador/urlAseguradoras";
import Swal from "sweetalert2";
import {map} from "rxjs/operators";
import {MatDialog} from "@angular/material/dialog";
import {BancoOnline} from "../../../../../@core/data/interfaces/cotizador/cotizar";
import {MessagePagoSuccessComponent} from "../../../../../shared/componentes/modal-pagos-online-new/componentes/message-pago-success/message-pago-success.component";
import {ProductoClienteService} from "../../../../../@core/data/services/venta-nueva/producto-cliente.service";
import {LogCotizadorService} from "../../../../../@core/data/services/cotizador/log-cotizador.service";
import {NotificacionesService} from "../../../../../@core/data/services/others/notificaciones.service";
import {RegistroOnline} from "../../../../../@core/data/interfaces/venta-nueva/registro";
import moment from "moment";
import {VisualizarPolizaComponent} from "../../modal/visualizar-poliza/visualizar-poliza.component";


@Component({
  selector: 'app-pagos-online-new',
  templateUrl: './pagos-online-new.component.html',
  styleUrls: ['./pagos-online-new.component.scss']
})
export class PagosOnlineNewComponent implements OnInit {

  @Input() idProductoCliente: number;
  @Input() idCliente: number;
  @Input() datosCliente: ClienteVn;
  @Input() idRegistro: number;
  idSolicitud: number;

  @ViewChild('stepper') stepper: MatStepper;
  @ViewChild('mostrarCodigoCheck') checkin: MatSlideToggle;
  @ViewChild('capture') capture: ElementRef;
  @ViewChild('fileInput') fileInput: ElementRef;
  @Output() idCobroSaliente = new EventEmitter<{ idproductoc: number, filepagado: FileList}>();
  @Output() Inspeccione = new EventEmitter<number>();
  @Output() emisionMal = new EventEmitter<number>();

  maskInput: string;
  recibo: Recibos;
  formaPagoData: FormaPagoOnline[];
  bancosArray: BancoVn[];
  carrierArray: Carrier[];
  tarjetasArray: TarjetasVn[];
  archivoValido = false;
  activarMesesSinIntereses = false;
  inputType = 'password';
  filesToUpload: FileList;
  extensionesDeArchivoAceptadas = ['application/pdf'];
  mesesSinIntereses = ['0 meses', '3 meses', '6 meses', '9 meses', '12 meses'];
  letras: RegExp = /[\\A-Za-zñÑ ]+/;
  idCarpetaDrive: string;
  ocultarEyePermanent: boolean;
  momentoCountdown = false;
  btnLoad = false;
  toggleActivo = true;
  // pago online
  token: string;
  // cotizacionAli: CotizacionesAli;
  cotizacionAli;
  pagoOnline: Pagos;
  response: any;
  responseObtenerEmision: any;
  dataArrayPago: any;
  dataArrarObtenerPoliza: any;
  dataArrayValidacionTarjeta: any;
  responsepago: any;
  nombrebanco: any;
  responsePago: any;
  mostrarbotonPago: boolean = false;
  nombreAseguradora: string;
  service: any;
  urlService: string;
  urlPago: string;
  urlEmision: string;
  fechaV: string;
  urlBanco: string;
  urlValidarTarjeta: string;
  urlMsi: string;
  urlObtenerEmision: string;
  // emision
  productoCliente: ProductoCliente;
  responseProducto: any;
  vehiculoJson: object;
  ciudad: string;
  poblacion: string;
  fechaNacCambio: any;
  codPostal: string;
  clientejson: object;
  jsonEmitir: any;
  responseEmision: any;
  responseEmisionPrima: any;
  dataArrayEmision: any;
  loader = false;
  bancosArrayExterno: BancoExterno[];
  ocultarFormulario = false;
  msi: MsiView[];
  msiGnp: MsiExtenoGnp[] = [{CLAVE: 1, NOMBRE: 'CONTADO', VALOR: 'CONTADO'}];
  valorMsi: any;
  // ocultarMsi: boolean = true;
   bancosOnline: BancoOnline[];
  Arraybanco: any;
  numTarj: string;
  banderaNumTarjeta = false;
  btnLoadP = false;
  valuePdfPago: any;
  pago: any;
  filesToUploadPago: FileList;
  jsonEmitirGnp: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
  };

  jsonPagoGnp: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
    poliza: null,
    banderaPoliza: 0,
  };

  jsonEmitirQualitas: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
    poliza: null,
  };

  jsonPagoQualitas: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
    poliza: null,
    banderaPagado: 0,
  };
  ocultarMsi = false;
  banderaEmitirMal = false;
  banderaPagoMal = false;
  registroOnline: RegistroOnline;
  // re
  peticionE: any;
  idBancoExterno: number;
  validarTarjetaGeneral: any;
  cardToken: string;
  metodoPagoGNP: string;

  // variable para mostrar el dialog cuando el pago sea true
  statusPayment = false;

  @ViewChild('reportContent') reportContent: ElementRef;

  // PATRON PARA ENMASCARAR EL INPUT Y VER SOLO LOS ULTIMOS 4 DIGITOS
  pattern = { 'Y': { pattern: new RegExp('\\d'), symbol: '•' }, '0': { pattern: new RegExp('\\d')}};

  // MASK DEL INPUT TARJETA
  maskCard = 'YYYYYYYYYYYY0000'
  formDatosPago: FormGroup = new FormGroup({
    cantidad: new FormControl({ value: '', disabled: true }),
    fechaPago: new FormControl({ value: new Date(), disabled: true }),
    idFormaPago: new FormControl(''),
    idBanco: new FormControl('', Validators.required),
    idCarrier: new FormControl({ disabled: true, value: '' }, Validators.required),
    cargoRecurrente: new FormControl(false),
    toggleCodigo: new FormControl(false),
    mesesSinIntereses: new FormControl('', Validators.required),
    comentarios: new FormControl(''),
    numeroTarjeta: new FormControl('', [Validators.required, Validators.pattern('[0-9]+'), Validators.minLength(15)]),
    titular: new FormControl('', [Validators.required, Validators.required]),
    codigo: new FormControl('', Validators.required),
    codigoToken: new FormControl(''),
    noClave: new FormControl(''),
    mesVencimiento: new FormControl('', [Validators.required, Validators.pattern('[0-9]+'), Validators.maxLength(2)]),
    anioVencimiento: new FormControl('', [Validators.required, Validators.pattern('[0-9]+'), Validators.maxLength(2)])
  });
  constructor(private pagosService: PagosService, private dc: ChangeDetectorRef,
              private formaPagoService: FormaPagoService,
              private carrierService: CarrierService,
              private bancoService: BancoVnService,
              private recibosService: RecibosService,
              private registroService: RegistroService,
              private cotizacionesAliService: CotizacionesAliService,
              private catalogoService: CatalogoService,
              private solicitudesService: SolicitudesvnService,
              private route: ActivatedRoute,
              private dialog: MatDialog, private router: ActivatedRoute,
              private productoClienteService: ProductoClienteService,
              private clienteService: ClienteVnService,
              private logCotizadorService: LogCotizadorService,
              private notificacionesService: NotificacionesService,
              protected cpService: SepomexService,
              private matDialog: MatDialog
  ) {
    this.nombreAseguradora = this.router.snapshot.paramMap.get('nombreAseguradora');
    this.service = urlAseguradoras.find(aseg => aseg.aseguradora === this.nombreAseguradora);
    this.urlService = this.service.service;
    this.urlPago = this.service.urlPago;
    this.urlEmision = this.service.urlEmision;
    this.urlBanco = this.service.urlBanco;
    this.urlValidarTarjeta = this.service.urlValidarTarjeta;
    this.validarTarjetaGeneral = this.service.validarTarjetaGeneral;
    this.urlMsi = this.service.urlMsi;
    this.urlObtenerEmision = this.service.urlObtenerEmision;
  }

  ngOnInit(): void {
    this.idSolicitud = Number(this.route.snapshot.paramMap.get('id_solicitud'));
    this.getFormaPago();
    this.getBancos();
    this.getCarriers();
    // obtiene la cantidad
    this.getRecibos(this.idSolicitud);
    // this.getRegistro();
    this.ocultarEye();
    this.emitir(this.idSolicitud);
  }

  get mesV () {
    return this.formDatosPago.controls['mesVencimiento'];
  }

  get anioV () {
    return this.formDatosPago.controls['anioVencimiento'];
  }

  fechaConsole(){
    console.log(`${this.mesV.value}/${this.anioV.value}`);
  }

  getBancos() {
    // BANCOS PARA GNP
    switch (this.nombreAseguradora) {
      case 'QUALITAS':
        // BANCOS PARA QUALITAS
        this.bancoService.getexterno().subscribe(data => {
          this.bancosArrayExterno = data;
        });
        break;
      case 'GNP' :
        // BANCOS PARA GNP
        // let databanco: any;
        this.formDatosPago.controls.idBanco.disable();

        break;
      default:

        break;
    }
  }

  getFormaPago() {
    this.formaPagoService.getOnline().pipe(map((res) => {
      return res.filter(data => data.id === 1 || data.id === 2);
    })).subscribe(result => {
      this.formaPagoData = result;
    });
  }

  getRecibos(idSol) {
    this.solicitudesService.getSolicitudesByIdOnline(idSol).subscribe(dataSol => {
      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        this.responseEmisionPrima = this.cotizacionAli.respuesta;
        console.log(this.responseEmisionPrima["cotizacion"]["primaNeta"]);
        this.formDatosPago.controls.cantidad.setValue(this.responseEmisionPrima["cotizacion"]["primaNeta"]);
      });
    });
  }

  getCarriers() {
    this.carrierService.get().pipe(map(data => data.filter(res => res.activo = 1))).subscribe(
      data => { this.carrierArray = data; });
  }

  verificarInputs() {
    this.cambioCT();
    if (this.formDatosPago.controls.idCarrier.value === '') {
      this.formDatosPago.controls.idCarrier.enable();
    }
  }

  // MSI
  gbanco(nombreBanco) {
    this.cambioCT();
    if (this.formDatosPago.controls.idCarrier.value === '') {
      this.formDatosPago.controls.idCarrier.enable();
    }

    switch (this.nombreAseguradora) {
      case 'QUALITAS':
        // msi PARA QUALITAS
        this.ocultarMsi = true;
         console.log(this.formDatosPago.controls['idBanco'].value);
        this.bancoService.getexternoOnly().pipe(map(result => {
          return result.filter(data => data.abreviacion === this.formDatosPago.controls['idBanco'].value);
        })).subscribe(data => {
          console.log(data);
          this.idBancoExterno  = +data[0].id_banco;
          console.log(this.idBancoExterno);
          if (this.formDatosPago.controls.idFormaPago.value === 2) {
            delete this.msi;
            this.msi = [
              { claveMsi: 'CL', nombreMsi: 'CONTADO'}
            ];
          } else {
            if ( this.idBancoExterno === 62 || this.idBancoExterno === 30 || this.idBancoExterno === 19 || this.idBancoExterno === 36
              || this.idBancoExterno === 59 || this.idBancoExterno === 42 || this.idBancoExterno === 36 || this.idBancoExterno === 127
              || this.idBancoExterno === 132 || this.idBancoExterno === 14 || this.idBancoExterno === 21 || this.idBancoExterno === 44
              || this.idBancoExterno === 72 || this.idBancoExterno === 58 ){
              delete this.msi;
              this.msi = [
                { claveMsi: 'CL', nombreMsi: 'CONTADO'},
                { claveMsi: '3MSI', nombreMsi: '3 MESES SIN INTERESES'},
                { claveMsi: '6MSI', nombreMsi: '6 MESES SIN INTERESES'},
                { claveMsi: '12MSI', nombreMsi: '12 MESES SIN INTERESES'},
              ];
            } else if ( this.idBancoExterno === 12  ) {
              delete this.msi;
              this.msi = [
                {claveMsi: 'CL', nombreMsi: 'CONTADO'},
                {claveMsi: '3MSI', nombreMsi: '3 MESES SIN INTERESES'},
                {claveMsi: '6MSI', nombreMsi: '6 MESES SIN INTERESES'},
              ];
            }else if ( this.idBancoExterno === 2  ) {
              delete this.msi;
              this.msi = [
                { claveMsi: 'CL', nombreMsi: 'CONTADO'},
                { claveMsi: '3MSI', nombreMsi: '3 MESES SIN INTERESES'},
                { claveMsi: '6MSI', nombreMsi: '6 MESES SIN INTERESES'},
                { claveMsi: '9MSI', nombreMsi: '9 MESES SIN INTERESES'},
                { claveMsi: '12MSI', nombreMsi: '12 MESES SIN INTERESES'},
              ];
            } else {
              delete this.msi;
              this.msi = [
                { claveMsi: 'CL', nombreMsi: 'CONTADO'},
              ];
            }
          }
        });

        break;
      case 'GNP' :
        // BANCOS PARA GNP
        // let databanco: any;
        this.ocultarMsi = false;
        if (this.formDatosPago.controls.numeroTarjeta.value !== '') {
            this.ocultarMsi = true;
            this.numTarj = this.formDatosPago.controls.numeroTarjeta.value;
            this.numTarj = this.numTarj.substring(0, 6);
            this.catalogoService.getMsiExterno(this.nombreAseguradora, this.urlMsi, this.numTarj).subscribe(dataMsiGNP => {
                this.msiGnp = [{CLAVE: 1, NOMBRE: 'CONTADO', VALOR: 'CONTADO'}];
              dataMsiGNP['ELEMENTO'].forEach(value => {
                 this.msiGnp.push(value);
               });
              },
              error => {},
              () => {
              });
            console.log(this.msiGnp);
        } else {
            this.ocultarMsi = false;
        }
        break;
      default:

        break;
    }
  }

  verificaMesesSinIntereses() {
    if (this.formDatosPago.controls.mesesSinIntereses.value === '0 meses') {
      this.activarMesesSinIntereses = false;
      this.formDatosPago.controls.mesesSinIntereses.setValue('');
    }
  }

  ocultarEye() {
    if (this.inputType === 'text') {
      return true;
    } else {
      this.ocultarEyePermanent = true;
      return false;
    }
  }

  ocultarInput() {
    const tarjeta = this.formDatosPago.controls.numeroTarjeta.valid;
    const codigo = this.formDatosPago.controls.codigo.valid;
    this.toggleActivo = !(!tarjeta || !codigo);

    if (!this.toggleActivo) {
      Swal.fire({
        text: 'Datos incompletos',
      });
    } else {
      let countDown = 60;
      Swal.fire({
        text: `¡Tienes ${countDown} segundos para verificar tus datos!`,
        showConfirmButton: true,
        showCancelButton: true,
      }).then(resp => {
        if (resp.value) {
          this.momentoCountdown = true;
          // SE CAMBIA EL MASK PARA VISUALIZAR EL INPUT SIN SIMBOLOS
          this.maskCard = '0000000000000000';
          this.inputType = 'text';
          const time = setInterval(() => {
            countDown--;
            const loader2 =
              `<div>
                <div class="row"">
                  <div class="spinner-grow text-info" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class="row" style="padding-top: 5px">
                    <p>&nbsp;&nbsp;&nbsp;Quedan </p> <p>&nbsp;<strong>${countDown}</strong></p><p>&nbsp; segundos &nbsp;</p>
                  </div>
                </div>
                </div>`;
            const loader3 =
              `<div>
                <div class="row">
                  <div class="spinner-grow text-info" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class="row" style="padding-top: 5px">
                    <p>&nbsp;&nbsp;&nbsp;Queda </p> <p>&nbsp;<strong>${countDown}</strong></p><p>&nbsp; segundo &nbsp;</p>
                  </div>
                </div>
                </div>`;
            switch (countDown) {
              case 1:
                const Toast3 = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                });
                Toast3.fire({
                  width: 165,
                  html: loader3,
                });
                break;
              case 0:
                clearInterval(time);
                this.momentoCountdown = false;
                // SE ENMASCARA EL INPUT NUEVAMENTE
                this.maskCard = 'YYYYYYYYYYYY0000';
                this.inputType = 'password';
                this.ocultarEyePermanent = false;
                const Toast0 = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: 2500,
                });
                Toast0.fire({
                  width: 165,
                  text: 'Tiempo terminado',
                });
                break;
              default:
                const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                });
                Toast.fire({
                  width: 180,
                  html: loader2,
                });
                break;
            }
          }, 1000);
        }
      });
    }
  }

  // Editando función de reset para conservar primeros tres datos de form
  seleccionFormaPago() {
    const cantidad = this.formDatosPago.controls.cantidad.value,
          fechaPago = this.formDatosPago.controls.fechaPago.value,
          idFormaPago = this.formDatosPago.controls.idFormaPago.value;

    this.formDatosPago.reset();
    this.formDatosPago.controls.idCarrier.setValue('');
    this.formDatosPago.controls.cantidad.setValue(cantidad);
    this.formDatosPago.controls.fechaPago.setValue(fechaPago);
    this.formDatosPago.controls.idFormaPago.setValue(idFormaPago);
  }

  desactivarBoton(): boolean {
    if (this.formDatosPago !== undefined) {
      if (this.formDatosPago.valid && this.archivoValido) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  cambioCT() {
    if (this.formDatosPago.contains('codigo')) {
      this.formDatosPago.addControl('codigoToken', new FormControl('', Validators.compose(
        [Validators.minLength(3), Validators.maxLength(6)])));
      //   this.formDatosPago.removeControl('codigo');
    } else {
      if (this.formDatosPago.contains('codigoToken')) {
        //     this.formDatosPago.addControl('codigo', new FormControl('', Validators.compose(
        //       [Validators.minLength(3), Validators.maxLength(5), Validators.required])));
        this.formDatosPago.removeControl('codigoToken');
      }
    }
  }

  // obtiene los datos del servicio pago y los guarda en cotizacion ali
  enviarpagoOnline($event: MouseEvent) {
    ($event.target as HTMLButtonElement).disabled = true;

    let dataPago: any;
    let dataValidacionTarjeta: any;
    let formaPago: string;

    this.solicitudesService.getSolicitudesByIdOnline(this.idSolicitud).subscribe(dataSol => {
      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        this.response = this.cotizacionAli.respuesta;
        // this.bancoService.getBancoById(this.formDatosPago.controls['idBanco'].value).subscribe(databanco => {
        delete this.response['pago'];
        // poner datos bien

        switch (this.nombreAseguradora) {
          case 'QUALITAS':
            // envia los pagos al localstorage para usarlos en el otro step registro
            const pago: Pagos = {
              id: 0,
              idRecibo: 0,
              idFormaPago: this.formDatosPago.controls.idFormaPago.value,
              idEstadoPago: 1,
              idEmpleado: parseInt(sessionStorage.getItem('Empleado'), 10),
              fechaPago: this.formDatosPago.controls.fechaPago.value,
              cantidad: this.formDatosPago.controls.cantidad.value,
              archivo: '',
              datos: '',
            };

            if (this.formDatosPago.controls.mesesSinIntereses.value === '') {
              this.valorMsi = 'CL';
              console.log(this.valorMsi);
            } else {
              this.valorMsi = this.formDatosPago.controls.mesesSinIntereses.value;
            }

            this.fechaV = `${this.mesV.value}/${this.anioV.value}`;
            const DatosPago = {
              idBanco: this.formDatosPago.controls.idBanco.value,
              idCarrier: this.formDatosPago.controls.idCarrier.value,
              cargoRecurrente: false,
              toggleCodigo: false,
              mesesSinIntereses: this.valorMsi,
              comentarios: this.formDatosPago.controls.comentarios.value,
              numeroTarjeta: this.formDatosPago.controls.numeroTarjeta.value,
              titular: this.formDatosPago.controls.titular.value,
              codigo: this.formDatosPago.controls.codigo.value,
              codigoToken: '',
              noClave: this.formDatosPago.controls.noClave.value,
              fechaVencimiento: this.fechaV,
            };

            localStorage.setItem('pago', JSON.stringify(pago));
            localStorage.setItem('datosPago', JSON.stringify(DatosPago));

            if (this.formDatosPago.controls.idFormaPago.value === 1) {
              formaPago = 'CREDITO';
            } else if (this.formDatosPago.controls.idFormaPago.value === 2) {
              formaPago = 'DEBITO';
            }

            this.response['pago'] = {
              msi: this.formDatosPago.controls.mesesSinIntereses.value,
              banco: this.formDatosPago.controls.idBanco.value,
              mesExp: this.mesV.value,
              anioExp: `20${this.anioV.value}`,
              carrier: this.formDatosPago.controls.idCarrier.value,
              noClabe: '',
              medioPago: formaPago,
              noTarjeta: this.formDatosPago.controls.numeroTarjeta.value,
              nombreTarjeta: this.formDatosPago.controls.titular.value,
              codigoSeguridad: this.formDatosPago.controls.codigo.value
            };

            this.cotizacionAli['respuesta'] = this.response;
            console.log('Response cotizacion Ali =>', this.cotizacionAli.respuesta);

            this.responsePago = this.cotizacionAli.respuesta;
            console.log(this.responsePago);

            // servicio real
            this.catalogoService.postPago(this.urlService, this.urlPago, this.responsePago)
              .subscribe(
                (data) => {
                  dataPago = data;

                },
                (error) => {
                  console.error('Error => ', error);
                },
                () => {
                  this.dataArrayPago = dataPago;
                  console.log(this.dataArrayPago)

                  // log de pago

                  this.jsonPagoQualitas.request = this.responsePago;

                  this.jsonPagoQualitas.idEmpleado = sessionStorage.getItem('Empleado');
                  this.jsonPagoQualitas.idSolicitudVn =  this.idSolicitud;
                  this.jsonPagoQualitas.response = this.dataArrayPago;
                  this.jsonPagoQualitas.error = this.dataArrayPago['codigoError'];
                  this.jsonPagoQualitas.poliza = this.dataArrayPago['emision']['poliza'];
                  if (this.dataArrayPago['emision']['resultado'] === true) {
                    this.jsonPagoQualitas.banderaPagado = 1;
                  }
                  this.logCotizadorService.qualitaspago(this.jsonPagoQualitas).subscribe(log => {
                  });
                  delete this.response;
                  this.response = this.dataArrayPago;
                  this.cotizacionAli['respuesta'] = this.response;

                  this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe( result => {
                  });

                  if (this.dataArrayPago['emision']['resultado'] === true) {
                    // muestre una ventana de pago exitoso
                    this.openDialogMessageSuccess();
                    this.banderaPagoMal = false;
                  } else if (this.dataArrayPago['codigoError'] !== '' && this.dataArrayPago['emision']['resultado'] === false){
                    ($event.target as HTMLButtonElement).disabled = false;

                    // no ha pagado-
                    this.banderaPagoMal = true;
                    this.noPayment();
                    console.log('El pago ha sido negado');
                  }


                });
            break;
          case 'GNP' :

            const pagognp: Pagos = {
              id: 0,
              idRecibo: 0,
              idFormaPago: this.formDatosPago.controls.idFormaPago.value,
              idEstadoPago: 1,
              idEmpleado: parseInt(sessionStorage.getItem('Empleado'), 10),
              fechaPago: this.formDatosPago.controls.fechaPago.value,
              cantidad: this.formDatosPago.controls.cantidad.value,
              archivo: '',
              datos: '',
            };

            if (this.formDatosPago.controls.mesesSinIntereses.value === '') {
              this.valorMsi = 'CL';
              console.log(this.valorMsi);
            } else {
              this.valorMsi = this.formDatosPago.controls.mesesSinIntereses.value;
            }
            this.fechaV = `${this.mesV.value}/${this.anioV.value}`;
            const DatosPagognp = {
              idBanco: this.formDatosPago.controls.idBanco.value,
              idCarrier: this.formDatosPago.controls.idCarrier.value,
              cargoRecurrente: false,
              toggleCodigo: false,
              mesesSinIntereses: this.valorMsi,
              comentarios: this.formDatosPago.controls.comentarios.value,
              titular: this.formDatosPago.controls.titular.value,
              codigo: this.formDatosPago.controls.codigo.value,
              codigoToken: '',
              noClave: this.formDatosPago.controls.noClave.value,
              fechaVencimiento: this.fechaV
            };

            localStorage.setItem('pago', JSON.stringify(pagognp));
            localStorage.setItem('datosPago', JSON.stringify(DatosPagognp));

            if (this.formDatosPago.controls.idFormaPago.value === 1) {
              formaPago = 'R';
            } else if (this.formDatosPago.controls.idFormaPago.value === 2) {
              formaPago = 'D';
            }


            if (this.formDatosPago.controls.mesesSinIntereses.value === '') {
              this.valorMsi = '';
              console.log(this.valorMsi);
            } else {
              this.valorMsi = this.formDatosPago.controls.mesesSinIntereses.value;
            }
            this.valorMsi = this.formDatosPago.controls.mesesSinIntereses.value;

            this.response['pago'] = {
              msi: this.valorMsi.toString(),
              banco: this.formDatosPago.controls.idBanco.value,
              mesExp: this.mesV.value,
              anioExp: `20${this.anioV.value}`,
              carrier: this.formDatosPago.controls.idCarrier.value,
              noClabe: '',
              medioPago: this.metodoPagoGNP,
              noTarjeta: this.cardToken,
              nombreTarjeta: this.formDatosPago.controls.titular.value,
              codigoSeguridad: this.formDatosPago.controls.codigo.value
            };

            this.cotizacionAli['respuesta'] = this.response;
            console.log('Response cotizacion Ali =>', this.cotizacionAli.respuesta);

            this.responsePago = this.cotizacionAli.respuesta;
            console.log(this.responsePago);

            // servicio real
            this.pagoGnp(dataSol.idCotizacionAli, $event);

            // simulacion


            break;
          default:

            break;
        }
      });
    });
  }

  private noPayment() {
    Swal.fire({
      title: 'El pago no ha sido procesado',
      text: '¡Por favor verifica los datos de tarjeta!',
    });
  }


  private openDialogMessageSuccess() {


    switch (this.nombreAseguradora) {
      case 'QUALITAS':
        const dialogRef2 = this.dialog.open(MessagePagoSuccessComponent, {
          panelClass: 'paySuccesModal',
          data: {
            //  numpoliza: this.response['emision']['poliza'],
            // real
              numpoliza:  this.dataArrayPago['emision']['poliza'],
            // numpoliza: 738778374,
            blobImage: ''
          },
          disableClose: true
        });
        // Se usa el evento después de cerrar para subir el archivo a la base de datos y a drive
        dialogRef2.afterClosed().subscribe((result) => {
          // En esta variable se almacena el blob que se retorna desde el modal y está en result
          const resultBlob = result;
          console.log('URL BLOB => ', resultBlob);
          // Se llama a la función y el resultado se asigna a la variable de tipo FileList
          // Se pasan como parámetros el blob y el nombre del archivo
          this.filesToUpload = this.dataUrlToFileList(resultBlob, 'Archivo de Prueba');

          console.log('entro al registro');
          console.log(this.filesToUpload);
          this.subirPagoDrive(this.idRegistro, this.filesToUpload);
        });
        break;
      case 'GNP' :

          console.log('entro al registro');
          console.log(this.filesToUpload);
          this.idCobroSaliente.emit({idproductoc: this.idProductoCliente, filepagado: this.filesToUpload});
        // });

          break;
      default:

        break;
    }


  }

  // Método que convierte a un nuevo blob y lo asigna a un FileList
  private dataUrlToFileList(resultBlob: Blob, fileName: string)/* : FileList */ {
    // console.log('Data URL en dataurltofilelist', resultBlob);
    // Arreglo en el cual se asignará el nuevo blob
    const FileArray: File[] = [];
    // se crea un nuevo objeto del nuevo blob
    const blobObject = new Blob([resultBlob], { type: 'application/pdf' });
    // Se crear un nuevo objeto de tipo file pasando como parametro el blob
    const file = new File([blobObject], fileName, {
      type: 'application/pdf'
    });
    // El objecto file se agrega al arreglo
    FileArray.push(file);
    // console.log("Arreglo de archivos FileList => ", FileArray);
    // Se retorna el arreglo de tipo file simulado como un FileList
    return FileArray as unknown as FileList;
  }

  emitir(idSol) {
    console.log(this.idRegistro);
    let dataEmision: any;
    this.solicitudesService.getSolicitudesByIdOnline(idSol).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;

        this.responseEmision = this.cotizacionAli.respuesta;

        switch (this.nombreAseguradora) {
          // NO ENTRA A EMITIR QUALITAS POR LA NUEVA LOGICA DE CAMBIO
          case 'QUALITASx':

            break;
          case 'GNP' :

            // si ya tiene un log de emision sin error ya se emitio bien y no va a volver a
            // utilizar el servicio de emision
            // si tiene un mensaje de error puede volver a amitir
                this.peticionE = this.cotizacionAli.peticion;
                this.productoClienteService.getProductoClienteById(this.idProductoCliente).subscribe(dataProducto => {
                  this.productoCliente = dataProducto;
                  this.responseProducto = this.productoCliente.datos;
                  this.vehiculoJson = {
                    "uso": "PARTICULAR",
                    "clave": this.responseProducto.clave,
                    "marca": this.responseProducto.marca,
                    "codUso": "",
                    "modelo": this.responseProducto.modelo,
                    "noMotor": this.responseProducto.numeroMotor,
                    "noSerie": this.responseProducto.numeroSerie,
                    "codMarca": "",
                    "noPlacas": this.responseProducto.numeroPlacas,
                    "servicio": this.responseProducto.servicio,
                    "subMarca": this.responseProducto.subMarca,
                    "descripcion": this.responseProducto.descripcion,
                    "codDescripcion": ""
                  };

                  this.clienteService.getClienteById(this.productoCliente.idCliente).subscribe(dataCliente => {
                    this.cpService.getColoniaByCp(dataCliente.cp).subscribe(datacp => {

                      if (dataCliente.numInt === null) {
                        dataCliente.numInt = '';
                      }
                      // console.log(datacp)
                      // se quitan acentos en poblacion y ciudad
                      // si no existe cuidad manda vacio
                      if (datacp[0].ciudad === null) {
                        this.poblacion = '';
                      } else if (datacp[0].ciudad !== null) {
                        this.poblacion = datacp[0].ciudad.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
                      }
                      this.ciudad = datacp[0].delMun.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
                      // console.log(this.poblacion);
                      // console.log(this.ciudad);

                      this.fechaNacCambio = dataCliente.fechaNacimiento;
                      this.fechaNacCambio = moment(dataCliente.fechaNacimiento).format('DD/MM/YYYY');

                      // se convierte codigo portal a string y si empieza con o0 se le agrega
                      this.codPostal = dataCliente.cp.toString();
                      // console.log(this.codPostal)
                      this.codPostal = this.codPostal.padStart(5, '0');
                      // console.log(this.codPostal)
                      if (this.nombreAseguradora === 'GNP'){
                        dataCliente.colonia = dataCliente.colonia.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
                      }
                      console.log(dataCliente.colonia);
                      this.clientejson = {
                        "rfc": dataCliente.rfc,
                        "curp": dataCliente.curp,
                        "edad": this.peticionE.edad,
                        "email": dataCliente.correo,
                        "genero": dataCliente.genero,
                        "nombre": dataCliente.nombre,
                        "telefono": dataCliente.telefonoMovil,
                        "direccion": {
                          "pais": dataCliente.nombrePaises,
                          "calle": dataCliente.calle,
                          "noEXT": dataCliente.numExt,
                          "noInt": dataCliente.numInt,
                          "ciudad": this.ciudad,
                          "colonia": dataCliente.colonia,
                          "codPostal": this.codPostal,
                          "poblacion": this.poblacion
                        },
                        "ocupacion": "",
                        "apellidoMat": dataCliente.materno,
                        "apellidoPat": dataCliente.paterno,
                        "tipoPersona": "",
                        "fechaNacimiento": this.fechaNacCambio,
                        "beneficiarioPreferente": ""
                      };

                      if (dataCliente.genero === 'M') {
                        delete this.clientejson['genero'];
                        this.clientejson['genero'] = "MASCULINO";
                      }
                      if (dataCliente.genero === 'F') {
                        delete this.clientejson['genero'];
                        this.clientejson['genero'] = "FEMENINO";
                      }
                      delete this.responseEmision['cliente'];
                      delete this.responseEmision['idSubRamo'];

                      this.responseEmision['cliente'] = this.clientejson;
                      console.log('aquiiiiiii');
                      console.log(this.responseEmision['cliente']);
                      this.responseEmision['emision']['fechaEmision'] = "";
                      delete this.responseEmision['vehiculo'];
                      this.responseEmision['vehiculo'] = this.vehiculoJson;
                      this.cotizacionAli['respuesta'] = this.responseEmision;
                      console.log(this.cotizacionAli);
                      this.cotizacionesAliService.putOnline(this.cotizacionAli.id, this.cotizacionAli).subscribe(result => {
                      });
                    });
                  });
                   console.log(this.vehiculoJson);
                console.log(this.responseEmision);
            });
            break;
          default:

            break;
        }
      });
    });

  }

  swalCargando2() {
    Swal.fire({
      title: 'Se está emitiendo, espere un momento.',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 5000);

  }
  // si se emitio bien
  swalCargando3() {
    Swal.fire({
      title: '¡Emison obtenida con exito!',
      text: 'Continua con Pago',
      confirmButtonText: ' Comenzar pago',
      width: 500,
    }).then(value => {
      if (value.value) {
        window.close();


      }
    });
    setTimeout(() => {
      Swal.close();
    }, 5000);
  }

  swalCargando4() {
    Swal.fire({
      title: 'Se obtuvo un error al emitir',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 6000);


  }
  // manda a llamar el servicio de pago de gnp
  pagoGnp(idCotizacionAli, $event: MouseEvent ) {
    let dataPago: any;

    console.log(this.responsePago);
    console.log(this.response);
    // servicio real
    // hace el cuadro de pago exitoso si pago y se obtiene la poliza de otro servicio postObtenerEmision
    this.catalogoService.postPago(this.urlService, this.urlPago, this.response)
      .subscribe(
        (data) => {
          dataPago = data;

        },
        (error) => {
          console.error('Error => ', error);
          this.cotizacionAli['respuesta'] = dataPago;
        },
        () => {
          this.dataArrayPago = dataPago;
          // this.responsepago = this.dataArrayPago;
          console.log(this.dataArrayPago);


          // log de pago
          this.jsonPagoGnp.request = this.response;
          delete this.response;
          this.response = this.dataArrayPago;



          this.jsonPagoGnp.idEmpleado =  sessionStorage.getItem('Empleado');
          this.jsonPagoGnp.idSolicitudVn =  this.idSolicitud;
          this.jsonPagoGnp.response = this.response;
          this.jsonPagoGnp.error = this.response['codigoError'];


          if (this.dataArrayPago['emision']['resultado'] === true) {
            this.jsonPagoGnp.poliza = this.response['emision']['poliza'];
            this.jsonPagoGnp.banderaPoliza = 1;
            this.logCotizadorService.gnppago(this.jsonPagoGnp).subscribe(log => {
            });
            this.cotizacionAli['respuesta'] = this.response;
            this.cotizacionesAliService.putOnline(idCotizacionAli, this.cotizacionAli).subscribe(result => {
            });
            this.openDialogMessageSuccess();
          } else {
            this.logCotizadorService.gnppago(this.jsonPagoGnp).subscribe(log => {
            });
            // no ha pagado-
            this.noPayment();
            console.log('El pago ha sido negado');
            this.cotizacionAli['respuesta'] = this.response;
            this.cotizacionesAliService.putOnline(idCotizacionAli, this.cotizacionAli).subscribe(result => {
            });
          }
        });
  }

  validarTarjeta(event) {
    let databanco: any;
    if (this.formDatosPago.controls.numeroTarjeta.valid){
      this.catalogoService.getValidarTarjeta(this.formDatosPago.controls.numeroTarjeta.value).subscribe({
        next: value => {
          if (this.nombreAseguradora === 'GNP'){
            this.formDatosPago.controls.idBanco.setValue(value.bank);
            this.formDatosPago.controls.idCarrier.setValue(value.carrier);
            this.cardToken = value.cardToken;
            this.metodoPagoGNP = value.typeCard;
            this.ocultarMsi = true;
            this.numTarj = this.formDatosPago.controls.numeroTarjeta.value;
            this.numTarj = this.numTarj.substring(0, 6);
            if (this.formDatosPago.controls.idFormaPago.value === 1) {
              this.catalogoService.getMsiExterno(this.nombreAseguradora, this.urlMsi, this.numTarj).subscribe(dataMsiGNP => {
                this.msiGnp = [{CLAVE: 1, NOMBRE: 'CONTADO', VALOR: 'CONTADO'}];
                dataMsiGNP['ELEMENTO'].forEach(value => {
                  this.msiGnp.push(value);
                });
              });
            }
          }
        },
        error: err => {
          this.formDatosPago.controls.numeroTarjeta.setValue('');
          this.swalCargandotarjetanovalida2();
        }
      });
    }
    switch (this.nombreAseguradora) {
      case 'QUALITAS':

        break;
      case 'GNP' :

        break;
      default:

        break;
    }
  }

  swalCargandotarjetanovalida(){
    Swal.fire({
      title: 'Error al validar la tarjeta',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 3000);
  }
  swalCargandotarjetanovalida2(){
    Swal.fire({
      title: 'Error al validar la tarjeta',
      allowOutsideClick: false,
      allowEscapeKey: false,
    });
    setTimeout(() => {
      Swal.close();
    }, 3000);
  }
  // hacer pago
  subirPagoDrive(idRegistro, idfiles) {

    // nuevo migracion
    this.btnLoadP = true;

    this.valuePdfPago = idfiles;
    console.log('direccion de pdf pago');
    console.log(this.valuePdfPago);

    this.filesToUploadPago = this.dataUrlToFileList(this.valuePdfPago, 'Archivo de Prueba');

    console.log(this.filesToUploadPago);
    console.log(idfiles);

    // nuevo

    this.pago = JSON.parse(localStorage.getItem('pago'));


    this.recibosService.getRecibosByIdRegistro(idRegistro).subscribe(dataR => {
      if (dataR) {
        this.recibo = dataR[0];

        const pago: Pagos = {
          id: 0,
          idRecibo: this.recibo.id,
          idFormaPago: this.pago.idFormaPago,
          idEstadoPago: 1,
          idEmpleado: this.pago.idEmpleado,
          fechaPago: this.pago.fechaPago,
          cantidad: this.pago.cantidad,
          archivo: '',
          datos: JSON.parse(localStorage.getItem('datosPago')),
        };

        this.registroService.getRegistroByIdOnline(this.idRegistro).subscribe(data => {

          console.log(data);
          this.registroOnline = data;
          this.registroOnline['online'] = 1;
          console.log(this.registroOnline);

          this.registroService.putOnline(this.idRegistro, this.registroOnline).subscribe({
            complete: () => {
            },
          });
        });
        this.pagosService.subirDrive(idfiles, this.route.snapshot.paramMap.get('id_solicitud'), pago).subscribe({
          next: dataP => {
            this.notificacionesService.exito('¡Tus archivos han sido subidos exitosamente!').then(() => {
              this.Inspeccione.emit(idRegistro);
            });
          },
          error: () => {
            this.notificacionesService.error();
          },
        });
      }
    });

  }
  visualizar(){

    this.matDialog.open(VisualizarPolizaComponent, {
      width: '900px',
      height: '670px',
      disableClose: false,
      data: {
        idRegistro: this.idRegistro,
      }
    }).afterClosed().subscribe(() => {

      return;
    });
  }
}
