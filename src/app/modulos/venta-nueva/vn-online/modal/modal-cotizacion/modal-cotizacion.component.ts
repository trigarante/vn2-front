import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MarcasOnline} from "../../../../../@core/data/interfaces/cotizador/marcas-online";
import {ModelosOnline} from "../../../../../@core/data/interfaces/cotizador/modelos-online";
import {DescripcionesOnline} from "../../../../../@core/data/interfaces/cotizador/descripciones-online";
import {SubmarcasOnline} from "../../../../../@core/data/interfaces/cotizador/submarcas-online";
import {CatalogoService} from "../../../../../@core/data/services/cotizador/catalogo.service";
import {CotizarRequest} from "../../../../../@core/data/interfaces/cotizador/cotizar";
import {Router} from "@angular/router";
import {ProspectoService} from "../../../../../@core/data/services/venta-nueva/prospecto.service";
import {SolicitudesvnService} from "../../../../../@core/data/services/venta-nueva/solicitudes.service";
import {CotizacionesAliService} from "../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service";
import {CotizacionesAli} from "../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali";
import {Sepomex} from '../../../../../@core/data/interfaces/catalogos/sepomex';
import {SepomexService} from '../../../../../@core/data/services/catalogos/sepomex.service';
import {map} from "rxjs/operators";
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import {urlAseguradoras} from "../../../../../@core/data/interfaces/cotizador/urlAseguradoras";

@Component({
  selector: 'app-modal-cotizacion',
  templateUrl: './modal-cotizacion.component.html',
  styleUrls: ['./modal-cotizacion.component.scss']
})
export class ModalCotizacionComponent implements OnInit {
  formCotizacion: FormGroup;

  edades = Array.from(Array(80).keys(), n => n + 17 + 1);
  descuentos = Array.from(Array(51).keys(), n => n);

  marca: string;
  modelo: string;
  submarca: string;
  descripcion: string;
  idColonia: Sepomex;
  marcas: MarcasOnline[];
  modelos: ModelosOnline[];
  descripciones: DescripcionesOnline[];
  submarcas: SubmarcasOnline[];
  colonias: any;
  coloniasF: any;

  descuento: string;
  edad: number;
  genero: string;
  fechaNacimiento;

  clave: number;
  fecha: any;
  anoActual: any;
  anoNacimiento: any;
  cotizacion: CotizarRequest ;
  jsoncotizacion: any;
  cotizacionAli: CotizacionesAli;
  cotizacionRequest: any;
  fechaNacimientoMaxima =  new Date();
  servicess: any;
  urlMarca: string;
  urlModelos: string;
  urlSubmarcas: string;
  urlDescripciones: string;


  constructor(public  dialogref: MatDialogRef<ModalCotizacionComponent>, private fb: FormBuilder,
              private catalogoService: CatalogoService, @Inject(MAT_DIALOG_DATA) public data ,
              private cpService: SepomexService,
              private notificaciones: NotificacionesService,
              private router: Router, private prospectoService: ProspectoService, private solictudesVnService: SolicitudesvnService,
              private cotizacionesAliService: CotizacionesAliService) {
    // Se iguala el objeto correspondiente al nombre de la aseguradora a la propiedad, this.servicess
    this.servicess = urlAseguradoras.find(aseg => aseg.aseguradora === this.data.nombreAseguradora);
    console.log(this.servicess);

    // Se le asigna el valor de las propiedades del objeto services a las propiedades del componente 
    this.urlMarca = this.servicess.urlMarca;
    this.urlModelos = this.servicess.urlModelos ;
    this.urlSubmarcas = this.servicess.urlSubmarcas ;
    this.urlDescripciones = this.servicess.urlDescripciones ;
  }

  ngOnInit(): void {
    this.fechaNacimientoMaxima.setMonth(this.fechaNacimientoMaxima.getMonth() - 216);
    this.formCotizacion = this.fb.group({
      edad: ['', Validators.required],
      fechaNacimiento: ['', Validators.required],
      cp: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
      genero: ['', Validators.required],
      marca: ['', Validators.required],
      modelos: ['', Validators.required],
      submarca: ['', Validators.required],
      descripcion: ['', Validators.required],
      descuento: [''],
      idColonia: [null]
    });
    this.getMarcas();
    this.getDatos();
    this.formCotizacion.controls.fechaNacimiento.valueChanges.subscribe(value => {
      if(value){
        const nacimiento = new Date(value);
        const hoy = new Date();
        this.formCotizacion.get("edad").setValue(Math.trunc((hoy.getTime() - nacimiento.getTime())/(3.156*10**10)));
        console.log('FORM EDAD', this.formCotizacion.get("edad").value);
      }
    })
  }
  getMarcas() {
    if (this.data.nombreAseguradora === 'GNP') {
      this.formCotizacion.controls.descuento.setValue(0);
    } else if (this.data.nombreAseguradora === 'MAPFRE') {
      this.formCotizacion.controls.descuento.setValue(25);
    } else {
      this.formCotizacion.controls.descuento.setValue(23);
    }
    this.formCotizacion.controls.descuento.disable();

    this.modelo = '';
    this.submarca = '';
    this.descripcion = '';
    this.formCotizacion.controls.modelos.setValue('');
    this.catalogoService.getMarcas(this.data.urlServidor, this.data.urlCatalogo, this.urlMarca).subscribe(m => {
      this.marcas = m;
    });
  }


    getModelos() {
    this.modelos = [];
    this.submarcas = [];
    this.descripciones = [];


    this.formCotizacion.controls.modelos.setValue('');
    this.formCotizacion.controls.submarca.setValue('');
    this.formCotizacion.controls.descripcion.setValue('');
    this.catalogoService.getModelos(this.data.urlServidor, this.data.urlCatalogo, this.marca, this.urlModelos).subscribe(m => {
      this.modelos = m;
    });
  }

  getSubmarca() {
    this.submarcas = [];
    this.descripciones = [];
    this.descripcion = '';
    this.formCotizacion.controls.submarca.setValue('');
    this.formCotizacion.controls.descripcion.setValue('');
    this.catalogoService.getSubMarca(this.data.urlServidor, this.data.urlCatalogo, this.marca, this.modelo, this.urlSubmarcas)
      .subscribe(m => {
        this.submarcas = m;
      });

  }

  getDescripcion() {
    this.descripciones = [];
    this.formCotizacion.controls.descripcion.setValue('');
    this.catalogoService.getDescripcion(this.data.urlServidor, this.data.urlCatalogo,
      this.marca, this.modelo,
      this.submarca, this.urlDescripciones).subscribe(m => {
      this.descripciones = m;
    });
  }

  async validarCp(value) {
    console.log('Entrrrrrrrrr');
    if (this.formCotizacion.controls.cp.valid) {
      // la promesa resuelve el sewrvicio de externo solo si estas en cotizador GNP y de lo contrario devuelve true
      const validarGNP = new Promise((resolve, reject) => {
        if (this.data.nombreAseguradora === 'GNP') {
          // servicio proporcionado por externo para ver si el cp es cotizable
          this.cpService.getCpValido(this.formCotizacion.controls.cp.value).subscribe({
            next: valid => {
              console.log('serv', valid);
              resolve(valid.valid);
            },
            error: () => {
              reject(new Error('Error en el servicio'));
              this.cpService.getColoniaByCpServer(this.formCotizacion.controls.cp.value).subscribe(data => {
                this.colonias = data;
              });
          }
        });
        } else {
          resolve(true);
        }
      });
      // dependiendo de la promesa ejecutara el sepomex o mandara una notificacion
      validarGNP.then( prom => {
        if (prom){
          console.log('EEEE');
          this.cpService.getColoniaByCpServer(this.formCotizacion.controls.cp.value).subscribe(data => {
            this.colonias = data;
          });
        } else {
          this.notificaciones.informacion('Tu código postal no es cotizable a esta aseguradora').then( () => {
            this.formCotizacion.controls.cp.reset();
          });
        }
      }).catch( error => this.notificaciones.error(error));

    }
  }
  validarCp2(value, idColonia){
    this.cpService.getColoniaByCpServer(this.formCotizacion.controls.cp.value).pipe(map(result => {
      return result.filter(data => data.idCP === idColonia);
    })).subscribe(data => {
      this.coloniasF = data[0];
      console.log(this.coloniasF);
    });
  }

  cotizar(){
    // se manda a llamar un componente donde se cotizara el auto
    console.log(this.formCotizacion.controls.idColonia.value);
    // se muestra una ventana para seleccionar los datos de cotizacion y se enviara a un modulo donde se mostrara la cotizacion real
    this.clave = this.descripciones.filter(descripcion => descripcion.text ===
      this.formCotizacion.controls.descripcion.value)[0].id,
      this.fecha = new Date();
    this.anoActual = this.fecha.getFullYear();
    
    if(!this.formCotizacion.get("edad").value){
      let hoy = new Date();
      let fecha = new Date(this.formCotizacion.getRawValue().fechaNacimiento);
      this.formCotizacion.get("edad").setValue(Math.trunc((hoy.getTime() - fecha.getTime())/(3.156*10**10)));
    }


    /*this.anoNacimiento = this.anoActual - this.formCotizacion.controls.edad.value;*/
    switch (this.data.nombreAseguradora) {
      // por aseguradora se mandan datos exactos
      case 'QUALITAS':
        this.cotizacion = {
          aseguradora: this.data.nombreAseguradora,
          clave: this.clave.toString(),
          cp: this.formCotizacion.controls.cp.value,
          descripcion: this.formCotizacion.controls.descripcion.value,
          descuento: this.formCotizacion.controls.descuento.value,
          edad: this.formCotizacion.get("edad").value.toString(),
          /*fechaNacimiento: '01/01/' + this.anoNacimiento,*/
          fechaNacimiento: (new Date(this.formCotizacion.getRawValue().fechaNacimiento).toISOString().split('T')[0]),
          genero: this.formCotizacion.controls.genero.value,
          marca: this.formCotizacion.controls.marca.value,
          modelo: this.modelo,
          subMarca:  this.formCotizacion.controls.submarca.value,
          movimiento: 'cotizacion',
          paquete: 'ALL',
          servicio: 'PARTICULAR',
          idColonia: this.formCotizacion.controls.idColonia.value.idCpBase,
          colonia: this.formCotizacion.controls.idColonia.value.d_asenta,

        };
        break;
      case 'GNP' :
        this.cotizacion = {
          aseguradora: this.data.nombreAseguradora,
          clave: this.clave.toString(),
          cp: this.formCotizacion.controls.cp.value,
          descripcion: this.formCotizacion.controls.descripcion.value,
          descuento: this.formCotizacion.controls.descuento.value,
          edad: this.formCotizacion.get("edad").value.toString(),
          fechaNacimiento: (new Date(this.formCotizacion.getRawValue().fechaNacimiento).toISOString().split('T')[0]),
          /*fechaNacimiento: '01/01/' + this.anoNacimiento,*/
          genero: this.formCotizacion.controls.genero.value,
          marca: this.formCotizacion.controls.marca.value,
          subMarca:  this.formCotizacion.controls.submarca.value,
          modelo: this.modelo,
          movimiento: 'cotizacion',
          paquete: 'All',
          servicio: 'PARTICULAR',
          idColonia: this.formCotizacion.controls.idColonia.value.idCpBase,
          colonia: this.formCotizacion.controls.idColonia.value.d_asenta,

        };
        break;
      case 'MAPFRE' :
        this.cotizacion = {
          aseguradora: this.data.nombreAseguradora,
          clave: this.clave.toString(),
          cp: this.formCotizacion.controls.cp.value,
          descripcion: this.formCotizacion.controls.descripcion.value,
          descuento: this.formCotizacion.controls.descuento.value,
          edad: this.formCotizacion.get("edad").value.toString(),
          fechaNacimiento: (new Date(this.formCotizacion.getRawValue().fechaNacimiento).toISOString().split('T')[0]),
          /*fechaNacimiento: '01/01/' + this.anoNacimiento,*/
          genero: this.formCotizacion.controls.genero.value,
          marca: this.formCotizacion.controls.marca.value,
          subMarca:  this.formCotizacion.controls.submarca.value,
          modelo: this.modelo,
          movimiento: 'cotizacion',
          paquete: 'ALL',
          servicio: 'PARTICULAR',

        };
        break;
      default:

        break;
    }

    this.jsoncotizacion = JSON.stringify(this.cotizacion);
    console.log(this.jsoncotizacion);

    if (this.data.tipo === undefined) {
      this.data.tipo = 'interno';
    }
    if (this.data.idTipoModificar === undefined) {
      this.data.idTipoModificar = 1;
    }

    if (this.data.idSolicitudDetalles === undefined) {
      this.data.idSolicitudDetalles = 1;

    }
    console.log(this.data);
    this.router.navigate(['/modulos/venta-nueva/mostrarCotizacion', this.jsoncotizacion, this.data.tipo,
      this.data.idProspecto, this.data.urlServidor, this.data.urlCotizacion, this.data.nombreAseguradora,
      this.formCotizacion.controls.submarca.value, this.data.idTipoModificar, this.data.idSolicitudDetalles, this.data.urlCatalogo, this.data.nombreCliente] );

    this.dialogref.close();
  }

  getDatos() {

    // si es recotizar pone datos del prospecto, datos de vehiculo
    // poner datos de prospecto a datos cotizacion edad copostal, genero
    // si es recotizacion volver a poner datos de marca auto

    this.prospectoService.getById(this.data.idProspecto).subscribe(
      dataPros => {
        this.formCotizacion.controls.edad.setValue(dataPros.edad);
        if (dataPros.sexo === 'M') {

          this.formCotizacion.controls.genero.setValue('MASCULINO');
        }
        if (dataPros.sexo === 'F') {

          this.formCotizacion.controls.genero.setValue('FEMENINO');
        }

      });
    // console.log(this.data.idTipoModificar)
    // si entra a recotizar busca los datos ateriores de cotizado y los vuelve a poner en su control, marca
    // modelo, submarca, descripcion
    if (this.data.idTipoModificar === '2') {
      // console.log('entro :)')
      this.solictudesVnService.getSolicitudesByIdOnline(this.data.idSolicitudDetalles).subscribe(data => {

        this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
          this.cotizacionAli = dataAli;
          this.cotizacionRequest = this.cotizacionAli.peticion;
          // console.log(this.cotizacionRequest.marca);
          // console.log(this.cotizacionRequest.marca)


          this.formCotizacion.controls.marca.setValue(this.cotizacionRequest['marca']);
          console.log(this.formCotizacion.getRawValue());
          this.marca = this.cotizacionRequest['marca'];
          this.catalogoService.getModelos(this.data.urlServidor, this.data.urlCatalogo, this.cotizacionRequest['marca'], this.urlModelos).subscribe(m => {
            this.modelos = m;
          });
          this.formCotizacion.controls.modelos.setValue(this.cotizacionRequest['modelo']);
          this.modelo = this.cotizacionRequest['modelo'];
          this.catalogoService.getSubMarca(this.data.urlServidor, this.data.urlCatalogo, this.cotizacionRequest['marca'],
            this.cotizacionRequest['modelo'], this.urlSubmarcas)
            .subscribe(m => {
              this.submarcas = m;
            });
          this.formCotizacion.controls.submarca.setValue(this.cotizacionRequest['subMarca']);
          this.submarca = this.cotizacionRequest['subMarca'];
          this.catalogoService.getDescripcion(this.data.urlServidor, this.data.urlCatalogo,
            this.cotizacionRequest['marca'],
            this.cotizacionRequest['modelo'],
            this.cotizacionRequest['subMarca'], this.urlDescripciones).subscribe(m => {
            this.descripciones = m;
          });
          this.formCotizacion.controls.descripcion.setValue(this.cotizacionRequest['descripcion']);
          this.formCotizacion.controls.cp.setValue(this.cotizacionRequest['cp']);
          this.formCotizacion.controls.fechaNacimiento.setValue(this.cotizacionRequest.fechaNacimiento);
          this.validarCp(0);
        });
      });
    }
    if (this.data.idTipoModificar === '3'){
      // volve a poner los datos al modal si sale error en cotizacion regresa esta ventana y vuelve a poner datos del vehiculo
      console.log(this.data);

      console.log(this.data.jsonCotizacion);
      this.formCotizacion.controls.marca.setValue(this.data.jsonCotizacion.marca);
      this.marca = this.data.jsonCotizacion.marca;
      this.catalogoService.getModelos(this.data.urlServidor, this.data.urlCatalogo, this.data.jsonCotizacion.marca,
        this.urlModelos).subscribe(m => {
        this.modelos = m;
      });
      this.formCotizacion.controls.modelos.setValue(this.data.jsonCotizacion.modelo);
      this.modelo = this.data.jsonCotizacion.modelo;
      this.catalogoService.getSubMarca(this.data.urlServidor, this.data.urlCatalogo, this.data.jsonCotizacion.marca,
        this.data.jsonCotizacion.modelo, this.urlSubmarcas)
        .subscribe(m => {
          this.submarcas = m;
        });
      this.formCotizacion.controls.submarca.setValue(this.data.subMarca);
      this.submarca = this.data.subMarca;
      this.catalogoService.getDescripcion(this.data.urlServidor, this.data.urlCatalogo,
        this.data.jsonCotizacion.marca,
        this.data.jsonCotizacion.modelo,
        this.data.subMarca, this.urlDescripciones).subscribe(m => {
        this.descripciones = m;
      });
      this.formCotizacion.controls.descripcion.setValue(this.data.jsonCotizacion.descripcion);
      this.formCotizacion.controls.cp.setValue(this.data.jsonCotizacion.cp);
      this.formCotizacion.controls.genero.setValue(this.data.jsonCotizacion.genero);
      this.formCotizacion.controls.edad.setValue(this.data.jsonCotizacion.edad);
      this.formCotizacion.controls.descuento.setValue(this.data.jsonCotizacion.descuento);

      this.validarCp(0);
      this.validarCp2(0, this.data.jsonCotizacion.idColonia);
      console.log(this.data.jsonCotizacion.idColonia);


      this.formCotizacion.controls.fechaNacimiento.setValue(this.cotizacionRequest.fechaNacimiento);
    }
  }

  refrescar() {
    this.formCotizacion.reset();
    this.formCotizacion.controls.descuento.setValue(
      (this.data.nombreAseguradora === 'QUALITAS') ? 23 : 
      (this.data.nombreAseguradora === 'MAPFRE') ? 25 : 0);
  }
  dismiss() {
    this.dialogref.close();
  }

}
