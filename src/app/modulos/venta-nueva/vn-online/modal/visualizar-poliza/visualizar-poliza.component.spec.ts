import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarPolizaComponent } from './visualizar-poliza.component';

describe('VisualizarPolizaComponent', () => {
  let component: VisualizarPolizaComponent;
  let fixture: ComponentFixture<VisualizarPolizaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizarPolizaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
