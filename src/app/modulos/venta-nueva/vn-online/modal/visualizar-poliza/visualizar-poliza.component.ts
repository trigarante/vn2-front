import {Component, Inject, OnInit} from '@angular/core';
import {RegistroService} from "../../../../../@core/data/services/venta-nueva/registro.service";
import {DriveService} from "../../../../../@core/data/services/ti/drive.service";
import {NotificacionesService} from "../../../../../@core/data/services/others/notificaciones.service";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-visualizar-poliza',
  templateUrl: './visualizar-poliza.component.html',
  styleUrls: ['./visualizar-poliza.component.scss']
})
export class VisualizarPolizaComponent implements OnInit {
  arrayArchivos;
  pdf: any;
  documentos: any = [
    {
      tipo: 'Cliente',
      img: null,
      id: 1,
    },
    {
      tipo: 'Poliza',
      img: null,
      id: 2,
    },
    ];
  registro: number;
  constructor( private registroService: RegistroService,
               private driveService: DriveService,
               private notificaciones: NotificacionesService,
               @Inject(MAT_DIALOG_DATA) public dataE: any,
               ) {this.registro = this.dataE.idRegistro;
  }

  ngOnInit(): void {
    this.registroService.getRegistroByIdViewer(this.registro).subscribe({
      next: data => {
        this.arrayArchivos = [data.carpetaCliente, data.archivoPoliza, data.archivoPago, data.archivoInspeccion];
        console.log(this.arrayArchivos);

        this.driveService.getArchivo(this.arrayArchivos[1], this.documentos[1].id).subscribe({
          next: result => {
            this.pdf = result[0];
            this.notificaciones.cerrar();
          },
          error: () => this.notificaciones.error(),
        });
      },
      complete: null
    });
  }

}
