import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarAgenteComponent } from './agregar-agente.component';

describe('AgregarAgenteComponent', () => {
  let component: AgregarAgenteComponent;
  let fixture: ComponentFixture<AgregarAgenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarAgenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarAgenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
