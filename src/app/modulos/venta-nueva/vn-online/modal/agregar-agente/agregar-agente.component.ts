import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CatalogoService} from "../../../../../@core/data/services/cotizador/catalogo.service";
import Swal from 'sweetalert2';
import {TicketsInspeccion} from "../../../../../@core/data/interfaces/cotizador/ticketsInspeccion";
import {TicketsInspeccionService} from "../../../../../@core/data/services/cotizador/tickets-inspeccion.service";
import {ActivatedRoute} from "@angular/router";
import {finalize, map} from "rxjs/operators";
import { InspeccionResp } from 'src/app/@core/data/interfaces/venta-nueva/Inspeccion.interface';
import { TicketStatus } from '../../../../../@core/data/interfaces/venta-nueva/Inspeccion.interface';

@Component({
  selector: 'app-agregar-agente',
  templateUrl: './agregar-agente.component.html',
  styleUrls: ['./agregar-agente.component.scss']
})
export class AgregarAgenteComponent implements OnInit {
  formAgente: FormGroup;
  formticket: FormGroup;
  ticketsInspeccion: TicketsInspeccion;
  ticketsInspeccions: TicketsInspeccion[];
  ticketsInspeccionMofidicar: TicketsInspeccion;
  solicitudVn: number;
  nuevoNombre: string;
  nombre: string;
  inspeccionResp: InspeccionResp;
  ticketResp: TicketStatus;
  loading: boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data,  private fb: FormBuilder,
              private catalogoService: CatalogoService, public  dialogref: MatDialogRef<AgregarAgenteComponent>,
              private ticketsInspeccionService: TicketsInspeccionService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.formAgente = this.fb.group({
      agente: new FormControl('', Validators.required),
      cuenta: new FormControl('', Validators.required),
    });
    this.formticket = this.fb.group({
      agente: new FormControl('', Validators.required),
      ticket: new FormControl('', Validators.required),
    });
    this.getTickets();
  }
  // se manda a llamar el servicio de qualitas para generar el link de inspeccion y se enviara al usuario y se
  // guardara el ticket en nuestra base de datos
  inspeccionar() {
    this.loading = true; // empieza a ejecutarse el método, el estado será cargando hasta que terminen todas las peticiones.
    this.nuevoNombre = this.data.nombreAsegurado;
    this.nombre = this.nuevoNombre.substring(0,9);
    console.log(this.nombre);

    const body = {
      "serie": this.data.serie,
      "claveAgente": this.formAgente.controls.agente.value,
      "telAsegurado": this.data.telAsegurado,
      "correo": this.data.correo,
      "nombreAsegurado": this.nombre,
      "usuario": this.formAgente.controls.cuenta.value,
      "correoAdicional": this.data.correoAdicional
    };

    // se llama el servicio de qualitas para generar una inspeccion, si reresa 0 me mando con un error
    // si regresa 1 se genero bien el ticket y se envia un link y un msm al usuario para que pueda subir foto de si vehiculo
    this.catalogoService
      .getInpeccionPrevia(body)
      .pipe(finalize(()=>{
        // se utiliza el operador finalize para saber cuando termina exactamente la subscripción
        // Como ya finalizo la subscripción se indica que no hay nada cargando
        this.loading = false
      }))
      .subscribe({
        next: (data) => {
          console.log(data);
          this.inspeccionResp = data;
        },
        complete: () => {
          if(this.inspeccionResp.codigo === "0"){
            if(this.inspeccionResp.mensaje === "Error en envío de mensaje"){
              Swal.fire({
                text: "Número de teléfono invalido, favor de virificarlo",
                icon: "error"
              });
            } else {
              Swal.fire({
                text: this.inspeccionResp.mensaje,
                icon: "error"
              });
            }
          } else{
            if(this.inspeccionResp.respuesta === null){
              // generalmente si regresa null la respuesta de qualitas, es por que es auto esta inspeccionado ya- regresara en
              // mensjae
              Swal.fire({
                text: this.inspeccionResp.mensaje
              });
            } else {
              this.solicitudVn = this.data.solicitudVn;
              // se creara o modificara un ticket en la base de datos
              this.ticketsInspeccionService.getSolicitudVNById(this.solicitudVn).pipe(finalize(() => {
                Swal.fire({
                  text: 'Se envio la solicitud correctamente via sms y correo, número de ticket: ' + this.inspeccionResp.respuesta,
                  icon: "success"
                }).then( result => {
                  if(result.isConfirmed){
                    this.dialogref.close();
                  }
                });
              })).subscribe(
                dataTicket => {
                  this.ticketsInspeccionMofidicar = dataTicket;
                  // si hay un archivo con ticket 0 lo modiicara y le pondra el num de ticket
                  if (this.ticketsInspeccionMofidicar['ticket'] === 0) {
                    this.ticketsInspeccionMofidicar['ticket'] = this.inspeccionResp.respuesta;
                    this.ticketsInspeccionMofidicar['inspeccionPrevia'] = 1;
                    this.ticketsInspeccionService.put(this.ticketsInspeccionMofidicar, this.ticketsInspeccionMofidicar.id).subscribe(
                      dataput => {
                        return dataput;
                      });
                    // si el arcivo ya tiene num ticket, hara un nuevo tickets
                  } else if (this.ticketsInspeccionMofidicar['ticket'] !== 0) {
                    const tickes = {
                      id: 0,
                      idEmpleado: Number(sessionStorage.getItem('Empleado')),
                      solicitudVn: this.data.solicitudVn,
                      ticket: this.inspeccionResp.respuesta,
                      status: 0,
                      inspeccionPrevia: 1,
                      claveAgente: body.claveAgente
                    };
                    this.ticketsInspeccionService.post(tickes).subscribe(
                      datacTicket => {
                        return datacTicket;
                      });
                  }
                  return dataTicket;
                });
            }
          }
        },
        error: error => {
          console.log(error);
          Swal.fire({
            text: "Ha ocurrido un error inesperado, verifique la conexion a internet",
            icon: "error"
          });
        }
    });
  }

  ticket(){
    this.loading = true;
    // Se manda a llamar el servicio externo de qualitas para checar si esta inspeccionado el auto o no por medio del ticket
    this.catalogoService.getTicketInspeccion(this.formticket.controls.ticket.value, this.formticket.controls.agente.value ).pipe(finalize(()=>{
      this.loading = false;
    })).subscribe({
      next: (data) => {
        this.ticketResp = data;
      },
      complete: () => {
        if(this.ticketResp.codigo === "0"){
          Swal.fire({
            text: this.ticketResp.mensaje,
            icon: 'error'
          })
        } else{
          if(this.ticketResp.codigo === "1"){
            this.ticketsInspeccionService.getByTicket(this.formticket.controls.ticket.value).subscribe(
                    dataTickett => {
                      this.ticketsInspeccion = dataTickett;
                      this.ticketsInspeccion['status'] = 1;
                      this.ticketsInspeccionService.put(this.ticketsInspeccion, this.ticketsInspeccion.id).subscribe(
                        dataput => {
                          return this.ticketResp;
                        });
                      return this.ticketResp;
            });
            Swal.fire({
              text: 'Vehículo Inspeccionado, podras generar la emision',
              icon: 'success'
            }).then(result => {
              this.dialogref.close("1");
            });
          } else {
            Swal.fire({
              text: 'Aún no se ha inspeccionado el vehículo',
              icon: 'info'
            }).then(result => {
              if(result.isConfirmed){
                this.dialogref.close();
              }
            });
          }
        }
      },
      error: error => {
        console.log(error);
        Swal.fire({
          text: "Ha ocurrido un error inesperado, verifique la conexion a internet",
          icon: "error"
        }).then(result => {
          if(result.isConfirmed){
            this.dialogref.close();
          }
        });
      }
    });
  }
  // muestra todos  los ticket que se hicieron en esa solicitud Vn
  getTickets(){
    this.solicitudVn = this.data.solicitudVn;
    this.ticketsInspeccionService.getSolicitudById(this.solicitudVn).pipe(map(result => {
      return result.filter(data => data.ticket !== 0);
    })).subscribe(
      dataTicket => {
        this.ticketsInspeccions = dataTicket;
        return dataTicket;
      });
  }
}
