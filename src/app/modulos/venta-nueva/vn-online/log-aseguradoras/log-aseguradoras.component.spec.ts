import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogAseguradorasComponent } from './log-aseguradoras.component';

describe('LogAseguradorasComponent', () => {
  let component: LogAseguradorasComponent;
  let fixture: ComponentFixture<LogAseguradorasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogAseguradorasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogAseguradorasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
