import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {ModalCotizacionComponent} from "../modal/modal-cotizacion/modal-cotizacion.component";
import {NotificacionesService} from "../../../../@core/data/services/others/notificaciones.service";
import {SnackPausaComponent} from "../../../telefonia/modals/snack-pausa/snack-pausa.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {SocketsService} from "../../../../@core/data/services/telefonia/sockets/sockets.service";
import {
  EstadoUsuarioTelefoniaService
} from "../../../../@core/data/services/telefonia/estado-usuario-telefonia.service";
import {ToolbarComponent} from "../../../../@theme/toolbar/toolbar.component";
import {SolicitudesvnService} from "../../../../@core/data/services/venta-nueva/solicitudes.service";

@Component({
  selector: 'app-log-aseguradoras',
  templateUrl: './log-aseguradoras.component.html',
  styleUrls: ['./log-aseguradoras.component.scss']
})
export class LogAseguradorasComponent implements OnInit {

  aseguradoras: any[] = [{value: '', aseguradora: 'QUALITAS', activo: 1, service: 'CATALOGO_AUTOSQUALITAS',
    catalogo: 'catalogos/', urlCotizacion: 'qualitas_cotizaciones/autos',
    departamento: [{depa: 3}, {depa: 2}, {depa: 1}, {depa: 33}, {depa: 37} , {depa: 36},
      {depa: 62} , {depa: 44}, {depa: 34},  {depa: 47},
       {depa: 63} , {depa: 85}, {depa: 48},
      {depa: 86} , {depa: 93} , {depa: 105} , {depa: 106} , {depa: 114} , {depa: 116},
      {depa: 158}, {depa: 159}, {depa: 165}, {depa: 210}, {depa: 223}, {depa: 227},
      {depa: 241}, {depa: 147}, {depa: 148}, {depa: 247}, {depa: 248},
      {depa: 79}, {depa: 78}, {depa: 249}, {depa: 167}, {depa: 163},
      {depa: 90}, {depa: 98}, {depa: 99}, {depa: 104}]},
    {value: '', aseguradora: 'GNP', activo: 1, service: 'CATALOGO_AUTOSGNP',
      catalogo: 'gnp_catalogos_autos/', urlCotizacion: 'gnp_autos/cotizaciones',
      urlEmision: 'gnp_autos/emisiones_full', urlPago: 'pago_gnp_3D/crear_id_pago_3d',
      departamento: [{depa: 1}, {depa: 3}, {depa: 0}, {depa: 2}, {depa: 33}, {depa: 34}, {depa: 36},
        {depa: 37}, {depa: 113}, {depa: 0}, {depa: 80}, {depa: 81}, {depa: 47},
        {depa: 91}, {depa: 48}, {depa: 157}, {depa: 158}, {depa: 166}, {depa: 225}, {depa: 245}, {depa: 248},
        {depa: 79}, {depa: 78}, {depa: 249}, {depa: 167}, {depa: 163},
        {depa: 90}, {depa: 98}, {depa: 99}, {depa: 104}]
    },
    {value: '', aseguradora: 'MAPFRE', service: 'CATALOGO_AUTOSMAPFRE', catalogo: 'v1/mapfre-car/',
      activo: 1, urlCotizacion: 'v1/mapfre-car/quotations-interno', departamento: [{depa: 3},
        {depa: 2}, {depa: 1}, {depa: 33}, {depa: 37}, {depa: 36}, {depa: 163}, {depa: 122}, {depa: 249},
        {depa: 136}, {depa: 246}, {depa: 258} , {depa: 267}, {depa: 105} , {depa: 109}, {depa: 248}, {depa: 158}, {depa: 48}]},
    {value: '', aseguradora: 'ABA', activo: 0, departamento: [{depa: 0}, {depa: 0}, {depa: 0}]},
    // {value: '', aseguradora: 'ABA2', activo: 0},
    {value: '', aseguradora: 'AFIRME', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'AIG', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'ANA', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'ATLAS', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'AXA', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'BANCOMER', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'BANORTE', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'BXMAS', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'ELAGUILA', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'ELPOTOSI', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'GENERALDESEGUROS', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'HDI', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'INBURSA', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'LALATINO', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'MIGO', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'PRIMEROSEGUROS', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'SURA', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'ZURICH', activo: 0, departamento: [{depa: 0}]},
  ];
  @Input() idProspecto: number;
  @Input() idSolicitudDetalles: number;
  @Input() tipoContacto: string;
  @Input() idTipoModificar: number;
  nombreCliente: string;
  idDepartamento =  +sessionStorage.getItem('idDepartamento');
  valor: boolean;
  extension = localStorage.getItem('extension');
  idUsuario = sessionStorage.getItem('Usuario');
  constructor(private dialog: MatDialog,
              private notificacionesService: NotificacionesService,
              private snakBar: MatSnackBar,
              private socketsService: SocketsService,
              private telefoniaUsuariosService: EstadoUsuarioTelefoniaService,
              private toolbarComponent: ToolbarComponent,
              private solicitudesService: SolicitudesvnService,
  ) {
  }

  ngOnInit(): void {
    // se mostrara todas las aseugradoras para cotizar al dar clic se enviara que tipos de auto para poder cotizar
    this.getAseguradoras();
    this.c(5);
  }

  // se bloqueara la aseguradora si el ejecutivo no tiene el idDepartamento correspondiente a la aseguradora
  c(depa) {
    let index;
    // se bloquea la aseguradora si no encuentra el idDepartamento del empleadoen el json
    for (index in this.aseguradoras) {
      const depas = this.aseguradoras[index].departamento;
      const finddepartamento = depas.find(departamentos => departamentos.depa === this.idDepartamento);
      // console.log(finddepartamento);
      if (finddepartamento === undefined) {
        this.aseguradoras[index].activo = 0;
      }
    }

  }

  getAseguradoras() {

  }

  // se abrira un modal donde se seleccionara el tipo de autos para poder cotizar
  getMarcas(aseguradora, event, catalogo, urlCotizacion) {
    this.solicitudesService.getSolicitudesById(this.idSolicitudDetalles).subscribe({
      next: value => {
        this.nombreCliente = value.nombreCliente;
        this.dialog.open(ModalCotizacionComponent, {
          width: 'auto',
          panelClass: 'modalCotizacion',
          data: {
            nombreAseguradora: aseguradora,
            urlServidor: event,
            tipo: this.tipoContacto,
            idProspecto: this.idProspecto,
            urlCatalogo: catalogo,
            urlCotizacion,
            idTipoModificar: this.idTipoModificar,
            idSolicitudDetalles: this.idSolicitudDetalles,
            nombreCliente: this.nombreCliente
          },
        });
      }
    });
    /*** Valida si estas conectado ***/
  //   this.telefoniaUsuariosService.getEstadoUsuario(Number(sessionStorage.getItem('Usuario'))).subscribe((data: number) => {
  //     if (data === 1) {
  //       this.toolbarComponent.usuario.estadoUsuario = 2;
  //       this.notificacionesService.informacionTimer('Se te pondra en pausa para realizar la contización.').then(() => {
  //         /*** Pausa en sockect ***/
  //         this.socketsService.conectarExtension(2, 'pausa');
  //         /*** Snack de pausa ***/
  //         this.snakBar.openFromComponent(SnackPausaComponent, {
  //           data: {
  //             idPausaUsuario: 24,
  //             tiempo: '2700',
  //           }
  //         }).afterDismissed().subscribe(() => {
  //           this.toolbarComponent.usuario.estadoUsuario = 1;
  //         });
  //         this.solicitudesService.getSolicitudesById(this.idSolicitudDetalles).subscribe({
  //           next: value => {
  //             this.nombreCliente = value.nombreCliente;
  //             this.dialog.open(ModalCotizacionComponent, {
  //               width: 'auto',
  //               panelClass: 'modalCotizacion',
  //               data: {
  //                 nombreAseguradora: aseguradora,
  //                 urlServidor: event,
  //                 tipo: this.tipoContacto,
  //                 idProspecto: this.idProspecto,
  //                 urlCatalogo: catalogo,
  //                 urlCotizacion,
  //                 idTipoModificar: this.idTipoModificar,
  //                 idSolicitudDetalles: this.idSolicitudDetalles,
  //                 nombreCliente: this.nombreCliente
  //               },
  //             });
  //           }
  //         });
  //       });
  //     } else {
  //       this.solicitudesService.getSolicitudesById(this.idSolicitudDetalles).subscribe({
  //         next: value => {
  //           this.nombreCliente = value.nombreCliente;
  //           this.dialog.open(ModalCotizacionComponent, {
  //             width: 'auto',
  //             panelClass: 'modalCotizacion',
  //             data: {
  //               nombreAseguradora: aseguradora,
  //               urlServidor: event,
  //               tipo: this.tipoContacto,
  //               idProspecto: this.idProspecto,
  //               urlCatalogo: catalogo,
  //               urlCotizacion,
  //               idTipoModificar: this.idTipoModificar,
  //               idSolicitudDetalles: this.idSolicitudDetalles,
  //               nombreCliente: this.nombreCliente
  //             },
  //           });
  //         }
  //       });
  //     }
  //   });
  }
}
