import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewStepOnlineComponent } from './new-step-online.component';

describe('NewStepOnlineComponent', () => {
  let component: NewStepOnlineComponent;
  let fixture: ComponentFixture<NewStepOnlineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewStepOnlineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewStepOnlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
