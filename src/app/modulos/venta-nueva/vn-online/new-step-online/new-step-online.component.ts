import {Component, OnInit, ViewChild} from '@angular/core';
import {MatStepper} from '@angular/material/stepper';
import {CotizacionesAli} from '../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';
import {ClienteVn} from '../../../../@core/data/interfaces/venta-nueva/cliente-vn';
import {Solicitudes, SolicitudesView} from '../../../../@core/data/interfaces/venta-nueva/solicitudes';
import {ActivatedRoute, Router} from '@angular/router';
import {StepsCotizadorService} from '../../../../@core/data/services/venta-nueva/steps-cotizador.service';
import {SolicitudesvnService} from '../../../../@core/data/services/venta-nueva/solicitudes.service';
import Swal from 'sweetalert2';
import {StepsCotizador} from '../../../../@core/data/interfaces/venta-nueva/steps-cotizador';
import {RegistroService} from '../../../../@core/data/services/venta-nueva/registro.service';
import {CotizacionesAliService} from '../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {TicketsInspeccion} from "../../../../@core/data/interfaces/cotizador/ticketsInspeccion";
import {TicketsInspeccionService} from "../../../../@core/data/services/cotizador/tickets-inspeccion.service";
import {map} from "rxjs/operators";
import moment from "moment";
import {ClienteVnService} from "../../../../@core/data/services/venta-nueva/cliente-vn.service";
import {SepomexService} from "../../../../@core/data/services/catalogos/sepomex.service";
import {ProductoClienteService} from "../../../../@core/data/services/venta-nueva/producto-cliente.service";
import {ProductoCliente} from "../../../../@core/data/interfaces/venta-nueva/producto-cliente";
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {LogCotizadorService} from "../../../../@core/data/services/cotizador/log-cotizador.service";

@Component({
  selector: 'app-new-step-online',
  templateUrl: './new-step-online.component.html',
  styleUrls: ['./new-step-online.component.scss']
})
export class NewStepOnlineComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;
  idSolicitud: number;
  solicitud: SolicitudesView;
  recibos: boolean;
  // Variables para el step de cliente
  idCliente: number = undefined;
  // Variables para el step de producto cliente
  idProductoCliente: number = undefined;
  // Variables para el step de registro poliza
  idRegistro: number = undefined;
  stepsIdentificados = false;
  datosCliente: ClienteVn;
  showStep = false;
  showStepEmitir = false;
  // por lo mientras solo para qualitas
  ocularStepRegistro = false;
  cotizacionAli: CotizacionesAli;
  cotizacionAli2: CotizacionesAli;
  responseEmision: any;
  responseEmision2: any;
  responseEmisionPolizaPagada: any;
  banderapagado: any;
  coverageArray: Array<any>;
  cobertura: any;
  nombreCobertura: any;
  primaTotall: any;
  precioTotal: any;
  ahorro: any;
  filesToUploadPagado: FileList;
  nombreAseguradorab: string;

  arrayDownladedPDF: Array<{
    solicitudID: number,
    downloaded: boolean
  }> = [];
  stepIcons = [
    'face_retouching_natural',
    'airport_shuttle',
    'payment',
    'post_add',
    'content_paste_search'
  ];
  displayedColumns: string[] = ['nombre', 'sumaAsegurada'];
  dataSource: Array<any>;
  ocultarEmision: boolean;
  inspeccion = false;
  // bandera para identificar si entro a inspeccion vehicular true- cotizacion normal, false- entro inspeccion vehicular
  ocultarinspeccionvechicular = false;
  responseEmisionPoliza: any;
  ticketsInspeccions: TicketsInspeccion[];
  ticketsInspeccion: TicketsInspeccion;
  banderaAutoInspeccionado = false;
  peticionE: any;
  vehiculoJson: object;
  ciudad: string;
  poblacion: string;
  fechaNacCambio: any;
  codPostal: string;
  clientejson: object;
  responseProducto: any;
  productoCliente: ProductoCliente;
  constructor(
    private notificacionesService: NotificacionesService,
    private route: ActivatedRoute,
    private router: Router,
    private stepsService: StepsCotizadorService,
    private solicitudesService: SolicitudesvnService,
    private registroData: RegistroService,
    private cotizacionesAliService: CotizacionesAliService,
    private ticketsInspeccionService: TicketsInspeccionService,
    protected clienteService: ClienteVnService,
    protected cpService: SepomexService,
    private productoClienteService: ProductoClienteService,
    private logCotizadorService: LogCotizadorService
  ) {
    this.notificacionesService.cerrar();
    // this.getCotizacion(this.idSolicitud);
  }

  ngOnInit(): void {
    this.idSolicitud = parseFloat(this.route.snapshot.paramMap.get('id_solicitud'));
    // console.log(this.idSolicitud);
    this.nombreAseguradorab = this.route.snapshot.paramMap.get('nombreAseguradora');
    this.downloadedpdfmethod();
    this.verificarInspeccion();
    this.getSolicitud();
    this.getCotizacion(this.idSolicitud);
    this.getNuevaEmision();
  }

  private downloadedpdfmethod() {
    const itemDownloadPDF = {solicitudID: this.idSolicitud, downloaded: false};
    if (!localStorage.getItem('arrayDownloadedPdf')) {
      this.arrayDownladedPDF.push(itemDownloadPDF);
      localStorage.setItem('arrayDownloadedPdf', JSON.stringify(this.arrayDownladedPDF));
    } else {
      this.arrayDownladedPDF = JSON.parse(localStorage.getItem('arrayDownloadedPdf'));
      const resultFind = this.arrayDownladedPDF.find(e => e.solicitudID === this.idSolicitud);
      if (resultFind === undefined) {
        this.arrayDownladedPDF.push(itemDownloadPDF);
        localStorage.removeItem('arrayDownloadedPdf');
        localStorage.setItem('arrayDownloadedPdf', JSON.stringify(this.arrayDownladedPDF));
      }
    }
    console.log('Array Downloaded PDF => ', JSON.parse(localStorage.getItem('arrayDownloadedPdf')));
  }

  getSolicitud() {
    this.solicitudesService.getSolicitudesByIdOnline(this.idSolicitud).subscribe(data => {
        this.solicitud = data;
      }, error => {
      },
      () => {
        this.getStepActual();
      });
  }

  nextSteper(num?: number) {
    this.stepper.next();
  }

  activarProductoCliente(value: any) {
    this.idProductoCliente = 0;
    this.idCliente = value.idCliente;
    this.stepper.next();
  }

  activarCobro(value: number) {
    this.ocultarEmision = true;
    this.idProductoCliente = value;
    // console.log(this.idProductoCliente);
    // console.log(value)
    this.datosCliente = {...this.datosCliente};
    switch (this.nombreAseguradorab) {
      case 'QUALITAS':
        break;
      default:

        break;
    }
    this.nextSteper();
  }

  activarCobrodesdeEmitir(value: any) {
    this.ocultarEmision = true;
    this.idProductoCliente = value;
    this.idRegistro = value.idRegistro;
    // console.log(this.idProductoCliente);
    // console.log(value)
    this.datosCliente = {...this.datosCliente};
    this.nextSteper();
  }

  activarEmitir(value: number) {
    this.idProductoCliente = value;
    this.showStepEmitir = true;
    this.stepper.next();
    this.inspeccion = true;
  }

  activarRegistroPoliza(value: any) {
    this.idRegistro = 0;
    this.idProductoCliente = value.idproductoc;
    this.datosCliente = {...this.datosCliente};
    this.filesToUploadPagado = value.filepagado;
    // console.log(value)
    // console.log(value.filepagado)
    this.nextSteper();
  }

  activarInspecciones(idRegistro: number) {
    this.idRegistro = idRegistro;
    this.stepper.next();
  }

  anterioStep() {
    this.stepper.previous();
  }

  getStepActual() {
    let infoCliente: StepsCotizador;
   // el servicio trae los datos de la solicutud y si tiene idCliente, IdProducto, IdRegistro, IdPago, idRecibo
    this.stepsService.getByIdOnline(this.idSolicitud).subscribe(
      data => {
        infoCliente = data[0];
      },
      () => {
        Swal.fire({
          text: 'Ocurrió un error al cargar la solicitud, se te regresará al menú de solicitudes',
        }).then(() => {
          window.close();
        });
      }, () => {
        // si ya hay cliente lo guarda en el json

        this.guardarjson(infoCliente);

          // si ya trae IdProductoCliente abrira step cliente, vehiculo
        if (infoCliente.idProductoCliente !== null) {
          console.log(infoCliente.idCliente);
          this.idCliente = infoCliente.idCliente;
          this.idProductoCliente = infoCliente.idProductoCliente;
          // this.idRegistro = 0;
          this.datosCliente = {...this.datosCliente};
          this.stepper.selectedIndex = 1;
          // console.log(this.stepper.selectedIndex);
          // this.stepper.selectedIndex = 2;

          switch (this.nombreAseguradorab) {
            case 'QUALITAS':
              // si es cotizacion normal se pare en emision
              if (this.ocultarinspeccionvechicular === true) {
                this.stepper.selectedIndex = 2;
                this.inspeccion = true;
              }
              // si es inspeccion se pare en step vehiculo y continue a emision si es auto esta inspeccionado
              if (this.ocultarinspeccionvechicular === false) {

                if (this.banderaAutoInspeccionado === true) {
                  this.stepper.selectedIndex = 2;
                }
              }
              break;
            case 'GNP' :
              this.stepper.selectedIndex = 2;
              // si ya si tiene un registro en cotizacion ali, en respuesta de que se obtubo una poliza, siempre
              // va a pasar a registro gnp si no tiene poliza se queda en pago

              this.solicitudesService.getSolicitudesByIdOnline(this.idSolicitud).subscribe(dataSol => {

                this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
                  this.cotizacionAli = dataAli;
                  console.log('----ali', dataAli);
                  // console.log(this.cotizacionAli);
                  this.responseEmisionPolizaPagada = this.cotizacionAli.respuesta;

                  this.banderapagado =  this.responseEmisionPolizaPagada['emision']['poliza'];
                  // si tiene poliza en respuesta en emision va al step de pago
                  if ( this.responseEmisionPolizaPagada['emision']['poliza'] !== '' ){
                    this.stepper.selectedIndex = 3;
                    // manda los valores requeridos al step registro gnp
                    this.idRegistro = 0;
                    this.idProductoCliente = infoCliente.idProductoCliente;
                    this.datosCliente = {...this.datosCliente};
                  }
                }); // fin cotizacion

              }); // fin solicitud
              break;
            default:

              break;
          }
        }
        // si tiene IdRegistro (ya se registro la poliza) que se quede en el step emision
        // si tiene pago que vaya al step pago
        if (infoCliente.idRegistro !== null) {
          this.idRegistro = infoCliente.idRegistro;
          this.datosCliente = {...this.datosCliente};
          switch (this.nombreAseguradorab) {
            case 'QUALITAS':
              this.ocultarEmision = true;
              this.inspeccion = true;
              break;
            case 'GNP' :
              break;
            default:

              break;
          }

          if (infoCliente.pagos > 0) {
            this.stepper.selectedIndex = 4;
          } else {
            this.stepper.selectedIndex = 3;
          }
        }
        this.stepsIdentificados = true;
      });
  }

  ultimoStep(step: number) {
    switch (step) {
      case 1:
        this.stepper.selectedIndex = 5;
        break;
      case 2:
        this.stepper.selectedIndex = 6;
        break;
    }

  }

  getCotizacion(idSolicitud) {
    // muestre los datos en resumen
    // let dataCotizacion: any;
    this.solicitudesService.getSolicitudesByIdOnline(idSolicitud).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        console.log('----ali', dataAli);
        // console.log(this.cotizacionAli);
        this.responseEmision = this.cotizacionAli.respuesta;
        console.log(this.responseEmision['coberturas'])


        const coverages = this.responseEmision['coberturas'];
        // console.log(coverages)

        this.coverageArray = coverages;
        this.nombreCobertura = this.responseEmision['paquete'];
        this.primaTotall = this.responseEmision['cotizacion']['primaTotal'];
        console.log(this.responseEmision['cotizacion']['primaTotal'])
        // QUALITAS
        if (this.ocultarinspeccionvechicular === true) {
          this.precioTotal = Math.round((+this.primaTotall / 77) * 100);
        } else if (this.ocultarinspeccionvechicular === false) {
          this.precioTotal = Math.round((+this.primaTotall / 70) * 100);
        }
        // this.precioTotal = Math.round((+this.primaTotall / 77) * 100);
        this.ahorro = Math.round((+this.precioTotal) - (+this.primaTotall));
        console.log(this.coverageArray)

        // this.responseEmision['cliente'] = this.clientejson;
        this.cotizacionAli['respuesta'] = this.responseEmision;
      }); // fin cotizacion

    }); // fin solicitud
  }

  getNuevaEmision() {
    // si es gnp cambiara los step
    // gnp - cliente, vehiculo, pago, emision, inspeccion
    this.ocultarEmision = false;
    switch (this.nombreAseguradorab) {
      case 'QUALITAS':
        // BANCOS PARA QUALITAS
        this.showStepEmitir = true;
        this.ocultarEmision = false;
        this.ocularStepRegistro = false;
        this.stepIcons = [
          'face_retouching_natural',
          'airport_shuttle',
          'subtitles',
          'payment',
          'content_paste_search'
        ];
        break;
      case 'GNP' :
        this.showStepEmitir = false;
        this.ocultarEmision = true;
        this.ocularStepRegistro = true;
        break;
      default:

        break;
    }
  }

//  checa si es por inspeccion previa
  verificarInspeccion() {
    // se consulta
    // identifica si se entro por inspeccion previa o no verificando si se hiso una tabla de inspeccion
    this.ticketsInspeccionService.getSolicitudById(this.idSolicitud).pipe(map(result => {
      return result.filter(data => data.inspeccionPrevia === 1);
    })).subscribe(
      dataTicket => {
        this.ticketsInspeccions = dataTicket;
        // si no hay datos en la tabla tickets no entra a inpeccion previa
        if (dataTicket.length === 0) {
          console.log('entro')
          // no hay tickets no entro a inpeccion previa
          this.ocultarinspeccionvechicular = true;
        } else {
          // si hay un dato en la tabla de ticket entra a inspeccion previa
          this.ocultarinspeccionvechicular = false;

          // verificar futuro
          // // si el ticket tiene status 1 ya verificado el step se va a pasar a emitir
          // if (dataTicket[0].status === 1){
          //   this.banderaAutoInspeccionado = true;
          // }
          // dataTicket[0].status = 1;
          // this.ocultarinspeccionvechicular = true;

        }
        return dataTicket;
      });
  }

  guardarjson(infoCliente) {
    this.solicitudesService.getSolicitudesByIdOnline(this.idSolicitud).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli2 = dataAli;

        this.peticionE = this.cotizacionAli.peticion;

        this.responseEmision2 = this.cotizacionAli.respuesta;
        if (infoCliente.idCliente !== null) {
          this.clienteService.getClienteById(infoCliente.idCliente).subscribe(dataCliente => {
            this.cpService.getColoniaByCp(dataCliente.cp).subscribe(datacp => {

              if (dataCliente.numInt === null) {
                dataCliente.numInt = '';
              }
              // console.log(datacp)
              // se quitan acentos en poblacion y ciudad
              // si no existe cuidad manda vacio
              if (datacp[0].ciudad === null) {
                this.poblacion = '';
              } else if (datacp[0].ciudad !== null) {
                this.poblacion = datacp[0].ciudad.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
              }
              this.ciudad = datacp[0].delMun.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
              // console.log(this.poblacion);
              // console.log(this.ciudad);

              this.fechaNacCambio = dataCliente.fechaNacimiento;
              this.fechaNacCambio = moment(dataCliente.fechaNacimiento).format('DD/MM/YYYY');

              // se convierte codigo portal a string y si empieza con o0 se le agrega
              this.codPostal = dataCliente.cp.toString();
              // console.log(this.codPostal)
              this.codPostal = this.codPostal.padStart(5, '0');
              // console.log(this.codPostal)
              this.clientejson = {
                "rfc": dataCliente.rfc,
                "curp": dataCliente.curp,
                "edad": this.peticionE.edad,
                "email": dataCliente.correo,
                "genero": dataCliente.genero,
                "nombre": dataCliente.nombre,
                "telefono": dataCliente.telefonoMovil,
                "direccion": {
                  "pais": dataCliente.nombrePaises,
                  "calle": dataCliente.calle,
                  "noExt": dataCliente.numExt,
                  "noInt": dataCliente.numInt,
                  "ciudad": this.ciudad,
                  "colonia": dataCliente.colonia,
                  "codPostal": this.codPostal,
                  "poblacion": this.poblacion
                },
                "ocupacion": "",
                "apellidoMat": dataCliente.materno,
                "apellidoPat": dataCliente.paterno,
                "tipoPersona": "",
                "fechaNacimiento": this.fechaNacCambio
              };

              if (dataCliente.genero === 'M') {
                delete this.clientejson['genero'];
                this.clientejson['genero'] = "MASCULINO";
              }
              if (dataCliente.genero === 'F') {
                delete this.clientejson['genero'];
                this.clientejson['genero'] = "FEMENINO";
              }
              delete this.responseEmision['cliente'];
              delete this.responseEmision['idSubRamo'];

              this.responseEmision['cliente'] = this.clientejson;
              this.cotizacionAli['respuesta'] = this.responseEmision;
              console.log(this.cotizacionAli);
              this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {

              });
            });

          });
        }
        if (infoCliente.idProductoCliente !== null) {
          this.productoClienteService.getProductoClienteById(infoCliente.idProductoCliente).subscribe(dataProducto => {
            this.productoCliente = dataProducto;
            console.log(this.productoCliente);
            this.responseProducto = this.productoCliente.datos;
            this.vehiculoJson = {
              "uso": "PARTICULAR",
              "clave": this.responseProducto.clave,
              "marca": this.responseProducto.marca,
              "codUso": "",
              "modelo": this.responseProducto.modelo,
              "noMotor": this.responseProducto.numeroMotor,
              "noSerie": this.responseProducto.numeroSerie,
              "codMarca": "",
              "noPlacas": this.responseProducto.numeroPlacas,
              "servicio": this.responseProducto.servicio,
              "subMarca": this.responseProducto.subMarca,
              "descripcion": this.responseProducto.descripcion,
              "codDescripcion": ""
            };
            delete this.responseEmision['vehiculo'];
            this.responseEmision['vehiculo'] = this.vehiculoJson;
            this.cotizacionAli['respuesta'] = this.responseEmision;

            this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {

            });
          });
        }

      });
    });

  }
}
