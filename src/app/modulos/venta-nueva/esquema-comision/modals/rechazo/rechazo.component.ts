import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import {ComisionesService} from '../../../../../@core/data/services/administracion-personal/comisiones.service';

@Component({
  selector: 'app-rechazo',
  templateUrl: './rechazo.component.html',
  styleUrls: ['./rechazo.component.scss']
})
export class RechazoComponent implements OnInit {

  motivos: FormControl = new FormControl('', Validators.required);
  constructor(public dialogRef: MatDialogRef<RechazoComponent>,
              private comisionesService: ComisionesService,
              private notificaciones: NotificacionesService,
              @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit(): void {
  }
  dismiss() {
    this.dialogRef.close();
  }
  rechazar(){
    this.comisionesService.rechazarEsquema(this.data, this.motivos.value).subscribe({
      next: () => this.notificaciones.carga(),
      complete: () => {
        this.notificaciones.exito('Esquema rechazado con éxito');
        this.dismiss();
      },
      error: () => this.notificaciones.error('error al rechazar, contacta a TI')
    });
  }

}
