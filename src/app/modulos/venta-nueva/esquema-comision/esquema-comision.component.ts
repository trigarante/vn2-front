import {Component, OnInit, ViewChild} from '@angular/core';
import {RechazoComponent} from './modals/rechazo/rechazo.component';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {MatSort} from '@angular/material/sort';
import {ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {ComisionesService} from '../../../@core/data/services/administracion-personal/comisiones.service';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-esquema-comision',
  templateUrl: './esquema-comision.component.html',
  styleUrls: ['./esquema-comision.component.scss']
})
export class EsquemaComisionComponent implements OnInit {
  permisos = JSON.parse(window.localStorage.getItem('User'));
  esquemas;
  datos: {departamentos: any[], arreglo: number[]} = { departamentos: [], arreglo: []};
  dataSource: any;
  niveles;
  baja: any[] = [];
  displayedColumns = ['departamentos', 'baja', 'zd'];
  displayedColumns2 = [];
  nuev = [];
  ya = false;
  idCatalogosTurnoModal: number[] = [4];
  tiposTabla = [];
  catalogosEsquema = [];
  btnCrear: boolean;
  tipoTabla = new FormControl('', Validators.required);
  catalogoCampana = new FormControl('', Validators.required);
  idEstadoEsquema: number;
  tituloPrincipal: string;
  // variable para saber si no hay esquemas en ese proceso y asi mostrar el texto de inicio
  vacio: boolean;
  meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  constructor(
    private comisionesService: ComisionesService,
    private dialog: MatDialog,
    private rutaActiva: ActivatedRoute,
    private notificaciones: NotificacionesService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit(): void {
    this.idEstadoEsquema = 3;
    this.tituloPrincipal = 'ESQUEMAS PENDIENTES DE AUTORIZAR';
    this.getEsquemas();
    //   this.rutaActiva.params.subscribe(params => {
    //   this.idEstadoEsquema = +params.idEstadoEsquema;
    //   switch (this.idEstadoEsquema) {
    //     case 0:
    //       this.tituloPrincipal = 'Estado Esquemas';
    //       this.getEsquemas();
    //       break;
    //     case 2:
    //       this.tituloPrincipal = 'Proceso Finanzas';
    //       this.getEsquemas();
    //       break;
    //     case 3:
    //       this.tituloPrincipal = 'Proceso Operaciones';
    //       this.getEsquemas();
    //       break;
    //     case 4:
    //       this.tituloPrincipal = 'Proceso Comercial';
    //       this.getEsquemas();
    //       break;
    //     case 5:
    //       this.tituloPrincipal = 'Proceso Ventas';
    //       this.getEsquemas();
    //       break;
    //     case 6:
    //       this.tituloPrincipal = 'Rechazadas';
    //       this.getEsquemas();
    //       break;
    //   }
    //   console.log('idEstadoEsquema', this.idEstadoEsquema);
    //   // this.getData(this.tipo);
    // });
    // ----metodos para traer los selects previos a abrir el modal----
    this.getTipoTabla();
    this.getCatalogos();
    // -----------------------------------------------------------
  }
  getTipoTabla(){
    this.comisionesService.getTiposTabla().subscribe( value => this.tiposTabla = value);
  }
  getCatalogos(){
    this.comisionesService.getCatalogoEsquema().subscribe( value => this.catalogosEsquema = value);
  }
  mostrarSelects(){
    this.tipoTabla.reset();
    this.catalogoCampana.reset();
    this.btnCrear = this.btnCrear ? false : true;
  }
  getEsquemas(){
    this.dataSource = [];
    this.esquemas = [];
    this.niveles = [];
    this.comisionesService.getDatosComision(this.idEstadoEsquema).subscribe({
      next: value => {
        this.datos = value;
        // -- DOC: linea para actualizar la variable que muestra el mensaje de inicio
        this.datos && this.datos.departamentos.length > 0 ? this.vacio = false : this.vacio = true;
        // -- fin DOC--
        let cont = 0;
        for (const dato of this.datos.departamentos) {
          this.esquemas[cont] = dato.datos.esquemas;
          this.niveles[cont] =  this.datos.arreglo[cont];
          this.dataSource[cont] = new MatTableDataSource(this.esquemas[cont]);
          this.displayedColumns2[cont] = [];
          for (let i = 0; i <= this.niveles[cont]; i++) {
            if (this.displayedColumns2[cont].length === this.niveles[cont]) {
              break;
            }else{
              this.displayedColumns2[cont].push('Nivel ' + (i + 1));
            }
            this.nuev[cont] = this.displayedColumns.concat(this.displayedColumns2[cont]);
            if (dato.idTipoTablaEsquema === 2) { this.nuev[cont].push('mes'); }
          }
          cont++;
        }
        this.ya = true;
        console.log('dt', this.datos);
      }
    });
  }
  async aprobarOrechazar(edo: number, dato: any) {
    // 1 es aprobar y 2 rechazar
    if ( edo === 1){
      const respuesta = await this.notificaciones.pregunta('¿Esta seguro de aprobar este esquema?', '', 'Si', 'No');
      if (respuesta.isConfirmed){
        console.log('si');
        const data = {id: dato.id, idEstadoEsquema: dato.idEstadoEsquema, idCatalogoEsquema: dato.idCatalogoEsquema};
        this.comisionesService.aprobarEsquema(data).subscribe({
          next: () => this.notificaciones.carga(),
          complete: () =>
          {
            this.notificaciones.exito('Esquema aprobado con éxito');
            this.getEsquemas();
          },
          error: () => this.notificaciones.error('error al aprobar, contacta a TI')
        });
      }
    } else if ( edo === 2 ) {
      this.dialog.open( RechazoComponent, {
        width: '500px',
        disableClose: true,
        data: dato.id
      }).afterClosed().subscribe(reload => {
        this.getEsquemas();
      });
    }
    // {id: dato.id, idEstadoEsquema: dato.idEstadoEsquema}
  }

  // editarModal(dato) {
  //   const modal: any = this.idCatalogosTurnoModal.includes(dato.idCatalogoEsquema) ?
  //     NuevoEsquemaTurnosComponent : NuevoEsquemaComponent;
  //   console.log(modal);
  //   this.dialog.open( modal, {
  //     width: '900px',
  //     disableClose: true,
  //     data: dato
  //   }).afterClosed().subscribe(reload => {
  //     this.getEsquemas();
  //   });
  // }

}
