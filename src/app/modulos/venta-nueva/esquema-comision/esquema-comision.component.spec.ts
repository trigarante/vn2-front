import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EsquemaComisionComponent } from './esquema-comision.component';

describe('EsquemaComisionComponent', () => {
  let component: EsquemaComisionComponent;
  let fixture: ComponentFixture<EsquemaComisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EsquemaComisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EsquemaComisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
