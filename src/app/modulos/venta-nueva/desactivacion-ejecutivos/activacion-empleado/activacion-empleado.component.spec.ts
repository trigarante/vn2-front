import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivacionEmpleadoComponent } from './activacion-empleado.component';

describe('ActivacionEmpleadoComponent', () => {
  let component: ActivacionEmpleadoComponent;
  let fixture: ComponentFixture<ActivacionEmpleadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivacionEmpleadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivacionEmpleadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
