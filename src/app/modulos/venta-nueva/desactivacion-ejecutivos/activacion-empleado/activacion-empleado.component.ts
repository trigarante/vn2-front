import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {EmpleadosService} from '../../../../@core/data/services/rrhh/empleados.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import Swal from 'sweetalert2';
import { MotivosDesactivacionService } from 'src/app/@core/data/services/activaciones-empleado/motivos-desactivacion.service';
import {
  SolicitudesCarruselService
} from '../../../../@core/data/services/activaciones-empleado/solicitudes-carrusel.service';

@Component({
  selector: 'app-activacion-empleado',
  templateUrl: './activacion-empleado.component.html',
  styleUrls: ['./activacion-empleado.component.scss']
})
export class ActivacionEmpleadoComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource = new MatTableDataSource();
  cols = ['id', 'nombre', 'fechaIngreso', 'sede', 'subarea', 'puesto', 'permiso', 'contador', 'acciones'];
  motivosDesactivacion;
  permisos = JSON.parse(window.localStorage.getItem('User'));

  constructor(private empleadosActivosService: EmpleadosService,
              private notificacionesService: NotificacionesService,
              private motivosDesactivacionService: MotivosDesactivacionService,
              private solicitudesCarrusel: SolicitudesCarruselService
              ) {
  }

  ngOnInit() {
    this.getEmpleadoActivo();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /** Con esta función se obtienen todos los empleados existentes en el servidor. */
  getEmpleadoActivo() {
    this.dataSource.data = [];
    this.empleadosActivosService.getEjecutivosActivos().subscribe({
      next: data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  async getMotivosDesactivacion(activacion: number) {
    const motivosDesactivacion: any = {};
    await this.motivosDesactivacionService.getActivosByIdTipo(activacion).toPromise().then(
      (data: any) => {
        this.motivosDesactivacion = data;
        data.forEach(motivo => motivosDesactivacion[motivo.id] = motivo.descripcion);
      },
    );
    return motivosDesactivacion;
  }


  async seleccionMotivo(activacion: number, infoEmpleado) {
    this.notificacionesService.carga('Descargando catálogos');
    const motivosDesactivacion = await this.getMotivosDesactivacion(activacion);
    Swal.fire({
      icon: 'question',
      title: (activacion === 1 ? 'ACTIVACIÓN' : 'DESACTIVACIÓN') + ' del empleado: ' + infoEmpleado.nombre,
      input: 'select',
      inputOptions: motivosDesactivacion,
      inputPlaceholder: 'Motivo de ' + (activacion === 1 ? 'ACTIVACIÓN' : 'DESACTIVACIÓN'),
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Continuar',
      reverseButtons: true,
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value) {
            resolve(null);
          } else {
            resolve('Debes seleccionar al menos un motivo para la desactivación/activacion');
          }
        });
      },
    }).then(async value => {
      if (value.value) {
        this.notificacionesService.carga('Ingresando la solicitud');
        this.solicitudesCarrusel.post({
          idMotivoDesactivacion: +value.value,
          idEmpleado: infoEmpleado.id,
          idEmpleadoSolicitante: +sessionStorage.getItem('Empleado'),
        }).subscribe({
          complete: () => this.notificacionesService.exito('Solicitud ingresada').then(() => {
            this.dataSource.data = [];
            this.getEmpleadoActivo();
          }),
        });
      }
    });
  }
}
