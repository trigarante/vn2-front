import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosCorreccionComponent } from './documentos-correccion.component';

describe('DocumentosCorreccionComponent', () => {
  let component: DocumentosCorreccionComponent;
  let fixture: ComponentFixture<DocumentosCorreccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentosCorreccionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosCorreccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
