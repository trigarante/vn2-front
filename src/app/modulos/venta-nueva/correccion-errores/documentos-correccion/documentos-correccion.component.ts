import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ErroresAnexosService} from '../../../../@core/data/services/venta-nueva/autorizacion/errores-anexos.service';
import {ErroresAutorizacionService} from '../../../../@core/data/services/venta-nueva/autorizacion/errores-autorizacion.service';
import {ErroresAutorizacion} from '../../../../@core/data/interfaces/venta-nueva/autorizacion/errores-autorizacion';
import {AutorizacionRegistroService} from '../../../../@core/data/services/venta-nueva/autorizacion/autorizacion-registro.service';
import {AutorizacionRegistro} from '../../../../@core/data/interfaces/venta-nueva/autorizacion/autorizacion-registro';
import {ErroresAnexosVService} from '../../../../@core/data/services/verificacion/errores-anexos-v.service';
import {ErroresVerificacionService} from '../../../../@core/data/services/verificacion/errores-verificacion.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {VerificacionRegistroService} from '../../../../@core/data/services/verificacion/verificacion-registro.service';
import {DriveService} from '../../../../@core/data/services/ti/drive.service';
import {PagosService} from '../../../../@core/data/services/venta-nueva/pagos.service';
import {DatosClienteCorreccionComponent} from '../componentes/datos-cliente-correccion/datos-cliente-correccion.component';
import {DatosRegistroCorreccionComponent} from '../componentes/datos-registro-correccion/datos-registro-correccion.component';
import {ProductoClienteCorreccionComponent} from '../componentes/producto-cliente-correccion/producto-cliente-correccion.component';
import {DatosPagoCorreccionComponent} from '../componentes/datos-pago-correccion/datos-pago-correccion.component';
import {Location} from '@angular/common';


@Component({
  selector: 'app-documentos-correccion',
  templateUrl: './documentos-correccion.component.html',
  styleUrls: ['./documentos-correccion.component.scss']
})
export class DocumentosCorreccionComponent implements OnInit {
  // Para activar los metodos de los hijos
  @ViewChild(DatosClienteCorreccionComponent) componenteCliente: DatosClienteCorreccionComponent;
  @ViewChild(DatosRegistroCorreccionComponent) componenteRegistro: DatosRegistroCorreccionComponent;
  @ViewChild(DatosPagoCorreccionComponent) componentePago: DatosPagoCorreccionComponent;
  @ViewChild(ProductoClienteCorreccionComponent) componenteProducto: ProductoClienteCorreccionComponent;

  tituloComponente: string;
  leyendaSuperior: string;
  tipoError: string = this._route.snapshot.paramMap.get('tipoError');
  etapaCorreccion: string = this._route.snapshot.paramMap.get('etapaCorreccion');
  comentariosDocumentos: any;
  idTipoDocumento: number = +this._route.snapshot.paramMap.get('idTipoDocumento');
  idErrorAnexo: number;
  variablesPorMostrar: string[] = [];
  variablesComentarios: string[] = [];
  camposPorCorregir;
  idverificacionAutorizacionRegistro: number = +this._route.snapshot.paramMap.get('idverificacionAutorizacionRegistro');
  formularioCorrecto: Boolean;
  tipoArchivo: string;
  datosPorCorregirA: ErroresAutorizacion;
  autorizacionRegistro: AutorizacionRegistro;
  valorRouta: string;
  permisos = JSON.parse(window.localStorage.getItem('User'));



  constructor(private _route: ActivatedRoute,
              private erroresAnexosAService: ErroresAnexosService,
              private erroresAutorizacionService: ErroresAutorizacionService,
              private autorizacionRegistroService: AutorizacionRegistroService,
              private erroresAnexosVService: ErroresAnexosVService,
              private erroresVerificacionService: ErroresVerificacionService,
              private notificaciones: NotificacionesService,
              private verificacionRegistroService: VerificacionRegistroService,
              private router: Router,
              private driveService: DriveService,
              private location: Location,
              private pagoService: PagosService) {
  }

  ngOnInit(): void {
    this.datosIniciales(this.tipoError === 'documento' ? 'Validar documentos' : 'Validar datos',
      this.tipoError === 'documento' ? 'Comentarios para los siguientes documentos:' : 'Comentarios acerca de los datos introducidos:',
      this.tipoError === 'documento' ? 'documentos' : 'datos');
    this.tipoError === 'documento' ? this.getErroresDocumentosA() : this.getErroresEnDatosA();
    this.getAutorizacionRegistro();
  }
  datosIniciales(titulo: string, leyenda: string, valor: string){
    this.valorRouta = valor;
    this.tituloComponente = titulo;
    this.leyendaSuperior = leyenda;
  }
  getErroresDocumentosA() {
    const op = this.etapaCorreccion === 'autorizacion' ? this.erroresAnexosAService.getByIdAutorizacionRegitro(
      this.idverificacionAutorizacionRegistro, this.idTipoDocumento) : this.erroresAnexosVService.getByIdVerificacionRegistro(
        this.idverificacionAutorizacionRegistro, this.idTipoDocumento);
    // @ts-ignore
    op.subscribe(result => {
      this.comentariosDocumentos = result.correcciones;
      this.idErrorAnexo = result.id;
    });
  }

  getErroresEnDatosA() {
    const op = this.etapaCorreccion === 'autorizacion' ? this.erroresAutorizacionService.getCamposConErrores(
      this.idverificacionAutorizacionRegistro, this.idTipoDocumento) : this.erroresVerificacionService.getCamposConErrores(
        this.idverificacionAutorizacionRegistro, this.idTipoDocumento);
    // @ts-ignore
    op.subscribe({
      next: data => {
        this.datosPorCorregirA = data;
        this.camposPorCorregir = this.datosPorCorregirA.correcciones;
        for (const campo of this.camposPorCorregir) {
          this.variablesPorMostrar.push(campo.campo);
          this.variablesComentarios.push(campo.comentario ? campo.comentario : 'Sin comentarios');
        }
      },
    });
  }

  getAutorizacionRegistro() {
    const op = this.etapaCorreccion === 'autorizacion' ? this.autorizacionRegistroService.getVnById(
      this.idverificacionAutorizacionRegistro) : this.verificacionRegistroService.getById(this.idverificacionAutorizacionRegistro);
    op.subscribe({
      next: data => {
        this.autorizacionRegistro = data;
      },
      complete: () => {
        switch (this.idTipoDocumento) {
          case 1: {
            this.tipoArchivo = 'cliente';
            break;
          }
          case 2: {
            this.tipoArchivo = 'poliza';
            break;
          }
          case 3: {
            this.tipoArchivo = 'poliza';
            break;
          }
          case 4: {
            this.tipoArchivo = 'pago';
            break;
          }
        }
      },
    });
  }

  isAutorizacion(value: any) {
    this.etapaCorreccion === 'autorizacion' ? this.putAutorizacion(value.titulo,  value.tipoDocumento) :
      this.putVerificacion(value.titulo,  value.tipoDocumento);
  }
  putAutorizacion(titulo: string, tipoDocumento: string) {
    this.erroresAutorizacionService.correcccionDatos(this.idverificacionAutorizacionRegistro, this.idTipoDocumento,
      this.datosPorCorregirA.id).subscribe({
      complete: () => {
        this.notificaciones.exito(`${titulo} corregido ¡La información del ${tipoDocumento} se ha actualizado exitosamente! ` )
          .then(() => {
            this.router.navigate([`modulos/venta-nueva/correccion-errores/${this.valorRouta }/${this.etapaCorreccion}`]);
          } );
      }});
  }

  putVerificacion(titulo: string, tipoDocumento: string) {
    this.verificacionRegistroService.documentoVerificado(this.idverificacionAutorizacionRegistro, this.idTipoDocumento, 3).subscribe({
      complete: () => {
        this.erroresVerificacionService.putEstadoCorreccion(this.datosPorCorregirA.id).subscribe({
          complete: () => {
            this.notificaciones.exito(`${titulo} corregido ¡La información del ${tipoDocumento} se ha actualizado exitosamente! ` )
            .then(() => {
              this.router.navigate([`modulos/venta-nueva/correccion-errores/${this.valorRouta }/${this.etapaCorreccion}`]);
              // this._location.back()
            } );
          }});
      },
    });
  }
  estaCorrecto(value: any) {
    this.formularioCorrecto = value.isCorrecto;
  }
  almacenarArchivo($event) {
    const filesToUpload = (event.target as HTMLInputElement).files;
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    const extensionValida: Boolean = extensionesDeArchivoAceptadas.includes(filesToUpload.item(0).type);
    const tamanioArchivo = filesToUpload.item(0).size * .000001;
    if (filesToUpload.length === 1) {
      // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
      if (extensionValida && tamanioArchivo < 7) {
        this.subirADrive(filesToUpload);
      } else {
        this.notificaciones.error(!extensionValida ? 'Solo puedes subir archivos pdf' :
            'Los archivos que subas deben pesar menos de 7 MB',
          !extensionValida ? 'Extensión no soportada' : 'Archivo demasiado grande');
      }
    }
  }

  subirADrive(fileToUpload: FileList) {
    this.notificaciones.carga('Se están subiendo los archivos');
    this.driveService.correccionDocumentos(fileToUpload, this.autorizacionRegistro.idRegistro, this.idTipoDocumento,
    this.idverificacionAutorizacionRegistro, this.idErrorAnexo, this.etapaCorreccion === 'autorizacion' ? 0 : 1).subscribe({
    next: () => this.notificaciones.exito('¡Tus archivos han sido subidos exitosamente!').then(() => {
        this.router.navigate([`modulos/venta-nueva/correccion-errores/${this.valorRouta }/${this.etapaCorreccion}`]);
      }),
    error: error => this.notificaciones.error(error.status !== 417 || error.status !== 400 ? 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información'
        : error.error)});
  }

  activarMetodoCliente() {
    this.componenteCliente.putCliente();
  }

  activarMetodoRegistro() {
    this.componenteRegistro.putRegistro();
  }

  activarMetodoPago() {
    this.componentePago.putPago();
  }

  activarMetodoProducto() {
    this.componenteProducto.putProducto();
  }
  cerrar(){
    this.location.back();
  }

}
