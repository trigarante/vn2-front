import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorreccionErroresDocumentosComponent } from './correccion-errores-documentos.component';

describe('CorreccionErroresDocumentosComponent', () => {
  let component: CorreccionErroresDocumentosComponent;
  let fixture: ComponentFixture<CorreccionErroresDocumentosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorreccionErroresDocumentosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorreccionErroresDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
