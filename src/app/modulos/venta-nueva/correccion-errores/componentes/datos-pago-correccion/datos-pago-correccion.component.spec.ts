import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosPagoCorreccionComponent } from './datos-pago-correccion.component';

describe('DatosPagoCorreccionComponent', () => {
  let component: DatosPagoCorreccionComponent;
  let fixture: ComponentFixture<DatosPagoCorreccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatosPagoCorreccionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosPagoCorreccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
