import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosRegistroCorreccionComponent } from './datos-registro-correccion.component';

describe('DatosRegistroCorreccionComponent', () => {
  let component: DatosRegistroCorreccionComponent;
  let fixture: ComponentFixture<DatosRegistroCorreccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatosRegistroCorreccionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosRegistroCorreccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
