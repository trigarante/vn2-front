import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewerCorreccionComponent } from './viewer-correccion.component';

describe('ViewerCorreccionComponent', () => {
  let component: ViewerCorreccionComponent;
  let fixture: ComponentFixture<ViewerCorreccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewerCorreccionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewerCorreccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
