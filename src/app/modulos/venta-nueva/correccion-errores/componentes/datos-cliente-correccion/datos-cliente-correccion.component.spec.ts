import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosClienteCorreccionComponent } from './datos-cliente-correccion.component';

describe('DatosClienteCorreccionComponent', () => {
  let component: DatosClienteCorreccionComponent;
  let fixture: ComponentFixture<DatosClienteCorreccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatosClienteCorreccionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosClienteCorreccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
