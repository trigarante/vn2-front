import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Paises} from '../../../../../@core/data/interfaces/catalogos/paises';
import {SepomexService} from '../../../../../@core/data/services/catalogos/sepomex.service';
import {ClienteVnService} from '../../../../../@core/data/services/venta-nueva/cliente-vn.service';
import {PaisesService} from '../../../../../@core/data/services/catalogos/paises.service';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import moment from 'moment';

@Component({
  selector: 'app-datos-cliente-correccion',
  templateUrl: './datos-cliente-correccion.component.html',
  styleUrls: ['./datos-cliente-correccion.component.scss']
})
export class DatosClienteCorreccionComponent implements OnInit {
  @Input() variablesPorMostrar: string[];
  @Input() variablesComentarios: string[];
  @Input() idCliente: number;
  @Output() isAutorizacion = new EventEmitter<any>();

  datosCliente: FormGroup = new FormBuilder().group({
    curp: new FormControl('', Validators.compose([Validators.required,
      Validators.pattern('^[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][H,M][A-Z][A-Z][A-Z][A-Z][A-Z][A-Z-0-9][A-Z-0-9]'),
      Validators.maxLength(18), Validators.minLength(18)])),
    rfc: new FormControl('', Validators.compose([Validators.required,
      Validators.pattern('[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
      Validators.maxLength(13), Validators.minLength(10)])),
    nombre: new FormControl('', Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(60)])),
    paterno: new FormControl('', Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(50)])),
    materno: new FormControl('', Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(50)])),
    correo: new FormControl('', Validators.compose([Validators.required, Validators.email])),
    cp: new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(5)])),
    idColonia: new FormControl('', Validators.compose([Validators.required])),
    calle: new FormControl('', Validators.compose([Validators.required])),
    numInt: new FormControl(''),
    numExt: new FormControl('', Validators.compose([Validators.required])),
    genero: new FormControl('', Validators.compose([Validators.required])),
    telefonoFijo: new FormControl('', Validators.compose([Validators.minLength(10),
      Validators.maxLength(10)])),
    telefonoMovil: new FormControl('', Validators.compose([Validators.required, Validators.minLength(10),
      Validators.maxLength(10)])),
    fechaNacimiento: new FormControl('', Validators.compose([Validators.required])),
    razonSocial: new FormControl(1),
    idPais: new FormControl(null, Validators.required),
    archivo: new FormControl(),
    archivoSubido: new FormControl(),
  });
  genders = [{label: 'MASCULINO', value: 'M'}, {label: 'FEMENINO', value: 'F'}];
  colonias: any[];
  idColonia;
  paisOrigen: Paises[];
  constructor( private sepomexService: SepomexService,
               private clienteService: ClienteVnService,
               private paisesService: PaisesService,
               private  notificaciones: NotificacionesService) { }

  ngOnInit(): void {
    this.getInformacionCliente();
  }

  actualizarFormulario() {
    if (this.datosCliente.controls.razonSocial.value === 2) {
      this.datosCliente.controls.curp.clearValidators();
      this.datosCliente.controls.paterno.clearValidators();
      this.datosCliente.controls.materno.clearValidators();
      this.datosCliente.controls.fechaNacimiento.clearValidators();
      this.datosCliente.controls.genero.clearValidators();
      this.datosCliente.controls.rfc.setValidators([
        Validators.pattern('[A-Z]{3}[0-9]{6}([A-Z-0-9]{3})?'),
        Validators.maxLength(12), Validators.minLength(9)]);
    } else {
      this.datosCliente.controls.curp.setValidators(Validators.compose([Validators.required,
        Validators.pattern('^[A-Z]{4}[0-9]{6}[H,M][A-Z]{5}[A-Z-0-9]{2}'),
        Validators.maxLength(18), Validators.minLength(18)]));
      this.datosCliente.controls.rfc.setValidators([
        Validators.required,
        Validators.pattern('[A-Z]{4}[0-9]{6}([A-Z-0-9]{3})?'),
        Validators.maxLength(13), Validators.minLength(10)]);
      this.datosCliente.controls.paterno.setValidators(Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(50)]));
      this.datosCliente.controls.materno.setValidators(Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(50)]));
      this.datosCliente.controls.fechaNacimiento.setValidators(Validators.required);
      this.datosCliente.controls.genero.setValidators(Validators.required);
    }
    this.datosCliente.controls.paterno.updateValueAndValidity();
    this.datosCliente.controls.materno.updateValueAndValidity();
    this.datosCliente.controls.fechaNacimiento.updateValueAndValidity();
    this.datosCliente.controls.genero.updateValueAndValidity();
    this.datosCliente.controls.rfc.updateValueAndValidity();
    this.datosCliente.controls.curp.updateValueAndValidity();
  }

  /** Función que obtiene las colonias y las almacena en el arreglo "colonias" */
  getColonias(cp) {
    if (cp.length < 4) {
      this.colonias = [];
    }
    this.sepomexService.getColoniaByCp(cp).subscribe(data => {
      this.colonias = data;
    });
  }

  getInformacionCliente() {
    let aux;
    this.clienteService.getClienteById(this.idCliente).subscribe({
      next: value => {
        this.getColonias(value.cp);
        this.getPaises();
        aux = value.idColonia;
        this.datosCliente.patchValue(value);
        // this.datosCliente.controls.fechaNacimiento.setValue(value.razonSocial === 1 ? value.fechaNacimiento + 'T06:00:00'
        //   : value.fechaNacimiento);
      },
      complete: () => {
        this.idColonia = aux;
        this.actualizarFormulario();
      },
    });
  }

  /** Función para obtener el país de origen del precandidato y lo almacena en el arreglo "pais". */
  getPaises() {
    this.paisesService.get().subscribe(result => {
      this.paisOrigen = result;
    });
  }

  putCliente() {
    this.notificaciones.carga('Subiendo la corrección ingresada');
    this.datosCliente.controls.fechaNacimiento.setValue(moment(this.datosCliente.controls.fechaNacimiento.value));
    const fecha = new Date(this.datosCliente.controls.fechaNacimiento.value);
    fecha.setTime(fecha.getTime() + fecha.getTimezoneOffset() * 60 * 1000);
    this.datosCliente.controls.fechaNacimiento.setValue(fecha);
    this.clienteService.put(this.idCliente, this.datosCliente.value).subscribe({
      complete: () => {
        this.isAutorizacion.emit({titulo: 'Cliente', tipoDocumento: 'cliente'});
      },
    });
  }

}
