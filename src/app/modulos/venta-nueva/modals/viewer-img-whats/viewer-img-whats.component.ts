import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-viewer-img-whats',
  templateUrl: './viewer-img-whats.component.html',
  styles: [
  ]
})
export class ViewerImgWhatsComponent {
  imagen: any;
  constructor(@Inject(MAT_DIALOG_DATA) public data) {
    this.imagen = this.data;
  }

}
