import {Component, OnInit} from '@angular/core';
import RfcFacil from 'rfc-facil';
import {FormControl, Validators} from '@angular/forms';
import generaCurp from '../../../../../assets/js/curp.js';
import { SlideInOutAnimation } from './animaciones';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [SlideInOutAnimation]
})
/**
 * Se declaran las varibles vacias
 * Se declara en un objeto los elementos de los valores
 * de "genero" y "estados"
 */
export class HomeComponent implements OnInit {
  nombre = '';
  appaterno = '';
  apmaterno = '';
  dianac = '';
  mesnac = '';
  anionac = '';
  rfc = '';
  genero = '';
  estado = '';
  curp = '';
  generos: object[] = [{value: 'H', text: 'HOMBRE'}, {value: 'M', text: 'MUJER'}];
  estados: object[] = [
    {value: 'AS', text: 'AGUASCALIENTES'},
    {value: 'BC', text: 'BAJA CALIFORNIA'},
    {value: 'BS', text: 'BAJA CALIFORNIA SUR'},
    {value: 'CC', text: 'CAMPECHE'},
    {value: 'CL', text: 'COAHUILA DE ZARAGOZA'},
    {value: 'CM', text: 'COLIMA'},
    {value: 'CS', text: 'CHIAPAS'},
    {value: 'CH', text: 'CHIHUAHUA'},
    {value: 'DF', text: 'DISTRITO FEDERAL'},
    {value: 'DG', text: 'DURANGO'},
    {value: 'GT', text: 'GUANAJUATO'},
    {value: 'GR', text: 'GUERRERO'},
    {value: 'HG', text: 'HIDALGO'},
    {value: 'JC', text: 'JALISCO'},
    {value: 'MC', text: 'MEXICO'},
    {value: 'MN', text: 'MICHOACAN DE OCAMPO'},
    {value: 'MS', text: 'MORELOS'},
    {value: 'NT', text: 'NAYARIT'},
    {value: 'NL', text: 'NUEVO LEON'},
    {value: 'OC', text: 'OAXACA'},
    {value: 'PL', text: 'PUEBLA'},
    {value: 'QT', text: 'QUERETARO DE ARTEAGA'},
    {value: 'QR', text: 'QUINTANA ROO'},
    {value: 'SP', text: 'SAN LUIS POTOSI'},
    {value: 'SL', text: 'SINALOA'},
    {value: 'SR', text: 'SONORA'},
    {value: 'TC', text: 'TABASCO'},
    {value: 'TS', text: 'TAMAULIPAS'},
    {value: 'TL', text: 'TLAXCALA'},
    {value: 'VZ', text: 'VERACRUZ'},
    {value: 'YN', text: 'YUCATAN'},
    {value: 'ZS', text: 'ZACATECAS'},
    {value: 'NE', text: 'NACIDO EN EL EXTRANJERO'}];
  nombreValidator: FormControl = new FormControl('', [Validators.required]);
  apPaternoValidator: FormControl = new FormControl('', [Validators.required]);
  apMaternoValidator: FormControl = new FormControl('', [Validators.required]);
  diaNacValidator: FormControl = new FormControl('', [Validators.required]);
  mesNacValidator: FormControl = new FormControl('', [Validators.required]);
  anioNacValidator: FormControl = new FormControl('', [Validators.required]);
  generoValidator: FormControl = new FormControl('', [Validators.required]);
  estadoValidator: FormControl = new FormControl('', [Validators.required]);

  constructor() { }
  ngOnInit() {
  }
  /**
   * Funcion generarDatos
   * Aqui obtiene el RFC declarando los variables
   * de acuerdp a la libreria que se incluyo de JS
   */
  generarDatos() {
    const rfc: string = RfcFacil.forNaturalPerson({
      name: this.nombre,
      firstLastName: this.appaterno,
      secondLastName: this.apmaterno,
      day: Number(this.dianac),
      month: Number(this.mesnac),
      year: Number(this.anionac)
    });
    /**
     * Funcion genera curp
     * Aqui se obtienen los valores
     * para generar la curp
     */
    const curp = generaCurp({
      nombre: this.nombre,
      apellido_paterno: this.appaterno,
      apellido_materno: this.apmaterno,
      sexo: this.genero,
      estado: this.estado,
      fecha_nacimiento: [this.dianac, this.mesnac, this.anionac],
    });
    this.curp = curp;
    this.rfc = rfc;
  }
  /**
   * Funcion limpia
   * Esta funcion sirve para limpiar todos los campos
   * del formulario
   */
  limpia() {
    this.nombre = '';
    this.appaterno = '';
    this.apmaterno = '';
    this.dianac = '';
    this.mesnac = '';
    this.anionac = '';
    this.genero = '';
    this.estado = '';
    this.curp = '';
    this.rfc = '';
  }
}


