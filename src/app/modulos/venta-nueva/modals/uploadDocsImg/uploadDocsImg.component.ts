import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DriveService} from '../../../../@core/data/services/ti/drive.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import {MatAccordion} from '@angular/material/expansion';
import {PDFDocument} from 'pdf-lib';
import {ProductoClienteService} from '../../../../@core/data/services/venta-nueva/producto-cliente.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { saveAs } from 'file-saver';
import {File} from '@angular/compiler-cli/src/ngtsc/file_system/testing/src/mock_file_system';
@Component({
  selector: 'app-subir-archivo-cliente',
  templateUrl: './uploadDocsImg.component.html',
  styleUrls: ['./uploadDocsImg.component.scss'],
})
export class UploadDocsImgComponent implements OnInit {
  permisos = JSON.parse(window.localStorage.getItem('User'));
  fileClient: boolean;
  fileAuto: boolean;
  fileDom: boolean;
  pdfClient: boolean;
  pdfAuto: boolean;
  pdfDom: boolean;
  doc = new jsPDF('p', 'px', 'a4');
  options = {
    background: 'white',
    scale: 3
  };
  docDown: any;
  pdfFileClient!: FileList;
  pdfFileProd: FileList;
  pdfFileDom: FileList;
  uploadPDF = false;
  uploadImage = false;
  arrayPDFs = [];
  docsAcceptedExtensions = ['application/pdf'];
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private matDialogRef: MatDialogRef<UploadDocsImgComponent>,
              private driveService: DriveService,
              private notificacionesService: NotificacionesService
    ) {}

  ngOnInit() {
  }

  dismiss(reload) {
     this.matDialogRef.close(reload);
  }
  async getBase64(file) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    return new Promise<any>((resolve, reject) => {
      reader.onload = () => {
        resolve(reader.result);
        return(reader.result);
      };
      reader.onerror = (error) => {
        return(reader.result);
      };
    });
  }
  async mergePDF(show: boolean, savePDF: boolean) {
    const mergedPdf = await PDFDocument.create();
    let pdfsToConvert;
    if (this.uploadImage && !this.uploadPDF) {
      if (savePDF) {
        const pageCount = this.docDown.internal.getNumberOfPages();
        this.docDown.deletePage(pageCount);
      }
      pdfsToConvert = [this.docDown.output('blob')];
    } else {
      if (this.uploadPDF && !this.uploadImage) {
        pdfsToConvert = this.arrayPDFs;
      } else {
        if (savePDF) {
          const pageCount = this.docDown.internal.getNumberOfPages();
          this.docDown.deletePage(pageCount);
        }
        pdfsToConvert = [...this.arrayPDFs, this.docDown.output('blob')];
      }
    }
    for (const pdftoConvert of pdfsToConvert) {
      const pdfBase64 = await this.getBase64(pdftoConvert);
      const pdfUploaded = await PDFDocument.load(pdfBase64);
      const copiedPages = await mergedPdf.copyPages(pdfUploaded, pdfUploaded.getPageIndices());
      copiedPages.forEach((page) => mergedPdf.addPage(page));
    }
    const mergedFiles = await mergedPdf.save();
    if (show) {
      const urlCan = URL.createObjectURL(new Blob([mergedFiles], {type: 'application/pdf'}));
      document.querySelector('#archivoPDF').setAttribute('src', urlCan);
    }
    // file que se mandara en el post
    if (savePDF) {
      const fileToUpload = new File([mergedFiles], 'Cliente');
      this.saveAllPDFs(fileToUpload);
    }
  }
  savePDFClient() {
    if (this.docsAcceptedExtensions.includes((event.target as HTMLInputElement).files[0].type)) {
      this.uploadPDF = true;
      this.pdfClient = true;
      this.pdfFileClient = (event.target as HTMLInputElement).files;
      this.arrayPDFs.push(this.pdfFileClient[0]);
    } else {
      this.notificacionesService.error('Extensión de archivo invalida');
    }
  }
  savePDFProd() {
    if (this.docsAcceptedExtensions.includes((event.target as HTMLInputElement).files[0].type)) {
      this.uploadPDF = true;
      this.pdfAuto = true;
      this.pdfFileProd = (event.target as HTMLInputElement).files;
      this.arrayPDFs.push(this.pdfFileProd[0]);
    } else {
      this.notificacionesService.error('Extensión de archivo invalida');
    }
  }
  savePDFDom() {
    if (this.docsAcceptedExtensions.includes((event.target as HTMLInputElement).files[0].type)) {
      this.uploadPDF = true;
      this.pdfDom = true;
      this.pdfFileDom = (event.target as HTMLInputElement).files;
      this.arrayPDFs.push(this.pdfFileDom[0]);
    } else {
      this.notificacionesService.error('Extensión de archivo invalida');
    }
  }
  uploadDocs(documento: any) {
    let DATA;
    switch (documento) {
      case 1:
        // documento de cliente
        DATA = document.getElementById('htmlData');
        break;
      case 2:
        // documento del producto
        DATA = document.getElementById('htmlDataProd');
        break;
      case 3:
        // documento de domicilio
        DATA = document.getElementById('htmlDataDom');
        break;
    }
    html2canvas(DATA, this.options).then((canvas) => {
      const img = canvas.toDataURL('image/PNG', 1);
      const bufferX = 30;
      const bufferY = 2;
      this.doc.addImage(img, 'PNG', bufferX, bufferY, 400, 400, undefined, 'FAST');
      this.doc.addPage();
      return this.doc;
    }).then((docResult) => {
      this.docDown = docResult;
    });
  }

  saveAllPDFs(file: any) {
    this.closeDialog(file);
  }
  closeDialog(data: any) {
    this.matDialogRef.close(data);
  }

  tabSelected(event: any) {
    if (event.index === 1 && (this.uploadImage || this.uploadPDF)) {
      this.mergePDF(true, false);
    }
  }
  imgClient() {
    this.uploadImage = true;
    this.fileClient = true;
    this.uploadDocs(1);
  }
  imgProd() {
    this.uploadImage = true;
    this.fileAuto = true;
    this.uploadDocs(2);
  }
  imgDomcilio() {
    this.uploadImage = true;
    this.fileDom = true;
    this.uploadDocs(3);
  }
}
