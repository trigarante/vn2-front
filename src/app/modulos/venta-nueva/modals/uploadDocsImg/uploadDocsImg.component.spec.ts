import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadDocsImgComponent } from './uploadDocsImg.component';

describe('SubirArchivoClienteComponent', () => {
  let component: UploadDocsImgComponent;
  let fixture: ComponentFixture<UploadDocsImgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadDocsImgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadDocsImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
