import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProductoSolicitudvnService} from '../../../../@core/data/services/venta-nueva/producto-solicitudvn.service';
import {MatAccordion} from '@angular/material/expansion';
import {CotizacionesAliService} from '../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';

@Component({
  selector: 'app-datos-solicitud',
  templateUrl: './datos-solicitud.component.html',
  styleUrls: ['./datos-solicitud.component.scss']
})
export class DatosSolicitudComponent implements OnInit {
  prospecto;
  datosEmision;
  solicitud: any;
  respuesta: any;
  mostrar: boolean;
  @ViewChild(MatAccordion) accordion: MatAccordion;
  constructor(public dialogRef: MatDialogRef<DatosSolicitudComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private cotizacionesAliService: CotizacionesAliService,
              private productoSolicitudService: ProductoSolicitudvnService) { }


  /** Esta función cierra el modal. */
  dismiss() {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.getDatosEmision();
    this.getProductoSolicitud();
  }

  getProductoSolicitud() {
    this.productoSolicitudService.getProductoSolicitud(this.data.idProducto).subscribe(data => {
      if (data !== null) {
        this.solicitud = data.datos;
      }
      this.prospecto = data;
    });
  }

  getDatosEmision() {
    this.cotizacionesAliService.getCotizacionAliById(this.data.idCotizacionAli).subscribe({
      next: result => {
        this.datosEmision  = result[0];
        this.respuesta  = result[0].respuesta;
        console.log(this.respuesta);
        if (Object.keys(this.respuesta).length !== 0){
          this.mostrar = true;
        }
      }, complete: () => {
      },
    });
  }
}
