import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PickerAseguradoraComponent } from './picker-aseguradora.component';

describe('PickerAseguradoraComponent', () => {
  let component: PickerAseguradoraComponent;
  let fixture: ComponentFixture<PickerAseguradoraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PickerAseguradoraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PickerAseguradoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
