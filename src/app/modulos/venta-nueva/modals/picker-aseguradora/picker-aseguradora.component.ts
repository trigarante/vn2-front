import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {SociosService} from '../../../../@core/data/services/comerciales/socios.service';
import {ActivatedRoute, Router, NavigationExtras} from '@angular/router';
import {SolicitudesvnService} from '../../../../@core/data/services/venta-nueva/solicitudes.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import Swal from "sweetalert2";
import {LogOcrService} from "../../../../@core/data/services/venta-nueva/log-ocr.service";
import {LogOcr} from "../../../../@core/data/interfaces/venta-nueva/log-ocr";
import {SociosComercialOcr} from "../../../../@core/data/interfaces/comerciales/socios-comercial-ocr";

@Component({
  selector: 'app-picker-aseguradora',
  templateUrl: './picker-aseguradora.component.html',
  styleUrls: ['./picker-aseguradora.component.scss']
})
export class PickerAseguradoraComponent implements OnInit {
  aseguradoraForm = new FormControl('', Validators.required);
  documentosForm: FormGroup = new FormGroup({
    cliente: new FormControl('', Validators.required),
    domicilio: new FormControl('', Validators.required),
    vehiculo: new FormControl('', Validators.required),
  });
  aseguradoraServ: any[] = [];
  filesToUpload: FileList;
  filesToUploadCliente: FileList;
  filesToUploadDomicilio: FileList;
  filesToUploadVehiculo: FileList;
  clientesDoc = ['INE', 'PASAPORTE', 'CEDULA', 'CARTILLA', 'LICENCIA'];
  domicilioDoc = ['INE', 'ESTADO DE CUENTA', 'GAS', 'TELEFONO', 'AGUA', 'PREDIAL', 'CFE'];
  vehiculoDoc = ['CIRCULACION', 'FACTURA'];
  idSolicitud;
  archivoValidoCliente = false;
  archivoValidoDomicilio = false;
  archivoValidoPoliza = false;
  archivoValidoVehiculo = false;
  aseg: SociosComercialOcr[];
  logOcr: LogOcr;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private notificaciones: NotificacionesService,
    private socioService: SociosService,
    private solicitudesVn: SolicitudesvnService,
    private logOcrService: LogOcrService,
    @Inject(MAT_DIALOG_DATA) public data,
    public ref: MatDialogRef<PickerAseguradoraComponent>
  ) { }

  ngOnInit(): void {
    this.idSolicitud = this.data;
    console.log(this.idSolicitud);
    this.getAseguradora();
    console.log(this.aseguradoraServ);
  }

  getAseguradora() {
    this.socioService.getAseguradoras().subscribe( data => {
      this.aseguradoraServ = this.aseg = data;
      console.log(this.aseguradoraServ);
    });
  }

  sendOCR() {
    Swal.fire({
      title: 'Espera a que el OCR haga magia',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    const jsonSend = {
      aseguradora: this.aseguradoraForm.value.nombreComercial,
      policy: this.filesToUpload,
      indentify: this.documentosForm.controls.cliente.value,
      indentity: this.filesToUploadCliente,
      addressType: this.documentosForm.controls.domicilio.value,
      addressFile: this.filesToUploadDomicilio,
      carIdentity: this.documentosForm.controls.vehiculo.value,
      car: this.filesToUploadVehiculo
    };
    this.logOcr = {
      idSolicitud: this.idSolicitud,
      idSocio: this.aseguradoraForm.value.id,
      idEmpleado: +window.sessionStorage.getItem('Empleado'),
      datos: {}
    };
    this.solicitudesVn.ocrService(jsonSend).subscribe({
      next: pol => {
        this.solicitudesVn.ocrServiceCliente(jsonSend).subscribe({
          next: client => {
            this.solicitudesVn.ocrServiceDomicilio(jsonSend).subscribe({
              next: doc => {
                this.solicitudesVn.ocrServiceRepuve(jsonSend).subscribe({
                  next: repuve => {
                    this.logOcr.datos = {
                        dataPoliza: pol,
                        dataCliente: client,
                        dataDomicilio: doc,
                        dataRepuve: repuve
                    };
                    this.logOcrService.post(this.logOcr).subscribe({
                      next: value => {
                        console.log(value);
                        this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                        this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                      },
                      error: err => {
                        this.notificaciones.error('Error al guardar el log');
                        this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                      }
                    });
                  },
                  error: err => {
                    this.solicitudesVn.ocrServiceVehiculo(jsonSend).subscribe({
                      next: vehiculo => {
                        this.logOcr.datos = {
                            dataPoliza: pol,
                            dataCliente: client,
                            dataDomicilio: doc,
                            dataRepuve: vehiculo
                          };
                        this.logOcrService.post(this.logOcr).subscribe({
                          next: value => {
                            console.log(value);
                            this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          },
                          error: err2 => {
                            this.notificaciones.error('Error al guardar el log');
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          }
                        });
                      },
                      error: err1 => {
                        this.logOcr.datos = {
                            dataPoliza: pol,
                            dataCliente: client,
                            dataDomicilio: doc,
                            dataRepuve: 0
                          };
                        this.logOcrService.post(this.logOcr).subscribe({
                          next: value => {
                            console.log(value);
                            this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          },
                          error: err2 => {
                            this.notificaciones.error('Error al guardar el log');
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          }
                        });
                      }
                    });
                  }
                });
              },
              error: errr => {
                console.log(errr);
                this.solicitudesVn.ocrServiceRepuve(jsonSend).subscribe({
                  next: repuve => {
                    this.logOcr.datos = {
                        dataPoliza: pol,
                        dataCliente: client,
                        dataDomicilio: 0,
                        dataRepuve: repuve
                      };
                    this.logOcrService.post(this.logOcr).subscribe({
                      next: value => {
                        console.log(value);
                        this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                        this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                      },
                      error: err => {
                        this.notificaciones.error('Error al guardar el log');
                        this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                      }
                    });
                  },
                  error: err => {
                    console.log(err);
                    this.solicitudesVn.ocrServiceVehiculo(jsonSend).subscribe({
                      next: vehiculo => {
                        this.logOcr.datos = {
                            dataPoliza: pol,
                            dataCliente: client,
                            dataDomicilio: 0,
                            dataRepuve: vehiculo
                          };
                        this.logOcrService.post(this.logOcr).subscribe({
                          next: value => {
                            console.log(value);
                            this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          },
                          error: err3 => {
                            this.notificaciones.error('Error al guardar el log');
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          }
                        });
                      },
                      error: err1 => {
                        console.log(err1);
                        this.logOcr.datos = {
                            dataPoliza: pol,
                            dataCliente: client,
                            dataDomicilio: 0,
                            dataRepuve: 0
                          };
                        this.logOcrService.post(this.logOcr).subscribe({
                          next: value => {
                            console.log(value);
                            this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          },
                          error: err3 => {
                            this.notificaciones.error('Error al guardar el log');
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          },
          error: errc => {
            console.log(errc);
            this.solicitudesVn.ocrServiceDomicilio(jsonSend).subscribe({
              next: doc => {
                this.solicitudesVn.ocrServiceRepuve(jsonSend).subscribe({
                  next: repuve => {
                    this.logOcr.datos = {
                        dataPoliza: pol,
                        dataCliente: 0,
                        dataDomicilio: doc,
                        dataRepuve: repuve
                      };
                    this.logOcrService.post(this.logOcr).subscribe({
                      next: value => {
                        console.log(value);
                        this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                        this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                      },
                      error: err => {
                        this.notificaciones.error('Error al guardar el log');
                        this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                      }
                    });
                  },
                  error: err => {
                    this.solicitudesVn.ocrServiceVehiculo(jsonSend).subscribe({
                      next: vehiculo => {
                        this.logOcr.datos = {
                            dataPoliza: pol,
                            dataCliente: 0,
                            dataDomicilio: doc,
                            dataRepuve: vehiculo
                          };
                        this.logOcrService.post(this.logOcr).subscribe({
                          next: value => {
                            console.log(value);
                            this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          },
                          error: err3 => {
                            this.notificaciones.error('Error al guardar el log');
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          }
                        });
                      },
                      error: err1 => {
                        this.logOcr.datos = {
                            dataPoliza: pol,
                            dataCliente: 0,
                            dataDomicilio: doc,
                            dataRepuve: 0
                          };
                        this.logOcrService.post(this.logOcr).subscribe({
                          next: value => {
                            console.log(value);
                            this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          },
                          error: err3 => {
                            this.notificaciones.error('Error al guardar el log');
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          }
                        });
                      }
                    });
                  }
                });
              },
              error: errr => {
                console.log(errr);
                this.solicitudesVn.ocrServiceRepuve(jsonSend).subscribe({
                  next: repuve => {
                    this.logOcr.datos = {
                        dataPoliza: pol,
                        dataCliente: 0,
                        dataDomicilio: 0,
                        dataRepuve: repuve
                      };
                    this.logOcrService.post(this.logOcr).subscribe({
                      next: value => {
                        console.log(value);
                        this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                        this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                      },
                      error: err => {
                        this.notificaciones.error('Error al guardar el log');
                        this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                      }
                    });
                  },
                  error: err => {
                    console.log(err);
                    this.solicitudesVn.ocrServiceVehiculo(jsonSend).subscribe({
                      next: vehiculo => {
                        this.logOcr.datos = {
                            dataPoliza: pol,
                            dataCliente: 0,
                            dataDomicilio: 0,
                            dataRepuve: vehiculo
                          };
                        this.logOcrService.post(this.logOcr).subscribe({
                          next: value => {
                            console.log(value);
                            this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          },
                          error: err3 => {
                            this.notificaciones.error('Error al guardar el log');
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          }
                        });
                      },
                      error: err1 => {
                        console.log(err1);
                        this.logOcr.datos = {
                            dataPoliza: pol,
                            dataCliente: 0,
                            dataDomicilio: 0,
                            dataRepuve: 0
                        };
                        this.logOcrService.post(this.logOcr).subscribe({
                          next: value => {
                            console.log(value);
                            this.notificaciones.exito().then(() => this.notificaciones.cerrar());
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          },
                          error: err3 => {
                            this.notificaciones.error('Error al guardar el log');
                            this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      },
      error: err =>  {
        this.notificaciones.error('No se pudo cargar el documento, se continuara la emision normalmente');
        this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);

      }
    });
    this.ref.close();
  }

  // async ocrService(json){
  //   await this.solicitudesVn.ocrService(json).subscribe({
  //     next: pol => {
  //       this.notificaciones.exito().then(() => this.notificaciones.cerrar());
  //       const navigationExtras: NavigationExtras = {state: {dataPoliza: pol}};
  //       this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`], navigationExtras);
  //     },
  //     error: err =>  {
  //       this.notificaciones.error('No se pudo cargar el documento, se continuara la emision normalmente');
  //       this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
  //
  //     }
  //   });
  //   this.ref.close();
  // }

  siguiente() {
    this.router.navigate([`modulos/venta-nueva/step-cotizador/${this.idSolicitud}`]);
    this.ref.close();
  }

  onKey(value) {
    const filter = value.toUpperCase();
    this.aseguradoraServ = this.aseg.filter(aseg => aseg.nombreComercial.toUpperCase().startsWith(filter));
  }

  cambiarCeCo(index, codigo) {
    console.log(index, codigo);
  }

  close(){
    this.ref.close();
  }
  almacenarArchivoCliente() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    this.filesToUploadCliente = (event.target as HTMLInputElement).files;
    if (this.filesToUploadCliente.length !== 0) {
      const extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUploadCliente.item(0).type);
      const tamanioArchivo = this.filesToUploadCliente.item(0).size * .000001;
      if (this.filesToUploadCliente.length === 1) {
        // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
        if (extensionValida && tamanioArchivo < 7) {
          this.notificaciones.exito('¡El archivo está listo para ser guardado!');
          this.archivoValidoCliente = true;
        } else {
          let texto: string;
          if (!extensionValida) {
            texto = 'Solo puedes subir archivos pdf';
          } else {
            texto = 'Los archivos que subas deben pesar menos de 7 MB';
          }
          this.notificaciones.error(texto);
          this.archivoValidoCliente = false;
        }
      }
    }
  }

  almacenarArchivoClienteDomicilio() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    this.filesToUploadDomicilio = (event.target as HTMLInputElement).files;
    if (this.filesToUploadDomicilio.length !== 0) {
      const extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUploadDomicilio.item(0).type);
      const tamanioArchivo = this.filesToUploadDomicilio.item(0).size * .000001;
      if (this.filesToUploadDomicilio.length === 1) {
        // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
        if (extensionValida && tamanioArchivo < 7) {
          this.notificaciones.exito('¡El archivo está listo para ser guardado!');
          this.archivoValidoDomicilio = true;
        } else {
          let texto: string;
          if (!extensionValida) {
            texto = 'Solo puedes subir archivos pdf';
          } else {
            texto = 'Los archivos que subas deben pesar menos de 7 MB';
          }
          this.notificaciones.error(texto);
          this.archivoValidoDomicilio = false;
        }
      }
    }
  }

  almacenarArchivoPoliza() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    this.filesToUpload = (event.target as HTMLInputElement).files;
    if (this.filesToUpload.length !== 0) {
      const extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);
      const tamanioArchivo = this.filesToUpload.item(0).size * .000001;
      if (this.filesToUpload.length === 1) {
        // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
        if (extensionValida && tamanioArchivo < 7) {
          this.notificaciones.exito('¡El archivo está listo para ser guardado!');
          this.archivoValidoPoliza = true;
        } else {
          let texto: string;
          if (!extensionValida) {
            texto = 'Solo puedes subir archivos pdf';
          } else {
            texto = 'Los archivos que subas deben pesar menos de 7 MB';
          }
          this.notificaciones.error(texto);
          this.archivoValidoPoliza = false;
        }
      }
    }
  }
  almacenarArchivoVehiculo() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    this.filesToUploadVehiculo = (event.target as HTMLInputElement).files;
    if (this.filesToUploadVehiculo.length !== 0) {
      const extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUploadVehiculo.item(0).type);
      const tamanioArchivo = this.filesToUploadVehiculo.item(0).size * .000001;
      if (this.filesToUploadVehiculo.length === 1) {
        // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
        if (extensionValida && tamanioArchivo < 7) {
          this.notificaciones.exito('¡El archivo está listo para ser guardado!');
          this.archivoValidoVehiculo = true;
        } else {
          let texto: string;
          if (!extensionValida) {
            texto = 'Solo puedes subir archivos pdf';
          } else {
            texto = 'Los archivos que subas deben pesar menos de 7 MB';
          }
          this.notificaciones.error(texto);
          this.archivoValidoVehiculo = false;
        }
      }
    }
  }


}
