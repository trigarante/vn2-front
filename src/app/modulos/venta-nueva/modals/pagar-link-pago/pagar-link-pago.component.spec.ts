import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagarLinkPagoComponent } from './pagar-link-pago.component';

describe('PagarLinkPagoComponent', () => {
  let component: PagarLinkPagoComponent;
  let fixture: ComponentFixture<PagarLinkPagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PagarLinkPagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagarLinkPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
