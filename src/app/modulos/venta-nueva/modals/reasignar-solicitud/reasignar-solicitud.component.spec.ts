import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasignarSolicitudComponent } from './reasignar-solicitud.component';

describe('ReasignarSolicitudComponent', () => {
  let component: ReasignarSolicitudComponent;
  let fixture: ComponentFixture<ReasignarSolicitudComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReasignarSolicitudComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasignarSolicitudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
