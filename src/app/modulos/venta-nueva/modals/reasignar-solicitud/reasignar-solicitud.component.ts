import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Empleados} from '../../../../@core/data/interfaces/rrhh/empleados';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {SolicitudesvnService} from '../../../../@core/data/services/venta-nueva/solicitudes.service';
import {EmpleadosService} from '../../../../@core/data/services/rrhh/empleados.service';

@Component({
  selector: 'app-reasignar-solicitud',
  templateUrl: './reasignar-solicitud.component.html',
  styleUrls: ['./reasignar-solicitud.component.scss'],
})
export class ReasignarSolicitudComponent implements OnInit {
  empleadosSubArea: Empleados[];
  empleadoSeleccionado: number;

  constructor(public dialogRef: MatDialogRef<ReasignarSolicitudComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private empleadoService: EmpleadosService,
              private solicitudesService: SolicitudesvnService,
              private notificaciones: NotificacionesService) {
  }

  ngOnInit() {
    this.getEjecutivosActivos();
  }

  getEjecutivosActivos() {
    this.empleadoService.getEjecutivosActivos().subscribe( data => {
      this.empleadosSubArea = data;
    });
  }

  putSolicitud() {
    this.notificaciones.carga('Se está actualizando la información, espere un momento.');
    this.solicitudesService.updateEmpleadoSolicitud(this.data.solicitud.id, this.empleadoSeleccionado).subscribe({
      error: () => {
        this.notificaciones.error('Ocurrió un error al cargar la solicitud, se te regresará al menú de solicitudes');
      }, complete: () => {
        this.notificaciones.exito('Solicitud reasignada').then(() => this.dialogRef.close(true));
      }
    });
  }

  dismiss(empleadoModificado: boolean) {
    this.dialogRef.close(empleadoModificado);
  }

  // reasignarCallbacks() {
  //   this.llamadaSalidaService.getLlamadasCallbackBySolicitud(this.data.id, this.data.idEmpleado, this.empleadoSeleccionado)
  //     .subscribe({
  //       complete: () => {
  //         this.notificaciones.exito('¡Solicitud actualizada con éxito!')
  //         .then( () => {
  //           this.dismiss(true);
  //         });
  //       },
  //     });
  // }

}

