import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InfoTarjeta } from '../../../../@core/data/interfaces/venta-nueva/solicitudesPago';

@Component({
  selector: 'app-info-tarjeta',
  templateUrl: './info-tarjeta.component.html',
  styleUrls: ['./info-tarjeta.component.scss']
})
export class InfoTarjetaComponent implements OnInit {
  infoTarjeta: InfoTarjeta;
  fechaPago: Date;
  constructor(
    @Inject(MAT_DIALOG_DATA) data: InfoTarjeta) {
      this.infoTarjeta = data;
      this.fechaPago = new Date(data.fechaPago);
    }

  ngOnInit(): void {
    console.log(this.infoTarjeta);
  }

}
