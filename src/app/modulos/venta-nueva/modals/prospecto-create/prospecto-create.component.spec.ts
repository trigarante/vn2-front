import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectoCreateComponent } from './prospecto-create.component';

describe('ProspectoCreateComponent', () => {
  let component: ProspectoCreateComponent;
  let fixture: ComponentFixture<ProspectoCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProspectoCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
