import {createReducer, on} from '@ngrx/store';
import {crearCliente} from "./venta-nueva.actions";

export interface VentaNuevaState {
  cliente: any;
  productoCliente: any;
  registro: any;
  recibos: any;
  pago: any;
  inspeccion: any;
}

export let ventaNuevaState: VentaNuevaState = {
  cliente: {},
  productoCliente: {},
  registro: {},
  recibos: {},
  pago: {},
  inspeccion: {},
};
//
// export const ventaNuevaReducers: ActionReducerMap<VentaNuevaState> = {
//   crearCliente: clienteReducer,
// };
// type VentaNuevaState = {};
export const initialState: VentaNuevaState[] = [];
  // [{
  //   cliente: {},
  //   productoCliente: {},
  //   registro: {},
  //   recibos: {},
  //   pago: {},
  //   inspeccion: {},
  // }];

const _clienteReducer = createReducer(
  initialState,
  on(crearCliente, (state, {data}) => [...state, data]),
);

export function ventaNuevaReducer(state, action) {
  return _clienteReducer(state, action);
}
