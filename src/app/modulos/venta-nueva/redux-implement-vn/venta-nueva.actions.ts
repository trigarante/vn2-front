import {createAction, props} from "@ngrx/store";
import {VentaNuevaState} from "./venta-nueva.reducer";


export const crearCliente = createAction(
  '[Cliente] Nuevo Cliente',
  props<{data: VentaNuevaState}>()
);
