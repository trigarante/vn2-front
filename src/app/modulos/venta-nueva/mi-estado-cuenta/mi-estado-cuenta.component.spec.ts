import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiEstadoCuentaComponent } from './mi-estado-cuenta.component';

describe('MiEstadoCuentaComponent', () => {
  let component: MiEstadoCuentaComponent;
  let fixture: ComponentFixture<MiEstadoCuentaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiEstadoCuentaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MiEstadoCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
