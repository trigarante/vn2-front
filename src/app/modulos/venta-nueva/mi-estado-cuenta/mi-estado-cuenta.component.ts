import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {FiltrosTablasService} from '../../../@core/filtros-tablas.service';
import {MiEstadoCuentaService} from '../../../@core/data/services/venta-nueva/mi-estado-cuenta.service';
import {MatTableDataSource} from '@angular/material/table';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import * as moment from "moment";
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import { MiEstadoCuenta } from 'src/app/@core/data/interfaces/venta-nueva/MiEstadoCuenta';
@Component({
  selector: 'app-mi-estado-cuenta',
  templateUrl: './mi-estado-cuenta.component.html',
  styleUrls: ['./mi-estado-cuenta.component.scss']
})
export class MiEstadoCuentaComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  dataSource;
  permisos = JSON.parse(window.localStorage.getItem('User'));
  errorSpin:boolean = false;
  displayedColumns: string[] = [ 'poliza',
    'empleado', 'socio', 'fechaInicio', 'subarea', 'comisionable',
    'pagado', 'autorizado', 'verificado',
    'aplicado', 'fechaAplicacionAseg', 'fechaAplicacion', 'fechaComision', 'fechaPago'];
  filterSelectObj = [];
  cargando = true;
  max = new Date();
  rangeDates: Date[];
  miEstadoCuenta: MiEstadoCuenta[];
  btnSendingDates: boolean = false;
  sendingDatesCounter: number = 0;

  constructor(private filtrosTablasService: FiltrosTablasService,
              private miEstadoCuentaService: MiEstadoCuentaService,
              private router: Router,
              private notificacionesService: NotificacionesService) {
    this.filterSelectObj = this.filtrosTablasService.createFilterSelect([{id: 'poliza', nombre: 'POLIZA'},
      {id: 'empleado', nombre: 'EMPLEADO'}, {id: 'socio', nombre: 'SOCIO'}, {id: 'subarea', nombre: 'SUBÁREA'},
     ]);

    this.rangeDates = [
      new Date(new Date().setDate(new Date().getDate() - 30)),
      new Date(new Date().setDate(new Date().getDate() + 1)),
    ];
  }

  ngOnInit(): void {
    this.getFechaRegistro(false, true);
  }
  getFechaRegistro(recarga: boolean, registro: boolean){
    if (recarga) this.dataSource = null;
    this.errorSpin = false;
    const fechaInicio = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaFin = (this.rangeDates[1].toISOString().split('T')[0]);

    const op = registro ? this.miEstadoCuentaService.getAllRegistroByIdUsuarioRegistro(fechaInicio, fechaFin)
                        : this.miEstadoCuentaService.getAllRegistroByIdUsuarioAplicado(fechaInicio, fechaFin)
    op.subscribe({
      next: data => {
        if (data.length === 0)
          this.notificacionesService.informacion('No hay solicitudes');

        this.miEstadoCuenta = data;
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;

        // Lineas para los filtros
        this.filterSelectObj.filter((o) => {
          o.options = this.filtrosTablasService.getFilterObject(data, o.columnProp);
        });

        // Lineas para los filtros
        this.dataSource.filterPredicate = this.filtrosTablasService.createFilter();
      },
      error: () => {
        this.btnSendingDates = false;
        this.errorSpin = true;
        this.notificacionesService.error('Error al cargar los datos');
      },
      complete: () => {
        this.sendingDatesCounter ++;
        if (this.sendingDatesCounter >= 2) {
          this.btnSendingDates = false;
        }
        this.btnSendingDates = false;
      },
    });
  }
  // Lineas para los filtros
  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();
    this.dataSource.filter = JSON.stringify(this.filterValues);
  }
  // Lineas para los filtros
  // escrituraendosos ;
  resetFilters() {
    this.filterValues = {};
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    });
    this.dataSource.filter = '';
  }

  openHelpModal() {
    Swal.fire({
      title: 'NOMENCLATURA DE COLORES',
      text: 'Seguido de la casilla de color se encuentra una descripción de lo que significa cada color en los iconos de los documentos',
      html: '<section style="text-align: left; font-family: Helvetica; margin-left: 50px;">' +
        '<p></p>' +
        '<div style="display: inline-flex;">' +
        '<div style="background-color: #1b5900; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 17px;"></div> ' +
        'Poliza verificada correctamente</div>' +

        '<div style="display: inline-flex;">' +
        '<div style="background-color: #e7a800; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 17px;"></div> ' +
        'Poliza pendiente de verificacion</div>' +

        '<div style="display: inline-flex;">' +
        '<div style="background-color: #850100; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 17px;"></div> ' +
        'Poliza que no ha llegado al proceso de verificacion</div>' +

        '</section>',
      icon: 'info',
    });
  }

  errorNavigate(estado: number) {
    if (estado === 2) {
      this.router.navigate(['/modulos/venta-nueva/correccion-errores/documentos/autorizacion']);
    } else if (estado === 5) {
      this.router.navigate(['/modulos/venta-nueva/correccion-errores/datos/autorizacion']);
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
