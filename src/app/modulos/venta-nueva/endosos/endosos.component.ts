import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {NotificacionesService} from "../../../@core/data/services/others/notificaciones.service";
import {FiltrosTablasService} from "../../../@core/filtros-tablas.service";
import {SolicitudEndosoService} from "../../../@core/data/services/endososCancelaciones/solicitud-endoso.service";

@Component({
  selector: 'app-endosos',
  templateUrl: './endosos.component.html',
  styleUrls: ['./endosos.component.scss']
})
export class EndososComponent implements OnInit {
  // ==================variables tabla==================
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  estadoVerificacion: string;
  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['poliza',
                                'nombre',
                                'nombreSolicitante',
                                'nombreEmisor',
                                'estadoSolicitudDesc',
                                'estadoEndoso',
                                'fechaSolicitud',
                                'departamento'
                                ];
  filterSelectObj = [];
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  permisos = JSON.parse(window.localStorage.getItem('User'));
  max:any = new Date();
  // ==================fin variables tabla==================
  errorSpin: boolean = false;
  constructor(private notificaciones:NotificacionesService,
              private filtrosTablasService: FiltrosTablasService,
              private solicitudEndosoService:SolicitudEndosoService) {
    this.filterSelectObj = this.filtrosTablasService.createFilterSelect([{id: 'poliza' , nombre: 'PÓLIZA'},
                                                                                  {id: 'nombre' , nombre: 'EMPLEADO'},
                                                                                  {id: 'nombreSolicitante' , nombre: 'EMPLEADO-SOLICITANTE'},
                                                                                  {id: 'nombreEmisor' , nombre: 'EMISOR'},
                                                                                  {id: 'estadoSolicitudDesc' , nombre: 'ESTADO-SOLICITUD'},
                                                                                  {id: 'estadoEndoso' , nombre: 'ESTADO-ENDOSO'},
                                                                                  {id: 'departamento' , nombre: 'DEPARTAMENTO'},]);
  }

  ngOnInit(): void {
    this.getSolicitudEndoso(false);
  }

  // ====================================================
  // ===========metodos tabla===========
  // ====================================================


  ngOnDestroy() {
    this.filtrosTablasService.filterSelect = [];
  }

  getSolicitudEndoso(recarga: boolean) {
    if (recarga) this.dataSource = null;
    this.errorSpin = false;
    const fechaInicio = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaFin = (this.rangeDates[1].toISOString().split('T')[0]);
    this.solicitudEndosoService.getSolicitudEndosoPendiente(fechaInicio, fechaFin).subscribe({
      next:   data =>  {
        if (data.length === 0)
          this.notificaciones.informacion('No tienes endosos')

        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error: () => {
        this.errorSpin = true;
        this.notificaciones.error('Error al cargar los datos')
      }
    });
  }



  // Lineas para los filtros
  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();

    this.dataSource.filter = JSON.stringify(this.filterValues);
  }


  // Lineas para los filtros
  resetFilters() {
    this.filterValues = {}
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    })
    this.dataSource.filter = "";
  }
  //====================================================
  // ===========fin metodos  tabla===========
  // ====================================================

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
