import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {Router} from '@angular/router';
import {GetTestDataService} from '../../../@core/data/services/get-test-data.service';
import {MatDialog} from '@angular/material/dialog';
import {SolicitudEndosoCreateComponent} from '../modals/solicitud-endoso-create/solicitud-endoso-create.component';
import {NuevoNumeroService} from '../../../@core/data/services/telefonia/nuevo-numero.service';
import {LlamadaSalidaComponent} from '../../telefonia/modals/llamada-salida/llamada-salida.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AgregarNumeroComponent} from '../../telefonia/modals/agregar-numero/agregar-numero.component';
import {Subscription} from 'rxjs';
import {WebrtcService} from '../../../@core/data/services/telefonia/webrtc.service';
import {SubirArchivoClienteComponent} from '../modals/subir-archivo-cliente/subir-archivo-cliente.component';
import {LogueoAppService} from '../../../@core/data/services/app/logueo-app.service';
import Swal from 'sweetalert2';
import {ModificarClienteComponent} from '../modals/modificar-cliente/modificar-cliente.component';
import {RegistroService} from '../../../@core/data/services/venta-nueva/registro.service';
import {SolicitudesvnService} from "../../../@core/data/services/venta-nueva/solicitudes.service";

@Component({
  selector: 'app-administracion-polizas',
  templateUrl: './administracion-polizas.component.html',
  styleUrls: ['./administracion-polizas.component.scss']
})
export class AdministracionPolizasComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource;
  errorSpin = false;
  displayedColumns: string[] = ['detalle', 'poliza', 'nombreCliente', 'nombre', 'nombreComercial', 'departamento', 'estadoPoliza', 'estadoPago', 'estadoRecibo',
    'telefonoMovil', 'fechaRegistro', 'numeroSerie', 'acceso', 'acciones'];
  filterSelectObj = [];


  rangeDates = [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  extension = localStorage.getItem('extension');
  puedeLlamar: boolean = false;
  tipo: string;
  cargando = true;
  numeros: any[];
  max = new Date();
  webRtcEvents: Subscription;
  permisos = JSON.parse(window.localStorage.getItem('User'));


  constructor(
    private solicitudesvnService: SolicitudesvnService,
    private adminVmViewService: RegistroService,
    private notificacionesService: NotificacionesService,
    private appServices: LogueoAppService,
    private testDataService: GetTestDataService,
    private agendaService: NuevoNumeroService,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private webRTCService: WebrtcService) {
    // setTimeout(() => {
    //   const element = document.getElementById('reconnect');
    //   element ? this.puedeLlamar = false : this.puedeLlamar = true;
    // }, 500);
  }

  copiarLink(valor) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = valor;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.notificacionesService.exito('Link copiado al portapeles');
  }

  ngOnInit(): void {
    this.getFechas(false);
    // this.listenWebrtcEvents();
  }

  ngOnDestroy() {
    if (this.webRtcEvents) {
      this.webRtcEvents.unsubscribe();
    }
  }

  getFechas(recarga: boolean) {
    if (recarga) {
      this.dataSource = null;
    }
    this.errorSpin = false;
    const fechaInicio = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaFin = (this.rangeDates[1].toISOString().split('T')[0]);

    this.adminVmViewService.getAllRegistroByIdUsuario(fechaInicio, fechaFin).subscribe({
      next: data => {
        if (data.length === 0) {
          this.notificacionesService.informacion('No hay solicitudes');
        }

        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error: () => {
        this.errorSpin = true;
        this.notificacionesService.error('Error al cargar los datos');
      }
    });
  }
  crearEndoso(idRegistro) {
    this.dialog.open(SolicitudEndosoCreateComponent, {
      data: idRegistro,

    }).afterClosed().subscribe(x => {
      if (x) {
        this.getFechas(true);
      }
    });

  }

  mostrarPoliza(id) {
    this.router.navigate(['modulos/venta-nueva/viewer/2', id]);
  }

  subirArchivo(id) {
    this.dialog.open(SubirArchivoClienteComponent, {
      width: '500px',
      data: id,
    }).afterClosed().subscribe(x => {
      if (x) {
        this.getFechas(true);
      }
    });
  }

  generarInspeccion(id, idCliente) {
    this.router.navigate(['modulos/venta-nueva/inspecciones', id, idCliente]);
  }

  recibos(idRegistro, idRecibo, idSolicitud) {
    this.router.navigate(['modulos/venta-nueva/recibos-administrador-recibos', idRegistro, idRecibo, idSolicitud]);
  }

  detallesPoliza(id) {
    this.router.navigate(['modulos/venta-nueva/detalles-poliza', id]);
  }

  enviarEmail(idRegistro) {
    Swal.fire({
      title: '¿Esta seguro que desea enviar email al cliente?',
      text: 'Se enviara un email al cliente con la información de acceso al portal y aplicación',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, enviar email',
      cancelButtonText: 'Cancelar',
      reverseButtons: true,
    }).then((result) => {
          if (result.value) {
        this.appServices.enviarEmail(idRegistro).subscribe({
          next: data => {
            this.notificacionesService.exito('Correo enviado');
            this.getFechas(false);
          },
          error: data => {
            if (data.error.message === 'Error idRegistro no ligado'){
              this.notificacionesService.error('aun no se encuentra ligado el usuario con el registro');
            }
          }
        });
      }
    });
  }
  modificarCliente(idCliente) {
    this.dialog.open(ModificarClienteComponent, {
      data: idCliente,
  });
  }

  getNumeros(idRegistro, numeroOriginal) {
    this.cargando = true;
    this.numeros = [];
    this.agendaService.getNumerosByIdRegistro(idRegistro).subscribe((resp: any) => {
        resp.forEach(telefono => this.numeros.push(telefono.numero));
        this.numeros.unshift(numeroOriginal);
      },
      () => {
      },
      () => this.cargando = false);
  }

  llamar(idSolicitud, numero) {
    this.puedeLlamar = true;
    this.solicitudesvnService.llamadaUcontact(idSolicitud, numero).subscribe();
    this.notificacionesService.exito('SE A REALIZADO LA LLAMADA EN UCONTACT');
    setTimeout(() => {
      this.puedeLlamar = false;
    }, 1000 * 30);
    // this.snackBar.openFromComponent(LlamadaSalidaComponent, {
    //   data: {
    //     idRegistro,
    //     numero,
    //   }
    // });
  }

  historicoLlamadas(idRegistro) {
    this.router.navigate(['/modulos/telefonia/historial-llamadas/registro/' + idRegistro]);
  }


  agregarNumero(idRegistro, poliza) {
    this.dialog.open(AgregarNumeroComponent, {
      data: {
        idRegistro,
        poliza
      }
    });
  }

  listenWebrtcEvents() {
    if (this.extension.length > 0 && this.extension !== 'null') {
      this.webRtcEvents = this.webRTCService.listenEvents().subscribe((resp) => {
        switch (resp.evento) {
          case 'connected': {
            setTimeout(() => {
              this.puedeLlamar = true;
            }, 5000);
            break;
          }
          case 'disconnected': {
            this.puedeLlamar = false;
            break;
          }
        }
      });
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}

