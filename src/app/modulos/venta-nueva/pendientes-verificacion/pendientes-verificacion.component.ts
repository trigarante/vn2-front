import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import Swal from 'sweetalert2';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {FiltrosTablasService} from '../../../@core/filtros-tablas.service';
import {VerificacionesPendientesService} from '../../../@core/data/services/venta-nueva/verificaciones-pendientes.service';
import {CorreccionErroresService} from '../../../@core/data/services/venta-nueva/correccion-errores.service';
import {VerificacionRegistroService} from '../../../@core/data/services/verificacion/verificacion-registro.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'app-pendientes-verificacion',
  templateUrl: './pendientes-verificacion.component.html',
  styleUrls: ['./pendientes-verificacion.component.scss']
})
export class PendientesVerificacionComponent implements OnInit, OnDestroy {
  // -----variables tabla---
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  verificacionesPendientes = new MatTableDataSource();
  displayedColumns: string[] = ['poliza', 'nombreComercial', 'estadoPago', 'fechaPago', 'fechaInicio', 'subarea', 'nombre', 'nombreEmpleadoMesa',
  'nombreCliente', 'numeroSerie', 'fechaCreacion', 'Acciones'];
  filterSelectObj = [];
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  permisos = JSON.parse(window.localStorage.getItem('User'));
  max: any = new Date();
  estadoVerificacion;
  // -----fin variables tabla---

  errorSpin = false;


  constructor(private filtrosTablasService: FiltrosTablasService,
              private correccionErroresService: CorreccionErroresService,
              private verificacionesPendientesService: VerificacionesPendientesService,
              private notificaciones: NotificacionesService) {
    this.filterSelectObj = this.filtrosTablasService.createFilterSelect([{id: 'poliza' , nombre: 'PÓLIZA'},
                                                                                  {id: 'nombreComercial' , nombre: 'SOCIO'},
                                                                                  {id: 'estadoPago' , nombre: 'ESTADO-PAGO'},
                                                                                  {id: 'subarea' , nombre: 'DEPARTAMENTO'},
                                                                                  {id: 'nombre' , nombre: 'EMPLEADO'},
                                                                                  {id: 'nombreCliente' , nombre: 'CLIENTE'}]);
  }

  ngOnInit(): void {
    this.getRegistrosEnProceso(false);
  }
  // ====================================================
  // ===========metodos tabla===========
  // ====================================================


  ngOnDestroy() {
    this.filtrosTablasService.filterSelect = [];
  }

  getRegistrosEnProceso(recarga: boolean) {
    if (recarga) { this.verificacionesPendientes = null; }
    this.errorSpin = false;
    const fechaInicio = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaFin = (this.rangeDates[1].toISOString().split('T')[0]);
    this.verificacionesPendientesService.get(this.estadoVerificacion, fechaInicio, fechaFin).subscribe({
      next: data => {
        if (data.length === 0) {
          this.notificaciones.informacion('No hay solicitudes');
        }

        this.verificacionesPendientes = new MatTableDataSource(data);
        this.verificacionesPendientes.sort = this.sort;
        this.verificacionesPendientes.paginator = this.paginator;
      },
      error: () => {
        this.errorSpin = true;
        this.notificaciones.error('Error al cargar los datos');
      }
      });
  }

  // Lineas para los filtros
  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();
    this.verificacionesPendientes.filter = JSON.stringify(this.filterValues);
  }


  // Lineas para los filtros
  resetFilters() {
    this.filterValues = {};
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    });
    this.verificacionesPendientes.filter = '';
  }
  // ====================================================
  // ===========fin metodos  tabla===========
  // ====================================================


  openHelpModal() {
    Swal.fire({
      title: 'NOMENCLATURA DE COLORES',
      text: 'Seguido de la casilla de color se encuentra una descripción de lo que significa cada color en los iconos de los documentos',
      html: '<section style="text-align: left; font-family: Helvetica; margin-left: 50px;">' +
        '<p></p>' +
        '<div style="display: inline-flex;">' +
        '<div style="background-color: #1b5900; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 17px;"></div> ' +
        'Verificación del documento concluida</div>' +

        '<div style="display: inline-flex;">' +
        '<div style="background-color: #e7a800; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 17px;"></div> ' +
        'Documento pendiente de verificar</div>' +

        '<div style="display: inline-flex;">' +
        '<div style="background-color: #850100; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 17px;"></div> ' +
        'Documento en proceso de corrección</div>' +

        '<div style="display: inline-flex;">' +
        '<div style="background-color: #155b94; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 17px;"></div> ' +
        'Documento corregido por el ejecutivo</div>' +

        '</section>',
      icon: 'info',
    });
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.verificacionesPendientes.filter = filterValue.trim().toLowerCase();
  }

}
