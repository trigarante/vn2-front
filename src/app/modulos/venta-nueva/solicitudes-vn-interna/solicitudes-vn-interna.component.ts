import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {SolicitudesvnService} from '../../../@core/data/services/venta-nueva/solicitudes.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {DatosSolicitudComponent} from '../modals/datos-solicitud/datos-solicitud.component';
import {Router} from '@angular/router';
import {AgendaLlamadasService} from '../../../@core/data/services/telefonia/agendaLlamadas/agenda-llamadas.service';
import {ProspectoCreateComponent} from '../modals/prospecto-create/prospecto-create.component';
import {Subscription} from 'rxjs';
import {WebrtcService} from "../../../@core/data/services/telefonia/webrtc.service";
import {AgregarNumeroComponent} from "../../telefonia/modals/agregar-numero/agregar-numero.component";
import {NuevoNumeroService} from "../../../@core/data/services/telefonia/nuevo-numero.service";
import {LlamadaSalidaComponent} from "../../telefonia/modals/llamada-salida/llamada-salida.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ChatWhatsappComponent} from "../modals/chat-whatsapp/chat-whatsapp.component";
import {WhatsMsgService} from "../../../@core/data/services/whatspp/whats-msg.service";
import {RegistroService} from "../../../@core/data/services/venta-nueva/registro.service";
import {CotizacionesAliService} from "../../../@core/data/services/venta-nueva/cotizaciones-ali.service";
import {CotizacionesAli} from "../../../@core/data/interfaces/venta-nueva/cotizaciones-ali";
import {Solicitudes} from '../../../@core/data/interfaces/venta-nueva/solicitudes';
import {ReasignarSolicitudComponent} from '../modals/reasignar-solicitud/reasignar-solicitud.component';
import {PickerAseguradoraComponent} from "../modals/picker-aseguradora/picker-aseguradora.component";


@Component({
  selector: 'app-solicitudes-vn-interna',
  templateUrl: './solicitudes-vn-interna.component.html',
  styleUrls: ['./solicitudes-vn-interna.component.scss']
})
export class SolicitudesVnInternaComponent implements OnInit, OnDestroy {
  // -----variables tabla---
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  dataSource;
  displayedColumns: string[] = ['detalle', 'id', 'nombreEmpleado', 'nombreaCliente', 'numero',
    'descripcionDepartemento', 'fechaSolicitud',   'acciones' ];
  filterSelectObj = [];
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  permisos = JSON.parse(window.localStorage.getItem('User'));
  max: any = new Date();
  // -----fin variables tabla---
  extension = localStorage.getItem('extension');
  puedeLlamar: boolean = false;
  cargando = true;
  numeros: any[];
  errorSpin: boolean = false;
  webRtcEvents: Subscription;
  mandarPlantilla = false;
  datosCanalMensajes: any;
  plantillaEnviada: boolean;
  cotizacionAli: CotizacionesAli;
  peticiones: any;
  idPuesto = +sessionStorage.getItem('idPuesto');
  salva = '';

  constructor(
    private solicitudesvnService: SolicitudesvnService,
    private notificacionesService: NotificacionesService,
    private dialog: MatDialog,
    private whatsMsgService: WhatsMsgService,
    private snackBar: MatSnackBar,
    private numerosService: NuevoNumeroService,
    private webRTCService: WebrtcService,
    private router: Router,
    private agendaService: AgendaLlamadasService,
    private solicitudesService: SolicitudesvnService,
    private cotizacionesAliService: CotizacionesAliService
  ) {
    // this.isConnected();
  }

  ngOnInit(): void {
    this.getFechas(false);
    // this.listenWebrtcEvents();
  }
  reasignarSolicitud(solicitud: Solicitudes) {
    this.dialog.open(ReasignarSolicitudComponent, {
      width: '400px',
      data: {
        solicitud,
        // tipo: this.tipo,
      },
    }).afterClosed().subscribe(data => {
      // Se ejecuta la peticion completa cuando hay un error para evitar perder información
      if (data)
        this.getFechas(true);
    });
  }
  listenWebrtcEvents() {
    if (this.extension.length > 0 && this.extension !== 'null') {
      this.webRtcEvents = this.webRTCService.listenEvents().subscribe((resp) => {
        switch (resp.evento) {
          case 'connected':
          {
            setTimeout(() => {
              this.puedeLlamar = true;
            }, 7000);
            break;
          }
          case 'disconnected':
          {
            this.puedeLlamar = false;
            break;
          }
        }
      });
    }
  }

  // ====================================================
  // =========== metodos tabla===========
  // ====================================================

  ngOnDestroy() {
    if (this.webRtcEvents) this.webRtcEvents.unsubscribe();
  }

  // ====================================================
  // ===========fin metodos  tabla===========
  // ====================================================

  crearSolicitudVN() {
    this.dialog.open(ProspectoCreateComponent);
  }

  getFechas(recarga: boolean) {

    if (recarga) this.dataSource = null;
    this.errorSpin = false;
    const fechaCompleta1 = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaCompleta2 = (this.rangeDates[1].toISOString().split('T')[0]);

    this.solicitudesvnService.getFechasint(fechaCompleta1, fechaCompleta2, 3, 2).subscribe({
      next: data => {
        if (data.length === 0)
          this.notificacionesService.advertencia('No hay solicitudes');

        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        // Lineas para los filtros
      },
      error: () => {
        this.errorSpin = true;
        this.notificacionesService.error('Error al cargar los datos');
      }
    });
  }

  verDatos(idProducto: number){
    this.dialog.open(DatosSolicitudComponent, {
      width: '500px',
      data: {idProducto}
    });
  }

  // etiquetaSolicitud(idSolicitud, idCotizacionAli, idEmpleado, idLlamada) {
  //   localStorage.setItem('tipificando', '1');
  //   this.dialog.open(EtiquetaSolicitudComponent, {
  //     data: {
  //       idSolicitud,
  //       idCotizacionAli,
  //       idEmpleado,
  //       idLlamada,
  //     },
  //   }).afterClosed().subscribe(reload => {
  //     // se ejcutara cuando haya un error
  //     localStorage.setItem('tipificando', '0');
  //     this.flujoLlamadaService.available();
  //     this.getFechas(false);
  //   });
  // }
  // reasignarSolicitud(id, nombre) {
  //   this.dialog.open(ReasignarSolicitudComponent, {
  //     width: '400px',
  //     data: {
  //       id, nombre,
  //     },
  //   }).afterClosed().subscribe(data => {
  //     // Se ejecuta la peticion  cuando hay un error para evitar perder información
  //     if (data) {
  //       this.getFechas(false);
  //     }
  //   });
  // }

  generarEmision(idSolicitud) {
    /*this.dialog.open(PickerAseguradoraComponent, {
      data: idSolicitud,
      width: '500px',
      height: '500px'
    });*/
    this.router.navigate([`modulos/venta-nueva/step-cotizador/${idSolicitud}`]);
  }


  mostrarChat(data) {
    //   this.socket.emit('chatBtnHeader', 'chat');
    //   if (!data.notificacion || data.notificacion === 1) {
    //     this.dataSource.data.map((dataMap, index) => {
    //       if (String(dataMap.id) === String(data.id)) {
    //         this.dataSource.data[index].notificacion = 0;
    //       }
    //     });
    //   }
    //   const indice = this.solicitudesConMensajes.indexOf(data.id);
    //   this.solicitudesConMensajes.splice(indice, 1);
    // }
    this.notificacionesService.carga('Abriendo chat');
    this.whatsMsgService.getMensajesByIdCanal(data.id, data.numero)
      .subscribe({
        next: (value: any) => {
          if (value.cuerpo === null) {
            this.plantillaEnviada = false;
            this.notificacionesService.informacion('No se puede mostrar el chat debido a que no se ha enviado una plantilla a este lead');
          } else {
            if (value.cuerpo !== null) {
              this.notificacionesService.exitoWhats();
              this.datosCanalMensajes = value;
              const credentials = value.catalogoData;
              this.plantillaEnviada = true;
              this.modalChat(data, credentials, value);
            }
          }
        },
        error: err => {
          this.notificacionesService.error('Ocurrió un error');
        }
      });
  }

  modalChat(data, credentials, dataCanal) {
    sessionStorage.setItem('idSolicitud', String(data.id));
    this.dialog.open(ChatWhatsappComponent, {
      width: '550px',
      height: '450px',
      data: {
        data,
        credentials,
        dataCanal
      }
      // panelClass: ['estilosModal', 'col-lg-8'],
      // data: {data: data, credentials: credentials},
    }).afterClosed().subscribe((value: any) => {
      // this.socket.emit(`solicitudesRoom`, 2);
      // // this.socket.emit(`tablaSolicitudesHeaderRoom`, 3);
      // if (data.notificacion !== false && data.notificacion === 1) {
      //   this.dataSource.data.map((dataMap, index) => {
      //     if (String(dataMap.id) === String(data.id)) {
      //       this.dataSource.data[index].notificacion = 0;
      //     }
      //   });
      // } else {
      //   if (!data.notificacion) {
      //     this.dataSource.data.map((dataMap, index) => {
      //       if (String(dataMap.id) === String(data.id)) {
      //         this.dataSource.data[index].notificacion = 0;
      //       }
      //     });
      //   }
      // }
    });
  }

  isConnected() {
    setTimeout(() => {
      const element = document.getElementById('reconnect');
      element ? this.puedeLlamar = false : this.puedeLlamar = true;
    }, 500);
  }

  agregarNumero(idSolicitud) {
    this.dialog.open(AgregarNumeroComponent, {
      data: {
        idSolicitud
      }
    });
  }
  //
  // llamar(idSolicitud, numero) {
  //   this.snackBar.openFromComponent(LlamadaSalidaComponent, {
  //     data: {
  //       idSolicitud,
  //       numero,
  //     }
  //   });
  // }

  llamar(idSolicitud, numero){
    this.puedeLlamar = true;
    this.solicitudesvnService.llamadaUcontact(idSolicitud, numero).subscribe();
    this.notificacionesService.exito('SE A REALIZADO LA LLAMADA EN UCONTACT');
    setTimeout(() => {
      this.puedeLlamar = false;
    }, 1000 * 30);
  }

  getNumeros(idSolicitud, numeroOriginal) {
    this.cargando = true;
    this.numeros = [];
    this.numerosService.getNumerosSolicitud(idSolicitud).subscribe((resp: any[]) => {
      resp.forEach((telefono) => {
        this.numeros.push(telefono.numero);
      });
      this.numeros.unshift(numeroOriginal);
      this.cargando = false;
    });
  }

  historicoLlamadas(idSolicitud) {
    this.router.navigate(['/modulos/telefonia/historial-llamadas/' + idSolicitud]);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  generarEmisionOnlineFunnel(idSolicitud) {

    this.solicitudesService.getSolicitudesByIdOnline(idSolicitud).subscribe(data => {
      this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        this.peticiones = this.cotizacionAli.respuesta;
        console.log(this.peticiones.aseguradora);

        // this.router.navigate(['modulos/venta-nueva/step-cotizador-online-funnel/' + idSolicitud + '/'
        // + this.peticiones.aseguradora ]);

        switch (this.peticiones.aseguradora) {
          case 'QUALITAS':
            this.router.navigate(['modulos/venta-nueva/step-online/' + idSolicitud + '/'
            + this.peticiones.aseguradora ]);
            break;
          // case 'GNP' :
          //   this.router.navigate(['modulos/venta-nueva/step-cotizador-online-funnel2/' + idSolicitud + '/'
          //   + this.peticiones.aseguradora ]);
          //   break;
          default:

            break;
        }

      });
    }); // fin de solicitud

  }
}
