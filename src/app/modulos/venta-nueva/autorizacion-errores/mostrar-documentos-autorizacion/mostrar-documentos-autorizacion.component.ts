import { Component, OnInit } from '@angular/core';
import {AutorizacionRegistro} from '../../../../@core/data/interfaces/venta-nueva/autorizacion/autorizacion-registro';
import {AutorizacionRegistroService} from '../../../../@core/data/services/venta-nueva/autorizacion/autorizacion-registro.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {ClienteVnService} from '../../../../@core/data/services/venta-nueva/cliente-vn.service';
import {PagosService} from '../../../../@core/data/services/venta-nueva/pagos.service';
import {ProductoClienteService} from '../../../../@core/data/services/venta-nueva/producto-cliente.service';
import {RegistroService} from '../../../../@core/data/services/venta-nueva/registro.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CampoCorregirAutorizacionService} from '../../../../@core/data/services/venta-nueva/autorizacion/campo-corregir-autorizacion.service';
import {TipoDocumentoAutorizacionService} from '../../../../@core/data/services/venta-nueva/autorizacion/tipo-documento-autorizacion.service';
import {ErroresAutorizacionService} from '../../../../@core/data/services/venta-nueva/autorizacion/errores-autorizacion.service';
import {ErroresAnexosService} from '../../../../@core/data/services/venta-nueva/autorizacion/errores-anexos.service';
import {Location} from '@angular/common';
import { filter } from 'rxjs/operators';
@Component({
  selector: 'app-mostrar-documentos-autorizacion',
  templateUrl: './mostrar-documentos-autorizacion.component.html',
  styleUrls: ['./mostrar-documentos-autorizacion.component.scss']
})
export class MostrarDocumentosAutorizacionComponent implements OnInit {
  textoDocumentoActual: string;
  documentoLegible = false;
  documentosPCorregir = [];
  autorizacionRegistro: AutorizacionRegistro;
  idAutorizacionRegistro: number = +this.route.snapshot.paramMap.get('id_autorizacion_registro');
  idTipoDocumento: number = +this.route.snapshot.paramMap.get('id_tipo_documento');
  idCorreccionActual: number;
  checkBoxes = [];
  permisos = JSON.parse(window.localStorage.getItem('User'));
  salva = '';
  // ----DESV: variable para saber si proviene de una poliza desviada---
  desviada = false;
  // -----FIN-DESV-----
  // si es desviada, dirigira al componente proceso desviada (5) si no lo es, dirige al de normales (2)
  direccion: number;
  constructor(
              private notificaciones: NotificacionesService,
              private clienteService: ClienteVnService,
              private pagoService: PagosService,
              private campoCorregirService: CampoCorregirAutorizacionService,
              private tipoDocumentosService: TipoDocumentoAutorizacionService,
              private registroService: RegistroService,
              private productoClienteService: ProductoClienteService,
              private autorizacionRegistroService: AutorizacionRegistroService,
              private route: ActivatedRoute,
              private erroresAutorizacionService: ErroresAutorizacionService,
              private router: Router,
              private location: Location,
              private erroresAnexosService: ErroresAnexosService) {
  }

  ngOnInit(): void {
    // se obtiene los queryparams para saber si se proviene de una desviada
    this.route.queryParams.pipe(
      filter(params => params.desviada)
    ).subscribe(params => {
          console.log(params);
          this.direccion = params.desviada === 'true' ? 5 : 2;
          console.log(this.direccion);
        }
      );
    if (this.idTipoDocumento) {
      this.getDocumentosPCorregir(this.idTipoDocumento);
      this.getVerificacionRegistro();
    } else {
      this.getEstadoCorreccion();
    }

  }
  getEstadoCorreccion() {
    this.autorizacionRegistroService.getById(this.idAutorizacionRegistro).subscribe({
      next: data => {
        this.autorizacionRegistro = data;
      },
      complete: () => {
        const { verificadoCliente, verificadoProducto, verificadoRegistro, verificadoPago} = this.autorizacionRegistro;
        if (verificadoCliente === 1){
          this.actualizarInfo(1, 'cliente', this.getInfo(1));
        } else if (verificadoProducto === 1){
          this.actualizarInfo(3, 'producto', this.getInfo(3));
        } else if (verificadoRegistro === 1){
          this.actualizarInfo(2, 'registro', this.getInfo(2));
        } else if (verificadoPago === 1){
          this.actualizarInfo(4, 'pago', this.getInfo(4));
        } else {
          this.actualizarInfo(1, 'cliente', this.getInfo(1));
        }
        this.getDocumentosPCorregir(this.idCorreccionActual);
      },
    });
  }
  actualizarInfo(correccion: number, texto: string, getInfo){
    this.idCorreccionActual = correccion;
    this.textoDocumentoActual = texto;
  }
  verificarCheck(esDocumento): boolean {
    return JSON.stringify(esDocumento ? this.documentosPCorregir : this.checkBoxes).includes('true');
  }

  getInfo(opcion) {
    let op;
    switch (opcion) {
      case 1:
        op = this.clienteService.getClienteById(this.autorizacionRegistro.idCliente);
        break;
      case 2:
        op = this.registroService.getRegistroById(this.autorizacionRegistro.idRegistro);
        break;
      case 3:
        op = this.productoClienteService.getProductoClienteById(this.autorizacionRegistro.idProducto);
        break;
      case 4:
        console.log(this.autorizacionRegistro.idPago);
        op = this.pagoService.getPagoById(this.autorizacionRegistro.idPago);
        break;
    }
    op.subscribe({
      next: value => {
        this.getCamposPCorregir(value, opcion);
      },
    });
  }

  async postErroresDocumentos() {
    /* Si se seleccionó al menos un documento a corregir, se mandará la información de lo contrario se notificará
       al usuario */
    if (JSON.stringify(this.documentosPCorregir).includes('true')) {
      const respuesta = await this.notificaciones.pregunta('¿Estás seguro que el documento no es legible?');
      if (respuesta.value) {
        this.notificaciones.carga('Mandando errores ingresados');

        const arregloDocumentos: any = [];
        const tipoDocumento = !this.idTipoDocumento ? this.idCorreccionActual : this.idTipoDocumento;
        for (const documento of this.documentosPCorregir) {
          if (documento.activado) {
            arregloDocumentos.push({
              idTipoDocumento: documento.id,
              nombreDocumentoCorregir: documento.descripcion,
              comentario: documento.comentario,
            });
          }
        }
        const errorAnexo = {
          idAutorizacionRegistro: this.idAutorizacionRegistro,
          idEmpleado: +sessionStorage.getItem('Empleado'),
          idTipoDocumento: tipoDocumento,
          idEmpleadoCorreccion: this.autorizacionRegistro.idEmpleado,
          correcciones: arregloDocumentos,
        };
        this.erroresAnexosService.post(errorAnexo).subscribe({
          error: () => this.notificaciones.error('Ha ocurrido un error, intenta nuevamente o contacta al departamento de TI'),
          complete: () => this.putAutorizacionRegistro(2),
        });
      }
    }
  }
  async postErroresEnCampos() {
    const respuesta = await this.notificaciones.pregunta('¿Estás seguro que el documento tiene errores en los campos que seleccionaste?');
    if (respuesta.value) {
      this.notificaciones.carga('Mandando errores ingresados');
      const arregloDocumentos: any = [];
      const idTipoDocumento = !this.idTipoDocumento ? this.idCorreccionActual : this.idTipoDocumento;
      for (const documento of this.checkBoxes) {
        if (documento.activo) {
          arregloDocumentos.push({
            idCampo: documento.id,
            campo: documento.campo,
            comentario: documento.comentario,
          });
        }
      }
      const errorAnexo = {
        idAutorizacionRegistro: this.idAutorizacionRegistro,
        idEmpleado: +sessionStorage.getItem('Empleado'),
        idEstadoCorreccion: 1,
        idTipoDocumento,
        idEmpleadoCorreccion: this.autorizacionRegistro.idEmpleado,
        correcciones: arregloDocumentos,
      };

      this.erroresAutorizacionService.post(errorAnexo).subscribe({
        error: () => this.notificaciones.error('Ha ocurrido un error, intenta nuevamente o contacta al departamento de TI'),
        complete: () => this.putAutorizacionRegistro(5),
      });
    }
  }
  cerrar(){
    this.location.back();
  }

  putAutorizacionRegistro(estado: number) {
    this.notificaciones.carga('Actualizando el registro');
    const documento = !this.idTipoDocumento ? this.idCorreccionActual : this.idTipoDocumento;
    this.autorizacionRegistroService.documentoVerificadoVn(this.idAutorizacionRegistro, documento, estado).subscribe({
      error: () => this.notificaciones.error('Ha ocurrido un error, intenta nuevamente o contacta al departamento de TI'),
      complete: () => {
        this.notificaciones.exito('Documento comprobado').then( () => {
          if (this.idTipoDocumento || this.idCorreccionActual === 4) {
            console.log('-dir-', this.direccion);
            this.router.navigate([`/modulos/venta-nueva/autorizacion-errores/${this.direccion}`]);
          } else {
            this.checkBoxes = [];
            this.documentosPCorregir = [];
            this.documentoLegible = false;
            this.getEstadoCorreccion();
          }
        });
      },
    });

  }
  getCamposPCorregir(info: any, idTabla: number) {
    console.log(info);
    if (info.datos?.numeroTarjeta) {
      for (let i = 0; i < info.datos.numeroTarjeta.length; i++) {
        if (i < info.datos.numeroTarjeta.length - 4) {
          this.salva = this.salva + 'x';
        }
        else {
          this.salva = this.salva + info.datos.numeroTarjeta[i];
        }
      }
    }
    const op = info.razonSocial === 2 ? this.campoCorregirService.getCamposDisponiblesForMoral(idTabla) :
      this.campoCorregirService.getCamposDisponibles(idTabla);
    op.subscribe({
      next: (result) => {
        if (idTabla === 3) {
          for (const campo of result) {
            this.checkBoxes.push({
              id: campo.id,
              idTabla: campo.idTabla,
              activo: false,
              nombre: campo.descripcion,
              campo: campo.campo,
              informacion: campo.campo === 'numeroSerie' ? info.noSerie : info.datos[campo.campo] === '' || !info.datos[campo.campo] ? 'No hay información' : info.datos[campo.campo],
              comentario: '',
            });
          }
        } else {
          for (const campo of result) {
            this.checkBoxes.push({
              id: campo.id,
              idTabla: campo.idTabla,
              activo: false,
              nombre: campo.descripcion,
              campo: campo.campo,
              // informacion: campo.campo === 'fechaInicio' ? info.fechaInicio.split('T')[0] : !info[campo.campo] ? 'No hay información' : info[campo.campo],
              informacion: campo.campo === 'fechaInicio' ? info.fechaInicio.split('T')[0] : campo.campo === 'numeroTarjeta' ? `${this.salva}` : info[campo.campo] === '' || !info[campo.campo] ? 'No hay información' : info[campo.campo],
              comentario: '',
            });
          }
        }
      },
    });
  }
  getDocumentosPCorregir(idTabla: number) {
    this.tipoDocumentosService.get(idTabla).subscribe({
      next: (data) => {
        for (const val of data) {
          this.documentosPCorregir.push({
            id: val.id,
            descripcion: val.descripcion,
            comentario: '',
            activado: false,
          });
        }
      },
    });
  }
  getVerificacionRegistro() {
    this.autorizacionRegistroService.getVnById(this.idAutorizacionRegistro).subscribe({
      next: data => this.autorizacionRegistro = data,
      complete: () => {
        switch (this.idTipoDocumento) {
          case 1:
            this.actualizarInfo(1, 'cliente', this.getInfo(1));
            break;
          case 2:
            this.actualizarInfo(2, 'registro', this.getInfo(2));
            break;
          case 3:
            this.actualizarInfo(3, 'producto', this.getInfo(3));
            break;
          case 4:
            this.actualizarInfo(4, 'pago', this.getInfo(4));
            break;
        }
      },
    });
  }
}
