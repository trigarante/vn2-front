import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-venta-nueva',
  template:  `
    <router-outlet></router-outlet>
  `,
})
export class VentaNuevaComponent {}
