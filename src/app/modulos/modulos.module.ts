import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModulosRoutingModule } from './modulos-routing.module';
import { ModulosComponent } from './modulos.component';
import {ThemeModule} from '../@theme/theme.module';
import {MaterialModule} from "./material.module";

import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    ModulosComponent,
  ],
  imports: [
    CommonModule,
    ModulosRoutingModule,
    ThemeModule,
    MaterialModule,
    FlexLayoutModule
  ]
})
export class ModulosModule { }
