import { Component, OnInit, NgZone } from '@angular/core';
import {AuthService} from "../../@core/AuthService";
import {ActualServerTelefoniaService} from "../../@core/data/services/telefonia/actual-server-telefonia.service";
import {WebrtcService} from "../../@core/data/services/telefonia/webrtc.service";

declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private currentServer: ActualServerTelefoniaService,
    private webrtc: WebrtcService,
    private ngZone: NgZone
  ) {
  }

  ngOnInit() {
    this.renderButton();
  }

  loginFunction() {
    this.authService.login();
  }

  renderButton() {
    gapi.signin2.render('my-signin2', {
      scope: 'profile email',
      width: 240,
      height: 50,
      longtitle: true,
      theme: 'dark',
      onsuccess: this.ngZone.run(() =>  this.onSuccess),
      onfailure: this.ngZone.run(() => this.onFailure),
    });
  }

  onSuccess = (googleUser) => {
    const id_token = googleUser.getAuthResponse().id_token;
    this.authService.sendToken(id_token);
  };

  onFailure(error) {
    console.log(error);
  }


}
