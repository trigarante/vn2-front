import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedimientosButtonModalComponent } from './procedimientos-button-modal.component';

describe('ProcedimientosButtonModalComponent', () => {
  let component: ProcedimientosButtonModalComponent;
  let fixture: ComponentFixture<ProcedimientosButtonModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcedimientosButtonModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedimientosButtonModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
