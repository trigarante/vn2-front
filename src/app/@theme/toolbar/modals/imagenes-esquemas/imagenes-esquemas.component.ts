import { Component, OnInit } from '@angular/core';
import {ImagenesEsquemasService} from "../../../../@core/data/services/rrhh/imagenes-esquemas.service";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-imagenes-esquemas',
  templateUrl: './imagenes-esquemas.component.html',
  styleUrls: ['./imagenes-esquemas.component.scss']
})
export class ImagenesEsquemasComponent implements OnInit {
  imagenes: any;
  contador = 0;
  areas: any;
  activeTab = 1;

  constructor(public dialogRef: MatDialogRef<ImagenesEsquemasComponent>,
              private imagenesEsquemasService: ImagenesEsquemasService) {
  }

  ngOnInit() {
    this.getAreas();
  }

  getImagenes() {
    if (this.imagenes) {
      this.imagenes = null;
      this.contador = 0;
    }
    this.imagenesEsquemasService.getByArea(this.activeTab).subscribe(data => this.imagenes = JSON.parse(data.links));
  }

  getAreas() {
    this.imagenesEsquemasService.getAreas().subscribe(data => {
      this.areas = data;
      this.activeTab = this.areas[0].id;
      this.getImagenes();
    });
  }

  moverContador(incremento: boolean) {
    if (incremento) {
      this.contador++;
    } else {
      this.contador--;
    }
  }

  dismiss() {
    this.dialogRef.close();
  }
}

