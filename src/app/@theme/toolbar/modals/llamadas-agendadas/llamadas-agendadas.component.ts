import {Component, OnInit, ViewChild} from '@angular/core';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {AgendaLlamadasService} from '../../../../@core/data/services/telefonia/agendaLlamadas/agenda-llamadas.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {
  EstadoUsuarioTelefoniaService
} from '../../../../@core/data/services/telefonia/estado-usuario-telefonia.service';
import {SocketsService} from '../../../../@core/data/services/telefonia/sockets/sockets.service';
import {LlamadaSalidaComponent} from '../../../../modulos/telefonia/modals/llamada-salida/llamada-salida.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-llamadas-agendadas',
  templateUrl: './llamadas-agendadas.component.html',
  styleUrls: ['./llamadas-agendadas.component.scss']
})
export class LlamadasAgendadasComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource;
  displayedColumns: string[] = ['idSolicitud', 'nombre', 'numero', 'cliente', 'comentario', 'acciones'];
  errorSpin: boolean = false;
  idEmpleado = sessionStorage.getItem('Empleado');
  puedeLlamar: boolean = false;
  idUsuario = Number(sessionStorage.getItem('Usuario'));

  constructor(private agendaLlamadasService: AgendaLlamadasService,
              private notificacionesService: NotificacionesService,
              private telefoniaUsuariosService: EstadoUsuarioTelefoniaService,
              private socketsService: SocketsService,
              private snackBar: MatSnackBar,
              public dialog: MatDialog,
              ) { }

  ngOnInit(): void {
    this.getPendientes();
    this.getEstadoUsuario();
  }

  getPendientes() {
    this.agendaLlamadasService.getAgendaByEmpleado(this.idEmpleado).subscribe({
      next: data => {
        this.errorSpin = false;
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error: () => {
        this.errorSpin = true;
        this.notificacionesService.error('Error al cargar los datos');
      }
    });
  }

  llamar(id: any, numero, idSolicitud) {
    this.snackBar.openFromComponent(LlamadaSalidaComponent, {
      data: {
        idSolicitud,
        numero,
      }
    });
    const idEstadoLlamada = 9;
    this.agendaLlamadasService.putPendiente(id, idEstadoLlamada).subscribe(data => {
      this.onNoClick();
      this.socketsService.agenda();
    });
  }

  atendida(id: any) {
    const idEstadoLlamada = 9;
    this.agendaLlamadasService.putPendiente(id, idEstadoLlamada).subscribe(data => {
      this.socketsService.agenda();
      this.getPendientes();
      this.notificacionesService.exito('Realizado');
    });
  }

  getEstadoUsuario(){
    this.telefoniaUsuariosService.getEstadoUsuario(this.idUsuario).subscribe((data: number) => {
      this.puedeLlamar = data === 1 ? true : false;
    });
  }

  onNoClick(): void {
    this.dialog.closeAll();
  }
}
