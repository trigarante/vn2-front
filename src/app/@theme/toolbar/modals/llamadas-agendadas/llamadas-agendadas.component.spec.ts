import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LlamadasAgendadasComponent } from './llamadas-agendadas.component';

describe('LlamadasAgendadasComponent', () => {
  let component: LlamadasAgendadasComponent;
  let fixture: ComponentFixture<LlamadasAgendadasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LlamadasAgendadasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LlamadasAgendadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
