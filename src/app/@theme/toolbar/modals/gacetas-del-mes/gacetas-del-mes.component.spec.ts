import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GacetasDelMesComponent } from './gacetas-del-mes.component';

describe('GacetasDelMesComponent', () => {
  let component: GacetasDelMesComponent;
  let fixture: ComponentFixture<GacetasDelMesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GacetasDelMesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GacetasDelMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
