import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MotivosPausa} from "../../../../@core/data/interfaces/telefonia/llamadas-pausas/motivos-pausas";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MotivosPausasService} from "./../../../../@core/data/services/telefonia/llamadas-pausas/motivos-pausas.service"
import {MatSnackBar} from "@angular/material/snack-bar";
import {WebrtcService} from "../../../../@core/data/services/telefonia/webrtc.service";
import {EstadoUsuarioTelefoniaService} from "../../../../@core/data/services/telefonia/estado-usuario-telefonia.service";
import {SnackPausaComponent} from "../../../../modulos/telefonia/modals/snack-pausa/snack-pausa.component";

@Component({
  selector: 'app-motivo-pausa',
  templateUrl: './motivo-pausa.component.html',
  styleUrls: ['./motivo-pausa.component.scss']
})
export class MotivoPausaComponent implements OnInit, OnDestroy {
  public motivos: MotivosPausa[];
  motivoPausaForm: FormGroup;
  public aparecer = false;
  idPausaUsuario: number;
  // grupo: any = localStorage.getItem('Grupo');

  constructor(public dialogRef: MatDialogRef<MotivoPausaComponent>,
              private motivosPausaService: MotivosPausasService,
              private fb: FormBuilder,
              private snackBar: MatSnackBar,
              private telefoniaUSuarioService: EstadoUsuarioTelefoniaService,
              private webRtcService: WebrtcService,
              @Inject(MAT_DIALOG_DATA) public data) {
    this.webRtcService.sipCall('call-audio', '*46');
  }

  ngOnInit() {
    this.motivoPausaForm = this.fb.group({
      motivos: new FormControl(2, Validators.required),
    });
    this.getMotivos();
    this.habilitarbotonCancelar();
  }

  ngOnDestroy() { }

  habilitarbotonCancelar() {
    setTimeout(() => {this.aparecer = true}, 5000);
  }
  getMotivos() {
    this.motivosPausaService.getMotivosPausas().subscribe((resp: any) => {
      this.motivos = resp;
    });
  }
  enviar() {
    const idMotivo = this.motivoPausaForm.get('motivos').value.id;
    const tiempo = this.motivoPausaForm.get('motivos').value.tiempo;
    this.telefoniaUSuarioService.inicioPausa(sessionStorage.getItem('Usuario'), idMotivo).subscribe((resp: any) => {
      this.idPausaUsuario = resp.id;
      sessionStorage.setItem('pu', String(resp.id));
    }, () => {}, () => {
      this.dialogRef.close({pauso: true, tiempo, idPausaUsuario: this.idPausaUsuario});
      // this.alertasEstadoUsuario(tiempo);
    });
  }

  alertasEstadoUsuario(tiempo) {
    this.snackBar.openFromComponent(SnackPausaComponent, {
      data: {
        idPausaUsuario: this.idPausaUsuario,
        tiempo,
      }
    });
  }

  close() {
    this.telefoniaUSuarioService.activarUsuario(sessionStorage.getItem('Usuario')).subscribe();
    this.webRtcService.sipCall('call-audio', '*47');
    this.dialogRef.close({pauso: false});
  }

}
