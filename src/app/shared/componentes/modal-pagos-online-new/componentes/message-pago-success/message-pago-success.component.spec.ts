import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagePagoSuccessComponent } from './message-pago-success.component';

describe('MessagePagoSuccessComponent', () => {
  let component: MessagePagoSuccessComponent;
  let fixture: ComponentFixture<MessagePagoSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessagePagoSuccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagePagoSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
