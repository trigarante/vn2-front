import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalViewPdfPolizaFunnelComponent } from './modal-view-pdf-poliza-funnel.component';

describe('ModalViewPdfPolizaFunnelComponent', () => {
  let component: ModalViewPdfPolizaFunnelComponent;
  let fixture: ComponentFixture<ModalViewPdfPolizaFunnelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalViewPdfPolizaFunnelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalViewPdfPolizaFunnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
