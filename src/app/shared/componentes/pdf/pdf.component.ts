import { DatePipe } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {CotizarRequest} from "../../../@core/data/interfaces/cotizador/cotizar";

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.scss']
})
export class PdfComponent implements OnInit {
  @Input() cotizacion!: any;
  @Input() cotizacionRespuesta: any;
  @Input() selected: number;
  @Input() imagenLogo: string;
  @Input() nombreCliente: string;
  @Input() reqCotizacion: string;
  cotizacionRequest: CotizarRequest;
  coberturas: any = [];
  preciosSeg: any = [];
  obtenerData: any;
  dataBeneficiario: any=[];
  numAsegurados: number = 0;
  estado: string = '';
  aseguradora: string = '';
  municipio: string = '';
  fechaCreacion: string = '';
  numCotizacion: string = '';
  extraPrima: string = '';
  primaN: string = '';
  dataCotizacion!: any;
  cotizacionResponse: any;
  dataCot: any;
  imageSRC: string;
  today: Date = new Date();
  pipe = new DatePipe('en-US');
  todayWithPipe = null;
  formaDePago: string;
  primaNeta: number;
  derechos: number;
  impuestos: number;
  primaTotal: number;
  primerPago: number;
  pagoSubsecuente: number;
  coberturaNombre: string;
  ejecutivoMail = window.sessionStorage.getItem('email');
  displayedColumns: string[] = ['nombre', 'sumaAsegurada', 'deducible'];
  dataSource: Array<any>;
  coverageArray: Array<any>;

  @ViewChild('paginador', { read: MatPaginator }) paginador: MatPaginator;
  @ViewChild('sort', { read: MatSort }) sort: MatSort;
  constructor() { }

  ngOnInit(): void {
    // this.cotizacionRequest = JSON.parse(this.reqCotizacion);
    console.log(this.cotizacionRespuesta);
    console.log(typeof this.reqCotizacion, this.reqCotizacion);
    this.cotizacionResponse = this.cotizacionRespuesta;
    // this.numCotizacion = this.cotizacionRespuesta["cotizacion"][this.selected.value][this.tabs[this.selected.value].toLowerCase()][0]
    //   [this.methodsPayment[this.methodSelected].text.toLowerCase()];
    this.inicio(this.imagenLogo);
    this.todayWithPipe = this.pipe.transform(Date.now(), 'dd/MM/yyyy');
  }
  inicio(imagenLogo){
    console.log(this.cotizacionRespuesta);
    this.aseguradora = this.cotizacionResponse["aseguradora"];
    this.imageSRC = imagenLogo;
    this.numCotizacion = this.cotizacionResponse["cotizacion"][0]['amplia'][0]
      ["anual"]["idCotizacion"];
    this.coverageArray = [...this.cotizacionResponse["coberturas"][0]['amplia']] ;
    this.formaDePago = "anual";
    this.primaNeta = Math.round(this.cotizacionResponse["cotizacion"][0]['amplia'][0]
      ["anual"]["primaNeta"]);
    this.derechos =Math.round( this.cotizacionResponse["cotizacion"][0]['amplia'][0]
      ["anual"]["derechos"]);
    this.impuestos =Math.round(this.cotizacionResponse["cotizacion"][0]['amplia'][0]
      ["anual"]["impuesto"]);
    this.primaTotal  =Math.round(this.cotizacionResponse["cotizacion"][0]['amplia'][0]
      ["anual"]["primaTotal"]);
    this.coberturaNombre = 'amplia';
    console.log(this.coverageArray);

  }
  conseguirdatos(data, value, cobertura, imagenLogo, metodoDePago){
    console.log(data);

    this.numAsegurados = value;
    this.numCotizacion = data["cotizacion"][value][cobertura.toLowerCase()][0]
      [metodoDePago]["idCotizacion"];
    this.coverageArray = data["coberturas"][value][cobertura.toLowerCase()];
    this.imageSRC = imagenLogo;
    this.formaDePago = metodoDePago;
    this.primaNeta = Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [metodoDePago]["primaNeta"]);
    this.derechos = Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [metodoDePago]["derechos"]);
    this.impuestos = Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [metodoDePago]["impuesto"]);
    this.primaTotal  =Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [metodoDePago]["primaTotal"]);
    this.primerPago= Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [metodoDePago]["primerPago"]);
    this.pagoSubsecuente = Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [metodoDePago]["pagosSubsecuentes"]);
    this.coberturaNombre = cobertura.toLowerCase();
  }
  conseguirPago(data, valuePeriodo, cobertura, value, imagenLogo){
    // this.numCotizacion = this.cotizacionRespuesta["cotizacion"][this.selected.value][this.tabs[this.selected.value].toLowerCase()][0]
    //   [this.methodsPayment[this.methodSelected].text.toLowerCase()];
    this.numCotizacion = data["cotizacion"][value][cobertura.toLowerCase()][0]
      [valuePeriodo]["idCotizacion"];
    this.coverageArray = data["coberturas"][value][cobertura.toLowerCase()];
    this.imageSRC = imagenLogo;
    this.formaDePago = valuePeriodo;
    this.primaNeta = Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [valuePeriodo]["primaNeta"]);
    this.derechos = Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [valuePeriodo]["derechos"]);
    this.impuestos =Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [valuePeriodo]["impuesto"]);
    this.primaTotal  = Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [valuePeriodo]["primaTotal"]);
    this.primerPago  = Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [valuePeriodo]["primerPago"]);
    this.pagoSubsecuente = Math.round(data["cotizacion"][value][cobertura.toLowerCase()][0]
      [valuePeriodo]["pagosSubsecuentes"]);
    this.coberturaNombre = cobertura.toLowerCase();
  }


}

