import {Component, Input, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {FormControl, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-tabla-emision-cotizacion',
  templateUrl: './tabla-emision-cotizacion.component.html',
  styleUrls: ['./tabla-emision-cotizacion.component.scss']
})
export class TablaEmisionCotizacionComponent implements OnInit {
  displayedColumns: string[] = ['nombre', 'sumaAsegurada', 'deducible'];
  responsabilidadCivil = [

         { value: '3000000', text: '$3,000,000.00'},
         { value: '4000000', text: '$4,000,000.00'},
         { value: '5000000', text: '$5,000,000.00'}
  ];
  gastosMedicos = [
    { value: '200000', text: '$200,000.00'},
    { value: '300000', text: '$300,000.00'},
    { value: '500000', text: '$500,000.00'}
  ];
  danosMateriales = [
    { value: '3', text: '3%'},
    { value: '5', text: '5%'}
    // { value: '10%', text: '10%'},
    // { value: '20%', text: '20%'},
  ];
  roboTotal = [
    { value: '5', text: '5%'},
    { value: '10', text: '10%'}
    // { value: '20%', text: '20%'},
  ];
  dataSource: Array<any>;
  deducibleForm: FormGroup = new FormGroup({
    responsabilidadcontrol: new FormControl({value: '', disabled: true}),
    gastosMedicoscontrol: new FormControl({value: '', disabled: true}),
    danosMaterialescontrol: new FormControl({value: '', disabled: true}),
    roboTotalcontrol: new FormControl({value: '', disabled: true}),
  });
  opcionalForm: FormGroup =  new FormGroup({
    asistenciaCivilcontrol: new FormControl(false),
    roboParcialcontrol: new FormControl(false),
  });
  coberturas: any ;
  coberturasModificadas: any;
  valoropcionalRoboParcial = false;
  valoropcionAsistenciaLegal = false;


  @ViewChild('paginador', { read: MatPaginator }) paginador: MatPaginator;
  @ViewChild('sort', { read: MatSort }) sort: MatSort;

  @Input() dataVehicle: any[];
  @Input() nombreAseguradora: string;
  @Input() banderaCotizacionInspeccion: string;
  @Output() coberturasSalida = new EventEmitter<any>();

  dataVehicleLocal: any;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.dataVehicleLocal = [...this.dataVehicle];
    console.log('DATOS DEL VEHICULO', this.dataVehicleLocal);
    this.inicio();
    this.coberturasModificadas = [];
  }
  inicio() {
    this.deducibleForm.reset();

    // this.deducibleForm.controls.responsabilidadcontrol.setValue(this.dataVehicle[2]["sumaAsegurada"]);
    this.deducibleForm.controls.responsabilidadcontrol.setValue(this.responsabilidadCivil[0].value);
    // this.deducibleForm.controls.responsabilidadcontrol.reset();
    console.log(this.deducibleForm.controls.responsabilidadcontrol.value);
    this.deducibleForm.controls.gastosMedicoscontrol.setValue(this.gastosMedicos[0].value);
    this.deducibleForm.controls.danosMaterialescontrol.setValue(this.danosMateriales[1].value);
    this.deducibleForm.controls.roboTotalcontrol.setValue(this.roboTotal[1].value);
  }

  rowType(index: number): string{
    if((index % 2) === 0){
      return 'Color';
    } else {
      return 'White';
    }
  }
}
