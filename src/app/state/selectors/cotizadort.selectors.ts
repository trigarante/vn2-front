import { createSelector } from "@ngrx/store";
import { StateCotizador, AppState } from '../interfaces/cotizador.stateInterfaces';

export const selectStateCotizador = (state: AppState) => state.leads;

export const selectAllState = createSelector(
  selectStateCotizador,
  (state: StateCotizador) => state
)