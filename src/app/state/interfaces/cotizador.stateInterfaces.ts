export interface StateCotizador {
  idSolicitud: number;
  idEmpleado: number;
}

export interface AppState{
  leads: StateCotizador
}