import { StateCotizador } from "../interfaces/cotizador.stateInterfaces";
import { createReducer, on } from '@ngrx/store';
import { setLead, cleanLead } from "../actions/cotizador.actions";

export const initialState: StateCotizador = {
  idEmpleado: 0,
  idSolicitud: 0
}

export const CotizadorReducer = createReducer(
  initialState,
  on(setLead, (state, leads) => ({idEmpleado: leads.idEmpleado, idSolicitud: leads.idSolicitud})),
  on(cleanLead, (state) => ({idEmpleado: 0, idSolicitud: 0}))
)