import {createAction,  props} from "@ngrx/store";
import { StateCotizador } from '../interfaces/cotizador.stateInterfaces';

export const setLead = createAction(
  '[Cotizador Lead] Set Lead',
  props<StateCotizador>()
);

export const cleanLead = createAction(
  '[Cotizador Lead] Clean Lead'
)