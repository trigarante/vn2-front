import { Injectable } from '@angular/core';
import { Drive } from '../../interfaces/ti/drive';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})

export class DriveService  {
  private baseURL;
  private baseURLOP;
  private CORE_URL;
  private PRUEBAURLBACK;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_DOCUMENTOS;
    this.baseURLOP = environment.GLOBAL_SERVICIOS_DOCUMENTOS_OPERACION;
    this.CORE_URL = environment.CORE_DOCUMENTOS + '/subida-archivos';
    this.PRUEBAURLBACK = environment.CORE_URL;
  }

  // NEW

  postFile(files: FileList, id, numero): Observable<string> {
    const formData = new FormData();
    formData.append('file', !files.item ? files[0] : files.item(0), 'archivo');
    formData.append('id', id.toString());
    formData.append('numero', numero.toString());
    return this.http.post<string>(this.CORE_URL + '/archivos-faltantes', formData);
  }

  putFile(files: FileList, id, numero): Observable<string> {
    const formData = new FormData();
    formData.append('file', !files.item ? files[0] : files.item(0), 'archivo');
    formData.append('id', id.toString());
    formData.append('numero', numero.toString());
    return this.http.put<string>(this.CORE_URL + '/corregir-archivos', formData);
  }

  correccionDocumentos(files: FileList, id, numero, idAutorizacionRegistro, idError, esVerificacion?): Observable<string> {
    const formData = new FormData();
    formData.append('file', !files.item ? files[0] : files.item(0), 'archivo');
    formData.append('id', id.toString());
    formData.append('numero', numero.toString());
    formData.append('idAutorizacionRegistro', idAutorizacionRegistro.toString());
    formData.append('idError', idError.toString());
    formData.append('esVerificacion', esVerificacion.toString());
    return this.http.post<string>(this.CORE_URL + '/correccion-documentos', formData);
  }

  // Get para obtener archivos del proceso de venta nueva
  getArchivo(parametroComplementario, tipo): Observable<any[]> {
    const headers = new HttpHeaders().append('archivoid', parametroComplementario).append('tipo', tipo.toString());
    return this.http.get<any[]>(this.CORE_URL + '/archivo-bajar', {headers});
  }

  // FIN


  // Get es para el módulo de llamadas
  get(fechaInicio: Date, fechaFin: Date, ACD: boolean, manual: boolean, numero: string): Observable<Drive[]> {
    let fechaFinal: number;

    if (fechaFin === null || fechaFin === undefined) {
      fechaFinal = 0;
    } else { fechaFinal = fechaFin.getTime(); }

    return this.http.get<Drive[]>(this.baseURL + 'v1/api-drive/' + fechaInicio.getTime() + '/'
      + fechaFinal + '/' + ACD + '/' + manual + '/' + numero);
  }



  post(files: FileList, tipoArchivo: string, idCliente: number, idRegistro: number): Observable<any> {
    const formData = new FormData();
    formData.append('file', files.item(0), tipoArchivo);

    return this.http.post(this.baseURL + 'v1/api-drive/upload-files/' + idCliente + '/' + idRegistro, formData,
      { responseType: 'text' as 'json' });
  }



  postOP(files: FileList, tipoArchivo: string, idCliente: number, idRegistro: number): Observable<any> {
    const formData = new FormData();
    if (!files.item) {
      formData.append('file', files[0], tipoArchivo);
    } else {
      formData.append('file', files.item(0), tipoArchivo);
    }
    return this.http.post(this.baseURLOP + 'v1/api-drive/upload-files/' + idCliente + '/' + idRegistro, formData,
      { responseType: 'text' as 'json' });
  }
}
