import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Usuario} from "../../interfaces/ti/usuario";
import {TipoSubarea} from "../../interfaces/marketing/catalogos/tipoSubarea";

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_URL;
  }

  get(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.baseURL + 'v1/usuarios');
  }

  getbyid(id): Observable<Usuario> {
    return this.http.get<Usuario>(this.baseURL + '/usuarios/getById/' + id);
  }

  put(idUsuario, usuario: Usuario): Observable<Usuario> {
    return this.http.put<Usuario>(this.baseURL + 'v1/usuarios/idEmpleado/' + idUsuario + '/'
      + sessionStorage.getItem('Empleado'), usuario);
  }

  putUsuario(idUsuario, usuario: Usuario): Observable<Usuario> {
    return this.http.put<Usuario>(this.baseURL + 'v1/usuarios/' + idUsuario + '/'
      + sessionStorage.getItem('Empleado'), usuario);
  }

  getUsuarioById(idUsuario): Observable<TipoSubarea[]> {
    return this.http.get<TipoSubarea[]>(this.baseURL + '/usuarios/' + sessionStorage.getItem('Empleado'));
  }

  // PutBajaGrupo(idGrupo): Observable<string> {
  //   return this.http.put<string>(this.baseURL + 'v1/usuarios/baja-grupo/' + idGrupo + '/'
  //     + sessionStorage.getItem('Empleado'), null);
  // }
  PutBajaGrupo(idGrupo): Observable<string> {
    return this.http.put<string>(this.baseURL + 'v1/usuarios/baja-grupo/' + idGrupo + '/'
      + sessionStorage.getItem('Empleado'), null, {headers: null , responseType: 'text' as 'json'});
  }
}
