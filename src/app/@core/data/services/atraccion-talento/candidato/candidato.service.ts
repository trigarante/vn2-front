import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class CandidatoService {
  private baseURL = environment.CORE_URL + '/atraccion-talento/candidato';

  constructor(
    private http: HttpClient
  ) {
  }
  put(data): Observable<any> {
    return this.http.put(this.baseURL, data);
  }
}

