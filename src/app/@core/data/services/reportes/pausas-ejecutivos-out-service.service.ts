import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";
import {PausasEjecutivosOut} from "../../interfaces/reportes/pausas-ejecutivos-out";

@Injectable({
  providedIn: 'root'
})
export class PausasEjecutivosOutService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES
  }

  getPausas(): Observable<PausasEjecutivosOut[]> {
    return this.http.get<PausasEjecutivosOut[]> (this.baseURL + 'v1/monitoreo-out/getPausas/' + sessionStorage.getItem('Empleado'));
  }

  getPausasReno(): Observable<PausasEjecutivosOut[]> {
    return this.http.get<PausasEjecutivosOut[]> (this.baseURL + 'v1/monitoreo-out/getPausasReno/' + sessionStorage.getItem('Empleado'));
  }
}
