import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {ProductividadUrl} from "../../interfaces/reportes/productividad-url";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProductividadUrlService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  getProductividadUrl( idSubarea: number, idTipoContacto: number,
                       fechaInicio: number, fechaFin: number): Observable<ProductividadUrl[]> {
    return this.http.get<ProductividadUrl[]>(this.baseURL + 'v1/productividad-url/' +
      idSubarea + '/' + idTipoContacto + '/' + fechaInicio + '/' + fechaFin);
  }
}
