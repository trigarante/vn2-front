import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ProductividadDistribuciones} from "../../interfaces/reportes/productividad-distribuciones";

@Injectable({
  providedIn: 'root'
})
export class ProductividadDistribucionesService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  getProductividadDistribuciones( idSubarea: number, idTipoContacto: number,
                                  fechaInicio: number, fechaFin: number): Observable<ProductividadDistribuciones[]> {
    return this.http.get<ProductividadDistribuciones[]>(this.baseURL + 'v1/distribucion/' +
      idSubarea + '/' + idTipoContacto + '/' + fechaInicio + '/' + fechaFin);
  }
}
