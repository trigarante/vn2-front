import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";
import {TiempoSinLlamadaOut} from "../../interfaces/reportes/tiempo-sin-llamada-out";

@Injectable({
  providedIn: 'root'
})
export class TiempoSinLlamadaOutService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES
  }

  getPausas(): Observable<TiempoSinLlamadaOut[]> {
    return this.http.get<TiempoSinLlamadaOut[]> (this.baseURL + 'v1/monitoreo-out/sinLlamada/' + sessionStorage.getItem('Empleado'));
  }
  getPausasReno(): Observable<TiempoSinLlamadaOut[]> {
    return this.http.get<TiempoSinLlamadaOut[]> (this.baseURL + 'v1/monitoreo-out/sinLlamadaReno/' + sessionStorage.getItem('Empleado'));
  }
}
