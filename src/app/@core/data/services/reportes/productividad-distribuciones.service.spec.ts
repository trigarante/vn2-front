import { TestBed } from '@angular/core/testing';

import { ProductividadDistribucionesService } from './productividad-distribuciones.service';

describe('ProductividadDistribucionesService', () => {
  let service: ProductividadDistribucionesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductividadDistribucionesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
