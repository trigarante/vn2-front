import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {DescargaLlamadas} from "../../interfaces/reportes/descarga-llamadas";
import {DashBoardNew} from "../../interfaces/reportes/dash-board-new";
import {HttpClient} from "@angular/common/http";
import {DescargaLeadsLlamados} from "../../interfaces/reportes/descarga-leads-llamados";
import {Observable} from "rxjs";
import {DescargaDash} from "../../interfaces/reportes/descarga-dash";

@Injectable({
  providedIn: 'root'
})
export class DashBoardNewService {

  private url;

  constructor(private http: HttpClient) {
    this.url = environment.CORE_DASH + '/dashboard-new/';
  }

  // NEW
  getDepartamentos() {
    return this.http.get<any[]>(this.url + 'departamentos');
  }
  getDashBoardPjsip(fechaInicio: number, fechaFin: number, idSubarea: number): Observable<DashBoardNew[]> {
    return this.http.get<DashBoardNew[]>(this.url + 'get-dashboard-pjsip/' + sessionStorage.getItem('Empleado') + '/'
      + fechaInicio + '/' + fechaFin + '/' + idSubarea);
  }

  getDescargaLeads(fechaInicio: number, fechaFin: number): Observable<DescargaLeadsLlamados[]> {
    return this.http.get<DescargaLeadsLlamados[]>(this.url + 'get-descarga-leads/' + sessionStorage.getItem('Empleado') + '/'
      + fechaInicio + '/' + fechaFin);
  }
  getDescargaLlamadas(fechaInicio: number, fechaFin: number): Observable<DescargaLlamadas[]> {
    return this.http.get<DescargaLlamadas[]>(this.url + 'get-descarga-llamadas/' + sessionStorage.getItem('Empleado') + '/'
      + fechaInicio + '/' + fechaFin);
  }
  getDescargaDash(fechaInicio: number, fechaFin: number, idEmpleado: number, tipo: number): Observable<DescargaDash[]> {
    return this.http.get<DescargaDash[]>(this.url + 'get-descarga-dash/' + idEmpleado + '/'
      + fechaInicio + '/' + fechaFin + '/' + tipo);
  }
  getDashBoardFiltroPjsip(fechaInicio: number, fechaFin: number): Observable<DashBoardNew[]> {
    return this.http.get<DashBoardNew[]>(this.url + 'get-dashboard-filter-pjsip/' + sessionStorage.getItem('Empleado') + '/'
      + fechaInicio + '/' + fechaFin);
  }
}
