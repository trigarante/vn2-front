import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {ProductividadEjecutivo} from "../../interfaces/reportes/productividad-ejecutivo";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProductividadEjecutivoService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_URL;
  }
  getProductividadEjecutivo( idSubarea: number, idTipoContacto: number,
                             fechaInicio: any, fechaFin: any): Observable<ProductividadEjecutivo[]> {
    return this.http.get<ProductividadEjecutivo[]>(this.baseURL + '/productividad/' +
      idSubarea + '/' + idTipoContacto + '/' + fechaInicio + '/' + fechaFin);
  }
  getProductividadEjecutivoOne( idEmpleado: any): Observable<ProductividadEjecutivo> {
    return this.http.get<ProductividadEjecutivo>(this.baseURL + 'v1/productividad/ejecutivo/' + idEmpleado);
  }
  getProductividadEjecutivoIn( idSubarea: number, fechaInicio: number, fechaFin: number): Observable<ProductividadEjecutivo[]> {
    return this.http.get<ProductividadEjecutivo[]>(this.baseURL + 'v1/productividad/in/' +
      idSubarea + '/' + fechaInicio + '/' + fechaFin);
  }
}
