import { TestBed } from '@angular/core/testing';

import { ProductividadEjecutivoService } from './productividad-ejecutivo.service';

describe('ProductividadEjecutivoService', () => {
  let service: ProductividadEjecutivoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductividadEjecutivoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
