import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";
import {LlamadasSinTipificarOut} from "../../interfaces/reportes/llamadas-sin-tipificar-out";

@Injectable({
  providedIn: 'root'
})
export class LlamadasSinTipificarOutService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES
  }

  getLlamadasSNT(): Observable<LlamadasSinTipificarOut[]> {
    return this.http.get<LlamadasSinTipificarOut[]> (this.baseURL + 'v1/monitoreo-out/sinTipificar/' + sessionStorage.getItem('Empleado'));
  }
  getLlamadasSNTReno(): Observable<LlamadasSinTipificarOut[]> {
    return this.http.get<LlamadasSinTipificarOut[]> (
      this.baseURL + 'v1/monitoreo-out/sinTipificarReno/' + sessionStorage.getItem('Empleado'));
  }
}
