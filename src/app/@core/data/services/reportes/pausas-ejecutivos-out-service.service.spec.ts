import { TestBed } from '@angular/core/testing';

import { PausasEjecutivosOutServiceService } from './pausas-ejecutivos-out-service.service';

describe('PausasEjecutivosOutServiceService', () => {
  let service: PausasEjecutivosOutServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PausasEjecutivosOutServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
