import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {EjecutivosEnLlamadaOut} from "../../interfaces/reportes/ejecutivos-en-llamada-out";

@Injectable({
  providedIn: 'root'
})
export class EjecutivosEnLlamadaOutService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES
  }

  getEjecutivos(): Observable<EjecutivosEnLlamadaOut[]> {
    return this.http.get<EjecutivosEnLlamadaOut[]> (this.baseURL + 'v1/monitoreo-out/getEnLlamada/' + sessionStorage.getItem('Empleado'));
  }

  getEjecutivosReno(): Observable<EjecutivosEnLlamadaOut[]> {
    return this.http.get<EjecutivosEnLlamadaOut[]> (
      this.baseURL + 'v1/monitoreo-out/getEnLlamadaReno/' + sessionStorage.getItem('Empleado'));
  }
}
