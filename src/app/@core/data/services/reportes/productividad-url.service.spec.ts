import { TestBed } from '@angular/core/testing';

import { ProductividadUrlService } from './productividad-url.service';

describe('ProductividadUrlService', () => {
  let service: ProductividadUrlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductividadUrlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
