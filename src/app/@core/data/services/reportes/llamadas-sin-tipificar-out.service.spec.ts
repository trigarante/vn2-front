import { TestBed } from '@angular/core/testing';

import { LlamadasSinTipificarOutService } from './llamadas-sin-tipificar-out.service';

describe('LlamadasSinTipificarOutService', () => {
  let service: LlamadasSinTipificarOutService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LlamadasSinTipificarOutService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
