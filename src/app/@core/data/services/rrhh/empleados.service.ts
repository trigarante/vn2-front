import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Empleados} from '../../interfaces/rrhh/empleados';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  private baseURL;
  private CORE_URL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.CORE_URL = environment.CORE_URL;
  }

  // NEW
  getEmpleadoById(idEmpleados): Observable<Empleados> {
    return this.http.get<Empleados>(this.CORE_URL + '/empleados/' + idEmpleados);
  }

  // FIN
  getPlazasHijo() {
    const headers = new HttpHeaders().set('id', sessionStorage.getItem('Empleado'));
    return this.http.get<any[]>(this.CORE_URL + `/administracion-personal/empleados/getPlazasHijo`, {headers});
  }
  get(): Observable<Empleados[]> {
    return this.http.get<Empleados[]>(this.baseURL + 'v1/empleados');
  }

  put(id, empleado, fechaIngreso): Observable<Empleados> {
    return this.http.put<Empleados>( this.baseURL + 'v1/empleados/' + id + '/' + fechaIngreso
      + '/' + sessionStorage.getItem('Empleado'), empleado);
  }
  getEjecutivosActivos(): Observable<Empleados[]> {
    const headers = new HttpHeaders().append('id', sessionStorage.getItem('Empleado'));
    return this.http.get<Empleados[]>(this.CORE_URL + '/empleados/ejecutivos/activos', {headers});
  }
}
