import { TestBed } from '@angular/core/testing';

import { ImagenesEsquemasService } from './imagenes-esquemas.service';

describe('ImagenesEsquemasService', () => {
  let service: ImagenesEsquemasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImagenesEsquemasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
