import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {VisualizacionesGaceta} from '../../interfaces/rrhh/visualizacionesGaceta';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class VisualizacionesGacetaService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_ESCUCHA + '/desarrollo-organizacional/visualizaciones-gaceta';
  }

  post(data: VisualizacionesGaceta): Observable<any> {
    return this.http.post<any>(this.baseURL, data);
  }

  getByIdEmpleado(): Observable<VisualizacionesGaceta[]> {
    return this.http.get<VisualizacionesGaceta[]>(`${this.baseURL}/visualizacionByIdEmpleado/` + sessionStorage.getItem('Empleado'));
  }
}
