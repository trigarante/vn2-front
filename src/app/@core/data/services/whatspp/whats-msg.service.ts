import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
const url = environment.nodeWhats;

@Injectable({
  providedIn: 'root'
})
export class WhatsMsgService {

  constructor(private http: HttpClient) { }

  getMensajesByIdCanal(idSolicitud, numeroProspecto) {
    return this.http.get(`${url}/mensajes/canal/${idSolicitud}/${numeroProspecto}`);
  }
}
