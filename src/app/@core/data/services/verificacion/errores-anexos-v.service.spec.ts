import { TestBed } from '@angular/core/testing';

import { ErroresAnexosVService } from './errores-anexos-v.service';

describe('ErroresAnexosVService', () => {
  let service: ErroresAnexosVService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErroresAnexosVService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
