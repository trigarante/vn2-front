import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErroresAnexosV} from '../../interfaces/verificacion/errores-anexos-v';

@Injectable({
  providedIn: 'root',
})
export class ErroresAnexosVService {
  private baseURL;
  private CORE_URL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS + 'v1/errores-anexos-verificacion/';
    this.CORE_URL = environment.CORE_URL + '/errores-anexos-verificacion/';

  }
  // NEW
  getByIdVerificacionRegistro(idVerificacionRegistro: number, idTipoDocumento: number): Observable<ErroresAnexosV> {
    return this.http.get<ErroresAnexosV>(this.CORE_URL + idVerificacionRegistro + '/'
      + idTipoDocumento);
  }
  putEstadoCorreccion(idErrorAnexo: number): Observable<any> {
    return this.http.put(this.baseURL + 'update-estado/' + idErrorAnexo, null);
  }
  // FIN

  get(): Observable<ErroresAnexosV[]> {
    return this.http.get<ErroresAnexosV[]>(this.baseURL);
  }

  post(error: ErroresAnexosV): Observable<any> {
    return this.http.post<string>(this.baseURL, error);
  }
}

