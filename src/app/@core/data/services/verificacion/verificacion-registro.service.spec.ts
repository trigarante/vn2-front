import { TestBed } from '@angular/core/testing';

import { VerificacionRegistroService } from './verificacion-registro.service';

describe('VerificacionRegistroService', () => {
  let service: VerificacionRegistroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VerificacionRegistroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
