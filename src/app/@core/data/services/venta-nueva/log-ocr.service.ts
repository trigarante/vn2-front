import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {Observable} from "rxjs";
import {LogOcr} from "../../interfaces/venta-nueva/log-ocr";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LogOcrService {
  private serviciosNode;

  constructor(private http: HttpClient) {
    this.serviciosNode = environment.CORE_URL + '/log-ocr';
  }

  getById(idSolicitud: number): Observable<LogOcr> {
    return this.http.get<LogOcr>(this.serviciosNode + '/bySolicitud/' + idSolicitud);
  }

  post(log: LogOcr): Observable<LogOcr> {
    return this.http.post<LogOcr>(this.serviciosNode, log);
  }
}
