import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Pagos, PagosComplemento} from '../../interfaces/venta-nueva/pagos';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PagosService{
  private baseURL;
  private baseURLADM;
  private baseURL2;
  private CORE;
  private DOCUMENTOS;


  constructor(private http: HttpClient ) {

    this.baseURLADM = environment.GLOBAL_SERVICIOS;
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.CORE = environment.CORE_URL + '/pago';
    this.DOCUMENTOS = environment.CORE_DOCUMENTOS + '/pago';
  }

  // NEW
  subirDrive(files: FileList, idSolicitud, pago): Observable<any> {
    const formData = new FormData();
    if (files) {
      formData.append('file', files.item ? files.item(0) : files[0], 'poliza');
    }
    formData.append('pago', JSON.stringify(pago));
    formData.append('idp', idSolicitud);
    return this.http.post(this.DOCUMENTOS + '/subir-archivo-otro', formData);
  }

  subirDriveRegistro(files: FileList, idRegistro, pago): Observable<any> {
    const formData = new FormData();
    if (files) {
      formData.append('file', files.item ? files.item(0) : files[0], 'poliza');
    }
    formData.append('pago', JSON.stringify(pago));
    formData.append('idp', idRegistro);
    return this.http.post(this.DOCUMENTOS + '/subir-archivo-otro-reno', formData);
  }

  getPagoById(id): Observable<Pagos> {
    return this.http.get<Pagos>(this.CORE + '/byId/' + id);
  }

  put( id, pago: Pagos ): Observable<Pagos> {
    return this.http.put<Pagos>(this.CORE + '/' + id, pago);
  }

  // FIN

  get(): Observable<Pagos[]> {
    return this.http.get<Pagos[]>(this.baseURL2 + 'v1/pagos');
  }

  post( pago ): Observable<Pagos> {
    return this.http.post<Pagos>(this.baseURL + 'v1/pagos', pago);
  }

  postOne( pago ): Observable<Pagos> {
    return this.http.post<Pagos>(this.CORE, pago);
  }
}
