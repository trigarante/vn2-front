import { TestBed } from '@angular/core/testing';

import { CotizacionesAliService } from './cotizaciones-ali.service';

describe('CotizacionesAliService', () => {
  let service: CotizacionesAliService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CotizacionesAliService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
