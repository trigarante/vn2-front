import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {timeout} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ScrappingService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_URL;
  }
  // getPaginaWhatsapp(email, password, url): Observable<JSON> {
  //   return this.http.get<JSON>( this.baseURL + '/scrapping/portal/' + email + '/' + password + '/'
  //     + url);
  // }
  getPaginaWhatsapp(json): Observable<JSON> {
    return this.http.post<JSON>( this.baseURL + '/scrapping/portal', json).pipe(timeout(30000));
  }

}
