import { Injectable } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {
  BancoExterno,
  BancoExternoMapfre,
  BancoVn,
  MsiView,
  MsiViewMapfre
} from '../../../interfaces/venta-nueva/catalogos/banco-vn';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BancoVnService  {

  private baseURL;
  private CORE_URL;


  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.CORE_URL = environment.CORE_URL;
  }

  // NEW
  get(): Observable<BancoVn[]> {
    return this.http.get<BancoVn[]>(this.CORE_URL + '/bancos');
  }
  getexterno(): Observable<BancoExterno[]> {
    return this.http.get<BancoExterno[]>( 'https://ws-qualitas.com/catalogos/bancos');
  }
  getexternoMapfre(): Observable<BancoExternoMapfre[]> {
    return this.http.get<BancoExternoMapfre[]>( 'https://dev.ws-mapfre.com/v1/mapfre-car/bank');
  }

  getexternoOnly(): Observable<BancoExterno[]> {
    return this.http.get<BancoExterno[]>( 'https://ws-qualitas.com/catalogos/bancos');
  }
  getexternoOnlyMapfre(): Observable<MsiViewMapfre[]> {
    return this.http.get<MsiViewMapfre[]>( 'https://dev.ws-mapfre.com/v1/mapfre-car/msi');
  }

  getMsi(idAseguradora, nombreBanco): Observable<MsiView[]> {
    return this.http.get<MsiView[]>( this.baseURL + 'v1/bancos/vistaMsi' + idAseguradora + '/' + nombreBanco );
  }
  // FIN



  // getActivos(activos: number): Observable<BancoVn[]> {
  //   return this.http.get<BancoVn[]>(this.baseURL2 + 'v1/bancos/activos/' + activos);
  // }
  //
  // getBancoById(id): Observable<BancoVn> {
  //   return this.http.get<BancoVn>(this.baseURL2 + 'v1/bancos/' + id);
  // }

  post(banco): Observable<BancoVn> {
    return this.http.post<BancoVn>(this.baseURL + 'v1/bancos', banco);
  }

  put(id, banco): Observable<BancoVn> {
    return this.http.put<BancoVn>(this.baseURL + 'v1/bancos' + id, banco);
  }
}
