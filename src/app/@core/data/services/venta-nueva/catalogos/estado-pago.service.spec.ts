import { TestBed } from '@angular/core/testing';

import { EstadoPagoService } from './estado-pago.service';

describe('EstadoPagoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstadoPagoService = TestBed.get(EstadoPagoService);
    expect(service).toBeTruthy();
  });
});
