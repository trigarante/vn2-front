import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
// import {Global} from '../../global';
import {Observable} from 'rxjs';
import {EstadoRecibos, EstadoRecibosData} from "../../../interfaces/venta-nueva/catalogos/estado-recibos";


@Injectable()
export class EstadoRecibosService extends EstadoRecibosData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    // this.baseURL = Global.url;
  }
  get(): Observable<EstadoRecibos[]> {
    return this.http.get<EstadoRecibos[]>(this.baseURL + 'estado-recibo');
  }

  post(estadoRecibos: EstadoRecibos): Observable<EstadoRecibos> {
    return this.http.post<EstadoRecibos>(this.baseURL + 'estado-recibo', estadoRecibos);
  }

  getEstadoReciboById(idEstadoRecibos): Observable<EstadoRecibos> {
    return this.http.get<EstadoRecibos>(this.baseURL + 'estado-recibo/' + idEstadoRecibos);
  }

  put(idEstadoRecibos, estadoRecibos: EstadoRecibos): Observable<EstadoRecibos> {
    return this.http.put<EstadoRecibos>(this.baseURL + 'estado-recibo/' + idEstadoRecibos, estadoRecibos);
    // + sessionStorage.getItem('Empleado')
  }
}
