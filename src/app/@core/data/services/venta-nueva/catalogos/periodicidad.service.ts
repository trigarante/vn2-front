import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Periodicidad} from "../../../interfaces/venta-nueva/catalogos/periodicidad";
import {environment} from "../../../../../../environments/environment";

@Injectable({
  providedIn: 'root',
})
export class PeriodicidadService {
  private baseURL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.CORE_URL + '/periodicidad';
  }

  // NEW
  get(): Observable<Periodicidad[]> {
    return this.http.get<Periodicidad[]>(this.baseURL);
  }

  // FIN

  getById(idPeriodicidad: number): Observable<Periodicidad> {
    return this.http.get<Periodicidad>(this.baseURL + '/periodicidad/' + idPeriodicidad);
  }
}
