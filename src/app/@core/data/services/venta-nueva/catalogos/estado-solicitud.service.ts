import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {EstadoSolicitud} from "../../../interfaces/venta-nueva/catalogos/estado-solicitud";

@Injectable({
  providedIn: 'root',
})
export class EstadoSolicitudService {

  private baseURL = environment.CORE_URL + '/etiqueta-solicitud';


  constructor(private http: HttpClient) {
  }

  get(): Observable<EstadoSolicitud[]> {
    return this.http.get<EstadoSolicitud[]>(this.baseURL);
  }

  getNode(idSubarea: string): Observable<any> {
    return this.http.get<EstadoSolicitud[]>(`${'RUTA'}/tipificaciones/estado-solicitud`);
  }

  post(estadoSolicitud: EstadoSolicitud): Observable<EstadoSolicitud> {
    return this.http.post<EstadoSolicitud>(this.baseURL + 'v1/etiqueta-solicitud', estadoSolicitud);
  }

  put(idEstadoSolicitud, estadoSolicitud: EstadoSolicitud) {
    return this.http.put<EstadoSolicitud>(this.baseURL + 'v1/etiqueta-solicitud/' + idEstadoSolicitud, estadoSolicitud);
  }
}
