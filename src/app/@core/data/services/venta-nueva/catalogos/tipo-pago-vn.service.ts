import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../../environments/environment';
import {TipoPagoVn} from "../../../interfaces/venta-nueva/catalogos/tipo-pago-vn";

@Injectable({
  providedIn: 'root'
})
export class TipoPagoVnService {

  private baseURL = environment.CORE_URL + '/tipo-pago';
  constructor(private http: HttpClient) {
  }
  // NEW
  get(): Observable<TipoPagoVn[]> {
    return this.http.get<TipoPagoVn[]>(this.baseURL);
  }
  // FIN

  post(tipoPago: TipoPagoVn): Observable<TipoPagoVn> {
    return this.http.post<TipoPagoVn>(this.baseURL + '/tipo-pago', tipoPago);
  }
  put(idTipoPago, tipoPago: TipoPagoVn): Observable<TipoPagoVn> {
    return this.http.put<TipoPagoVn>(this.baseURL + 'v1/tipo-pago/' + idTipoPago + '/'
      + sessionStorage.getItem('Empleado'), tipoPago);
  }
}
