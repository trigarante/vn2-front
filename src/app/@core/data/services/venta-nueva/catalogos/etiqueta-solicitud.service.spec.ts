import { TestBed } from '@angular/core/testing';

import { EtiquetaSolicitudService } from './etiqueta-solicitud.service';

describe('EtiquetaSolicitudService', () => {
  let service: EtiquetaSolicitudService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EtiquetaSolicitudService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
