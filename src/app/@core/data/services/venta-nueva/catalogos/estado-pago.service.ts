import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {EstadoPago} from "../../../interfaces/venta-nueva/catalogos/estado-pago";


@Injectable()
export class EstadoPagoService {
  private baseURL2;


  constructor(private http: HttpClient) {
    this.baseURL2 = environment.CORE_URL;
  }

  // NEW
  get(): Observable<EstadoPago[]> {
    return this.http.get<EstadoPago[]>(this.baseURL2 + 'v1/estado-pago');
  }
  getEstadoPagoById(idEstadoPago): Observable<EstadoPago> {
    return this.http.get<EstadoPago>(this.baseURL2 + 'v1/estado-pago/' + idEstadoPago);
  }

  // FIN
}
