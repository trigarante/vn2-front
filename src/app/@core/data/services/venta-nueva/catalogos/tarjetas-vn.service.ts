import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {TarjetasVn} from '../../../interfaces/venta-nueva/catalogos/tarjetas-vn';

@Injectable({
  providedIn: 'root'
})
export class TarjetasVnService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
  }

  get(): Observable<TarjetasVn[]> {
    return this.http.get<TarjetasVn[]>(this.baseURL + 'v1/tarjetas');
  }

  getTarjetaById(id): Observable<TarjetasVn> {
    return this.http.get<TarjetasVn>(this.baseURL + 'v1/tarjetas' + id);
  }

  getByIdBancoYCarrier (idBanco: number, idCarrier: number): Observable<TarjetasVn[]> {
    return this.http.get<TarjetasVn[]>(this.baseURL + 'v1/tarjetas/' + idBanco + '/' + idCarrier);
  }

  post(tarjeta): Observable<TarjetasVn> {
    return this.http.post<TarjetasVn>(this.baseURL + 'v1/tarjetas', tarjeta);
  }

  put(id, tarjeta): Observable<TarjetasVn> {
    return this.http.put<TarjetasVn>(this.baseURL + 'v1/tarjetas' + id, tarjeta);
  }
}
