import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {FlujoSolicitud, FlujoSolicitudData} from "../../../interfaces/venta-nueva/catalogos/flujo-solicitud";

@Injectable({
  providedIn: 'root',
})
export class FlujoSolicitudService extends FlujoSolicitudData {
  private baseURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.CORE_URL;
  }
  get(): Observable<FlujoSolicitud[]> {
    return this.http.get<FlujoSolicitud[]>(this.baseURL + 'v1/flujo-solicitud');
  }
  post(flujo: FlujoSolicitud): Observable<FlujoSolicitud> {
    return this.http.post<FlujoSolicitud>(this.baseURL + 'v1/flujo-solicitud', flujo);
  }
  getflujoById(idEtiqueta): Observable<FlujoSolicitud> {
    return this.http.get<FlujoSolicitud>(this.baseURL + 'v1/flujo-solicitud/' + idEtiqueta);
  }
  put(idEtiqueta, etiquetaSolicitud: FlujoSolicitud) {
    return this.http.put<FlujoSolicitud>(this.baseURL + 'v1/flujo-solicitud/' + idEtiqueta + '/'
      + sessionStorage.getItem('Empleado') , etiquetaSolicitud);
  }
}
