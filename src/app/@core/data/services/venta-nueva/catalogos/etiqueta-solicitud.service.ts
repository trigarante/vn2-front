import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {EtiquetaSolicitud} from '../../../interfaces/venta-nueva/catalogos/etiqueta-solicitud';

@Injectable({
  providedIn: 'root',
})
export class EtiquetaSolicitudService  {
  private baseURL;
  private nodeURL;
  private baseURL2;


  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<EtiquetaSolicitud[]> {
    return this.http.get<EtiquetaSolicitud[]>(this.baseURL2 + 'v1/etiqueta-solicitud');
  }

  getActivo(): Observable<EtiquetaSolicitud[]> {
    return this.http.get<EtiquetaSolicitud[]>(this.baseURL2 + 'v1/etiqueta-solicitud/activos');
  }

  post(etiqueta: EtiquetaSolicitud): Observable<EtiquetaSolicitud> {
    return this.http.post<EtiquetaSolicitud>(this.baseURL + 'v1/etiqueta-solicitud', etiqueta);
  }

  getEtiquetaById(idEtiqueta): Observable<EtiquetaSolicitud> {
    return this.http.get<EtiquetaSolicitud>(this.baseURL2 + 'v1/etiqueta-solicitud/' + idEtiqueta);
  }

  put(idEtiqueta, etiquetaSolicitud: EtiquetaSolicitud) {
    return this.http.put<EtiquetaSolicitud>(this.baseURL + 'v1/etiqueta-solicitud/' + idEtiqueta + '/'
      + sessionStorage.getItem('Empleado') , etiquetaSolicitud);
  }
}
