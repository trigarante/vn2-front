import { Injectable } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Carrier} from '../../../interfaces/venta-nueva/catalogos/carrier';

@Injectable({
  providedIn: 'root'
})
export class CarrierService {

  private baseURL;
  private baseURL2;
  private CORE_URL;
  private CORE_URLM;


  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.CORE_URL = environment.CORE_URL;
    this.CORE_URLM = environment.CATALOGO_AUTOSMAPFRE;
  }

  // NEW
  get(): Observable<Carrier[]> {
    return this.http.get<Carrier[]>(this.CORE_URL + '/bancos/carrier');
  }

  getMapfre(): Observable<any[]> {
    return this.http.get<any[]>(this.CORE_URLM + 'v1/mapfre-car/card');
  }
  // FIN

  getActivos(): Observable<Carrier[]> {
    return this.http.get<Carrier[]>(this.baseURL2 + 'v1/carrier/activos');
  }

  post(carrier): Observable<Carrier> {
    return this.http.post<Carrier>(this.baseURL + 'v1/carrier', carrier);
  }

  put(id, carrier): Observable<Carrier> {
    return this.http.put<Carrier>(this.baseURL + 'v1/carrier' + id, carrier);
  }

}
