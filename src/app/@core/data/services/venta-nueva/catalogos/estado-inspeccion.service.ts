import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../../environments/environment';
import {EstadoInspeccion} from "../../../interfaces/venta-nueva/catalogos/estado-inspeccion";

@Injectable({
  providedIn: 'root',
})
export class EstadoInspeccionService {
  private baseURL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.CORE_URL;

  }

  // NEW
  get(): Observable<EstadoInspeccion[]> {
    return this.http.get<EstadoInspeccion[]>(this.baseURL + '/estado-inspeccion');
  }

  getActivos(): Observable<EstadoInspeccion[]> {
    return this.http.get<EstadoInspeccion[]>(this.baseURL + '/estado-inspeccion/activos');
  }

  post(estadoInspeccion: EstadoInspeccion): Observable<EstadoInspeccion> {
    return this.http.post<EstadoInspeccion>(this.baseURL + '/estado-inspeccion', estadoInspeccion);
  }

  put(idEstadoInspeccion, estadoInspeccion: EstadoInspeccion): Observable<EstadoInspeccion> {
    return this.http.put<EstadoInspeccion>(this.baseURL + '/estado-inspeccion/' + idEstadoInspeccion, estadoInspeccion);
    // + sessionStorage.getItem('Empleado')
  }

  // FIN

}
