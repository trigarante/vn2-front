import { TestBed } from '@angular/core/testing';

import { EstadoSolicitudService } from './estado-solicitud.service';

describe('EstadoSolicitudService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstadoSolicitudService = TestBed.get(EstadoSolicitudService);
    expect(service).toBeTruthy();
  });
});
