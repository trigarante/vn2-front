import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecibosService {
  private CORE_URL;

  constructor(private http: HttpClient) {
    this.CORE_URL = environment.CORE_URL + '/recibo';
  }

  getRecibosByIdRegistro(idRegistro: number, id?): Observable<any[]> {
    const headers = new HttpHeaders();
    if (id) {
      headers.append('idrecibo', id.toString());
    }
    return this.http.get<any[]>(this.CORE_URL + '/registro/' + idRegistro, {headers});
  }

  getForPago(idRegistro: number): Observable<any> {
    return this.http.get<any>(this.CORE_URL + '/forPago/' + idRegistro);
  }

  post(recibos: any): Observable<any> {
    return this.http.post<any>(this.CORE_URL + '/', recibos);
  }
}

