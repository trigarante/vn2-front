import { TestBed } from '@angular/core/testing';

import { CorreccionErroresService } from './correccion-errores.service';

describe('CorreccionErroresService', () => {
  let service: CorreccionErroresService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CorreccionErroresService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
