import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {CotizacionesAli} from '../../interfaces/venta-nueva/cotizaciones-ali';

@Injectable({
  providedIn: 'root'
})
export class CotizacionesAliService {

  private baseURL;
  private baseURL2;
  private CORE_URL;


  constructor(private http: HttpClient) {
    // super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/cotizacionesAli';
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura + 'v1/cotizacionesAli';
    this.CORE_URL = environment.CORE_URL + '/cotizacionesAli';
  }

  // NEW
  getCotizacionAliById(id): Observable<CotizacionesAli> {
    return this.http.get<CotizacionesAli>(this.CORE_URL + '/' + id);
  }
  getCotizacionAliByIdOnline(id): Observable<CotizacionesAli> {
    return this.http.get<CotizacionesAli>(this.CORE_URL + '/Online/' + id);
  }
  putOnline(idCotizacionAli, cotizacionAli: CotizacionesAli): Observable<CotizacionesAli> {
    return this.http.put<CotizacionesAli>(this.CORE_URL + '/Online/' + idCotizacionAli , cotizacionAli);
  }

  // FIN

  get(): Observable<CotizacionesAli[]> {
    return this.http.get<CotizacionesAli[]>(this.baseURL2);
  }

  getByIdCotizacion(idCotizacion: number): Observable<CotizacionesAli> {
    return this.http.get<CotizacionesAli>(this.baseURL2 + '/idCotizacion/' + idCotizacion);
  }

  post(cotizacionAli: CotizacionesAli): Observable<CotizacionesAli> {
    return this.http.post<CotizacionesAli>(this.baseURL, cotizacionAli);
  }

  // getCotizacionAliById(id): Observable<CotizacionesAli> {
  //   return this.http.get<CotizacionesAli>(this.baseURL2 + '/' + id);
  // }

  put(idCotizacionAli, cotizacionAli: CotizacionesAli): Observable<CotizacionesAli> {
    return this.http.put<CotizacionesAli>(this.CORE_URL + '/' + idCotizacionAli + '/'
      + sessionStorage.getItem('Empleado'), cotizacionAli);
  }
}

