import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../../../environments/environment";
import {Observable} from "rxjs";
import {SolicitudesVN} from "../../../../interfaces/venta-nueva/solicitudes";

@Injectable({
  providedIn: 'root'
})
export class EstadoUsuarioService {
  private CORE_URL;

  constructor(private http: HttpClient) {
    this.CORE_URL = environment.CORE_URL;
  }

  updateClienteSockets(idUsuario): Observable<any> {
    return this.http.put<any>(this.CORE_URL + '/telefonia/' + idUsuario, null);
  }
}
