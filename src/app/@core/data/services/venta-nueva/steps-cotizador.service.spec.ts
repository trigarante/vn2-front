import { TestBed } from '@angular/core/testing';

import { StepsCotizadorService } from './steps-cotizador.service';

describe('StepsCotizadorService', () => {
  let service: StepsCotizadorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StepsCotizadorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
