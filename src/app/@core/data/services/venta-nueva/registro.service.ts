import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Registro, RegistroOnline} from '../../interfaces/venta-nueva/registro';
import {Observable} from 'rxjs';
import {AdminVnView} from "../../interfaces/venta-nueva/adminVnView";

@Injectable({
  providedIn: 'root',
})
export class RegistroService  {
  private CORE_URL;
  private DOCUMENTOS;

  constructor(private http: HttpClient) {
    this.CORE_URL = environment.CORE_URL + '/registro-vn';
    this.DOCUMENTOS = environment.CORE_DOCUMENTOS + '/registro-vn';

  }
  // NEW
  getPolizaExistente(poliza: string, idSocio: string, fechaInicio: string): Observable<string> {
    const headers = new HttpHeaders().append('poliza', poliza.toString()).append('idsocio', idSocio.toString())
      .append('fechainicio', fechaInicio.toString());
    return this.http.get<string>(this.CORE_URL + `/existePoliza/poliza`, {headers});
  }

  postInOne(registro, files: FileList, idSolicitud): Observable<any> {
    const formData = new FormData();
    if (!files.item) {
      formData.append('file', files[0], 'poliza');
    } else {
      formData.append('file', files.item(0), 'poliza');
    }
    formData.append('registro', JSON.stringify(registro));
    formData.append('id', idSolicitud);
    return this.http.post(this.DOCUMENTOS + '/All', formData);
  }
  postInOneOnlinePro(registro, idSolicitud): Observable<any> {
    const headers = new HttpHeaders().append('idSolicitud', idSolicitud.toString());
    return this.http.post(this.DOCUMENTOS + '/AllOnline', registro, {headers});
  }
  getNoSerieExistente(numSerie: string): Observable<any> {
    const headers = new HttpHeaders().append('noserie', numSerie);
    return this.http.get(this.CORE_URL + '/noSerieExiste', {headers});
  }

  getForInspecciones(id): Observable<any> {
    const headers = new HttpHeaders().append('id', id.toString());
    return this.http.get(this.CORE_URL + '/forInpecciones', {headers});
  }

  getRegistroById(idRegistro): Observable<any> {
    return this.http.get<any>(this.CORE_URL + '/' + idRegistro);
  }
  getRegistroByIdOnline(idRegistro): Observable<any> {
    return this.http.get<any>(this.CORE_URL + '/Online/' + idRegistro);
  }
  getForRegistroById(idRegistro): Observable<any> {
    return this.http.get<any>(this.CORE_URL + '/getForRegistroById/' + idRegistro);
  }
  getRegistroByIdViewer(idRegistro): Observable<any> {
    return this.http.get<any>(this.CORE_URL + '/viewer/' + idRegistro);
  }
  detallesPoliza(idAplicacionView): Observable<any> {
    return this.http.get<any>(this.CORE_URL + '/detalles-poliza/' + idAplicacionView);
  }
  put(id, registro: Registro): Observable<Registro> {
    return this.http.put<Registro>(this.CORE_URL, {registro, id});
  }
  // lo usa cotizacion onlie
  putOnline(id, registro: RegistroOnline): Observable<RegistroOnline> {
    return this.http.put<RegistroOnline>(this.CORE_URL + '/UpdateOnline/', {registro, id});
  }

  getAllRegistroByIdUsuario(fechaInicio, fechaFin): Observable<AdminVnView[]> {
    return this.http.get<AdminVnView[]>(this.CORE_URL + '/' + sessionStorage.getItem('Empleado') + `/${fechaInicio}/${fechaFin}`);
  }
}
