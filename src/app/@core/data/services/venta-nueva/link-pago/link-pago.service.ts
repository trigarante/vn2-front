import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LinkPagoService {
  private url = environment.CORE_URL + '/prepago';

  constructor(private http: HttpClient) { }

  get(): Observable<any> {
    return this.http.get(this.url, {headers: {empleado: sessionStorage.getItem('Empleado')}});
  }

  getById(id): Observable<any> {
    return this.http.get(this.url + '/getById', {headers: {id: id.toString()}});
  }

  getInfoCliente(idrecibo, idregistro): Observable<any> {
    return this.http.get(this.url + '/getInfoCliente',
      {headers: {idrecibo: idrecibo.toString(), idregistro: idregistro.toString()}});
  }

  post(data): Observable<any>{
    return this.http.post(this.url, data);
  }
}
