import { TestBed } from '@angular/core/testing';

import { LinkPagoService } from './link-pago.service';

describe('LinkPagoService', () => {
  let service: LinkPagoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LinkPagoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
