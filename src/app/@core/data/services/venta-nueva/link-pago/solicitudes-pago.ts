import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import { SolicitudesPago } from '../../../interfaces/venta-nueva/solicitudesPago';

@Injectable({
  providedIn: 'root',
})
export class SolicitudesPagoService {
  baseURL: string;
  appUrl: string;
  private authUrl;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_URL + '/prepago';
    this.appUrl = environment.APPURL;
    this.authUrl = environment.AUTH;
  }
  postAuth(): Observable<any> {
    const  body = {
      tokenData: 'mHf/0x8xqWmYlrjaRWECOzmkksnuNDZv1fBvMLjpI2g='
    };
    return  this.http.post<any>(this.authUrl, body);
  }
  getLinkPago(idRecibo, token): Observable<any> {
    return this.http.get<any>(this.appUrl + '?idRecibo=' + idRecibo, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  }
  getByIdEmpleado(id: string): Observable<SolicitudesPago[]>{
    return this.http.get<SolicitudesPago[]>(this.baseURL, {headers: {empleado: sessionStorage.getItem('Empleado')}});
  }

}
