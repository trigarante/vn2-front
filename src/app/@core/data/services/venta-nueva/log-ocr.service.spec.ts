import { TestBed } from '@angular/core/testing';

import { LogOcrService } from './log-ocr.service';

describe('LogOcrService', () => {
  let service: LogOcrService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LogOcrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
