import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Inspecciones} from '../../interfaces/venta-nueva/inspecciones';

@Injectable({
  providedIn: 'root'
})
export class InspeccionesService  {
  private baseURL;
  private baseURL2;
  private core;
  private DOCUMENTOS;


  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.core = environment.CORE_URL + '/inspecciones';
    this.DOCUMENTOS = environment.CORE_DOCUMENTOS + '/inspecciones';
  }

  // NEW
  postOne(Inspeccion: Inspecciones,  files: FileList, idRegistro): Observable<Inspecciones> {
    const formData = new FormData();
    if (!files.item) {
      formData.append('file', files[0], 'poliza');
    } else {
      formData.append('file', files.item(0), 'poliza');
    }
    formData.append('inspeccion', JSON.stringify(Inspeccion));
    formData.append('id', idRegistro);
    return this.http.post<Inspecciones>(this.DOCUMENTOS + '/all/', formData);
  }
  // FIN

  get(): Observable<Inspecciones[]> {
    return this.http.get<Inspecciones[]>(this.baseURL2 + 'v1/inspecciones');
  }

  post(Inspeccion: Inspecciones): Observable<Inspecciones> {
    return this.http.post<Inspecciones>(this.baseURL + 'v1/inspecciones/', Inspeccion);
  }

  put(idInspeccion, Inspeccion: Inspecciones): Observable<Inspecciones> {
    return this.http.put<Inspecciones>(this.baseURL + 'v1/inspecciones/' + idInspeccion + '/'
      + sessionStorage.getItem('Empleado') , Inspeccion);
  }
}
