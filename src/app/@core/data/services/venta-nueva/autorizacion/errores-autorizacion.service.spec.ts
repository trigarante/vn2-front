import { TestBed } from '@angular/core/testing';

import { ErroresAutorizacionService } from './errores-autorizacion.service';

describe('ErroresAutorizacionService', () => {
  let service: ErroresAutorizacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErroresAutorizacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
