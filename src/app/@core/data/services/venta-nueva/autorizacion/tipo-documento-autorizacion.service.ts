import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../../environments/environment";
import {Observable} from "rxjs";
import {TipoDocumentos} from "../../../interfaces/verificacion/tipo-documentos";
@Injectable({
  providedIn: 'root'
})

export class TipoDocumentoAutorizacionService {
  private baseURL: string;
  private baseURL2;
  private CORE_URL;


  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.CORE_URL = environment.CORE_URL;
  }

  // NEW
  get(idTabla: number): Observable<TipoDocumentos[]> {
    return this.http.get<TipoDocumentos[]>(this.CORE_URL + '/tipo-documentos/' + idTabla);
  }

  // FIN
}
