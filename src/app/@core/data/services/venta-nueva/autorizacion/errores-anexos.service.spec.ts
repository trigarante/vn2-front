import { TestBed } from '@angular/core/testing';

import { ErroresAnexosService } from './errores-anexos.service';

describe('ErroresAnexosService', () => {
  let service: ErroresAnexosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErroresAnexosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
