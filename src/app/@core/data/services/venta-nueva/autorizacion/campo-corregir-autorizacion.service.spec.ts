import { TestBed } from '@angular/core/testing';

import { CampoCorregirAutorizacionService } from './campo-corregir-autorizacion.service';

describe('CampoCorregirAutorizacionService', () => {
  let service: CampoCorregirAutorizacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampoCorregirAutorizacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
