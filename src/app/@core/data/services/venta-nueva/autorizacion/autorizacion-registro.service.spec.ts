import { TestBed } from '@angular/core/testing';

import { AutorizacionRegistroService } from './autorizacion-registro.service';

describe('AutorizacionRegistroService', () => {
  let service: AutorizacionRegistroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutorizacionRegistroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
