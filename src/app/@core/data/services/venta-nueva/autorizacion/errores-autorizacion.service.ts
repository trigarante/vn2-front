import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErroresAutorizacion} from '../../../interfaces/venta-nueva/autorizacion/errores-autorizacion';

@Injectable({
  providedIn: 'root'
})
export class ErroresAutorizacionService  {
  private baseURL;
  private baseURL2;
  private CORE_URL;


  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.CORE_URL = environment.CORE_URL + '/errores-autorizacion';
  }

  // NEW

  getCamposConErrores(idAutorizacionRegistro: number, idTipoDocumento: number): Observable<ErroresAutorizacion> {
    return this.http.get<ErroresAutorizacion>(this.CORE_URL + '/' + idAutorizacionRegistro + '/'
      + idTipoDocumento);
  }
  post(error): Observable<string> {
    return this.http.post<string>(this.CORE_URL , error);
  }

  putEstadoCorreccion(idErrorAutorizacion: number): Observable<any> {
    return this.http.put(this.CORE_URL + '/' + idErrorAutorizacion, null);
  }

  correcccionDatos(idAutorizacionRegistro: number, documentoVerificado: number, idError: number): Observable<any> {
    return this.http.put<string>(this.CORE_URL + '/correccion-datos', {idAutorizacionRegistro, documentoVerificado, idError});
  }

  // FIN

  get(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL2 + 'v1/errores-autorizacion');
  }


}
