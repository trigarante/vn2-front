import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import { Observable} from 'rxjs';
import {StepsCotizador} from '../../interfaces/venta-nueva/steps-cotizador';



@Injectable({
  providedIn: 'root'
})
export class StepsCotizadorService {

  private baseURL;
  private serviciosNode;
  private CORE_VN2;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.serviciosNode = environment.SERVICIOS_NUEVA_LOGICA_NODE;
    this.CORE_VN2 = environment.CORE_URL + '/step-cotizador';
  }
  getById(idSolicitud: number): Observable<StepsCotizador> {
    return this.http.get<StepsCotizador>(this.serviciosNode + '/step-cotizador/' + idSolicitud);
  }
  // new
  getByIdOnline(idSolicitud: number): Observable<StepsCotizador> {
    return this.http.get<StepsCotizador>( this.CORE_VN2 + '/' + idSolicitud);
  }
}
