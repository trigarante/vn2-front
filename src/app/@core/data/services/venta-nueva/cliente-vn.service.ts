
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ClienteVn} from '../../interfaces/venta-nueva/cliente-vn';


@Injectable({
  providedIn: 'root'
})

export class ClienteVnService   {

  private CORE_URL = environment.CORE_URL + '/cliente';
  private CORE_VN3 = environment.CORE_URL + '/clienteOnline';
  private baseURL; // borrar

  constructor(private http: HttpClient) {
  }

  // NEW
  getClienteById(idCliente): Observable<ClienteVn> {
    return this.http.get<ClienteVn>(`${this.CORE_URL}/byId/${idCliente}`);
  }

  curpExist(curp) {
    const headers = new HttpHeaders().append('curp', curp.toString());
    return this.http.get<ClienteVn>(this.CORE_URL + '/existeCurp', {headers});
  }

  rfcExist(rfc) {
    const headers = new HttpHeaders().append('rfc', rfc.toString());
    return this.http.get<ClienteVn>(this.CORE_URL + '/existeRFC', {headers});
  }

  put(idCliente, cliente: ClienteVn): Observable<ClienteVn> {
    return this.http.put<ClienteVn>(this.CORE_URL + '/' + idCliente, cliente);
  }

  post(cliente: ClienteVn): Observable<ClienteVn> {

    return this.http.post<ClienteVn>(this.CORE_URL, cliente);

  }
  postOnline(cliente: ClienteVn): Observable<ClienteVn> {
    return this.http.post<ClienteVn>(this.CORE_VN3, cliente);
  }

  putInOne(idCliente,  files: FileList): Observable<any> {
    const formData = new FormData();
    if (files) {
      formData.append('file', !files.item ? files[0] : files.item(0), 'poliza');
    }
    formData.append('idcliente', idCliente.toString());
    return this.http.post<any>(this.CORE_VN3 + '/subir-archivo', formData);
  }
  // FIN
putArchivo(idCliente: number, idCarpetaDrive: string): Observable<any> {
  return this.http.put(this.baseURL + 'v1/cliente/update-archivo/' + idCliente + '/' + idCarpetaDrive, null);
}

putArchivoSubido(idCliente: number, idArchivoSubido: number): Observable<any> {
  return this.http.put(this.baseURL + 'v1/cliente/update-archivo-subido/' + idCliente + '/' + idArchivoSubido, null);
}
}
