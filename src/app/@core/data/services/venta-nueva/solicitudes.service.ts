import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {
  Solicitudes,
  SolicitudesView,
  SolicitudesVN,
  SolicitudesVNInterno
} from '../../interfaces/venta-nueva/solicitudes';

@Injectable({
  providedIn: 'root',
})
export class SolicitudesvnService {
  private baseURL;
  public idCliente = new BehaviorSubject<number>(0);
  public  urlPostventa = new BehaviorSubject<number>(0);
  private socketURL;
  private baseURL2;
  private serviciosNode;
  private uContactUrl;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.serviciosNode = environment.CORE_URL + '/solicitudes-vn';
    this.uContactUrl = environment.CORE_NET2PHONE;
  }

  // NEW
  stepsById(idSolicitud): Observable<any> {
    return this.http.get<any>(this.serviciosNode + '/steps/' + idSolicitud);
  }

  getFechas(fechaInicial: string, fechaFinal: string): Observable<SolicitudesVNInterno[]> {
    return this.http.get<SolicitudesVNInterno[]>(`${this.serviciosNode}/todas/${sessionStorage.getItem('Empleado')}/${fechaInicial}/${fechaFinal}`);
  }
  // net2phone
  llamadaUcontact(id, numero): Observable<any> {
    return this.http.post<any>(this.uContactUrl + 'leads/salida', {id, numero});
  }
  // fin net2phone
  getFechasint(fechaInicial: string, fechaFinal: string, idTipoContacto, idEstadoSolicitud): Observable<SolicitudesVNInterno[]> {
    return this.http.get<SolicitudesVNInterno[]>(`${this.serviciosNode}/todasint/${sessionStorage.getItem('Empleado')}/${fechaInicial}/${fechaFinal}/${idTipoContacto}/${idEstadoSolicitud}`);
  }
  getFechasPredictivo(fechaInicial: string, fechaFinal: string, idTipoContacto, idEstadoSolicitud): Observable<SolicitudesVNInterno[]> {
    return this.http.get<SolicitudesVNInterno[]>(`${this.serviciosNode}/predictivo/${sessionStorage.getItem('Empleado')}/${fechaInicial}/${fechaFinal}/${idTipoContacto}/${idEstadoSolicitud}`);
  }
  getDatosProspectoStepCliente(idSolicitud): Observable<any> {
    const headers = new HttpHeaders().append('id', idSolicitud.toString());
    return this.http.get<any>(this.serviciosNode + '/stepCliente', {headers});
  }

  getSolicitudesById(idSolicitud): Observable<SolicitudesVNInterno> {
    return this.http.get<SolicitudesVNInterno>(this.serviciosNode + '/get-by-id/' + idSolicitud);
  }
  getSolicitudesByIdOnline(idSolicitud): Observable<SolicitudesView> {
    return this.http.get<SolicitudesView>(this.serviciosNode + '/get-by-idOnline/' + idSolicitud);
  }
  entryIdCliente(value) {
    this.idCliente.next(value);
  }
  returnentryURLType(): Observable<number> {
    return this.urlPostventa.asObservable();
  }
  // FIN NEW

  get(): Observable<Solicitudes[]> {
    return this.http.get<Solicitudes[]>(this.baseURL2 + 'v1/solicitudes-vn/' + sessionStorage.getItem('Usuario'));
  }

  post(solicitud: Solicitudes): Observable<Solicitudes> {
    return this.http.post<Solicitudes>(this.baseURL + 'v1/solicitudes-vn', solicitud);
  }

  put(idSolicitud, solicitud): Observable<Solicitudes> {
    return this.http.put<Solicitudes>(this.baseURL + 'v1/solicitudes-vn/' + idSolicitud + '/'
      + sessionStorage.getItem('Empleado'), solicitud);
  }

  // putSolicitud(idSolicitud, solicitud): Observable<string> {
  //   return this.http.put<string>(`${this.nodeURL}/llamada-salidas/solicitud/${idSolicitud}`, solicitud);
  // }

  updateEmpleadoSolicitud(idSolicitud: number, idEmpleado: number): Observable<any> {
    return this.http.put(this.serviciosNode + '/update-empleado/' + idSolicitud + '/' + idEmpleado, null);
  }

  reasignarEmpleadoSocket(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnsolicitudes/updateEmpleado', json);
  }

  cambiarEstadoSolicitud(idSolicitud, idEstadoSolicitud) {
    return this.http.put(`${this.serviciosNode}/cambiar-estado/${idSolicitud}/${idEstadoSolicitud}`, null);
  }

  ocrService(json){
    const formData = new FormData();
    formData.append('file', json.policy.item(0), 'file');
    formData.append('aseguradora', json.aseguradora.toString());
    console.log(json.file);
    console.log(json.aseguradora);
    return this.http.post('https://aws.core-ahorraseguros.com/services/textract/file-policy', formData);
  }
  ocrServiceCliente(json){
    const formData = new FormData();
    formData.append('policy', json.policy.item(0), 'file');
    formData.append('insurance', json.aseguradora.toString());
    formData.append('identity', json.indentity.item(0), 'file2');
    formData.append('identify', json.indentify.toString());
    console.log(json.file);
    console.log(json.aseguradora);
    return this.http.post('https://aws.core-ahorraseguros.com/services/textract/validate/identity-vs-policy', formData);
  }
  ocrServiceDomicilio(json){
    const formData = new FormData();
    formData.append('policy', json.policy.item(0), 'policy');
    formData.append('insurance', json.aseguradora.toString());
    formData.append('addressFile', json.addressFile.item(0), 'addressFile');
    formData.append('addressType', json.addressType.toString());
    console.log(json.file);
    console.log(json.aseguradora);
    return this.http.post('https://aws.core-ahorraseguros.com/services/textract/validate/address-vs-policy', formData);
  }
  ocrServiceRepuve(json){
    const formData = new FormData();
    formData.append('policy', json.policy.item(0), 'policy');
    formData.append('insuranse', json.aseguradora.toString());
    console.log(json.file);
    console.log(json.aseguradora);
    return this.http.post('https://aws.core-ahorraseguros.com/services/textract/validate/repuve-vs-policy', formData);
  }
  ocrServiceVehiculo(json){
    const formData = new FormData();
    formData.append('policy', json.policy.item(0), 'policy');
    formData.append('insurance', json.aseguradora.toString());
    formData.append('car', json.car.item(0), 'car');
    formData.append('carIdentity', json.carIdentity.toString());
    console.log(json.file);
    console.log(json.aseguradora);
    return this.http.post('https://aws.core-ahorraseguros.com/services/textract/validate/car-vs-policy', formData);
  }

  logOcr(json){

  }

  validarOCR(idSolicitud, idTipoContacto) {
    return this.http.get(`${this.serviciosNode}/validacionOCR/${idTipoContacto}/${idSolicitud}`);
  }
}

