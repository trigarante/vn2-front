import { TestBed } from '@angular/core/testing';

import { ValidacionTelefonoService } from './validacion-telefono.service';

describe('ValidacionTelefonoService', () => {
  let service: ValidacionTelefonoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValidacionTelefonoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
