import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {DetallesPoliza} from '../../interfaces/venta-nueva/detalles-poliza';

@Injectable({
  providedIn: 'root',
})
export class DetallesPolizaService {

  private baseURL;
  private baseURL2;
  private CORE_URL;


  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.CORE_URL = environment.CORE_URL;

  }
  // NEW
  getAplicacionViewById(idAplicacionView): Observable<any> {
    return this.http.get<any>(this.CORE_URL + '/detalles-poliza/' + idAplicacionView);
  }

  // FIN
  get(): Observable<DetallesPoliza> {
    return this.http.get<DetallesPoliza>(this.baseURL2 + 'v1/aplicaciones-view');
  }
  getById(id): Observable<DetallesPoliza> {
    return this.http.get<DetallesPoliza>(this.baseURL2 + 'v1/aplicaciones-view/' + id);
  }

}
