import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SolicitudesVN} from '../../interfaces/venta-nueva/solicitudes';


@Injectable({
  providedIn: 'root'
})
export class AdminVnViewService {
  private socketURL;


  constructor(private http: HttpClient) {
    this.socketURL = environment.GLOBAL_SOCKET;
  }
  updateArchivoSubidoSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnAdministradorPolizas/updateArchivoSubido', json);
  }
}
