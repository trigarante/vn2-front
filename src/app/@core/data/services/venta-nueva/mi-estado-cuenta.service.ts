import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MiEstadoCuenta} from '../../interfaces/venta-nueva/MiEstadoCuenta';

@Injectable({
  providedIn: 'root'
})
export class MiEstadoCuentaService {
  private baseURL2;

  constructor(private http: HttpClient) {
    this.baseURL2 = environment.CORE_URL;
  }

  getAllRegistroByIdUsuarioRegistro(fechaInicio: any, fechaFin: any): Observable<MiEstadoCuenta[]> {
    return this.http.get<MiEstadoCuenta[]>(this.baseURL2 +
      '/estado-cuenta/getRegistro/' + sessionStorage.getItem('Empleado')
      + '/' + fechaInicio + '/' + fechaFin);
  }

  getAllRegistroByIdUsuarioAplicado(fechaInicio: any, fechaFin: any): Observable<MiEstadoCuenta[]> {
    return this.http.get<MiEstadoCuenta[]>(this.baseURL2 +
      '/estado-cuenta/getAplicado/' + sessionStorage.getItem('Empleado') + '/' +
      fechaInicio + '/' + fechaFin);
  }
}
