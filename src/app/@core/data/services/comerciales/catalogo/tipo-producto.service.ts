import {Injectable} from '@angular/core';
import {TipoProducto, TipoProductoData} from '../../../interfaces/comerciales/catalogo/tipo-producto';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class TipoProductoService extends TipoProductoData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.CORE_URL;
    this.socketURL = environment.CORE_URL;
  }

  get(): Observable<TipoProducto[]> {
    return this.http.get<TipoProducto[]>(this.baseURL + 'v1/tipo-producto');
  }

  post(tipoProducto: TipoProducto): Observable<TipoProducto> {
    return this.http.post<TipoProducto>(this.baseURL + 'v1/tipo-producto', tipoProducto);
  }
  put(idTipoProducto, tipoProducto: TipoProducto): Observable<TipoProducto> {
    return this.http.put<TipoProducto>(this.baseURL + 'v1/tipo-producto/' + idTipoProducto + '/'
      + sessionStorage.getItem('Empleado'), tipoProducto);
  }
}
