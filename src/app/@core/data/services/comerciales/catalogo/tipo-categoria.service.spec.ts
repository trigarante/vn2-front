import { TestBed } from '@angular/core/testing';

import { TipoCategoriaService } from './tipo-categoria.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';

describe('TipoCategoriaService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [TipoCategoriaService, HttpClient],
  }));

  it('should be created', () => {
    const service: TipoCategoriaService = TestBed.get(TipoCategoriaService);
    expect(service).toBeTruthy();
  });
});
