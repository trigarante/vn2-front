import {Injectable} from '@angular/core';
import {TipoCategoria, TipoCategoriaData} from '../../../interfaces/comerciales/catalogo/tipo-categoria';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class TipoCategoriaService extends TipoCategoriaData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<TipoCategoria[]> {
    return this.http.get<TipoCategoria[]>(this.baseURL + 'v1/tipo-subramo');
  }

  post(tipoCategoria: TipoCategoria): Observable<TipoCategoria> {
    return this.http.post<TipoCategoria>(this.baseURL + 'v1/tipo-subramo', tipoCategoria);
  }

  getTipoCategoriaById(idTipoCategoria): Observable<TipoCategoria> {
    return this.http.get<TipoCategoria>(this.baseURL + 'v1/tipo-subramo/' + idTipoCategoria);
  }

  put(idTipoCategoria, tipoCategoria: TipoCategoria): Observable<TipoCategoria> {
    return this.http.put<TipoCategoria>(this.baseURL + 'v1/tipo-subramo/' + idTipoCategoria, tipoCategoria);
  }
}
