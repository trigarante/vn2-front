import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {TipoRamo} from '../../../interfaces/comerciales/catalogo/tipo-ramo';

@Injectable({
  providedIn: 'root',
})
export class TipoRamoService  {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.CORE_URL;
    this.socketURL = environment.CORE_URL;
  }

  get(): Observable<TipoRamo[]> {
    return this.http.get<TipoRamo[]>(this.baseURL + 'v1/tipo-ramo');
  }
  post(tipoRamo: TipoRamo): Observable<TipoRamo> {
    return this.http.post<TipoRamo>(this.baseURL + 'v1/tipo-ramo', tipoRamo);
  }
  put(idTipoRamo, tipoRamo: TipoRamo): Observable<TipoRamo> {
    return this.http.put<TipoRamo>(this.baseURL + 'v1/tipo-ramo/' + idTipoRamo + '/'
      + sessionStorage.getItem('Empleado') , tipoRamo);
  }
}
