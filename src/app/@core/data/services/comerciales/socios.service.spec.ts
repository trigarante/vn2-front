import {getTestBed, TestBed} from '@angular/core/testing';
import { SociosService } from './socios.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('Socios Servicio', () => {
  let injector: TestBed;
  let service: SociosService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    idEstadoSocio: 1,
    prioridad: 2,
    nombreComercial: 'testing',
    rfc: 'PARS940232PT5',
    razonSocial: 'MORAL',
    alias: 'MORAL',
    prioridades: 'MEDIA',
    estado: 'ACTIVO',
    activo: '1',
    id: 1,
  },
    {
      id: 'socios',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [SociosService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(SociosService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/socios');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/socios', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/socios');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/socios', '/1/74');
      });
    });
  }
});
