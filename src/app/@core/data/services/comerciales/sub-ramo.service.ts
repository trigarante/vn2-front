import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SubRamo} from '../../interfaces/comerciales/sub-ramo';

@Injectable({
  providedIn: 'root',
})
export class  SubRamoService  {

  private baseURL = environment.CORE_URL + '/subRamo';

  constructor(private http: HttpClient) {
  }

  // NEW
  getByIdRamo(idRamo: number): Observable<SubRamo[]> {
    const headers = new HttpHeaders().append('id', idRamo.toString());
    return this.http.get<SubRamo[]>(this.baseURL + '/getByIdRamo', {headers});
  }
  getSubramosByIdRamo(idRamo: number): Observable<SubRamo[]> {
    return this.http.get<SubRamo[]>(this.baseURL + '/id-ramo/' + idRamo);
  }
  getSubramoById(idSubRamo): Observable<SubRamo> {
    return this.http.get<SubRamo>(this.baseURL + '/subramo/' + idSubRamo);
  }
  getTipoAndAlias(idTipoSubramo, alias): Observable<SubRamo> {
    return this.http.get<SubRamo>(this.baseURL + '/alias/' + idTipoSubramo + '/' + alias);
  }
  // FIN

  get(): Observable<SubRamo[]> {
    return this.http.get<SubRamo[]>(this.baseURL + '/subramo');
  }
  post(subramo: SubRamo): Observable<SubRamo> {
    return this.http.post<SubRamo>(this.baseURL + '/subramo', subramo);
  }

  put(idSubRamo, subramo: SubRamo): Observable<SubRamo> {
    return this.http.put<SubRamo>(this.baseURL + '/subramo/' + idSubRamo + '/'
      + sessionStorage.getItem('Empleado'), subramo);
  }
}
