import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SociosComercial} from '../../interfaces/comerciales/socios-comercial';
import {SociosComercialOcr} from "../../interfaces/comerciales/socios-comercial-ocr";

@Injectable({
  providedIn: 'root',
})
export class SociosService  {
  private baseURL = environment.CORE_URL + '/socios';

  constructor(private http: HttpClient) {
  }

  // NEW
  getByIdPaisStep(idPais: number): Observable<SociosComercial[]> {
    const headers = new HttpHeaders().append('id', idPais.toString());
    return this.http.get<SociosComercial[]>(this.baseURL + '/byIdPaisStep', {headers});
  }
  // FIN

  get(): Observable<SociosComercial[]> {
    return this.http.get<SociosComercial[]>(this.baseURL + '/socios');
  }

  post(socios: SociosComercial): Observable<SociosComercial> {
    return this.http.post<SociosComercial>(this.baseURL + '/socios', socios);
  }
  put(idSocios, socios: SociosComercial): Observable<SociosComercial> {
    return this.http.put<SociosComercial>(this.baseURL + '/socios/' + idSocios + '/'
      + sessionStorage.getItem('Empleado'), socios);
  }
  getActivos(activos: number): Observable<SociosComercial[]> {
    return this.http.get<SociosComercial[]>(this.baseURL + '/socios/activos/' + activos);
  }
  getAseguradoras(): Observable<SociosComercialOcr[]>{
    return this.http.get<SociosComercialOcr[]>(this.baseURL + '/aseguradoras');
  }
}
