import { TestBed } from '@angular/core/testing';

import { WhatsappMsgService } from './whatsapp-msg.service';

describe('WhatsappMsgService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WhatsappMsgService = TestBed.get(WhatsappMsgService);
    expect(service).toBeTruthy();
  });
});
