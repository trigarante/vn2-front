import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PlantillasWhatsApp} from "../../interfaces/whatsapp/plantillasTipoSubarea";
import {environment} from "../../../../../environments/environment";

@Injectable({
  providedIn: 'root',
})
export class WhatsappMsgService {

  private urlWhats;
  private urlWhatsappNode;
  private idEmpleado;

  constructor(private http: HttpClient) {
    this.idEmpleado = sessionStorage.getItem('Empleado');
    this.urlWhats = '/v1/whatsapp/send';
    this.urlWhatsappNode = environment.nodeWhats;
  }

  sendMessage(message, credentials) {
    const mensajeConUseryTokenData = {
      message: message,
      credentials: credentials,
    };
    return this.http.post(this.urlWhatsappNode + '/send', mensajeConUseryTokenData );
  }

  sendSMS(message): Observable<any> {
    return this.http.get<any[]>(this.urlWhatsappNode + '/send/sms', {params: message} );
  }

  postCanalWhatsapp(datos) {
    return this.http.post(this.urlWhatsappNode + `/solicitudes/numero-prospecto/${datos.dataSend[0].correlationId}/${this.idEmpleado}`, datos);
  }

  postPlantillasJson(datos) {
    return this.http.post('https://wavy.mark-43.net/send-plantilla', datos);
  }

  postCanalWhatsappPlantilla(datos) {
    return this.http.post(this.urlWhatsappNode + '/solicitudes/send-plantilla', datos);
  }

  getPlantillaByIdEmpleadoTipoSubarea(idEmpleado: number): Observable<PlantillasWhatsApp[]> {
    return this.http.get<PlantillasWhatsApp[]>(this.urlWhatsappNode + `/plantillas-whatsapp/tipo-subarea/${idEmpleado}`);
  }

  getMensajesByIdCanal(id: any): Observable<any[]> {
    return this.http.get<any[]>(this.urlWhatsappNode + `/mensajes/canal-solicitud/${id}`);
  }

  getEjecutivosCanalesBySupervisor(): Observable<any[]> {
    const data = {
      idEmpleado: sessionStorage.getItem('Empleado'),
      idPuesto: sessionStorage.getItem('idPuesto'),
      idSubarea: sessionStorage.getItem('idSubarea'),
    };
    return this.http.get<any[]>(this.urlWhatsappNode + `/mensajes-de-ejecutivos-por-supervisor`, {params: data});
  }

  getEjecutivosCanalesBySupervisorAndFecha(fechaInicio, fechaFin): Observable<any[]> {
    const data = {
      idEmpleado: sessionStorage.getItem('Empleado'),
      idPuesto: sessionStorage.getItem('idPuesto'),
      idSubarea: sessionStorage.getItem('idSubarea'),
      fechaInicio: fechaInicio,
      fechaFin: fechaFin,
    };
    return this.http.get<any[]>(this.urlWhatsappNode + `mensajes-de-ejecutivos-por-supervisor-fecha`, {params: data});
  }

  getCampanasPorSupervisor(): Observable<any[]> {
    const data = {
      idEmpleado: sessionStorage.getItem('Empleado'),
    };
    return this.http.get<any[]>(this.urlWhatsappNode + `campanas-por-supervisor`, {params: data});
  }

  getMonitoreoGeneralConFiltroFecha(fechaInicio, fechaFin): Observable<any[]> {
    const data = {
      // idEmpleado: sessionStorage.getItem('Empleado'),
      // idPuesto: sessionStorage.getItem('idPuesto'),
      // idSubarea: sessionStorage.getItem('idSubarea'),
      fechaInicio: fechaInicio,
      fechaFin: fechaFin,
    };
    return this.http.get<any[]>(this.urlWhatsappNode + `monitoreo-gral`, {params: data});
  }

  getMensajesCanalGeneral(): Observable<any[]> {
    return this.http.get<any[]>(this.urlWhatsappNode + `/mensajes/canal-general/${this.idEmpleado}`);
  }

  getMensajesRespondidos(): Observable<any[]> {
    return this.http.get<any[]>(this.urlWhatsappNode + `/mensajes-respondidos/${this.idEmpleado}`);
  }

  mensajeRespondido(mensjes): Observable<any[]> {
    return this.http.put<any>(this.urlWhatsappNode + `/mensajes/respondido/${this.idEmpleado}`, mensjes);
  }

  activarCanalPorSegundaPlantilla(data): Observable<any[]> {
    return this.http.put<any>(this.urlWhatsappNode + `activar-canal-por-segunda-plantilla`, data);
  }

  postMensajePorSegundaPlantilla(data): Observable<any[]> {
    return this.http.post<any>(this.urlWhatsappNode + `mensaje-por-segunda-plantilla`, data);
  }

  chatTwice(numero, idCotizacionAli): Observable<any> {
    return this.http.get<any>(this.urlWhatsappNode + `chatTwice/${numero}/${idCotizacionAli}`);
  }
  // 'Content-Type': 'aplication/json',
  getCatalogoNumerosWavy(): Observable<any[]> {
    return this.http.get<any[]>(this.urlWhatsappNode + `catalogo-numeros-wavy`);
  }

  getNumOrigenDestino(destino, origen): Observable<any> {
    return this.http.get<any>(this.urlWhatsappNode + `canal-origen-destino/${destino}/${origen}`);
  }

  getEjecutivosPorSubarea(idSubarea: number, fechaInicio, fechaFin): Observable<any[]> {
    const data = {
      fechaInicio: fechaInicio,
      fechaFin: fechaFin,
    };
    // return this.http.get<any[]>(this.urlWhatsappNode + `ejecutivos-por-subarea/${idSubarea}`, {params: data});
    return this.http.get<any[]>(this.urlWhatsappNode + `ejecutivos-por-subarea/${idSubarea}`);
  }

  getAllEjecutivosPorSubarea(idSubarea: number, fechaInicio, fechaFin): Observable<any[]> {
    const data = {
      fechaInicio: fechaInicio,
      fechaFin: fechaFin,
    };
    return this.http.get<any[]>(this.urlWhatsappNode + `all-ejecutivos-por-subareas-activas/${idSubarea}`, {params: data});
  }

  getCanalesPorEjecutivo(idEmpleado: number, fechaInicio, fechaFin): Observable<any[]> {
    const data = {
      fechaInicio: fechaInicio,
      fechaFin: fechaFin,
    };
    return this.http.get<any[]>(this.urlWhatsappNode + `canales-por-ejecutivos/${idEmpleado}`, {params: data});
  }
}
