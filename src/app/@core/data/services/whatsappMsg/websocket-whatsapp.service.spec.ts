import { TestBed } from '@angular/core/testing';

import { WebsocketWhatsappService } from './websocket-whatsapp.service';

describe('WebsocketWhatsappService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebsocketWhatsappService = TestBed.get(WebsocketWhatsappService);
    expect(service).toBeTruthy();
  });
});
