import { Injectable } from '@angular/core';
import {SocketsWhatsappService} from './sockets-whatsapp.service';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class WebsocketWhatsappService {
  private urlWhatsappNode;
  constructor(private socketsWhatsappService: SocketsWhatsappService) {
    // this.urlWhatsappNode = 'http://localhost:3000/whatsapp/';
    // this.urlWhatsappNode = 'https://wavy.mark-43.net/whatsapp/';
    this.urlWhatsappNode = environment.NODEJS_SOCKET;
    // this.urlWhatsappNode = environment.NODEJS_SOCKETS;
  }

  /*getMensaje() {
    return this.socketsWhatsappService.leeMensaje(`mensaje-nuevo${sessionStorage.getItem('idSolicitud')}`);
  }

  getMensajesSolicitudes() {
    return this.socketsWhatsappService.leeMensaje(`msg-solicitudes-${sessionStorage.getItem('Empleado')}`);
  }

  getCantidadMensajesNoRespondidosSolicitudes() {
    return this.socketsWhatsappService.leeMensaje(`msg-noRespondido-${sessionStorage.getItem('Empleado')}`);
  }*/

}
