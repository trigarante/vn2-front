import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Departamento} from '../../interfaces/catalogos/departamento';

@Injectable({
  providedIn: 'root'
})
export class ComisionesService {

  private baseURL = environment.CORE_URL + '/administracion-personal/comisiones/';
  constructor(private http: HttpClient) { }

  getDeptos(): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + 'departamentos');
  }

  getDatosComision(estado?: number): Observable<{departamentos: any[], arreglo: number[]}> {
    if (estado) {
      const headers = new HttpHeaders().append('edoproceso', estado.toString());
      return this.http.get<{departamentos: any[], arreglo: number[]}>(this.baseURL + 'esquemas-comision', {headers});
    }
    return this.http.get<{departamentos: any[], arreglo: number[]}>(this.baseURL + 'esquemas-comision');
  }
// el signo de ? en post y update es temporal, ya que se tiene que ajustar el componente de nuevos turnos(inbound)
  postEsquema(idTipoTablaEsquema, esquema, idCatalogoEsquema, dptoArray?): Observable<any> {
    esquema.idEmpleadoADP = +sessionStorage.getItem('Empleado');
    esquema.idCatalogoEsquema = idCatalogoEsquema;
    esquema.idDptoArray = dptoArray;
    esquema.idTipoTablaEsquema = idTipoTablaEsquema;
    return this.http.post<any>(this.baseURL + 'crear' , esquema);
  }
  updateEsquema(esquema, id, idCatalogoEsquema, dptoArray?, rechazado?): Observable<any> {
    console.log(esquema);
    esquema.idEmpleadoADP = +sessionStorage.getItem('Empleado');
    esquema.id = id;
    esquema.idCatalogoEsquema = idCatalogoEsquema;
    esquema.idDptoArray = dptoArray;
    esquema.idEstadoEsquema = rechazado;
    return this.http.put<any>(this.baseURL + 'editar' , {esquema});
  }

  getCatalogoEsquemaById(id): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + `catalogo-esquema/${id}`);
  }
  getCatalogoEsquema(): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + `get-catalogos-esquema`);
  }
  getTiposTabla(): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + `get-tipo-tabla`);
  }
  rechazarEsquema(data, motivos): Observable<any> {
    const datos = {
      idempleado: sessionStorage.getItem('Empleado'),
      id: data,
      motivos
    };
    console.log(datos);
    return this.http.put<any>(this.baseURL + `rechazar`, {datos});
  }
  aprobarEsquema(data): Observable<any> {
    return this.http.put<any>(this.baseURL + `aprobar`, {
      idEmpleado: +sessionStorage.getItem('Empleado'),
      id: +data.id,
      idEstadoEsquema: +data.idEstadoEsquema,
      idCatalogoEsquema: +data.idCatalogoEsquema
    });
  }
}
