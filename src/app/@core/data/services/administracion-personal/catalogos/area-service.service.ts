import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Area } from '../../../interfaces/catalogos/area';
import {environment} from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AreaServiceService {

  private baseURL = environment.CORE_URL + 'administracion-personal/area/';
  constructor(
    private http: HttpClient
  ) { }

  getArea(): Observable<Area[]> {
    return this.http.get<Area[]>(this.baseURL + 'getAll');
  }
  getNombreById(id): Observable<string[]> {
    return this.http.get<string[]>(this.baseURL + 'getNombreById/' + id);
  }
  getAreas(): Observable<Area[]> {
    return this.http.get<Area[]>(this.baseURL +  '/areas');
  }

  create(datos: any): Observable<Area> {
    return this.http.post<Area>(`${this.baseURL}`, datos);
  }

  editar(datos, id: number): Observable<Area> {
    return this.http.put<Area>(`${this.baseURL}/${id}`, {...datos});
  }

  borrar(id: number): Observable<Area> {
    return this.http.put<Area>(`${this.baseURL}/borrar/${id}`, null);
  }
}
