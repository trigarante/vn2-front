import { TestBed } from '@angular/core/testing';

import { MotivoJustificacionService } from './motivo-justificacion.service';

describe('MotivoJustificacionService', () => {
  let service: MotivoJustificacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotivoJustificacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
