import { TestBed } from '@angular/core/testing';

import { JustificacionesService } from './justificaciones.service';

describe('SolicitudIncapacidadService', () => {
  let service: JustificacionesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JustificacionesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
