import { TestBed } from '@angular/core/testing';

import { EstadoAsistenciaService } from './estado-asistencia.service';

describe('EstadoAsistenciaService', () => {
  let service: EstadoAsistenciaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoAsistenciaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
