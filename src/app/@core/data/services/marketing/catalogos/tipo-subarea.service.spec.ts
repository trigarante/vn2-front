import { TestBed } from '@angular/core/testing';

import { TipoSubareaService } from './tipo-subarea.service';

describe('TipoSubareaService', () => {
  let service: TipoSubareaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoSubareaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
