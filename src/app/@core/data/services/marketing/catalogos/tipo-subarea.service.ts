import { Injectable } from '@angular/core';
import {environment} from "../../../../../../environments/environment";
import {TipoSubarea} from "../../../interfaces/marketing/catalogos/tipoSubarea";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TipoSubareaService {

  private baseURL;
  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  get(): Observable<TipoSubarea[]> {
    return this.http.get<TipoSubarea[]>(this.baseURL + 'v1/tipo-subarea');
  }

  getAllMedios(): Observable<TipoSubarea[]> {
    return this.http.get<TipoSubarea[]>(this.baseURL + 'v1/tipo-subarea/activas');
  }

  getAllPolizas(): Observable<TipoSubarea[]> {
    return this.http.get<TipoSubarea[]>(this.baseURL + 'v1/tipo-subarea/polizas');
  }
  getAllIn(): Observable<TipoSubarea[]> {
    return this.http.get<TipoSubarea[]>(this.baseURL + 'v1/tipo-subarea/in');
  }
  getAllByUsuario(): Observable<TipoSubarea[]> {
    return this.http.get<TipoSubarea[]>(this.baseURL + 'v1/tipo-subarea/usuario/' + window.sessionStorage.getItem('Usuario'));
  }

  post(tipoSubarea: TipoSubarea): Observable<TipoSubarea> {
    return this.http.post<TipoSubarea>(this.baseURL + 'v1/tipo-subarea', tipoSubarea);
  }

  getTipoSubareaById(idTipoSubarea): Observable<TipoSubarea> {
    return this.http.get<TipoSubarea>(this.baseURL + 'v1/tipo-subarea/' + idTipoSubarea);
  }
}
