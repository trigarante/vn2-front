import { TestBed } from '@angular/core/testing';

import { LlamadasSalidaService } from './llamadas-salida.service';

describe('LlamadasSalidaService', () => {
  let service: LlamadasSalidaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LlamadasSalidaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
