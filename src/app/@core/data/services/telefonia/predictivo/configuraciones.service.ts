import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfiguracionesService {
  private serviciosNode;
  private serviciosNode2;

  constructor(private http: HttpClient) {
    this.serviciosNode = environment.SERVICIOS_TELEFONIA + '/predictivo/configuraciones';
    this.serviciosNode2 = environment.SERVICIOS_TELEFONIA + '/intento';
  }

  getConfiguracionesAll(): Observable<any[]> {
    return this.http.get<any[]>(`${this.serviciosNode}`);
  }

  getConfiguracionById(id): Observable<any[]> {
    return this.http.get<any[]>(`${this.serviciosNode}/byid/${id}`);
  }

  getDepartamentos(): Observable<any[]> {
    return this.http.get<any[]>(`${this.serviciosNode}/departamentos`);
  }

  getConfiguracionByIdEdit(id): Observable<any> {
    return this.http.get<any>(`${this.serviciosNode}/config/${id}`);
  }

  postConfiguracion(json): Observable<any[]> {
    return this.http.post<any[]>(`${this.serviciosNode}/create`, json);
  }

  putConfiguracion(json, id): Observable<any[]> {
    return this.http.put<any[]>(`${this.serviciosNode}/update/${id}`, json);
  }

  eliminarConfiguracion(json, id): Observable<any[]> {
    return this.http.put<any[]>(`${this.serviciosNode}/delete/${id}`, json);
  }

  getIntentoContacto(): Observable<any[]> {
    return this.http.get<any[]>(`${this.serviciosNode2}`);
  }

  guardarConfiguracion(id, json): Observable<any[]> {
    return this.http.put<any[]>(`${this.serviciosNode}/guardar-configuracion/${id}`, json);
  }
}
