import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from './../../../../../../environments/environment';
import {Observable} from 'rxjs';
const telefoniaEndpoints = environment.SERVICIOS_TELEFONIA;
const vnEndpoints = environment.CORE_URL;

@Injectable({
  providedIn: 'root',
})
export class AgendaLlamadasService {

  constructor(private http: HttpClient) {
  }

  // put(id, json): Observable<JSON> {
  //   return this.http.put<JSON>(`${this.baseURL}/callcenter/llamadasalida/adendarLlamada/` + id, json,
  //   );
  // }
  getNumerosByIdSolicitud(idSolicitud): Observable<any[]> {
    return this.http.get<any[]>(`${telefoniaEndpoints}/agregar-numero/numeros-solicitud/${idSolicitud}`);
  }

  getAgendaByEmpleado(idEmpleado): Observable<any>{
    return this.http.get<any>(vnEndpoints + `/agendarLlamadas/${idEmpleado}` );
  }

  postPendiente(agregaPndiete): Observable<any>{
    return this.http.post(vnEndpoints + `/agendarLlamadas`, agregaPndiete);
  }

  putPendiente(idAgenda, idEstadoLlamada): Observable<any> {
    return this.http.put<any>(vnEndpoints + `/agendarLlamadas/status`, {idAgenda, idEstadoLlamada});
  }

  getPendientes(idEmpleado): Observable<any> {
    return this.http.get<any>(`${vnEndpoints}/agendarLlamadas/pendientes/${idEmpleado}`);
  }
}

