import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
const baseUrl = environment.SERVICIOS_TELEFONIA;

@Injectable({
  providedIn: 'root'
})
export class LogLlamadasService {

  constructor(private http: HttpClient) { }

  createLog(json) {
    return this.http.post(`${baseUrl}/log-llamadas`, json);
  }
}
