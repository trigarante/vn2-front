import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
const baseURL = environment.SERVICIOS_TELEFONIA;


@Injectable({
  providedIn: 'root'
})
export class SpeechService {
  public speechDefault = `
1 .- SALUDO “Excelente día mi nombre es _____________ de Ahorra Seguros del área de promociones y descuentos ¿tengo el gusto con el Sr.(ita) _______________?

“"Gracias Sr.(ita) ___, estoy dando seguimiento a su contratación de seguro de auto con un MEJOR PRECIO,

Para ayudarlo con la contratación de su seguro de auto, le confirmo los siguientes datos de su vehículo ¿correcto?

• ¿Su vehículo es para uso particular o para UBER?
•  El AÑO de su vehículo es ___________?
•  La Marca de su vehículo es _________?
•  El Modelo y versión de su vehículo es ________?
  ¿ El vehículo es nacional o legalizado?
   ¿ Su automóvil proviene de salvamento o recuperación?
2.- COTIZACIÓN
El precio ofrecido para su seguro de auto era de $____  con la aseguradora _____

En este momento tenemos 3 mejores precios con las aseguradoras ____ ,____ y ____

En la aseguradora 1 _____ el precio es de ____ con una cobertura Amplia
En la aseguradora 2_____ el precio es de _____ con una cobertura Amplia
En la aseguradora 3 _____ el precio es de _____ de igual manera con una cobertura Amplia

3.- CIERRE DE VENTAS
Estamos a punto de concluir, para finalizar su emisión solo requiero los siguientes datos:

4.- DATOS DE EMISIÓN Validación de datos
•  Teléfono, C.P, dirección completa.
• Número de Serie (si no cuenta con este dato, solicitarle No. de placa para buscarlo en REPUVE)
• Para gozar con meses sin intereses indíqueme los datos de su tarjeta.
5.- Solicitud al cliente de 2 números telefónicos de contacto
1. Requiero por favor me proporcione un número telefónico de contacto (cliente indica el 1er. teléfono) . Ante cualquier siniestro o eventualidad que pueda presentarse nos es importante tener a la mano un contacto de emergencia,
me proporciona por favor el nombre de este contacto __nombre y apellidos__, cual es el número al cual le podamos llamar.

6.-INSPECCIÓN  Física o Digital
Sr.(ita) ____ le informo que la aseguradora ----realizará una inspección para garantizar la protección de su automóvil a partir de las condiciones actuales, ya sea de manera física o digital, en caso de ser digital recibirá un sms y/o e mail(validar teléfono y correo electrónico) con la liga digital para realizar el proceso de inspección.
En caso de ser física, recibirá una llamada telefónica para confirmar una cita con usted en el día y lugar de su preferencia para que un representante debidamente identificado acuda a realizar la inspección vehicular. Aplicable únicamente a ABA: De no llevar a cabo la siguiente inspección vehicular, se duplicará el deducible para las coberturas de robo total y daños materiales de lo establecido en la carátula de su póliza.
En caso de aplicar Art. 492,
Sr.(ita)____ Le solicitamos nos envíe una identificación oficial (INE  pasaporte y/o cedula profesional) al siguiente correo: midocumentación@trigarante.com, para garantizar las condiciones actuales de su unidad con la finalidad de protegerlo en todo momento y cumplir con lo requerido por la ley vigente.
6.-DATOS DE INSPECCIÓN  (Se menciona únicamente cuando el cliente acepte la compra del servicio)
De igual manera Sr/Srita. le comento que recibirá un SMS con un Link para realizar una inspección a su unidad, esto con el fin de confirmar que la unidad se encuentra en perfecto estado, le menciono que usted tendrá que tomar fotografías a su unidad siguiendo las indicaciones del link de inspección. .
7.-RECORDATORIO DE DEPÓSITOS BANCARIOS (ÚNICAMENTE CUANDO REALIZAN VENTA O PAGO)
Ahorra seguros le recuerda que los depósitos bancarios se realizan exclusivamente  a en cuentas a nombre de las compañías de seguro  y NUNCA a nombre de particulares.
8.- SOLICITUD DE REFERENCIADOS. (únicamente se menciona en venta)
Sr. Cliente ¿Me puede brindar un teléfono y correo electrónico de algún amigo, familiar o conocido, el cual cuente con algún vehículo y pueda ofrecerle una póliza según sean sus requerimientos?
9.-DESPEDIDA
“Gracias por haber tomado mi llamada, mi nombre es ________________ de Ahorra Seguros, ¡que pase un excelente día!, le invitamos a que nos siga en Facebook y Twitter”
`;

  constructor(private http: HttpClient) { }

  getSpeechByDepartamento() {
    const departamento = +sessionStorage.getItem('idDepartamento');
    return this.http.get(`${baseURL}/speech/${departamento}`);
  }
}
