import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
const base_url = environment.SERVICIOS_TELEFONIA;
const extension = localStorage.getItem('extension');
const idEmpleado = sessionStorage.getItem('Empleado');

@Injectable({
  providedIn: 'root'
})
export class LlamadasOutService {
  constructor(private http: HttpClient) { }

  iniciarLlamada(numero: string, idSolicitud?: number, idRegistro?: number) {
    return this.http.post(`${base_url}/llamada-salidas/inicio-llamada/${extension}`, { numero, idSolicitud, idRegistro, idEmpleado});
  }

  finLlamada(idLlamada, idEstadoLlamada) {
    return this.http.put(`${base_url}/llamada-salidas/fin-llamada/${idLlamada}/${idEstadoLlamada}`, null);
  }

  tipificarLlamada(idLlamada, idSubEtiqueta, comentarios) {
    return this.http.put(`${base_url}/llamada-salidas/tipificar-llamada/${idLlamada}`, {idSubEtiqueta, comentarios});
  }

  getLlamadasCb() {
    return this.http.get(`${base_url}/llamada-salidas/llamada-pendiente/${idEmpleado}`);
  }

  inicioCb(idLlamada) {
    return this.http.put(`${base_url}/llamada-salidas/inicio-cb/${idLlamada}`, null);
  }
}
