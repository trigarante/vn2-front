import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private net2phone = environment.SERVICIOS_TELEFONIA_NET2PHONE;

  constructor(private http: HttpClient) { }

  postToken(user, password): Observable<any> {
    const body = new URLSearchParams();
    body.set('user', user);
    body.set('password', password);
    const options = {headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')};
    return this.http.post(`${this.net2phone}Integra/resources/auth/getUserToken`, body.toString(), options);
  }

  postKillToken(user) {
    const body = new URLSearchParams();
    body.set('user', user);
    body.set('token', sessionStorage.getItem('token'));
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        .append('Authorization', `Basic ${sessionStorage.getItem('token')}`)
    };
    return this.http.post(`${this.net2phone}Integra/resources/auth/EndSession`, body.toString(), options);
  }

  createCall(call) {
    const body = new URLSearchParams();
    body.set('call', call);
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        .append('Authorization', `Basic ${sessionStorage.getItem('token')}`)
    };
    return this.http.post(`${this.net2phone}Integra/resources/Dialers/DialerTask`, body.toString(), options);
  }

  createCallTest(user, campaña) {
    const body = new URLSearchParams();
    body.set('from', user);
    body.set('to', sessionStorage.getItem('token'));
    body.set('campaigns', campaña);
    body.set('metrics', 'duration');
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        .append('Authorization', `Basic ${sessionStorage.getItem('token')}`)
    };
    return this.http.post(`${this.net2phone}Integra/resources/auth/EndSession`, body.toString(), options);
  }
}
