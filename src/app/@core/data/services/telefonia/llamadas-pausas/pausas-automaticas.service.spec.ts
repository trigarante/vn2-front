import { TestBed } from '@angular/core/testing';

import { PausasAutomaticasService } from './pausas-automaticas.service';

describe('PausasAutomaticasService', () => {
  let service: PausasAutomaticasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PausasAutomaticasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
