import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../../environments/environment";
import {EstadoUsuario} from "../../../interfaces/telefonia/llamadas-pausas/estadoUsuario";
const base_url = environment.SERVICIOS_TELEFONIA;

@Injectable({
  providedIn: 'root'
})
export class EstadoUsuarioService {

  constructor(private http: HttpClient) { }

  postInicioPausa(json) {
    return this.http.post<EstadoUsuario[]>(`${base_url}/llamadas-pausas/estado-usuario-inicio`, json);
  }

  putFinPausa(idEstadoUsuario) {
    return this.http.put<EstadoUsuario[]>(`${base_url}/llamadas-pausas/estado-usuario-fin/${idEstadoUsuario}`, null);
  }

}
