import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
const base_url = environment.SERVICIOS_TELEFONIA;
const idEmpleado = sessionStorage.getItem('Empleado');

@Injectable({
  providedIn: 'root'
})
export class SolicitudesvnTelefoniaService {

  constructor(private http: HttpClient) { }

  asignarEmpleadoSolicitud(idSolicitud) {
    return this.http.put(`${base_url}/solicitudes/asignar-empleado/${idSolicitud}/${idEmpleado}`, null);
  }

  getSolicitudes(fechaInicio, fechaFin) {
    return this.http.get(`${base_url}/solicitudes/empleado/${idEmpleado}/${fechaInicio}/${fechaFin}`);
  }

  getSolicitudById(idSolicitud) {
    return this.http.get(`${base_url}/solicitudes/solicitud-id/${idSolicitud}`);
  }

  desactivarContactaciones(idHistorico) {
    return this.http.put(`${base_url}/solicitudes/desactvar-contactaciones/${idHistorico}`, null);
  }


}
