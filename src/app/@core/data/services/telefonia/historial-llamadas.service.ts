import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HistorialLlamadasService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.SERVICIOS_TELEFONIA;
  }

  getHistorialLlamadasByIdSolicitud(idSolicitud: string) {
    return this.http.get<any[]>(`${this.baseURL}/llamada-salidas/historial-llamada/${idSolicitud}`);
  }

  getHistorialLlamadasByidRegistro(idRegistro: string) {
    return this.http.get<any[]>(`${this.baseURL}/llamada-salidas/historial-llamada/registro/${idRegistro}`);
  }
}
