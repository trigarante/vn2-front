import {EventEmitter, Injectable} from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PeticioneAmiService {
  baseUrl: any;

  constructor(private http: HttpClient) {
    this.baseUrl = environment.SERVICIOS_TELEFONIA_DATOS;
  }

  public respSolicitud: EventEmitter<any> = new EventEmitter<any>();

  getSolicitud(extension: string, numero: string) {
    return this.http.get(`${this.baseUrl}/ami/asterisk-ami/${extension}/${numero}`);
  }
}
