import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
const baseUrl = environment.SERVICIOS_TELEFONIA;

@Injectable({
  providedIn: 'root'
})
export class EstadoUsuarioTelefoniaService {

  constructor(private http: HttpClient) { }
  activarUsuario(idUser) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-activo/${idUser}`, null);
  }

  initUsuario(idUser) {
    return this.http.post(`${baseUrl}/estado-u-telefonia/inicio-usuario/${idUser}`, null);
  }

  usuarioInactivo(idUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-inactivo/${idUsuario}`, null);
  }

  usuarioInactivoCerrar(idUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/cerrar-ventana/${idUsuario}`, null);
  }

  usuarioPausa(idUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-pausado/${idUsuario}`, null);
  }

  getEstadoUsuario(idUsuario): Observable<any> {
    return this.http.get(`${baseUrl}/estado-u-telefonia/estado-by-usuario/${idUsuario}`);
  }

  getEstadoUsuarioVentana(idUsuario): Observable<any> {
    return this.http.get(`${baseUrl}/estado-u-telefonia/estado-ventana/${idUsuario}`);
  }

  cambiarEstadoVentana(idUsuario, json) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-ventana/${idUsuario}`, json);
  }

  cambiarEstado(idEstadoSesion, idUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/cambiar-estado/${idUsuario}/${idEstadoSesion}`, null);
  }

  inicioPausa(idUsuario, idMotivoPausa) {
    return this.http.post(`${baseUrl}/estado-u-telefonia/inicio-pausa/${idUsuario}/${idMotivoPausa}`, null);
  }

  finPausa(idPausaUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/fin-pausa/${idPausaUsuario}`, null);
  }

  enLlamada(idUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-en-llamada/${idUsuario}`, null);
  }
}
