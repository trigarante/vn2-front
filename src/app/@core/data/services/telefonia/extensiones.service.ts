import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Extensiones} from "../../interfaces/telefonia/extensiones";
import {Observable} from "rxjs";
const base_url = environment.SERVICIOS_TELEFONIA;

@Injectable({
  providedIn: 'root'
})
export class ExtensionesService {
  constructor(private http: HttpClient) { }

  getContactos(): Observable<Extensiones[]> {
    return this.http.get<Extensiones[]>(`${base_url}/transferencias`);
  }
}
