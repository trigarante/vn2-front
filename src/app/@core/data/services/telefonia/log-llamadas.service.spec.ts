import { TestBed } from '@angular/core/testing';

import { LogLlamadasService } from './log-llamadas.service';

describe('LogLlamadasService', () => {
  let service: LogLlamadasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LogLlamadasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
