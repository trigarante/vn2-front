import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../../../environments/environment";
import {FormaPago, FormaPagoData} from "../../interfaces/venta-nueva/catalogos/forma-pago";


@Injectable()
export class FormaPagoService extends FormaPagoData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.CORE_URL;
  }
  get(): Observable<FormaPago[]> {
    return this.http.get<FormaPago[]>(this.baseURL + 'v1/forma-pago');
  }

  getActivos(): Observable<FormaPago[]> {
    return this.http.get<FormaPago[]>(this.baseURL + 'v1/forma-pago/activos');
  }
  post(formaPago): Observable<FormaPago> {
    return this.http.post<FormaPago>(this.baseURL + 'v1/forma-pago', formaPago );
  }
  getFormaPagoById(idFormaPago): Observable<FormaPago> {
    return this.http.get<FormaPago>(this.baseURL + 'v1/forma-pago' + idFormaPago);
  }
  put(idformaPago, formaPago): Observable<FormaPago> {
    return this.http.put<FormaPago>(this.baseURL + 'v1/forma-pago' + idformaPago, formaPago);
  }
}
