import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TipoRamo} from '../../interfaces/comerciales/catalogo/tipo-ramo';

@Injectable({
  providedIn: 'root'
})
export class SepomexService {
  private baseURLVn2;
  private baseURLGnp;
  constructor(
    private http: HttpClient
  ) {
    this.baseURLVn2 = environment.CORE_URL + '/sepomex';
    this.baseURLGnp = environment.CATALOGO_AUTOSGNPDEV + 'v3/gnp-autos/cpValidate';
  }

  getColoniaByCp(cp): Observable<any[]> {
      const headers = new HttpHeaders().append('cp', cp.toString());
      return this.http.get<any[]>(this.baseURLVn2, {headers});
  }
  getColoniaByCpServer(cp): Observable<any[]> {
    const headers = new HttpHeaders().append('cp', cp.toString());
    return this.http.get<any[]>(this.baseURLVn2 + '/server', {headers});
  }
  getCpValido(cp): Observable<any> {
    // const params = new HttpParams().set('cp', cp.toString());
    // return this.http.get<any>(this.baseURLGnp, {params});
    // peticion desde el back
    const headers = new HttpHeaders().append('cp', cp.toString());
    return this.http.get<any>(this.baseURLVn2 + '/cpValido', {headers});
  }
}
