import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {TipoContactoVn} from "../../interfaces/catalogos/tipo-contacto-vn";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TipoContactoVnService {

  private baseURL;

  constructor( private http: HttpClient ) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<TipoContactoVn[]> {
    return this.http.get<TipoContactoVn[]>(this.baseURL + 'v1/tipoContactoVn');
  }

  post(contacto: TipoContactoVn): Observable<TipoContactoVn> {
    return this.http.post<TipoContactoVn>(this.baseURL + 'v1/tipoContactoVn', contacto);
  }

  getContactoById(idContacto): Observable<TipoContactoVn> {
    return this.http.get<TipoContactoVn>(this.baseURL + 'v1/tipoContactoVn/' + idContacto);
  }

  put(idContacto, contacto: TipoContactoVn): Observable<TipoContactoVn> {
    return this.http.put<TipoContactoVn>(this.baseURL + 'v1/tipoContactoVn/' + idContacto + '/'
      + sessionStorage.getItem('Empleado'), contacto);
  }
}
