import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Sede} from "../../interfaces/catalogos/sede";

@Injectable({
  providedIn: 'root'
})
export class SedeService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Sede[]> {
    return this.http.get<Sede[]>(this.baseURL + 'v1/sedes');
  }

  post(sede: Sede): Observable<Sede> {
    return this.http.post<Sede>(this.baseURL + 'v1/sedes', sede);
  }

  getSedeById(idSede): Observable<Sede> {
    return this.http.get<Sede>(this.baseURL + 'v1/sedes/' + idSede);
  }

  put(idSede, sede: Sede): Observable<Sede> {
    return this.http.put<Sede>(this.baseURL + 'v1/sedes/' + idSede + '/'
      + sessionStorage.getItem('Empleado'), sede);
  }
}
