import { Injectable } from '@angular/core';
import Menu from '../interfaces/menu/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  permisos = JSON.parse(window.localStorage.getItem('User'));
  menu: Menu[] = [
    {
        icon: 'arrow_right',
        name: 'Solicitudes',
        permiso: true,
        children: [
          { rutas: 'venta-nueva/solicitudes-interna', icon: '', name: 'Interno', permiso: this.permisos.VN?.interno },
          // { rutas: 'venta-nueva/solicitudes', icon: '', name: 'Predictivo', permiso: this.permisos.VN?.predictivo },
          { rutas: 'venta-nueva/solicitudes-vn', icon: '', name: 'Solicitudes', permiso: this.permisos.VN?.out},
          // { rutas: 'telefonia/predictivo-configuraciones', icon: '', name: 'Configuraciones Predictivo', permiso: true },
          // { name: 'Outbound', icon: 'arrow_right', permiso: true, grandChildren: [
          //   { rutas: 'venta-nueva/solicitudes-vn/no-contactado', icon: '', name: 'No Contactado', permiso: this.permisos.VN?.out},
          //   { rutas: 'venta-nueva/solicitudes-vn/contactado', icon: '', name: 'Contactado', permiso: this.permisos.VN?.out},
          // ]}
          // {rutas: 'telefonia/monitoreo', icon: '', name: 'Monitoreo' , permiso:"monitoreo"},
          // { rutas : 'telefonia/llamadas-in', icon: '', name: 'Llamadas In Actuales', permiso:"in"},
          // { rutas : 'telefonia/llamadas-out', icon: '', name: 'Llamadas Out Actuales' , permiso:"out"},
          // { rutas : 'telefonia/directorio-aseguradora', icon: '', name: 'Directorio aseguradora', permiso:"directorio"},
        ]
    },
    {
        name: 'Administración de Pólizas',
        rutas: 'venta-nueva/administracion-polizas',
        permiso: this.permisos.VN?.adminPolizas
    },
    {
      name: 'Solicitud de Pago',
      rutas: 'venta-nueva/link-pago',
      permiso: true
    },

    // {
    //     icon: 'corporate_fare',
    //     name: 'Directorio de Aseguradoras',
    //     rutas: '',
    // },
    // {
    //     icon: 'corporate_fare',
    //     name: 'Monitoreo Outbound ',
    //     rutas: '',
    // },
    // {
    //     icon: 'corporate_fare',
    //     name: 'Activacion de Empleado',
    //     rutas: '',
    // },
    // {
    //     icon: 'corporate_fare',
    //     name: 'Carrusel Configuracion',
    //     rutas: '',
    // },
    // {
    //     icon: 'corporate_fare',
    //     name: 'Solicitudes Desativacion',
    //     rutas: '',
    // },
    {
        icon: 'arrow_right',
        name: 'Autorización de Errores',
        permiso: true,
        children:
          [
          { name: 'Normales', icon: 'arrow_right', permiso: true, grandChildren: [
              { rutas: 'venta-nueva/autorizacion-errores/1', icon: '', name: 'Pendientes', permiso: this.permisos.VN?.autoErrores },
              { rutas: 'venta-nueva/autorizacion-errores/2', icon: '', name: 'En Proceso', permiso: this.permisos.VN?.autoErrores },
              { rutas: 'venta-nueva/autorizacion-errores/3', icon: '', name: 'Corregidas', permiso: this.permisos.VN?.autoErrores},
            ]
          },
          { name: 'Desviadas', icon: 'arrow_right', permiso: true, grandChildren: [
            { rutas: 'venta-nueva/autorizacion-errores/4', icon: '', name: 'Pendientes', permiso: this.permisos.VN?.polizasDesviadas },
            { rutas: 'venta-nueva/autorizacion-errores/5', icon: '', name: 'En Proceso', permiso: this.permisos.VN?.polizasDesviadas },
            { rutas: 'venta-nueva/autorizacion-errores/6', icon: '', name: 'Corregidas', permiso: this.permisos.VN?.polizasDesviadas},
            ]
          },
          ]
    },
    {
      icon: 'arrow_right',
      name: 'Corrección de errores',
      permiso: true,
      children: [
        { name: 'Autorización', icon: 'arrow_right', permiso: true, grandChildren: [
            { rutas: 'venta-nueva/correccion-errores/documentos/autorizacion', icon: '', name: 'Documentos', permiso: this.permisos.VN?.corrErrDocs },
            { rutas: 'venta-nueva/correccion-errores/datos/autorizacion', icon: '', name: 'Datos', permiso: this.permisos.VN?.corrErrDatos },
          ]
        },
        { name: 'Verificación', icon: 'arrow_right', permiso: true, grandChildren: [
            { rutas: 'venta-nueva/correccion-errores/documentos/verificacion', icon: '', name: 'Documentos', permiso: this.permisos.VN?.corrErrDocs },
            { rutas: 'venta-nueva/correccion-errores/datos/verificacion', icon: '', name: 'Datos', permiso: this.permisos.VN?.corrErrDatos },
          ]
        },
      ]
    },
    {
        icon: '',
        name: 'Verificaciones Pendientes',
        rutas: 'venta-nueva/pendientes-verificacion',
        permiso: this.permisos.VN?.verifPendientes
    },
    {
        icon: '',
        name: 'Activación de Empleado',
        rutas: 'venta-nueva/activacion-empleados',
        permiso: this.permisos.VN?.activEmpleado
    },
    {
        icon: '',
        name: 'Solicitudes Desactivación',
        rutas: 'venta-nueva/solicitudes-desactivacion',
        permiso: this.permisos.VN?.solDesactiv
    },
    // {
    //     icon: 'corporate_fare',
    //     name: 'Mi Estado de Cuenta',
    //     rutas: '',
    // },
    // {
    //   icon: 'drag_handle',
    //   name: 'Catalogos',
    //   children: [
    //     { rutas: '', icon: '', name: 'Tipo Contacto' },
    //     { rutas: '', icon: '', name: 'Tipo Pago' },
    //   ]
    // },
    // {
    //   icon: 'drag_handle',
    //   name: 'Reportes',
    //   children: [
    //     { rutas: '', icon: '', name: 'Panel' },
    //     { rutas: '', icon: '', name: 'Contactos' },
    //     { name: 'Reporte,Conversion y Venta', icon: 'drag_handle', grandChildren: [
    //         { rutas: '', icon: '', name: 'Productividad' },
    //         { rutas: '', icon: '', name: 'Conversion Aseguradora' },
    //         { rutas: '', icon: '', name: 'Conversion Campaña' },
    //         { rutas: '', icon: '', name: 'Reporte de Cobrado VS Aplicado' },
    //       ]
    //     },
    //     { rutas: '', icon: '', name: 'Reporte Operativo' },
    //   ]
    // },
    // {
    //   icon: 'corporate_fare',
    //   name: 'Correccion de Polizas ',
    //   rutas: '',
    // },
    {
      icon: '',
      name: 'Endosos ',
      rutas: 'venta-nueva/endosos',
      permiso: this.permisos.VN?.endosos
    },
    // {
    //   icon: '',
    //   name: 'Directorio de aseguradoras',
    //   rutas: 'telefonia/directorio-aseguradora',
    //   permiso: this.permisos.VN?.aseguradora
    // },
    // {
    //   icon: '',
    //   name: 'Monitoreo de personal',
    //   rutas: 'telefonia/monitoreo',
    //   permiso: this.permisos?.VN?.monitoreo
    // },
    // {
    //   icon: '',
    //   name: 'Llamadas recibidas',
    //   rutas: 'telefonia/historico-ib',
    //   permiso: this.permisos.VN?.llamadas
    // },
    {
      icon: 'arrow_right',
      name: 'Reportes',
      permiso: true,
      children: [
        { name: 'Reporte Conversión y venta', icon: 'arrow_right', permiso: true, grandChildren: [
            { rutas: 'venta-nueva/reportes/conversion-venta/productividad', icon: '', name: 'Productividad', permiso: this.permisos.VN?.productividad },
          ]
        },
      ]
    },
    {
      icon: '',
      name: 'Mi estado de cuenta',
      rutas: 'venta-nueva/mi-estado-cuenta',
      permiso: this.permisos.VN?.miEstadoCuenta
    },
    {
      icon: '',
      name: 'Esquemas comisión',
      rutas: 'venta-nueva/esquemas-comision',
      permiso: this.permisos.VN?.esquemaComision
    },


    // {
    //   icon: '',
    //   name: 'Tabla cascaron',
    //   rutas: 'venta-nueva/tabla-cascaron'
    // },
    // {
    //   icon: 'drag_handle',
    //   name: 'Estado Ejecutivos',
    //   children: [
    //     { rutas: '', icon: '', name: 'En Linea' },
    //     { rutas: '', icon: '', name: 'En Pausa' },
    //     { rutas: '', icon: '', name: 'En Llamada entrante'},
    //     { rutas: '', icon: '', name: 'En Llamada salida'},
    //   ]
    // },
    // {
    //   icon: 'corporate_fare',
    //   name: 'Monitoreo de llamadas',
    //   rutas: '',
    // },
  ];

  getMenu(): Menu[]{
    return this.menu;
  }

}
