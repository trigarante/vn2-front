import { TestBed } from '@angular/core/testing';

import { TrigaranteEscuchaService } from './trigarante-escucha.service';

describe('TrigaranteEscuchaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrigaranteEscuchaService = TestBed.get(TrigaranteEscuchaService);
    expect(service).toBeTruthy();
  });
});
