import { TestBed } from '@angular/core/testing';

import { RepuveServiceService } from './repuve-service.service';

describe('RepuveServiceService', () => {
  let service: RepuveServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RepuveServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
