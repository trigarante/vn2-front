import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RepuveServiceService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.REPUVE;
  }

  getRepuve(placa): Observable<any> {
    const token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiZ3VhZGFycmFtYUBhaG9ycmEuaW86NCIsImV4cCI6MjY1MjMyNTczMCwiaWF0IjoxNjUyMzI1NzMxfQ.qCgfLs148JNQdCde8fF_i0Hpj6xzbrosK_pEzx8P0rUFEQXSTjezY8NIHObkzGnJTXhsVJW-901awDLRW6zwOA';
    return this.http.get<any>(this.baseURL + '/' + placa, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  }
}
