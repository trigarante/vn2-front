import {Observable, Subject} from "rxjs";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {SesionUsuario} from "../../interfaces/sesiones/sesion-usuario.interface";

@Injectable({
  providedIn: 'root',
})
export class SesionUsuarioService{
  private baseURL;
  private estadoSesionListener = new Subject<number>();

  constructor(private http: HttpClient) {
    this.baseURL = environment.LOGIN_URL;
  }

  put(idSesion, idEstado): Observable<any> {
    this.estadoSesionListener.next(idEstado);
    return this.http.put<any>(`${this.baseURL}/auth1/cambio-estado`, {
      id: idSesion,
      idEstadoSesion: idEstado,
    });
  }

  getEstadoSesionById(idSesion): Observable<SesionUsuario[]> {
    return this.http.get<SesionUsuario[]>(`${this.baseURL}/auth1/${idSesion}`);
  }

  getestadoSesionListener() {
    return this.estadoSesionListener.asObservable();
  }
}
