import { TestBed } from '@angular/core/testing';

import { MotivosDesactivacionService } from './motivos-desactivacion.service';

describe('MotivosDesactivacionService', () => {
  let service: MotivosDesactivacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotivosDesactivacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
