import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SolicitudesCarruselService {

  url = environment.CORE_URL + '/solicitudes-carrusel';
  constructor(private http: HttpClient) { }

  post(data) {
    return this.http.post(this.url, data);
  }

  getByIdEstado(idEstado): Observable<any> {
    const headers = new HttpHeaders().append('id', idEstado.toString()).append('idempleado', sessionStorage.getItem('Empleado'));
    return this.http.get<any>(this.url + '/byIdEstado', {headers});
  }

  update(data) {
    return this.http.put(this.url, data);
  }
}
