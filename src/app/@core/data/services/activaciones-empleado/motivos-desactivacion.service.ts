import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class MotivosDesactivacionService {
  url = environment.CORE_URL + '/motivos-desactivacion';
  constructor(private http: HttpClient) { }

  getActivosByIdTipo(tipo) {
    const headers = new HttpHeaders().append('tipo', tipo.toString());
    return this.http.get(this.url, {headers});
  }
}
