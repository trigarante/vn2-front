import { TestBed } from '@angular/core/testing';

import { SocketsVn2Service } from './sockets-vn2.service';

describe('SocketsVn2Service', () => {
  let service: SocketsVn2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SocketsVn2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
