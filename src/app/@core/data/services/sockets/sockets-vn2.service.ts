import { Injectable } from '@angular/core';
import {Socket} from "ngx-socket-io";
import {environment} from "../../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SocketsVn2Service extends Socket {

  constructor() {
    super({ url: 'RUTA', options: {
        transports: ['websocket'],
        reconnection: true,
      },
    });
    this.connectError();
  }

  connectError() {
    let counter = 0;
    this.on("connect_error", (error) => {
      setTimeout(() => {
        this.connect();
        counter = counter + 1;
        if (counter === 3) {
          this.cerrarSocket();
          console.log('Se cerraron los sockets debido a que no hubo una conexión estable');
        }
      }, 3000);
    });
  }

  cerrarSocket() {
    this.disconnect();
    console.log('Todos los sockets se cerraron');
  }
}
