import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class CotizarService {

  constructor(private http: HttpClient) {}

  cotizar(c: any): Observable<Object> {
    return this.http.post<any>(
      `${environment.CORE_URL}/cotizar`, c);
  }
  emitir(c: any): Observable<Object> {
    return this.http.post<Object>(
      `${environment.CORE_URL}/emitir`, c);
  }

  getAutentoken(): Observable<JSON> {
    return this.http.post<JSON>(
      `${environment.CORE_URL}v1/authenticate`, {"username": "trigarante", "password": "trigarante2020"});
  }
}
