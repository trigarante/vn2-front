import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {TicketsInspeccion} from "../../interfaces/cotizador/ticketsInspeccion";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TicketsInspeccionService {
  private serviciosNode;

  constructor(private http: HttpClient) {
    this.serviciosNode = environment.CORE_URL + '/ticketsInspeccion';
  }
  post(ticketsInspeccion: TicketsInspeccion ): Observable<TicketsInspeccion> {
    return this.http.post<TicketsInspeccion>(this.serviciosNode, ticketsInspeccion);
  }
  // trae todas las solicitudes
  getSolicitudById(idSolicitud: number): Observable<TicketsInspeccion[]> {
    return this.http.get<TicketsInspeccion[]>(this.serviciosNode + '/bySolicitud/' + idSolicitud);
  }
  // trae una solicitud vn
  getSolicitudVNById(idSolicitud: number): Observable<TicketsInspeccion> {
    return this.http.get<TicketsInspeccion>(this.serviciosNode + '/bySolicitudVn/' + idSolicitud);
  }
  getByTicket(ticket: number): Observable<TicketsInspeccion> {
    return this.http.get<TicketsInspeccion>(this.serviciosNode + '/byTicket/' + ticket);
  }
  put(ticketsInspeccion: TicketsInspeccion, id: number): Observable<TicketsInspeccion> {
    return this.http.put<TicketsInspeccion>(this.serviciosNode + '/UpdateTickets/' + id, ticketsInspeccion );
  }
  putInpeccionPreviadesactivado( id: number): Observable<TicketsInspeccion> {
    return this.http.put<TicketsInspeccion>(this.serviciosNode + '/UpdateInspeccionPrevia/' + id, '' );
  }
}
