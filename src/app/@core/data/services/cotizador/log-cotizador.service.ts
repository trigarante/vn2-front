import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";
import {TicketsInspeccion} from "../../interfaces/cotizador/ticketsInspeccion";
import {LogGnpEmision} from "../../interfaces/cotizador/logGnpEmision";

@Injectable({
  providedIn: 'root'
})
export class LogCotizadorService {
  private baseURL;
  private serviciosNode;

  constructor(private http: HttpClient) {
    this.serviciosNode = environment.CORE_URL + '/logCotizador';
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  gnpemitir( jsonEmision): Observable<JSON> {

    return this.http.post<JSON>(this.serviciosNode  + '/logGnpEmision', jsonEmision);
  }

  gnppago( jsonEmision): Observable<JSON> {

    return this.http.post<JSON>( this.serviciosNode + '/logGnpPago', jsonEmision);
  }

  qualitasemitir( jsonEmision): Observable<JSON> {

    return this.http.post<JSON>(this.serviciosNode + '/logQualitasEmision', jsonEmision);
  }

  qualitaspago( jsonEmision): Observable<JSON> {

    return this.http.post<JSON>( this.serviciosNode + '/logQualitasPago', jsonEmision);
  }

  getGnpEmision(id: number): Observable<LogGnpEmision[]> {
    return this.http.get<LogGnpEmision[]>(this.serviciosNode + '/bySolicitudEmisionGnp/' + id);
  }

  mapfreEmitir( jsonEmision): Observable<JSON> {

    return this.http.post<JSON>(this.serviciosNode + '/logMapfreEmision', jsonEmision);
  }

  mapfrePago( jsonEmision): Observable<JSON> {

    return this.http.post<JSON>( this.serviciosNode + '/logMapfrePago', jsonEmision);

  }
}
