import { TestBed } from '@angular/core/testing';

import { TicketsInspeccionService } from './tickets-inspeccion.service';

describe('TicketsInspeccionService', () => {
  let service: TicketsInspeccionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TicketsInspeccionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
