import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {MarcaInterno} from "../../interfaces/cotizador/marca-interno";
import {DetalleInterno} from "../../interfaces/cotizador/detalle-interno";
import {ModeloInterno} from "../../interfaces/cotizador/modelo-interno";
import {DescripcionInterno} from "../../interfaces/cotizador/descripcion-interno";
import {SubdescripcionInterno} from "../../interfaces/cotizador/subdescripcion-interno";

@Injectable({
  providedIn: 'root',
})
export class CatalogoInternoService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_URL;
  }

  getMarcas(idTipoSubRamo: number): Observable<MarcaInterno[]> {
    const headers = new HttpHeaders().append('id', idTipoSubRamo.toString());
    return this.http.get<MarcaInterno[]>(this.baseURL + '/marca-interno/getByIdTipoSubramo/', {headers});
  }

  getModelos(): Observable<ModeloInterno[]> {
    return this.http.get<ModeloInterno[]>(this.baseURL + '/modelo-interno/getByIdTipoSubramo');
  }

  getDescripcion(idMarca: number, idModelo: number): Observable<DescripcionInterno[]> {
    const headers = new HttpHeaders().append('idmarca', idMarca.toString()).append('idmodelo', idModelo.toString());
    return this.http.get<DescripcionInterno[]>(this.baseURL + '/detalle-interno/getByIdMarcaAndIdModelo', {headers});
  }

  getSubdescripcion(idMarca: number, idModelo: number, idDescripcion: number): Observable<SubdescripcionInterno[]> {
    const headers = new HttpHeaders().append('idmarca', idMarca.toString()).append('idmodelo', idModelo.toString())
      .append('iddescripcion', idDescripcion.toString());
    return this.http.get<SubdescripcionInterno[]>(this.baseURL + '/detalle-interno/getByIdMarcaAndIdModeloAndIdDescripcion', {headers});
  }

  getDetalles(idMarca: number, idModelo: number, idDescripcion: number, idSubDescripcion: number): Observable<DetalleInterno[]> {
    const headers = new HttpHeaders().append('idmarca', idMarca.toString()).append('idmodelo', idModelo.toString())
      .append('iddescripcion', idDescripcion.toString()).append('idSubDescripcion', idSubDescripcion.toString());
    return this.http.get<DetalleInterno[]>(this.baseURL + '/detalle-interno/getByIdMarcaAndIdModeloAndIdDescripcionAndIdSubDescripcion',
      {headers});
  }

}
