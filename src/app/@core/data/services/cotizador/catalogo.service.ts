import {Injectable} from '@angular/core';
import {Auten} from '../../interfaces/cotizador/catalogo';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {CotizarData, CotizarOnlineResponse} from "../../interfaces/cotizador/cotizar";
import {MarcasOnline} from "../../interfaces/cotizador/marcas-online";
import {DescripcionesOnline} from "../../interfaces/cotizador/descripciones-online";
import {ModelosOnline} from "../../interfaces/cotizador/modelos-online";
import {SubmarcasOnline} from "../../interfaces/cotizador/submarcas-online";
import { InspeccionResp, TicketStatus } from '../../interfaces/venta-nueva/Inspeccion.interface';

@Injectable({
  providedIn: 'root',
})
export class CatalogoService {

  constructor(private http: HttpClient) {

  }
  // getAutentoken(): Observable<Auten> {
  //   return this.http.post<Auten>(
  //     environment.CATALOGO_AUTOS + 'v1/authenticate', {"username": "trigarante", "password": "trigarante2020"});
  // }
  // emitir( accessToken: string, jsonEmision):  Observable<JSON> {
  //   const reqHeader = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + accessToken,
  //   });
  //   return this.http.post<JSON>(environment.CATALOGO_AUTOS + 'v1/extras/emitir/'
  //     , jsonEmision, { headers: reqHeader } );
  // }
  // postPago(aseguradora: string, idSolicitud: number, accessToken: string, jsonPago):
  //   Observable<JSON> {
  //   const reqHeader = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + accessToken,
  //   });
  //   return this.http.post<JSON>(environment.CATALOGO_AUTOS + 'v1/' + aseguradora +
  //     '/pagos?&idSolicitud=' + idSolicitud , jsonPago, { headers: reqHeader });
  //
  // }
  // getCotizacion(aseguradora: string, accessToken: string, jsonCotizacion):  Observable<JSON> {
  //   const reqHeader = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + accessToken,
  //   });
  //
  //   return this.http.post<JSON>(environment.CATALOGO_AUTOS + 'v1/' + aseguradora + '/cotizaciones'
  //     , jsonCotizacion, { headers: reqHeader } );
  // }

  emitir(aseguradora: string, urlEmision: string, jsonEmision): Observable<JSON> {

    return this.http.post<JSON>( environment[aseguradora] + urlEmision,
      jsonEmision, { headers:
          {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });
  }
  postPago(aseguradora: string, urlPago: string, jsonPago):
    Observable<JSON> {
    return this.http.post<JSON>(environment[aseguradora] + urlPago, jsonPago, { headers:
        {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });

  }
  pdf(aseguradora: string, urldocumentos: string, poliza: string): Observable<string> {
    return this.http.get<string>(environment[aseguradora] + urldocumentos +
      '/imprimir?noPoliza=' + poliza, { headers: null,  responseType: 'text' as 'json'} );
  }

  getCotizacion(aseguradora: string, urlCotizacion: string, jsonCotizacion): Observable<JSON> {
    return this.http.post<JSON>( environment[aseguradora] + urlCotizacion, jsonCotizacion,
      { headers:
          {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });
  }
  getMarcas(aseguradora: string, catalogo: string, subCatalogo: string): Observable<MarcasOnline[]> {
    return this.http.get<MarcasOnline[]>(environment[aseguradora] + catalogo + subCatalogo);
  }

  getModelos(aseguradora: string, catalogo: string, marca: string, subCatalogo: string): Observable<ModelosOnline[]> {
    return this.http.get<ModelosOnline[]>( environment[aseguradora] + catalogo  + subCatalogo +
      '?&marca=' + marca);
  }

  getSubMarca(aseguradora: string, catalogo: string, marca: string, modelo: string, subCatalogo: string): Observable<SubmarcasOnline[]> {
    return this.http.get<SubmarcasOnline[]>(environment[aseguradora] + catalogo + subCatalogo +
      '?marca=' + marca + '&modelo=' + modelo);
  }

  getDescripcion(aseguradora: string, catalogo: string, marca: string, modelo: string, submarca: string, subCatalogo: string):
    Observable<DescripcionesOnline[]> {
    return this.http.get<DescripcionesOnline[]>(    environment[aseguradora] + catalogo + subCatalogo +
      '?marca=' + marca + '&modelo=' + modelo + '&submarca=' + submarca);

  }

  // gnp catalogos
  // // gnp bancos
  getBancosExterno(aseguradora: string, urlBanco: string): Observable<JSON> {
    return this.http.get<JSON>(environment[aseguradora] + urlBanco);
  }
  getValidarTarjeta(tarjeta: string): Observable<any> {
    return this.http.get<any>(environment.CATALOGO_AUTOSGNP + 'pago_gnp_3D/v2/validate-card', {
      params: {
        card: tarjeta
      }
    });
  }
  getTipoTarjetasExterno(aseguradora: string, urlTipoTarjetas: string): Observable<JSON> {
    return this.http.get<JSON>(environment[aseguradora] + urlTipoTarjetas);
  }
  getMsiExterno(aseguradora: string, urlMsi: string, numTarjeta: string): Observable<JSON> {
    return this.http.get<JSON>('https://web-gnp.mx/pago_gnp_3D/obtener_planes_pago?bin=' + numTarjeta);
  }

  postValidacionTarjeta(aseguradora: string, urlValidarTarjeta: string, jsonValidar):
    Observable<JSON> {
    return this.http.post<JSON>(environment[aseguradora] + urlValidarTarjeta, jsonValidar, { headers:
        {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });

  }
  postObtenerEmision(aseguradora: string, urlObtenerEmision: string, jsonObtener):
    Observable<JSON> {
    return this.http.post<JSON>(environment[aseguradora] + urlObtenerEmision, jsonObtener, { headers:
        {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });

  }
  pdfgnp(aseguradora: string, urldocumentos: string, poliza: string): Observable<any> {
    return this.http.post<any>(  'https://web-gnp.mx/' + urldocumentos +
      '/impresion?numeroPoliza=' + poliza, null, { headers:
        {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });
  }
 // // ejemplo de inpeccion previa
 //  getInpeccionPrevia(body): Observable<JSON> {
 //    const reqHeader = new HttpHeaders({
 //      'Content-Type': 'application/json',
 //      'Access-Control-Allow-Origin': '*',
 //      Authorization: 'Basic' + btoa('genesys-api:dl.#LG3n4p1')
 //    });
 //    return this.http.post<JSON>('https://qa.qualitas.com.mx:49121/genesis-api/rest/inspeccion-vehicular/enlace-personalizado', body,
 //      { headers: reqHeader });
 //  }

  // ejemplo de inpeccion previa
  getInpeccionPrevia(body): Observable<InspeccionResp> {
    const reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: 'Basic ' + btoa('genesys-api:dl.#LG3n4p1')
    });
    return this.http.post<InspeccionResp>('https://ws-qualitas.com/v1/pre/inspection/enlace', body,
      { headers: reqHeader });
  }

  // getInpeccionPrevia(body): Observable<JSON> {
  //   return this.http.post<JSON>('https://qa.qualitas.com.mx:49121/genesis-api/rest/inspeccion-vehicular/enlace-personalizado', body,
  //     { headers:
  //         {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });
  // }

  // getCotizacionCoberturas(aseguradora: string, urlCotizacion: string, jsonCotizacion): Observable<JSON> {
  //   return this.http.post<JSON>( environment[aseguradora] + urlCotizacion, jsonCotizacion,
  //     { headers:
  //         {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });
  // }
  getCotizacionCoberturas(aseguradora: string, urlCotizacion: string, jsonCotizacion): Observable<JSON> {
    return this.http.post<JSON>( environment[aseguradora] + urlCotizacion, jsonCotizacion,
      { headers:
          {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });
  }

  getEmisionCoberturas(aseguradora: string, urlCotizacion: string, jsonCotizacion): Observable<JSON> {
    return this.http.post<JSON>( environment[aseguradora] + urlCotizacion, jsonCotizacion,
      { headers:
          {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });
  }

  getTicketInspeccion(ticket, agente): Observable<TicketStatus> {
    const reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: 'Basic ' + btoa('genesys-api:dl.#LG3n4p1')
    });
    return this.http.get<TicketStatus>('https://ws-qualitas.com/v1/pre/inspection/consulta?ticket=' + ticket  + '&agente=' + agente,
      { headers: reqHeader });
  }
}
