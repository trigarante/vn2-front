import { TestBed } from '@angular/core/testing';

import { LogCotizadorService } from './log-cotizador.service';

describe('LogCotizadorService', () => {
  let service: LogCotizadorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LogCotizadorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
