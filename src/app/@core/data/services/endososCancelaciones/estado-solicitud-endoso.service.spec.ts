import { TestBed } from '@angular/core/testing';

import { EstadoSolicitudEndosoService } from './estado-solicitud-endoso.service';

describe('EstadoSolicitudEndosoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstadoSolicitudEndosoService = TestBed.get(EstadoSolicitudEndosoService);
    expect(service).toBeTruthy();
  });
});
