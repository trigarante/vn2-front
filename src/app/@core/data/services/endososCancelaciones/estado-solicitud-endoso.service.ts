import { Injectable } from '@angular/core';
// import {
//   EstadoSolicitudEndoso,
//   EstadoSolicitudEndosoData,
// } from '../../../interfaces/endososCancelaciones/catalogos/estadoSolicitudEndoso';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

class EstadoSolicitudEndosoData {
}

class EstadoSolicitudEndoso {
}

@Injectable()
export class EstadoSolicitudEndosoService extends EstadoSolicitudEndosoData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;

  }
  get(): Observable<EstadoSolicitudEndoso[]> {
    return this.http.get<EstadoSolicitudEndoso[]>(this.baseURL + 'v1/estado-solicitud-endoso');
  }

  post(estadoSolicitudEndoso: EstadoSolicitudEndoso ): Observable<EstadoSolicitudEndoso> {
    return this.http.post<EstadoSolicitudEndoso>(this.baseURL + 'v1/estado-solicitud-endoso', estadoSolicitudEndoso);
  }

  getEstadoSolicitudEndosoById(idEstadoSolicitudEndoso): Observable<EstadoSolicitudEndoso> {
    return this.http.get<EstadoSolicitudEndoso>(this.baseURL + 'v1/estado-solicitud-endoso/' + idEstadoSolicitudEndoso);
  }

  put(idEstadoSolicitudEndoso, estadoSolicitudEndoso: EstadoSolicitudEndoso): Observable<EstadoSolicitudEndoso> {
    return this.http.put<EstadoSolicitudEndoso>(this.baseURL + 'v1/estado-solicitud-endoso/'
      + idEstadoSolicitudEndoso + '/' + sessionStorage.getItem('Empleado'), estadoSolicitudEndoso);
  }
}
