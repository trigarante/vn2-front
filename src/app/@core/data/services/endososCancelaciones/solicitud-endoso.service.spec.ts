import { TestBed } from '@angular/core/testing';

import { SolicitudEndosoService } from './solicitud-endoso.service';

describe('SolicitudEndosoService', () => {
  let service: SolicitudEndosoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SolicitudEndosoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
