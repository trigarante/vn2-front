import { TestBed } from '@angular/core/testing';

import { LogueoAppService } from './logueo-app.service';

describe('LogueoAppService', () => {
  let service: LogueoAppService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LogueoAppService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
