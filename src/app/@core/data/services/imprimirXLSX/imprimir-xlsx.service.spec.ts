import { TestBed } from '@angular/core/testing';

import { ImprimirXlsxService } from './imprimir-xlsx.service';

describe('ImprimirXlsxService', () => {
  let service: ImprimirXlsxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImprimirXlsxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
