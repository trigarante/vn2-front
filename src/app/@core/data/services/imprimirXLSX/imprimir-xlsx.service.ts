import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
@Injectable({
  providedIn: 'root'
})
export class ImprimirXlsxService {

  constructor() { }

  imprimirXLSX(data: any, headers: string[], title: string) {
    // tslint:disable-next-line:variable-name
    const max_filas_por_hoja = 64000;
    let pagina = null;
    const  hojaDeCalculo = XLSX.utils.book_new();
    const paginas = (Math.floor(data.length / max_filas_por_hoja)) + 1;
    for (let i = 0; i < paginas; i++) {
      pagina = XLSX.utils.json_to_sheet(data.slice(i * max_filas_por_hoja, (i + 1) * max_filas_por_hoja),
        // Lista de columnas a incluir en el archivo de Excel
        {
          header: headers });
      XLSX.utils.book_append_sheet(hojaDeCalculo, pagina, 'Trigarante_' + (i + 1) );
    }
    // construye el nombre del archivo, recuerda que no todos los componentes tienen titulo component
    let nombreArchivo = (title + '.xls').toLocaleLowerCase();
    nombreArchivo = nombreArchivo.replace(/ /g, '-').replace(/:/g, '-');

    XLSX.writeFile(hojaDeCalculo, nombreArchivo || ('SheetJSTableExport.' + ('xls')));
  }
}
