export interface SolicitudEndosoPendiente {
  id: number;
  idRegistro: number;
  idEmpleado: number;
  fechaSolicitud: Date;
  activo: number;
  poliza: string;
  estadoSolicitudDesc: string;
  estadoEndoso: string;
  nombreSolicitante: string;
  apellidoPaternoSolicitante: string;
  apellidoMaternoSolicitante: string;
  idsubareaRegistro: number;
  nombreEmisor: string;
  apellidoPaternoEmisor: string;
  apellidoMaternoEmisor: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  idSubarea: number;
  subarea: string;
}
