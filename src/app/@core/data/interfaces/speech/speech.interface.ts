export interface SpeechInterface {
  id?: number;
  idSubArea?: number;
  speech: string;
  idEstadoSpeech?: number;
  idEmpleadoCreador?: number;
  idEmpleadoAtorizo?: number;
  titulo?: string;
  idEmpresa?: number;
  idGrupo?: number;
  idSede?: string;
  fechaCreacion?: Date;
  fechaAutorizacion?: Date;
  fechaInhabilitacion?: Date;
  fechaDesautorizacion?: Date;
}

export interface SpeechInterfaceView {
  id: number;
  subarea: string;
  speech: string;
  idEstadoSpeech: number;
  descripcion: string;
  nombreEmpleadoCreador: string;
  appEmpleadoCreador: string;
  apmEmpleadoCreador: string;
  nombreEmpleadoAutorizo: string;
  appEmpleadoAutorizo: string;
  apmEmpleadoAutorizo: string;
  titulo: string;
  idEmpresa?: number;
  idGrupo?: number;
  idSede?: string;
}
