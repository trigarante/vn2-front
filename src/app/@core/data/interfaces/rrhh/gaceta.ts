export interface Gaceta {
  id: number;
  idEmpleado: number;
  descripcion: string;
  fechaLanzamiento: Date;
  fechaRegistro: Date;
  activo: number;
}
