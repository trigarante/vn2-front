
export default interface Menu{
  name: string,
  icon?: string,
  children?: Menu[],
  rutas?: string
  grandChildren?: Menu[]
  permiso: any;
}
