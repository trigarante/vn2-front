import {Observable} from 'rxjs';


export interface Recibos {
  id: number;
  idRegistro: number;
  idEmpleado: number;
  idEstadoRecibos: number;
  fechaVigencia: Date;
  numero: number;
  cantidad: number;
  estdo: string;
  primaNeta: string;
  poliza: string;
  estado: string;
  fechaLiquidacion: Date;
  nombreComercial?: string;
  area?: string;
  idEstadoPoliza: number;
  fechaRegistro: string;
  idFlujoPoliza?: number;
  fechaInicio: string;
  fechaPromesaPago: string;
  fechaPago: Date;
  datos: string;
  idFormaPago: number;
  activo: number;
  archivo: string;
  idPago: number;
  nombreEmpleadoPago: string;
  apellidoPaternoEmpleadoPago: string;
  apellidoMaternoEmpleadoPago: string;
}

export interface RecibosComplemento {
  id: number;
  idRegistroComplemento: number;
  idEstadoRecibo: number;
  idEmpleado: number;
  cantidad: number;
  numero: number;
  fechaRegistro: Date;
  fechaVigencia: Date;
  fechaLiquidacion: Date;
  activo: number;
}
