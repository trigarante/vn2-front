
export interface Inspecciones {
  id: number;
  idRegistro: number;
  idEstadoInspeccion: number;
  cp?: string;
  idColonia?: number;
  fechaInspeccion?: Date;
  kilometraje?: number;
  placas?: number;
  urlPreventa?: number;
  urlPostventa?: string;
  archivo?: string;
  archivoRegistro?: string;
  comentarios?: string;
  estado?: string;
  asenta?: string;
  poliza?: string;
  primaNeta?: number;
  calle?: string;
  numeroPlacas?: string;
  numInt: number;
  numExt: number;
}

