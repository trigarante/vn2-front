import {Observable} from 'rxjs';
export interface Prospecto {
  id: number;
  numero: string;
  correo: string;
  sexo: number;
  edad: number;
  fechaCreacion: string;
  nombre: string;
  productos?: any[];
}

export interface ProspectoO {
  id: number;
  numero: string;
  correo: string;
  sexo: string;
  edad: number;
  fechaCreacion: string;
  nombre: string;
  productos?: any[];
}


