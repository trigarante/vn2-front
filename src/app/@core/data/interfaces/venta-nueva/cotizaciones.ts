

export interface Cotizaciones {
  id: number;
  idProducto: number;
  idPagina: number;
  idMedioDifusion: number;
  idTipoContacto: number;
  idEstadoCotizacion: number;
  fechaCreacion: string;
  url: string;
  idTipoPagina: number;
  idCampana: number;
  idProveedor: number;
  regla: number;
  presupuesto: number;
  descripcion: string;
  estado: string;
  idProspecto: number;
  idTipoCategoria: number;
}


