

export interface Pagos {
  id: number;
  idRecibo: number;
  idFormaPago: number;
  idEstadoPago: number;
  idEmpleado: number;
  fechaPago: Date | string ;
  cantidad: number;
  archivo: string;
  datos: string;
  numero?: number;
  recibosCantidad?: number;
  formapagoDescripcion?: string;
  estadoPagoDescripcion?: string;
  numeroTarjeta?: string;
  titular?: string;
}


export interface PagosComplemento {
  id: number;
  idReciboComplemento: number;
  idFormaPago: number;
  idEstadoPago: number;
  idEmpleado: number;
  cantidad: number;
  archivo: string;
  fechaRegistro: Date;
  fechaPago: Date;
  datos: string;
}

