export interface UserApp {
  id?: number;
  numeroMovil?: string;
  correo?: string;
  password?: string;
  nombre?: string;
}
