
export interface Registro {
  id: number;
  idEmpleado: number;
  idProducto: number;
  idTipoPago: number;
  idEstadoPoliza: number;
  idTipoPoliza: number;
  idProductoSocio: number;
  idFlujoPoliza: number;
  idPeriodicidad: number;
  periodicidad: string;
  productoSocio: string;
  tipoRamo: string;
  tipoSubramo: string;
  idSocio: number;
  idRamo: number;
  idSubramo: number;
  poliza: string;
  fechaInicio: Date;
  primaNeta: number;
  fechaRegistro: Date;
  datos: any;
  archivo: string;
  idCliente?: number;
  cantidadPagos: number;
  archivoSubido: number;
  fechaFin: Date;
  idPais: number;
  oficina?: string;
  idSubarea: number;
  fechaCierre: Date;
}

export interface RegistroSoloDatos {
  idSocio: number;
  idRamo: number;
  idSubRamo: number;
  idProductoSocio: number;
  fechaInicio: Date;
  idPeriodicidad: number;
  poliza: string;
  oficina?: string;
  primaNeta: number;
}

export interface RegistroComplemento {
  id: number;
  idRegistro: number;
  idEstadoRegistroComplemento: number;
  idEmpleado: number;
  idTipoPago: number;
  primaNeta: number;
  fechaInicio: Date;
  fechaRegistro: Date;
  idPeriodicidad: number;
  archivo: string;
}

export interface RegistroOnline {
  id: number;
  idEmpleado: number;
  idProducto: number;
  idTipoPago: number;
  idEstadoPoliza: number;
  idTipoPoliza: number;
  idProductoSocio: number;
  idFlujoPoliza: number;
  idPeriodicidad: number;
  periodicidad: string;
  productoSocio: string;
  tipoRamo: string;
  tipoSubramo: string;
  idSocio: number;
  idRamo: number;
  idSubramo: number;
  poliza: string;
  fechaInicio: Date;
  primaNeta: number;
  fechaRegistro: Date;
  datos: any;
  archivo: string;
  idCliente?: number;
  cantidadPagos: number;
  archivoSubido: number;
  fechaFin: Date;
  idPais: number;
  oficina?: string;
  idSubarea: number;
  fechaCierre: Date;
  online: number;
  emisionCotizador: number;
}

