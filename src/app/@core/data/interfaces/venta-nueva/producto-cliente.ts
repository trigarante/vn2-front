
export interface ProductoCliente {
  length: boolean;
  id: number;
  idCliente: number;
  idSolicitud: number;
  idCategoriaSocio: number;
  datos: any;
  idLlamadaEntrantes?: any;
  correo?: string;
  idEmision: number; // renovacion
}
export interface ProductoClienteOnline {
  length: boolean;
  id: number;
  idCliente: number;
  idSolictud: number;
  idCategoriaSocio: number;
  datos: any;
  idSubRamo?: number;
}
