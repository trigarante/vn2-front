

export interface EstadoInspeccion {
  id: number;
  estado: string;
  activo: number;
}

