export interface SubetiquetaSolicitud {
  id: number;
  idEtiquetaSolicitud: number;
  descripcion: string;
  activo: number;
}
