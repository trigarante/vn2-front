export interface BancoVn {
  id: string;
  nombre: string;
  activo: number;
  idCarrier: number;
}
export interface BancoExterno {
  id_banco: string;
  nombre: string;
  abreviacion: string;
}
export interface BancoExternoMapfre {
  cod_ENTIDAD: string;
  nom_ENTIDAD: string;
}

// export interface MsiView {
//   id: number;
//   idAseguradora: number;
//   idBanco: number;
//   idMsi: number;
//   nombreAseguradora: string;
//   nombreBanco: string;
//   nombreMsi: number;
//   nombreCompleto: string;
// }

export interface MsiView {
  claveMsi: string;
  nombreMsi: string;
}
export interface MsiViewMapfre {
  msi: string;
  clave: string;
}

