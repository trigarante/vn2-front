
export interface CotizacionesAli {
  id: number;
  idCotizacion: number;
  idCategoriaSocio: number;
  cotizo: number;
  peticion: string;
  respuesta: any;
  interesado: number;
  fechaCotizacion: string;
  prima: number;
  fechaActualizacion: string;
  idSubRamo: number;
  nuevoJson: number;
}

