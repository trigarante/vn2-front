

export interface ErroresAutorizacion {
  id: number;
  idRegistro: number;
  idEstadoVerificacion: number;
  numero: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
  verificadoInspeccion: number;
  fechaCreacion: Date;
  idErrorAutorizacion?: number;
  idErrorAnexo?: number;
  idEstadoCorreccion: number;
  idEmpleadoCorreccion: number;
  idEmpleado: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  poliza: string;
  idCliente: number;
  nombreCliente: string;
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
  carpetaCliente: string;
  carpetaRegistro: string;
  idPago: number;
  archivoPago: string;
  estadoVerificacion: string;
}

export interface ErroresVerificacion {
  id: number;
  idRegistro: number;
  idEstadoVerificacion: number;
  estadoVerificacion: string;
  numero: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
  verificadoInspeccion: number;
  fechaCreacion: Date;
  idErrorAnexo?: number;
  idErrorAutorizacion?: number;
  idEstadoCorreccion: number;
  idEmpleadoCorreccion: number;
  idEmpleado: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  poliza: string;
  idCliente: number;
  nombreCliente: string;
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
  carpetaCliente: string;
  carpetaRegistro: string;
  idPago: number;
  archivoPago: string;
  fechaCreacionError?: Date;
  fechaPago?: Date;
  subarea: string;
  nombreComercial: string;
  horasRestantes: number;
}
