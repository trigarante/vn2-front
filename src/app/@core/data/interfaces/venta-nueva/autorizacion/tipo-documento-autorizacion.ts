import {Observable} from 'rxjs';

export interface TipoDocumentosAutorizacion {
  id: number;
  idTabla: number;
  nombre: string;
  descripcion: string;
  activo: boolean;
}

export abstract class TipoDocumentosAutorizacionData {
  abstract get(idTabla: number): Observable<TipoDocumentosAutorizacion[]>;
}
