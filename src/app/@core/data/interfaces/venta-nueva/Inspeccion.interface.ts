export interface InspeccionResp {
  codigo: string;
  mensaje: string;
  respuesta: any
}

export interface TicketStatus{
  codigo: string;
  mensaje: string;
  respuesta: TicketResp
}

interface TicketResp{
  agente: string;
  correoAsegurado: string;
  estatus: string;
  nombreAsegurado: string;
  serie: string;
  servicio: any;
  telAsegurado: string;
}
