export interface LogOcr {
  id?: number;
  idSolicitud: number;
  idSocio: number;
  idEmpleado: number;
  datos: any;
  error?: any;
}
