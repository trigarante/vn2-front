export interface Grabaciones {
  "id": number;
  "uniqueid": string;
  "calldate": Date;
  "src": string;
  "recordingfile": string;
  "park": string;

}
