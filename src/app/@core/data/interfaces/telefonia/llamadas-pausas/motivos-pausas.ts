export interface MotivosPausa {
  id: number;
  descripcion: string;
  tiempo: number;
  activo: number;
}
