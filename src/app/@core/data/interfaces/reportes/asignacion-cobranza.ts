export interface AsignacionCobranza {
  poliza: string;
  fechaInicio: string;
  fechaVigenciaCobranza: string;
  fechaCobranza: string;
  estadoPoliza: string;
  estadoRecibo: string;
  primaNeta: number;
  aseguradora: string;
  tipoPoliza: string;
  numeroSerie: string;
  marca: string;
  modelo: string;
  correo: string;
  telefonoMovil: string;
  estadoPago: string;
  ejecutivo: string;
  tipoAsignacion: string;
}
