export interface DescargaLeadsLlamados {
  idSolicitud: number;
  idSubarea: number;
  fechaSolicitud: string;
  area: string;
  subarea: string;
  ejecutivo: string;
  numero: string;
  llamadas: number;
  llamadasEfectivas: number;
  callbackEfectivo: string;
  minutosAtencion: number;
}
