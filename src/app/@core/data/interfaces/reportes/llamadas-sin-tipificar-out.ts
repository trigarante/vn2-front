export interface LlamadasSinTipificarOut {
  id: number;
  idEmpleado: number;
  idSubarea: number;
  subarea: string;
  idSolicitud: number;
  empleado: string;
  llamada: number;
}
