export interface Area {
  id: number;
  nombre: string;
  descripcion: string;
  idSede: number;
  activo: number;
  codigo: string;
  idMarcaEmpresarial: number;
  nombreSede?: string;
  nombreEmpresa?: string;
}
