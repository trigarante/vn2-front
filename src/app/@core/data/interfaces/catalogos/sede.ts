export interface Sede {
  id: number;
  idEmpresa: number;
  idPais: number;
  nombre: string;
  cp: string;
  colonia: string;
  calle: string;
  numero: string;
  codigo: string;
  nombreEmpresa?: string;
}
