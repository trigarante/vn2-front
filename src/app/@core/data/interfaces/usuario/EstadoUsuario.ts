export interface EstadoUsuario {
    id?: number;
    idSesionUsuario?: number;
    idEstadoSesion?: number;
    fechaInicio?: string;
    fechaFin?: string;
    idMotivosPausa?: number;
}
