export interface ErroresAnexosV {
  id: number;
  idVerificacionRegistro: number;
  idEmpleado: number;
  idEstadoCorreccion: number;
  idTipoDocumento: number;
  idEmpleadoCorreccion: number;
  fechaCreacion?: Date;
  fechaCorreccion?: Date;
  correcciones: string;
}
