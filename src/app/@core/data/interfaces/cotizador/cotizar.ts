import {Observable} from 'rxjs';
import {Auten} from "./catalogo";


export class CotizarRequest {
  aseguradora?: string;
  clave?: string;
  cp?: string;
  descripcion?: string;
  descuento?: number;
  edad?: string;
  fechaNacimiento?: string;
  edadFechaNacimiento?: string;
  genero?: string;
  marca?: string;
  subMarca?: string;
  modelo?: string;
  movimiento?: string;
  paquete?: string;
  servicio?: string;
  colonia?: string;
  idColonia?: string;

  constructor(aseguradora: string, clave: string, cp: string, descripcion: string, descuento: number, edad: string,
              fechaNacimiento: string, genero: string, marca: string, modelo: string, movimiento: string,
              paquete: string, servicio: string, colonia: string) {
    this.aseguradora = aseguradora;
    this.clave = clave;
    this.cp = cp;
    this.descripcion = descripcion;
    this.descuento = descuento;
    this.edad = edad;
    this.fechaNacimiento = fechaNacimiento;
    this.genero = genero;
    this.marca = marca;
    this.modelo = modelo;
    this.movimiento = movimiento;
    this.paquete = paquete;
    this.servicio = servicio;
    this.colonia = colonia;
  }
}

export class CotizarResponse {
  Aseguradora: string;
  Cliente: Object;
  Coberturas: Object[];
  CodigoError: Object;
  Cotizacion: Object;
  Descuento: string;
  Emision: Object;
  Pago: Object;
  Paquete: string;
  PeriodicidadDePago: number;
  Vehiculo: Object;
  urlRedireccion: Object;


  constructor(Aseguradora: string, Cliente: Object, Coberturas: Object[], CodigoError: Object, Cotizacion: Object,
              Descuento: string, Emision: Object, Pago: Object, Paquete: string, PeriodicidadDePago: number,
              Vehiculo: Object, urlRedireccion: Object) {
    this.Aseguradora = Aseguradora;
    this.Cliente = Cliente;
    this.Coberturas = Coberturas;
    this.CodigoError = CodigoError;
    this.Cotizacion = Cotizacion;
    this.Descuento = Descuento;
    this.Emision = Emision;
    this.Pago = Pago;
    this.Paquete = Paquete;
    this.PeriodicidadDePago = PeriodicidadDePago;
    this.Vehiculo = Vehiculo;
    this.urlRedireccion = urlRedireccion;
  }
}


export class CotizarOnlineResponse {
  Aseguradora: string;
  Cliente: Object;
  Coberturas: Object[];
  CodigoError: Object;
  Cotizacion: Object;
  Descuento: string;
  Emision: Object;
  Pago: Object;
  Paquete: string;
  PeriodicidadDePago: number;
  Vehiculo: Object;
  urlRedireccion: Object;

  constructor(Aseguradora: string, Cliente: Object, Coberturas: Object[], CodigoError: Object, Cotizacion: Object,
              Descuento: string, Emision: Object, Pago: Object, Paquete: string, PeriodicidadDePago: number,
              Vehiculo: Object, urlRedireccion: Object) {
    this.Aseguradora = Aseguradora;
    this.Cliente = Cliente;
    this.Coberturas = Coberturas;
    this.CodigoError = CodigoError;
    this.Cotizacion = Cotizacion;
    this.Descuento = Descuento;
    this.Emision = Emision;
    this.Pago = Pago;
    this.Paquete = Paquete;
    this.PeriodicidadDePago = PeriodicidadDePago;
    this.Vehiculo = Vehiculo;
    this.urlRedireccion = urlRedireccion;
  }
}
export class CotizarRequestOnline {
  aseguradora?: string;
  clave?: string;
  cp?: string;
  descripcion?: string;
  descuento?: number;
  edad?: string;
  fechaNacimiento?: string;
  genero?: string;
  marca?: string;
  modelo?: string;
  movimiento?: string;
  paquete?: string;
  servicio?: string;
  subMarca?: string;
  colonia?: string;
  idColonia?: string;

  constructor(aseguradora: string, clave: string, cp: string, descripcion: string, descuento: number, edad: string,
              fechaNacimiento: string, genero: string, marca: string, modelo: string, movimiento: string,
              paquete: string, servicio: string, subMarca:string, colonia: string, idColonia: string) {
    this.aseguradora = aseguradora;
    this.clave = clave;
    this.cp = cp;
    this.descripcion = descripcion;
    this.descuento = descuento;
    this.edad = edad;
    this.fechaNacimiento = fechaNacimiento;
    this.genero = genero;
    this.marca = marca;
    this.modelo = modelo;
    this.movimiento = movimiento;
    this.paquete = paquete;
    this.servicio = servicio;
    this.subMarca = subMarca;
    this.colonia = colonia;
    this.idColonia = idColonia;
  }
}
export interface BancoOnline {
  CLAVE: string;
  NOMBRE: string;
  VALOR: object;
}

export abstract class CotizarData {

  abstract cotizar(c: CotizarRequest): Observable<Object>;
  abstract emitir(c: any): Observable<Object>;
  abstract getAutentoken();
}
