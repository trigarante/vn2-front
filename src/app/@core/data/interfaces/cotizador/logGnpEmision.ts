export interface LogGnpEmision {
  id: number;
  idSolicitudVn: number;
  idEmpleado: any;
  request: any;
  error: any;
  fechaLog: string;
}
