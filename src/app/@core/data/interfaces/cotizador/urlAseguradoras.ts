export let urlAseguradoras = [
  {value: '', aseguradora: 'ABA', activo: 0},
  {value: '', aseguradora: 'ABA2', activo: 0},
  {value: '', aseguradora: 'AFIRME', activo: 0},
  {value: '', aseguradora: 'AIG', activo: 0},
  {value: '', aseguradora: 'ANA', activo: 0},
  {value: '', aseguradora: 'ATLAS', activo: 0},
  {value: '', aseguradora: 'AXA', activo: 0},
  {value: '', aseguradora: 'BANCOMER', activo: 0},
  {value: '', aseguradora: 'BANORTE', activo: 0},
  {value: '', aseguradora: 'BXMAS', activo: 0},
  {value: '', aseguradora: 'ELAGUILA', activo: 0},
  {value: '', aseguradora: 'ELPOTOSI', activo: 0},
  {value: '', aseguradora: 'GENERALDESEGUROS', activo: 0},
  {
    value: '', 
    aseguradora: 'GNP', 
    activo: 1, 
    service: 'CATALOGO_AUTOSGNP',
    catalogo: 'gnp_catalogos_autos/', 
    urlCotizacion: 'gnp_autos/cotizaciones',
    urlEmision: 'gnp_autos/emisiones_full', 
    urlPago: 'v3/gnp-full/issue/ahorra',
    urlBanco: 'datos_pago_gnp/get_bancos', 
    urlTipoTarjetas: 'datos_pago_gnp/get_tipo_tarjeta_valida',
    urlMsi: 'pago_gnp_3D/obtener_planes_pago',
    urlValidarTarjeta: 'pago_gnp_3D/validaciones_tarjetas',
    urlValidarTarjetasGeneral: 'pago_gnp_3D/v2/validate-card',
    urlObtenerEmision: 'pago_gnp_3D/obtener_emision', 
    urldocumentos: 'impresiones_polizas_gnp',
    urlMarca: 'marcas_autos', 
    urlModelos: 'modelos_autos', 
    urlSubmarcas: 'submarcas_autos',
    urlDescripciones: 'descripciones_autos'
  },
  {value: '', aseguradora: 'HDI', activo: 0},
  {value: '', aseguradora: 'HDI2', activo: 0},
  {value: '', aseguradora: 'INBURSA', activo: 0},
  {value: '', aseguradora: 'LALATINO', activo: 0},
  {
    value: '', 
    aseguradora: 'MAPFRE', 
    activo: 1 , 
    service: 'CATALOGO_AUTOSMAPFRE', 
    catalogo: 'v1/mapfre-car/',
    urlCotizacion: 'brand', 
    urlMarca: 'brand', 
    urlModelos: 'year', 
    urlSubmarcas: 'model', 
    urlDescripciones: 'variant',
    urlEmision: 'v1/mapfre-car/issue-interno', 
    urlPago: 'v1/mapfre-car/payment-interno'
  },
  {value: '', aseguradora: 'MIGO', activo: 0},
  {value: '', aseguradora: 'PRIMEROSEGUROS', activo: 0},
  {
    value: '', 
    aseguradora: 'QUALITAS', 
    activo: 1, 
    service: 'CATALOGO_AUTOSQUALITAS',
    catalogo: 'catalogos/', 
    urlCotizacion: 'qualitas_cotizaciones/autos',
    urlEmision: 'qualitas_emision/autos', 
    urlPago: 'qualitas_cobros/autos',
    urldocumentos: 'qualitas_documentos', 
    urlModificacionCoberturas: 'v1/quotation/coverage',
    urlEmisionCoberturas: 'v1/qualitas-car/issues-custom', 
    urlMarca: 'marcas_autos',
    urlModelos: 'modelos_autos', 
    urlSubmarcas: 'submarcas_autos', 
    urlDescripciones: 'descripciones_autos'
  },
  {value: '', aseguradora: 'SURA', activo: 0},
  {value: '', aseguradora: 'ZURICH', activo: 0},
];

export interface MsiExtenoGnp {
  CLAVE: number;
  NOMBRE: string;
  VALOR: string;
}

export interface MsiExtenoMapfre {
  cod_ENTIDAD: string;
  nom_ENTIDAD: string;
}
