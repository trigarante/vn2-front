export interface MarcaInterno {
  id: number;
  nombre: string;
  idTipoSubRamo: number;
}

