export interface DetalleInterno {
  id: number;
  idMarca: number;
  marca: string;
  idModelo: number;
  modelo: string;
  idDescripcion: number;
  descripcion: string;
  idSubDescripcion: number;
  subDescripcion: string;
  detalle: string;
}

