export interface TicketsInspeccion {
  id: number;
  idEmpleado: number;
  solicitudVn: number;
  ticket: number;
  status: number;
  inspeccionPrevia: number;
}
