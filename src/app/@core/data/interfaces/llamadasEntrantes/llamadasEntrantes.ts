export interface LlamadaEntrantes {
  id?: number;
  idTipoLlamada?: number;
  fechaInicio?: string;
  fechaFinal?: string;
  estadoAsterisk?: string;
  idEstadoLlamada?: number;
  idEmpleado?: number;
  idAsterisk?: string;
  idSubEtiqueta?: number;
  idSolicitud?: number;
  numero?: string;
  fechaRegistro?: string;
  idAsteriskTroncal?: string;
  finTipificacion?: string;
}
