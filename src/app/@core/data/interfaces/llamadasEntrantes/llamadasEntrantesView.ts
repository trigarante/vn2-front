export interface LlamadaEntrantesView {
  id?: number;
  idTipoLlamada?: number;
  fechaInicio?: Date;
  fechaFinal?: Date;
  idEstadoLlamada?: number;
  idEmpleado?: number;
  idAsteriskUno?: string;
  idSubEtiqueta?: number;
  idSolicitud?: number;
  numero?: string;
  fechaRegistro?: Date;
  // Datos de la vista
  estadoLlamada?: string;
  idEmpleadoSolicitud?: string;
  nombreProspecto?: string;
  segundosLlamada?: number;
  nombreArchivo?: number;
  idAsterisk?: string;
}

export interface LlamadaEntrantesCalidadView {
  id?: number;
  idEmpleado?: number;
  nombreArchivo?: string;
  nombre?: string;
  apellidoPaterno?: string;
  apellidoMaterno?: string;
  fechaInicio?: Date;
  segundosLlamada?: number;
  estadoRh?: string;
  numeroCliente?: string;
  subarea?: string;
  idAsterisk?: string;
}
