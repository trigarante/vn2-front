
export interface Categoria {
  id: number;
  tipo: string;
  regla: string;
  activo: number;
}
