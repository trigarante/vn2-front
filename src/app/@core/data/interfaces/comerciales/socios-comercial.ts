
export interface SociosComercial {
  idEstadoSocio: number;
  prioridad: number;
  nombreComercial: string;
  rfc: string;
  razonSocial: string;
  alias: string;
  prioridades: string;
  estado: number;
  activo: number;
  id: number;
  idPais: number;
  expresionRegular?: string;
  ejemploExpresionRegular?: string;
  // en la vista de GM existe el idSocio
  idSocio?: number;
}

export interface SociosComercialOnline {
  id: number;
  nombreComercial: string;
  expresionRegular?: string;
  ejemploExpresionRegular?: string;
}


