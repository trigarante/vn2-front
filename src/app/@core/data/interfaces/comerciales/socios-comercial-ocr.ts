
export interface SociosComercialOcr {
  id: number;
  idEstadoSocio: number;
  nombreComercial: string;
  activo: number;
}

