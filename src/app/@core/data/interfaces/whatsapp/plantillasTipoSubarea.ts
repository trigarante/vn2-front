export interface PlantillasTipoSubarea {
  idEmpleado: number;
  nombrePlantilla: string;
  activoPlantillas: number;
  idTipoPlantilla: number;
  descripcion: string;
  activoTipoPlantilla: number;
  idTipoSubarea: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
}

export interface PlantillasWhatsApp {
  id: number;
  nombre_plantilla: string;
  idTipoSubarea: number;
  idTipoPlantilla: number;
  activo: number;
}
