export interface LlamadaSalida {
  id?: number;
  idTipoLlamada?: number;
  fechaInicio?: string;
  fechaFinal?: string;
  idEstadoLlamada?: number;
  idEmpleado?: number;
  idAsterisk?: string;
  idSubEtiqueta?: number;
  idSolicitud?: number;
  numero?: string;
  fechaRegistro?: string;
  finTipificacion?: string;
  idRegistro?: number;
  // Datos de la vista
  estadoLlamada?: string;
  idEmpleadoSolicitud?: string;
  nombreProspecto?: string;
  segundosTipificacion?: number;
  segundosLlamada?: number;
  nombreArchivo?: string;
  nombre?: string;
  apellidoPaterno?: string;
  apellidoMaterno?: string;
  subetiqueta?: string;
}
export interface LlamadaSalidaCalidadView {
  id?: number;
  idEmpleado?: number;
  nombreArchivo?: string;
  nombre?: string;
  apellidoPaterno?: string;
  apellidoMaterno?: string;
  fechaInicio?: Date;
  segundosLlamada?: number;
}

