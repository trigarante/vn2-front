// import { AuthExp } from './@core/services/login/auth-exp.guard';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Error404Component} from './modulos/error404/error404.component';
import {LoaderComponent} from './loader/loader.component';
import {LoginComponent} from './@theme/login/login.component';
import {AuthGuard} from './@core/data/services/login/auth.guard';

const routes: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {
        path: 'modulos',
        loadChildren: () => import('./modulos/modulos.module').then(m => m.ModulosModule),
        // canActivate: [AuthGuard, AuthExp]
      // canActivateChild: [AuthGuard]
      canActivate: [AuthGuard],
    },
    // {
    //   path: 'app-component',
    //   loadChildren: () => import('policyRegister/Component').then(m => m.AppComponent)
    // },
    {path: 'login', component: LoginComponent},
    {path: 'loader', component: LoaderComponent},
    {path: '**', component: Error404Component},
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
    exports: [RouterModule]
})

export class AppRoutingModule {
}
