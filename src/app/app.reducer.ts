import {ActionReducerMap} from "@ngrx/store";
import {ventaNuevaReducer, VentaNuevaState} from "./modulos/venta-nueva/redux-implement-vn/venta-nueva.reducer";

// export interface AppState {
//   cliente: Cliente[];
//   productoCliente: any[];
// }
export interface AppState {
  ventaNueva: VentaNuevaState[];
  // productoCliente: any[];
}

export const appReducers: ActionReducerMap<AppState> = {
  ventaNueva: ventaNuevaReducer,
};
