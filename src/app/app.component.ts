import {Component, OnInit} from '@angular/core';
import { datadogRum } from '@datadog/browser-rum';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',

})
export class AppComponent {

  constructor(){
    window.sessionStorage.setItem('idFlujoPoliza','1');
  }

}
