import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {Error404Component} from './modulos/error404/error404.component';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {LoaderComponent } from './loader/loader.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {SocketIoModule} from 'ngx-socket-io';
import {SocketsVn2Service} from './@core/data/services/sockets/sockets-vn2.service';
import { NgxMaskModule, IConfig } from 'ngx-mask'

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    AppComponent,
    Error404Component,
    // SafePipe,
    LoaderComponent,
    // AplicacionView.TsComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule,
    MatTooltipModule,
    MatSnackBarModule,
    SocketIoModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
    MatBottomSheet,
    SocketsVn2Service
  ],
  exports: [
    LoaderComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
